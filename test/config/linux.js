/* eslint-disable */
require('babel-register')

var common = require('./common')
var caps = require('./caps/linux')

exports.config = Object.assign({}, common, {
  capabilities: caps,
  specs: [
    'test/spec/**.js',
  ],
})
