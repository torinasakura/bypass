/* eslint-disable */
require('babel-register')

var common = require('./common')
var caps = require('./caps/ios')

exports.config = Object.assign({}, common, {
  capabilities: caps,
})
