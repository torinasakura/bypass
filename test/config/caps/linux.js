/* eslint-disable */

module.exports = [
  {
    browserName: 'chrome',
    platform: 'linux',
    viewport: {
      width: 1366,
      height: 768,
    },
  },
  {
    browserName: 'chrome',
    platform: 'linux',
    viewport: {
      width: 1920,
      height: 1080,
    },
  },
  {
    browserName: 'firefox',
    platform: 'linux',
    viewport: {
      width: 1366,
      height: 768,
    },
  },
  {
    browserName: 'firefox',
    platform: 'linux',
    viewport: {
      width: 1920,
      height: 1080,
    },
  },
]
