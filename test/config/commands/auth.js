import fetch from 'node-fetch'
import config from '../../../src/app/config'

const getToken = async () => {
  if (process.env.BYPASS_TOKEN) {
    return new Promise(resolve => resolve(process.env.BYPASS_TOKEN))
  }

  const response = await fetch(`${config.api_url}register`)
  const body = await response.json()

  return new Promise(resolve => resolve(body.result))
}

export default async function auth() {
  const token = await getToken()

  await browser.url('/ru/')
  await browser.element('.SrcUiInputInput').setValue(token)

  await browser.click('.SrcUiButtonButton')

  return await browser.waitUntil(() => this.isVisible('.SrcUiLoaderLoader').then(isVisible => !isVisible))
}
