/* eslint-disable */
require('babel-register')

var common = require('./common')
var ios = require('./caps/ios')
var linux = require('./caps/linux')

exports.config = Object.assign({}, common, {
  capabilities: [].concat(ios, linux),
})
