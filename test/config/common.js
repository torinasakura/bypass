import chai from 'chai'
import chaiString from 'chai-string'
import chaiAsPromised from 'chai-as-promised'
import commands from './commands'

module.exports = {
  host: process.env.HUB_PORT_4444_TCP_ADDR,
  port: process.env.HUB_PORT_4444_TCP_PORT,
  path: '/wd/hub',
  specs: [
    'test/spec/**',
  ],
  maxInstances: 1,
  capabilities: [],
  sync: true,
  logLevel: 'error',
  coloredLogs: true,
  screenshotPath: 'shots',
  baseUrl: `http://${process.env.APP_TCP_ADDR}:${process.env.APP_TCP_PORT}`,
  waitforTimeout: 150000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  framework: 'mocha',
  reporters: ['dot'],
  reporterOptions: {
    outputDir: 'coverage',
  },
  mochaOpts: {
    compilers: ['js:babel-register'],
    timeout: 150000,
  },
  before: () => {
    chai.should()
    chai.use(chaiString)
    chai.use(chaiAsPromised)
    global.assert = chai.assert
    global.expect = chai.expect

    Object.keys(commands).forEach(name => browser.addCommand(name, commands[name]))

    chaiAsPromised.transferPromiseness = browser.transferPromiseness
  },
}
