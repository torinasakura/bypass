import crypto from 'crypto'
import TicketsPage from '../pageobjects/TicketsPage'

describe('tickets', () => {
  const page = new TicketsPage()
  const subject = `Test-${crypto.randomBytes(20).toString('hex').substr(0, 4)}`

  it('open page', () => {
    page.auth()
    page.open('#/tickets')

    page.waitLoader()
    page.screenshot('open tickets page')
  })

  it('check create ticket', () => {
    page.createTicket()
    page.setSubject(subject)
    page.setMessage('Test message')
    page.screenshot('create ticket')

    page.saveTicket()
    page.waitLoader()
  })

  it('check ticket', () => {
    page.openTicket(subject)
    page.screenshot('open ticket')

    page.setMessage('New ticket message')
    page.addMessage()
    page.waitLoader()
    page.screenshot('add message')

    page.closeTicket()
    page.waitLoader()
    page.screenshot('close ticket')
  })
})
