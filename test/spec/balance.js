/* eslint-disable no-unused-expressions */
import BalancePage from '../pageobjects/BalancePage'

describe('balance', () => {
  const page = new BalancePage()

  it('open page', () => {
    page.auth()
    page.open('#/billing/balance')

    page.waitLoader()
    page.screenshot('open balance page')
  })

  it('check refill', () => {
    page.openRefill()
    page.screenshot('create wallet')

    page.createWallet()
    page.waitLoader()
    page.screenshot('refill')

    expect(page.getBitcoinAddress()).to.be.ok
    expect(page.getBitcoinQr()).to.be.ok

    page.closeModal()
  })

  it('check create ticket', () => {
    page.createTicket()
    page.screenshot('create ticket')

    page.selectCheck()
    page.selectWait()
    page.selectWrite()
    page.screenshot('confirm create ticket')

    expect(page.canCreateTicket()).to.be.ok
  })
})
