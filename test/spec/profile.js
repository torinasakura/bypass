import ProfilePage from '../pageobjects/ProfilePage'

describe('profile', () => {
  const page = new ProfilePage()

  it('open page', () => {
    page.auth()
    page.open('#/profile')

    page.waitLoader()
    page.screenshot('open profile page')
  })

  it('check provider', () => {
    page.toggleTab(1)
    page.screenshot('open provider tab')

    page.changeProvider()
    page.screenshot('change provider')

    page.saveProvider()
    page.waitLoader()
    page.screenshot('save provider')

    expect(page.getSaveProviderMessage()).to.contain(page.getCurrentProvider())

    page.closeModal()
  })

  it('check csv', () => {
    page.toggleTab(2)
    page.screenshot('open csv')

    page.saveCsvFormat()
    page.waitLoader()
    page.screenshot('save csv')

    expect(page.getModalBodyText()).to.equal('CSV-формат успешно изменен.')

    page.closeModal()
  })

  it('check csv extended', () => {
    page.toggleTab(3)
    page.screenshot('open csv extended tab')

    page.setCsvExtendedFormat('%number%:%exp%:%cvv2%:%address%:%zip%:%bank%')
    page.screenshot('change csv extended format')

    page.saveCsvExtendedFormat()
    page.waitLoader()
    page.screenshot('save csv extended')

    expect(page.getModalBodyText()).to.equal('CSV-формат успешно изменен.')

    page.closeModal()
  })

  it('check statistic', () => {
    page.toggleTab(4)
    page.screenshot('open statistic tab')

    expect(page.getStatistics().length).to.equal(12)
  })

  it('check show options', () => {
    page.toggleTab(5)
    page.screenshot('open show options tab')

    page.changeShowInvalid()
    page.changeShowConfirm()
    page.changeShowAdv()
    page.screenshot('change show options')

    page.saveShowOptions()
    page.waitLoader()
    page.screenshot('save show options')

    expect(page.getModalBodyText()).to.equal('Опции отображения успешно изменены.')

    page.closeModal()
  })

  it('check fast buy', () => {
    page.toggleTab(6)
    page.screenshot('open fast buy tab')

    page.changeFastBuyCheck()
    page.changeFastBuyRegirect()
    page.changeFastBuyOpen()
    page.changeFastBuyShowCsv()
    page.screenshot('change fast buy')

    page.saveFastBuy()
    page.waitLoader()
    page.screenshot('save fast buy')

    expect(page.getModalBodyText()).to.equal('Опции отображения успешно изменены.')

    page.closeModal()
  })

  it('check search', () => {
    page.toggleTab(7)
    page.screenshot('open search tab')

    page.saveSearch()
    page.waitLoader()

    expect(page.getModalBodyText()).to.equal('Отображение поиска успешно изменено')

    page.closeModal()
  })

  it('check per page', () => {
    page.toggleTab(8)
    page.screenshot('open per page tab')

    page.savePerPage()
    page.waitLoader()

    expect(page.getModalBodyText()).to.equal('Опции отображения успешно изменены.')

    page.closeModal()
  })
})
