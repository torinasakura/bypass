import AuthPage from '../pageobjects/AuthPage'

describe('auth', () => {
  const page = new AuthPage()

  it('init', () => {
    page.open()
    page.screenshot('init')
  })

  it('change lange', () => {
    page.changeLange()
    page.screenshot('change lange')
  })

  it('register', () => {
    page.register()
    page.screenshot('register')
  })
})
