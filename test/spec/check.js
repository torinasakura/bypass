import CheckPage from '../pageobjects/CheckPage'

describe('check', () => {
  const page = new CheckPage()

  it('open page', () => {
    page.auth()
    page.open('#/check/add')

    page.waitLoader()
    page.screenshot('open check page')
  })

  it('check save format', () => {
    page.setFormat('%num%|%m%|%y%|%cvv2%')
    page.saveFormat()
    page.waitLoader()
    page.screenshot('save format')

    expect(page.getModalBodyText()).to.equal('Формат успешно сохранен')

    page.closeModal()
  })

  it('check preview', () => {
    page.setCardFormat('1212365896578900|01|2018|600')
    page.showPreview()
    page.screenshot('preview')

    // expect(page.getPreviewCardNumber()).to.equal('1212365896578900')
  })
})
