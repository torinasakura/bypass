import FaqPage from '../pageobjects/FaqPage'

describe('faq', () => {
  const page = new FaqPage()

  it('open page', () => {
    page.auth()
    page.open('#/faq')

    page.waitLoader()
    page.screenshot('open faq page')
  })

  it('check faq', () => {
    page.selectGroup(2)
    page.screenshot('change faq group')

    page.toggleTab(2)
    page.screenshot('change faq tab')
  })
})
