import Page from './Page'

class AuthPage extends Page {
  changeLange() {
    browser.element('.SrcUiNavigationItem:nth-Child(1)').click()
  }

  register() {
    browser.element('.SrcUiNavigationItem:nth-Child(2)').click()
    browser.waitForVisible('.SrcUiModalModal')
  }

  setPassword(password) {
    browser.element('.SrcUiInputInput').setValue(password)
  }

  login() {
    browser.click('.SrcUiButtonButton')
  }
}

export default AuthPage
