import Page from './Page'

class TicketsPage extends Page {
  createTicket() {
    browser.element('.SrcUiButtonButton').click()
  }

  setSubject(subject) {
    browser.element('.SrcUiInputInput').setValue(subject)
  }

  setMessage(message) {
    browser.element('.SrcUiTextareaTextarea').setValue(message)
  }

  saveTicket() {
    browser.element('.SrcUiModalModal .SrcUiButtonButton').click()
  }

  openTicket(subject) {
    browser.element(`//*[text()='${subject}']`).click()
  }

  addMessage() {
    browser.element('.SrcUiButtonButton').click()
  }

  closeTicket() {
    browser.moveToObject('.SrcPagesTicketsComponentsChatHeaderStatusStatus')
    browser.element('.SrcPagesTicketsComponentsChatHeaderStatusItems div:nth-Child(1) .SrcUiLinkLink').click()
  }
}

export default TicketsPage
