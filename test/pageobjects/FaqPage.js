import Page from './Page'

class FaqPage extends Page {
  selectGroup(index) {
    browser.element(`.SrcPagesFaqComponentsFaqTabsTabs div:nth-Child(${index})`).click()
  }

  toggleTab(index) {
    browser.element(`.SrcUiAccordionPanel:nth-Child(${index})`).click()
  }
}

export default FaqPage
