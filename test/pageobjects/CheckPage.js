import Page from './Page'

class CheckPage extends Page {
  setFormat(format) {
    browser.element('.SrcUiInputInput').setValue(format)
  }

  setCardFormat(format) {
    browser.element('.SrcUiContentEditableContentEditable').setValue(format)
  }

  saveFormat() {
    browser.element('.SrcUiLayoutLayout div .SrcUiLayoutLayout:nth-Child(1) .SrcUiLinkLink').click()
  }

  loadFormat() {
    browser.element('.SrcUiLayoutLayout div .SrcUiLayoutLayout:nth-Child(2) .SrcUiLinkLink').click()
  }

  selectFormat(format) {
    browser.element(`//*[text()='${format}']`).click()
  }

  showPreview() {
    browser.element('.SrcPagesCheckComponentsCheckBlock .SrcUiButtonButton').click()
  }

  getPreviewCardNumber() {
    return browser.element('table tbody td:nth-Child(2)').getText()
  }
}

export default CheckPage
