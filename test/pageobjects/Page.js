import fs from 'fs'
import path from 'path'
import { mkdirsSync } from 'fs-extra'

class Page {
  constructor() {
    const page = this.constructor.name
    this.shots = path.join(__dirname, `../../coverage/shots/${page}`)

    if (!fs.existsSync(this.shots)) {
      mkdirsSync(this.shots)
    }
  }

  open(target = '') {
    const { viewport } = browser.desiredCapabilities

    if (viewport) {
      browser.setViewportSize(viewport, false)
    }

    return browser.url(`/ru/${target}`)
  }

  screenshot(name) {
    const res = browser.screenshot()
    const screenshot = new Buffer(res.value, 'base64')

    fs.writeFileSync(this.getScreenshotPath(name, browser.desiredCapabilities), screenshot)
  }

  getScreenshotPath(name = '', caps) {
    const { platform, browserName, platformName, deviceName, orientation, viewport } = caps

    const filename = name.split(' ')

    if (platformName) {
      filename.push(platformName.toLowerCase())
    } else if (platform) {
      filename.push(platform.toLowerCase())
    }

    if (browserName) {
      filename.push(browserName.toLowerCase())
    }

    if (deviceName) {
      filename.push(deviceName.toLowerCase().replace(/ /g, '_'))
    }

    if (orientation) {
      filename.push(orientation.toLowerCase())
    }

    if (viewport) {
      filename.push(Object.keys(viewport).map(key => viewport[key]).join('x'))
    }

    return path.join(this.shots, `${filename.join('_')}.png`)
  }

  auth() {
    browser.auth()
  }

  waitLoader() {
    browser.waitUntil(() => browser.isVisible('.SrcUiLoaderLoader').then(isVisible => !isVisible))
  }

  closeModal() {
    browser.element('.SrcUiModalClose').click()
  }

  getModalBodyText() {
    return browser.element('.SrcUiModalBody').getText()
  }
}

export default Page
