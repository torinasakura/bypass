import Page from './Page'

class ProfilePage extends Page {
  toggleTab(index) {
    browser.element(`.SrcUiAccordionPanel:nth-Child(${index})`).click()
  }

  changeProvider() {
    const options = browser.elements('.SrcUiSelectSelect option').getValue()
    const currentValue = browser.element('.SrcUiSelectSelect').getValue()
    const [nextValue] = options.filter(value => value !== currentValue)

    browser.element('.SrcUiSelectSelect').selectByValue(nextValue)
  }

  saveProvider() {
    browser.element('.SrcUiAccordionPanel .SrcUiButtonButton').click()
  }

  getSaveProviderMessage() {
    browser.waitForVisible('.SrcUiModalModal')

    return browser.element('.SrcUiModalModal .SrcUiModalBody').getText()
  }

  getCurrentProvider() {
    const value = browser.element('.SrcUiSelectSelect').getValue()
    const index = browser.elements('.SrcUiSelectSelect option').getValue().indexOf(value) + 1

    const [provider] = browser.elements(`.SrcUiSelectSelect option:nth-Child(${index})`).getText().split(' ')

    return provider
  }

  saveCsvFormat() {
    browser.element('.SrcUiButtonButton').click()
  }

  setCsvExtendedFormat(value) {
    browser.element('.SrcUiInputInput').setValue(value)
  }

  saveCsvExtendedFormat() {
    browser.element('.SrcUiButtonButton').click()
  }

  getStatistics() {
    return browser.elements('.SrcUiListListItem').value
  }

  changeShowInvalid() {
    browser.element('.SrcUiListListItem:nth-Child(1) .SrcUiCheckboxCheckbox').click()
  }

  changeShowConfirm() {
    browser.element('.SrcUiListListItem:nth-Child(2) .SrcUiCheckboxCheckbox').click()
  }

  changeShowAdv() {
    browser.element('.SrcUiListListItem:nth-Child(3) .SrcUiCheckboxCheckbox').click()
  }

  saveShowOptions() {
    browser.element('.SrcUiButtonButton').click()
  }

  changeFastBuyCheck() {
    browser.element('.SrcUiListListItem:nth-Child(1) .SrcUiCheckboxCheckbox').click()
  }

  changeFastBuyRegirect() {
    browser.element('.SrcUiListListItem:nth-Child(2) .SrcUiCheckboxCheckbox').click()
  }

  changeFastBuyOpen() {
    browser.element('.SrcUiListListItem:nth-Child(3) .SrcUiCheckboxCheckbox').click()
  }

  changeFastBuyShowCsv() {
    browser.element('.SrcUiListListItem:nth-Child(4) .SrcUiCheckboxCheckbox').click()
  }

  saveFastBuy() {
    browser.element('.SrcUiButtonButton').click()
  }

  saveSearch() {
    browser.element('.SrcUiButtonButton').click()
  }

  savePerPage() {
    browser.element('.SrcUiButtonButton').click()
  }
}

export default ProfilePage
