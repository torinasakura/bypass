import Page from './Page'

class BalancePage extends Page {
  openRefill() {
    browser.element('.SrcPagesBillingComponentsBalanceBlocksButtons:nth-Child(1)').click()
  }

  openCheck() {
    browser.element('.SrcPagesBillingComponentsBalanceBlocksButtons:nth-Child(2)').click()
  }

  createWallet() {
    browser.element('.SrcUiModalModal .SrcUiButtonButton').click()
  }

  getBitcoinAddress() {
    return browser.element('.SrcUiModalBody b').getText()
  }

  getBitcoinQr() {
    return browser.element('.SrcUiModalBody .react-qr').getAttribute('src')
  }

  createTicket() {
    browser.element('.SrcPagesBillingComponentsBalanceBlocksTrouble .SrcUiButtonButton').click()
  }

  selectCheck() {
    browser.element('.SrcPagesTicketsModalsDummyProtection div:nth-Child(1) .SrcUiCheckboxCheckbox').click()
  }

  selectWait() {
    browser.element('.SrcPagesTicketsModalsDummyProtection div:nth-Child(2) .SrcUiCheckboxCheckbox').click()
  }

  selectWrite() {
    browser.element('.SrcPagesTicketsModalsDummyProtection div:nth-Child(3) .SrcUiCheckboxCheckbox').click()
  }

  canCreateTicket() {
    return !browser.element('.SrcUiModalModal .SrcUiButtonButton').getAttribute('disabled')
  }
}

export default BalancePage
