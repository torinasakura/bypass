import cookie from 'js-cookie'
import script from 'scriptjs'

let locale = cookie.get('locale')

if (!locale) {
  locale = 'ru'
  cookie.set('locale', locale)
}

script(`main.${locale}.js`)
