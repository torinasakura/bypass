export const actions = {
  show: '@@bypass/loader/SHOW',
  hide: '@@bypass/loader/HIDE',
}

export function show() {
  return {
    type: actions.show,
  }
}

export function hide() {
  return {
    type: actions.hide,
  }
}
