export const actions = {
  open: '@@bypass/modal/OPEN',
  close: '@@bypass/modal/CLOSE',
}

export function open(name, props = {}) {
  return {
    type: actions.open,
    name,
    props,
  }
}

export function close() {
  return {
    type: actions.close,
  }
}
