import React from 'react'
import { connect } from 'react-redux'

const Viewport = ({ viewport, components = {} }) => {
  const target = components[viewport]

  if (target) {
    return React.createElement(target)
  }

  return null
}

export default connect(
  state => ({
    viewport: state.viewport.type,
  }),
)(Viewport)
