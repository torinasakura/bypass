import { connect } from 'react-redux'
import { Loader } from 'bypass/ui/loader'

export default connect(
  state => state.loader,
)(Loader)
