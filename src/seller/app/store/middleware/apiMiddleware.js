import api from 'bypass/app/api'
import config from '../../config'

export default function apiMiddleware({ dispatch, getState }) {
  return next => action => {
    if (typeof action === 'function') {
      action(dispatch, getState, api(dispatch, getState, config))
    } else {
      next(action)
    }
  }
}
