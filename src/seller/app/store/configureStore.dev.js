import viewportListener from 'react-media-queries/lib/viewportListener'
import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import { persistState } from 'redux-devtools'
import { createViewportListener } from '../../utils'
import DevTools from '../containers/DevTools'
import rootReducer from '../reducers'
import { history } from '../history'
import { apiMiddleware } from './middleware'

const createHistory = () => history

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(apiMiddleware),
  DevTools.instrument(),
  persistState(
    window.location.href.match(
      /[?&]debug_session=([^&#]+)\b/
    )
  ),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  viewportListener(createViewportListener(store))

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default)
    )
  }

  return store
}
