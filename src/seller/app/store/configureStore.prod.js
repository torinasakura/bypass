import viewportListener from 'react-media-queries/lib/viewportListener'
import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import { createViewportListener } from '../../utils'
import rootReducer from '../reducers'
import { history } from '../history'
import { apiMiddleware } from './middleware'

const createHistory = () => history

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(apiMiddleware),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  viewportListener(createViewportListener(store))

  return store
}
