import { actions } from '../actions/modal'

const initialState = {
  name: null,
  props: {},
}

export default function loader(state = initialState, { type, name, props }) {
  switch (type) {
    case actions.open:
      return { name, props }
    case actions.close:
      return initialState
    default:
      return state
  }
}
