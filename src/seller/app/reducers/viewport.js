import { getViewport } from 'bypass/utils/index'

export default function viewport(state = getViewport(), action) {
  switch (action.type) {
    case 'VIEWPORT_CHANGE':
      return action.viewport
    default:
      return state
  }
}
