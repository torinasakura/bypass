import { combineReducers } from 'redux'
import { routerStateReducer as router } from 'redux-router'
import modal from './modal'
import loader from './loader'
import viewport from './viewport'

const pages = require.context(
  '../../pages',
  true,
  /^\.\/[a-z]+\/reducers\/index\.js$/i
)

const reducers = pages.keys().reduce((result, key) => {
  const name = key.split('/')[1]
  const page = pages(key)

  return {
    ...result,
    [name]: page.default ? page.default : page,
  }
}, {})

export default combineReducers({
  router,
  modal,
  loader,
  viewport,
  ...reducers,
})
