import React from 'react'
import { render } from 'react-dom'
import configureStore from './store/configureStore'
import Root from './containers/Root'

import './app.css'

const store = configureStore()

render(
  <Root store={store} />,
  document.getElementById('container')
)

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    const Root = require('./containers/Root').default // eslint-disable-line no-shadow

    render(
      <Root store={store} />,
      document.getElementById('container')
    )
  })
}
