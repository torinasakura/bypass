import Workspace from '../../pages/workspace/containers/Workspace'
import Auth from '../../pages/auth/containers/Auth'

export default function getRoutes(store) {
  const pages = require.context(
    '../../pages',
    true,
    /^\.\/[a-z]+\/routes\/([a-z]+)\.js$/i
  )

  const childRoutes = pages.keys().reduce((result, key) => result.concat(pages(key).default(store)), [])

  return [
    {
      path: '/',
      getComponent(location, callback) {
        if (!store.getState().auth.token) {
          return callback(null, Auth)
        }

        return callback(null, Workspace)
      },
      onEnter: (nextState, replace) => {
        if (nextState.location.pathname !== '/' && !store.getState().auth.token) {
          replace('/')
        }

        if (store.getState().auth.token && nextState.location.pathname === '/') {
          replace('/sells')
        }
      },
      childRoutes,
    },
  ]
}
