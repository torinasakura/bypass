import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { List } from './List'

const Mobile = ({ sells, total }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('SELLER.SELLS.HEAD')}
          </Title>
        </Layout>
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            sells={sells}
            total={total}
            rowHeight={30}
            minWidth={980}
          />
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
