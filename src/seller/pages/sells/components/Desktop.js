import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { List } from './List'

const Desktop = ({ sells, total }) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout>
            <Title>
              {__i18n('SELLER.SELLS.HEAD')}
            </Title>
          </Layout>
          <Layout grow={1} shrink={1}>
            <List
              sells={sells}
              total={total}
              rowHeight={30}
            />
          </Layout>
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
