import { connect } from 'react-redux'
import * as Sells from '../components'

const connector = connect(
  state => ({
    sells: state.sells.sells,
    total: state.sells.total,
  }),
)

export const Desktop = connector(Sells.Desktop)

export const Tablet = connector(Sells.Mobile)

export const Mobile = connector(Sells.Mobile)

