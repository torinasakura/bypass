import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants'

const initialState = {
  sells: fromJS([]),
  total: 1,
}

export default createReducer(initialState, {
  [actions.load]: (state, { stat = {} }) => {
    const balance = parseInt(stat.profit - stat.payout - stat.card_refund - stat.check_refund, 10).toFixed(2)
    const inFlow = parseInt(stat.total, 10) - parseInt(stat.sold, 10) - parseInt(stat.invalid, 10)

    return {
      ...state,
      sells: fromJS([{
        ...stat,
        balance,
        inFlow,
      }]),
    }
  },
})
