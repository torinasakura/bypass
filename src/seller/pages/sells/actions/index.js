import * as actions from '../constants'

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('/')

    dispatch({
      type: actions.load,
      ...result,
    })
  }
}
