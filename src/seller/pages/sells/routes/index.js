import { createViewport } from 'bypass/app/utils'
import { load } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'sells',
    onEnter() {
      dispatch(load())
    },
    getComponent(location, callback) {
      System.import('../containers/Sells').then(Sells => callback(null, createViewport(Sells)))
    },
  }]
}
