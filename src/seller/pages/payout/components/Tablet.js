import React from 'react'
import { Title } from 'bypass/ui/title'
import { Text } from 'bypass/ui/text'
import { Input } from 'bypass/ui/input'
import { Button } from 'bypass/ui/button'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { List } from './List'
import Payout from './Payout'

const Tablet = ({ payments, total, amount, address, canSend, onChangeAmount, onChangeAddress, onSend }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('SELLER.PAYOUT.HEAD')}
          </Title>
        </Layout>
        <Layout>
          <Payout indent>
            <ColumnLayout align='center'>
              <Layout grow={1} />
              <Layout>
                <Text size={14}>
                  {__i18n('SELLER.PAYOUT.SUM')}
                </Text>
              </Layout>
              <Layout basis='5px' />
              <Layout basis='65px'>
                <Input
                  value={amount}
                  placeholder='100.00'
                  onChange={onChangeAmount}
                />
              </Layout>
              <Layout basis='20px' />
              <Layout>
                <Text size={14}>
                  {__i18n('SELLER.PAYOUT.WALLET')}
                </Text>
              </Layout>
              <Layout basis='5px' />
              <Layout>
                <Input
                  value={address}
                  onChange={onChangeAddress}
                />
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Button size='small' disabled={!canSend} onClick={onSend}>
                  {__i18n('LANG.BUTTONS.SEND')}
                </Button>
              </Layout>
              <Layout grow={1} />
            </ColumnLayout>
          </Payout>
        </Layout>
        <Layout basis='20px' />
        <Layout grow={1} shrink={1}>
          <List
            payouts={payments}
            total={total}
            rowHeight={30}
          />
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
