import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    padding: '20px 0',
    background: '#eceff1',
    border: '1px solid #e0e0e0',
    borderRadius: '5px',
  },
  indent: {
    margin: '0 20px',
  },
  plain: {
    borderRadius: '0px',
    padding: '10px 0',
  },
})

const Payout = ({ children, indent, plain }) => (
  <div className={styles({ indent, plain })}>
    {children}
  </div>
)

export default Payout
