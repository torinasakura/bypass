import React from 'react'
import { Title } from 'bypass/ui/title'
import { Text } from 'bypass/ui/text'
import { Input } from 'bypass/ui/input'
import { Button } from 'bypass/ui/button'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { List } from './List'
import Payout from './Payout'

const Mobile = ({ payments, total, amount, address, canSend, onChangeAmount, onChangeAddress, onSend }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small' white>
            {__i18n('SELLER.PAYOUT.HEAD')}
          </Title>
        </Layout>
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            payouts={payments}
            total={total}
            rowHeight={30}
            minWidth={800}
          />
        </Layout>
        <Layout>
          <Payout plain>
            <RowLayout>
              <Layout>
                <ColumnLayout align='center'>
                  <Layout basis='10px' />
                  <Layout basis='60px'>
                    <Text size={14}>
                      {__i18n('SELLER.PAYOUT.WALLET')}
                    </Text>
                  </Layout>
                  <Layout grow={1}>
                    <Input
                      value={address}
                      onChange={onChangeAddress}
                    />
                  </Layout>
                  <Layout basis='10px' />
                </ColumnLayout>
              </Layout>
              <Layout basis='8px' />
              <Layout>
                <ColumnLayout align='center' justify='center'>
                  <Layout basis='10px' />
                  <Layout basis='60px'>
                    <Text size={14}>
                      {__i18n('SELLER.PAYOUT.SUM')}
                    </Text>
                  </Layout>
                  <Layout basis='65px'>
                    <Input
                      value={amount}
                      placeholder='100.00'
                      onChange={onChangeAmount}
                    />
                  </Layout>
                  <Layout grow={1} />
                  <Layout>
                    <Button size='small' disabled={!canSend} onClick={onSend}>
                      {__i18n('LANG.BUTTONS.SEND')}
                    </Button>
                  </Layout>
                  <Layout basis='10px' />
                </ColumnLayout>
              </Layout>
            </RowLayout>
          </Payout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
