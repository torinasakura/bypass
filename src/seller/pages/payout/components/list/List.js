import React from 'react'
import { Table, Column } from 'bypass/ui/table'

const statusMessages = {
  in_progress: __i18n('SELLER.PAYOUT.STATUS_PROCESSING'),
  processed: __i18n('SELLER.PAYOUT.STATUS_PROCESSED'),
  rejected: __i18n('SELLER.PAYOUT.STATUS_REJECTED'),
}

const List = ({ payouts, total, minWidth, rowHeight }) => (
  <Table
    list={payouts}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
  >
    <Column
      label='#'
      width='40px'
      dataKey='index'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.ID')}
      width='115px'
      dataKey='payout_id'
    />
    <Column
      label={__i18n('SELLER.PAYOUT.DATE')}
      width='80px'
      flexGrow={1}
      dataKey='add_time'
    />
    <Column
      label={__i18n('SELLER.PAYOUT.STATUS')}
      width='80px'
      flexGrow={1}
      dataKey='status'
      cellDataGetter={(dataKey, rowData) => {
        if (statusMessages[rowData.get(dataKey)]) {
          return statusMessages[rowData.get(dataKey)]
        }

        return `<${__i18n('COM.EMPTY')}>`
      }}
    />
    <Column
      label={__i18n('SELLER.PAYOUT.SUM')}
      width='80px'
      flexGrow={1}
      dataKey='amount'
    />
    <Column
      label={__i18n('SELLER.PAYOUT.WALLET')}
      width='80px'
      flexGrow={1}
      dataKey='address'
      cellDataGetter={(dataKey, rowData) => {
        if (rowData.get(dataKey)) {
          return rowData.get(dataKey)
        }

        return `<${__i18n('COM.EMPTY')}>`
      }}
    />
  </Table>
)

export default List
