import { connect } from 'react-redux'
import { change, send } from '../actions'
import * as Payout from '../components'

const connector = connect(
  state => ({
    payments: state.payout.payments,
    total: state.payout.total,
    amount: state.payout.amount,
    address: state.payout.address,
    canSend: state.payout.canSend,
  }),
  dispatch => ({
    onChangeAmount: ({ target }) => {
      dispatch(change('amount', target.value))
    },
    onChangeAddress: ({ target }) => {
      dispatch(change('address', target.value))
    },
    onSend: () => {
      dispatch(send())
    },
  })
)

export const Desktop = connector(Payout.Desktop)

export const Tablet = connector(Payout.Tablet)

export const Mobile = connector(Payout.Mobile)

