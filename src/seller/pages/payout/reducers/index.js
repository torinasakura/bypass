import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { timeConverter } from 'bypass/app/utils/utils'
import * as actions from '../constants'

const initialState = {
  payments: fromJS([]),
  total: 0,
  amount: '',
  address: '',
}

const canSend = ({ amount = '', address = '' }) => amount.length > 0 && address.length > 0

export default createReducer(initialState, {
  [actions.load]: (state, { data, offset, count }) => {
    const payments = data.map((payment, index) => ({
      ...payment,
      index: offset + index + 1,
      add_time: timeConverter(payment.add_time, true, true),
    }))

    return {
      payments: offset === 0 ? fromJS(payments) : state.payments.concat(fromJS(payments)),
      total: parseInt(count, 10),
    }
  },

  [actions.change]: (state, { field, value }) => {
    const updated = {
      ...state,
      [field]: value,
    }

    return {
      ...updated,
      canSend: canSend(updated),
    }
  },
})
