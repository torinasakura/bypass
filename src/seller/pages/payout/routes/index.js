import { createViewport } from 'bypass/app/utils'
import { load } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'payout',
    onEnter() {
      dispatch(load())
    },
    getComponent(location, callback) {
      System.import('../containers/Payout').then(Payout => callback(null, createViewport(Payout)))
    },
  }]
}
