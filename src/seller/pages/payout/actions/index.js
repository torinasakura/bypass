import * as actions from '../constants'

export function load(offset = 0) {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('payout/list', { body: { offset, perpage: 30 }, loader: offset === 0 })

    return dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function change(field, value) {
  return {
    type: actions.change,
    field,
    value,
  }
}

export function send() {
  return async (dispatch, getState, { post }) => {
    const { amount, address } = getState().payout

    const body = {
      amount: parseFloat(amount).toFixed(2),
      address,
    }

    await post('payout/add', { body })

    dispatch(load())
    dispatch(change('amount', ''))
    dispatch(change('address', ''))
  }
}
