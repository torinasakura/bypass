import React from 'react'
import { Button } from 'bypass/ui/button'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'

const Errors = ({ header, body, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      {header}
    </Header>
    <Body white alignCenter>
      {body}
    </Body>
    <Footer white alignCenter>
      <Button onClick={onClose}>
        OK
      </Button>
    </Footer>
  </Modal>
)

export default Errors
