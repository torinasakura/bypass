import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#37474f',
    width: '100%',
    minHeight: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const Content = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)
export default Content
