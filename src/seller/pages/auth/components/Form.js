import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Input } from 'bypass/ui/input'
import { Button } from 'bypass/ui/button'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    margin: '0 auto',
    padding: '0 15px',
    maxWidth: '480px',
  },
})

const Form = ({ password, onChangePassword, onLogin }) => (
  <div className={styles()}>
    <ColumnLayout>
      <Layout grow={1} shrink={1}>
        <Input
          login
          autofocus
          name='password'
          value={password}
          placeholder={__i18n('LOGIN.PASW_PLC')}
          onChange={onChangePassword}
        />
      </Layout>
      <Layout>
        <Button onClick={onLogin}>
          {__i18n('LANG.BUTTONS.ENTER')}
        </Button>
      </Layout>
    </ColumnLayout>
  </div>
)

export default Form
