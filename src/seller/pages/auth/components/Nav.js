import React from 'react'
import { Navigation, ItemGroup, ChangeLangItem } from 'bypass/ui/navigation'

const Nav = () => (
  <Navigation>
    <ItemGroup fill justify='center'>
      <ChangeLangItem replace={false} />
    </ItemGroup>
  </Navigation>
)

export default Nav
