import cookie from 'js-cookie'
import { actions } from './../actions'

const initialState = {
  token: cookie.get('seller_token'),
  password: '',
}

export default function auth(state = initialState, { type, password, token }) {
  switch (type) {
    case actions.changePassword:
      return {
        ...state,
        password,
      }
    case actions.login:
      return {
        token,
        password: '',
      }
    case actions.logout:
      return {
        token: null,
        password: '',
      }
    default:
      return state
  }
}
