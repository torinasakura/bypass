import cookie from 'js-cookie'
import { open } from '../../../app/actions/modal'

export const actions = {
  login: '@@bypass/auth/LOGIN',
  changePassword: '@@bypass/auth/CHANGE_PASSWORD',
  logout: '@@bypass/auth/LOGOUT',
}

export function login() {
  return async (dispatch, getState, { post }) => {
    const password = getState().auth.password

    if (!password) {
      return
    }

    const { result, error } = await post('login/authorize', { body: { password }, skipError: true })

    if (result.token) {
      cookie.set('seller_token', result.token)

      dispatch({
        type: actions.login,
        token: result.token,
      })

      window.location.hash = '/sells'
    }

    if (error == 1) {
      dispatch(open('auth/Errors', {
        header: __i18n('LANG.ERRORS.ERROR'),
        body: __i18n('LANG.ERRORS.INCORRECT_PASSWORD'),
      }))
    }
  }
}

export function changePassword(password) {
  return {
    type: actions.changePassword,
    password,
  }
}

export function logout() {
  return async dispatch => {
    cookie.remove('seller_token')

    dispatch({ type: actions.logout })

    window.location.hash = '/'
  }
}
