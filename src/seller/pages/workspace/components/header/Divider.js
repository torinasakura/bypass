import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
    height: '100%',
    flexBasis: '80px',
  },
})

const Divider = () => (
  <div className={styles()} />
)

export default Divider
