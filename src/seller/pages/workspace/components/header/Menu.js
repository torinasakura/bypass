import React from 'react'
import { ItemGroup, Item, ChangeLangItem } from 'bypass/ui/navigation'
import { ExitIcon } from 'bypass/ui/icons'

const Menu = ({ fill = false, onLogout }) => (
  <ItemGroup fill={fill} justify={fill ? 'space-around' : null}>
    <ChangeLangItem replace={false} />
    <Item withouthBorder onClick={onLogout}>
      <ExitIcon width={24} />
    </Item>
  </ItemGroup>
)

export default Menu
