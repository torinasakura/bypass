import React from 'react'
import { StyleSheet } from 'quantum'
import { ItemGroup, Item } from 'bypass/ui/navigation'

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
    height: '100%',
    flexGrow: 1,
  },
})

const Nav = () => (
  <div className={styles()}>
    <ItemGroup fill smallLimit justify='space-between'>
      <Item to='/sells'>
        {__i18n('SELLER.NAV.STAT')}
      </Item>
      <Item to='/payout'>
        {__i18n('SELLER.NAV.DISBURSEMENT')}
      </Item>
      <Item to='/pricing'>
        {__i18n('SELLER.NAV.PRICING')}
      </Item>
    </ItemGroup>
  </div>
)

export default Nav
