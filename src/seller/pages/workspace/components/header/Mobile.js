import React from 'react'
import { Navigation, Item, Menu as NavMenu, MenuItem } from 'bypass/ui/navigation'
import { MenuIcon } from 'bypass/ui/icons'
import Menu from './Menu'

const menu = (
  <NavMenu>
    <MenuItem to='/sells'>
      {__i18n('SELLER.NAV.STAT')}
    </MenuItem>
    <MenuItem to='/payout'>
      {__i18n('SELLER.NAV.DISBURSEMENT')}
    </MenuItem>
    <MenuItem to='/pricing'>
      {__i18n('SELLER.NAV.PRICING')}
    </MenuItem>
  </NavMenu>
)

const Header = ({ onLogout }) => (
  <Navigation full small>
    <Item withouthBorder menu={menu}>
      <MenuIcon width={36} />
    </Item>
    <Menu fill onLogout={onLogout} />
  </Navigation>
)

export default Header
