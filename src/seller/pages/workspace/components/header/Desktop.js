import React from 'react'
import { Navigation } from 'bypass/ui/navigation'
import Nav from './Nav'
import Menu from './Menu'
import Divider from './Divider'

const Desktop = ({ onLogout }) => (
  <Navigation>
    <Nav />
    <Divider />
    <Menu onLogout={onLogout} />
  </Navigation>
)

export default Desktop
