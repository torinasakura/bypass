import React from 'react'
import { StyleSheet } from 'quantum'
import { RowLayout, Layout, AutoSizer } from 'bypass/ui/layout'
import Header from './Header'

const styles = StyleSheet.create({
  self: {
    background: '#f5f5f5',
    width: '100%',
    height: '100%',
    position: 'relative',
    zIndex: 1,
  },
  push: {
    left: '245px',
    transition: 'left 0.2s',
  },
})

const Container = ({ children }) => (
  <div className={styles()}>
    <RowLayout>
      <Layout>
        <Header />
      </Layout>
      <Layout grow={1} shrink={1} scrollY>
        <AutoSizer>
          {children}
        </AutoSizer>
      </Layout>
    </RowLayout>
  </div>
)

export default Container
