import React from 'react'
import { connect } from 'react-redux'
import { logout } from '../../auth/actions'
import { Desktop, Mobile } from '../components/header'

const Container = ({ viewport: { type } = {}, ...props }) => {
  if (type === 'Mobile') {
    return (
      <Mobile {...props} />
    )
  }

  return (
    <Desktop {...props} />
  )
}

export default connect(
  state => ({
    viewport: state.viewport,
  }),
  dispatch => ({
    onLogout: () => {
      dispatch(logout())
    },
  })
)(Container)
