import { createViewport } from 'bypass/app/utils'
import { load } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'pricing',
    onEnter() {
      dispatch(load())
    },
    getComponent(location, callback) {
      System.import('../containers/Pricing').then(Pricing => callback(null, createViewport(Pricing)))
    },
  }]
}
