import * as actions from '../constants'

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('price')

    dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function add(countries, price) {
  return async (dispatch, getState, { get }) => {
    const body = {
      country_code: countries.map(country => country.country_code),
      price: parseFloat(price).toFixed(2),
    }

    await get('price/change', { body })

    dispatch(load())
  }
}

export function remove(countries) {
  return async (dispatch, getState, { get }) => {
    const body = {
      country_code: countries.map(country => country.country_code),
    }

    await get('price/delete', { body })

    dispatch(load())
  }
}
