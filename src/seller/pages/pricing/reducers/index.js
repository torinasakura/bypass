import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants'

const initialState = {
  country: [],
  price: [],
}

export default createReducer(initialState, {
  [actions.load]: (state, { country, price }) => {
    const countryByCode = country.reduce((result, item) => ({
      ...result,
      [item.country_code]: item.country,
    }), {})

    return {
      country,
      price: price.map(item => ({
        ...item,
        country: countryByCode[item.country_code],
      })),
    }
  },
})
