import { connect } from 'react-redux'
import { add, remove } from '../actions'
import * as Pricing from '../components'

const connector = connect(
  state => ({
    countries: state.pricing.country,
    prices: state.pricing.price,
  }),
  dispatch => ({
    onAdd: (countries, price) => {
      dispatch(add(countries, price))
    },
    onRemove: (prices) => {
      dispatch(remove(prices))
    },
  })
)

export const Desktop = connector(Pricing.Desktop)

export const Tablet = connector(Pricing.Tablet)

export const Mobile = connector(Pricing.Mobile)

