import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { AddPrice, UpdatePrice } from './editor'

const Desktop = ({ countries, prices, onAdd, onRemove }) => (
  <Wrapper>
    <Container indent>
      <RowLayout>
        <Layout>
          <Title>
            {__i18n('SELLER.PRICING.HEAD')}
          </Title>
        </Layout>
        <Layout basis='470px'>
          <ColumnLayout justify='center'>
            <Layout basis='470px'>
              <AddPrice
                countries={countries}
                onAdd={onAdd}
              />
            </Layout>
            <Layout basis='20px' />
            <Layout basis='470px'>
              <UpdatePrice
                prices={prices}
                onUpdate={onAdd}
                onRemove={onRemove}
              />
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </Container>
  </Wrapper>
)

export default Desktop
