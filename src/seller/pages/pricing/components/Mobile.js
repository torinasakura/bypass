import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { AddPrice, UpdatePrice } from './editor'

const Mobile = ({ countries, prices, onAdd, onRemove }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small'>
            {__i18n('SELLER.PRICING.HEAD')}
          </Title>
        </Layout>
        <Layout basis='10px' />
        <Layout basis='300px'>
          <ColumnLayout justify='center'>
            <Layout basis='10px' />
            <Layout grow={1} shrink={1}>
              <AddPrice
                countries={countries}
                onAdd={onAdd}
              />
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='15px' />
        <Layout basis='300px'>
          <ColumnLayout>
            <Layout basis='10px' />
            <Layout grow={1} shrink={1}>
              <UpdatePrice
                prices={prices}
                onUpdate={onAdd}
                onRemove={onRemove}
              />
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='15px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
