import AddPrice from './AddPrice'
import UpdatePrice from './UpdatePrice'

export {
  AddPrice,
  UpdatePrice,
}
