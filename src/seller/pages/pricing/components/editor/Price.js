import React from 'react'
import { StyleSheet } from 'quantum'
import { Checkbox } from 'bypass/ui/checkbox'

const styles = StyleSheet.create({
  self: {
    padding: '0 10px',
    fontSize: '16px',
    textAlign: 'left',
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: '38px',
    width: '100%',
    borderRadius: '5px',
    boxSizing: 'border-box',
    '& svg': {
      position: 'relative',
      top: '-2px',
    },
    '& div:nth-child(2)': {
      flexGrow: 1,
    },
    '&:hover': {
      background: '#ececec',
    },
  },
})

const Price = ({ price, onSelect }) => {
  const handleSelect = event => {
    event.preventDefault()
    event.stopPropagation()

    if (onSelect) {
      onSelect(price)
    }
  }

  return (
    <div className={styles()} onClick={handleSelect}>
      <div>
        <Checkbox
          checked={price.selected}
          onChange={() => {}}
        />
      </div>
      <div>
        {price.country} ({price.country_code})
      </div>
      <div>
        {price.price}$
      </div>
    </div>
  )
}

export default Price
