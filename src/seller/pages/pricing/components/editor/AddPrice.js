import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Autocomplete } from 'bypass/ui/autocomplete'
import { Input } from 'bypass/ui/input'
import { Text } from 'bypass/ui/text'
import Toolbar from './Toolbar'
import Country from './Country'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    background: '#ffffff',
    width: '100%',
    display: 'flex',
  },
})

class AddPrice extends Component {
  static defaultProps = {
    countries: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      countries: [],
      items: [],
      price: '',
    }
  }

  onLookupCountry = (value = '') => {
    const { countries } = this.props
    const { items } = this.state

    const matcher = new RegExp(value, 'i')

    if (value.length < 2) {
      return
    }

    this.setState({
      countries: countries.filter(country => matcher.test(country.country) && !items.includes(country)),
    })
  }

  onSelectCountry = ([country] = []) => {
    const { items } = this.state

    this.setState({
      items: items.concat(country),
    })
  }

  onRemoveCountry = (country) => {
    const { items } = this.state

    this.setState({
      items: items.filter(item => item !== country),
    })
  }

  onChangePrice = ({ target }) => {
    this.setState({ price: target.value })
  }

  onAdd = () => {
    const { onAdd } = this.props
    const { price, items } = this.state

    if (onAdd) {
      onAdd(items, price)
    }

    this.setState({ price: '', items: [] })
  }

  renderTopTollbar() {
    const { countries } = this.state

    return (
      <ColumnLayout align='center'>
        <Layout>
          <Text size={14}>
            {__i18n('SELLER.PRICING.ADD')}
          </Text>
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <Autocomplete
            showValues={false}
            items={countries}
            itemKey='country'
            placeholder={__i18n('SELLER.PRICING.SEARCH')}
            onType={this.onLookupCountry}
            onChange={this.onSelectCountry}
          />
        </Layout>
      </ColumnLayout>
    )
  }

  renderCountries() {
    const { items } = this.state

    return (
      <div style={{ width: '100%' }}>
        {items.map((country, index) => (
          <Country
            key={index}
            country={country}
            onRemove={this.onRemoveCountry}
          />
        ))}
      </div>
    )
  }

  renderBottomToolbar() {
    const { price, items } = this.state

    return (
      <ColumnLayout align='center'>
        <Layout basis='230px' shrink={1}>
          <Input
            value={price}
            disabled={!items.length}
            placeholder={__i18n('SELLER.PRICING.PRICE')}
            onChange={this.onChangePrice}
          />
        </Layout>
        <Layout grow={1} />
        <Layout basis='10px' />
        <Layout>
          <Button
            size='small'
            disabled={!(items.length && price.length)}
            onClick={this.onAdd}
          >
            {__i18n('SELLER.PRICING.ADD')}
          </Button>
        </Layout>
      </ColumnLayout>
    )
  }

  render() {
    return (
      <div className={styles()}>
        <RowLayout>
          <Layout>
            <Toolbar>
              {this.renderTopTollbar()}
            </Toolbar>
          </Layout>
          <Layout grow={1}>
            {this.renderCountries()}
          </Layout>
          <Layout>
            <Toolbar>
              {this.renderBottomToolbar()}
            </Toolbar>
          </Layout>
        </RowLayout>
      </div>
    )
  }
}

export default AddPrice
