import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Input } from 'bypass/ui/input'
import { Text } from 'bypass/ui/text'
import Toolbar from './Toolbar'
import Price from './Price'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    background: '#ffffff',
    width: '100%',
    display: 'flex',
  },
})

class UpdatePrice extends Component {
  static defaultProps = {
    prices: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      prices: props.prices,
      price: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.prices !== this.props.prices) {
      this.setState({ prices: nextProps.prices })
    }
  }

  onSelectPrice = (price) => {
    const { prices } = this.state

    this.setState({
      prices: prices.map(item => {
        if (item === price) {
          return {
            ...item,
            selected: !item.selected,
          }
        }

        return item
      }),
    })
  }

  onChangePrice = ({ target }) => {
    this.setState({ price: target.value })
  }

  onUpdate = () => {
    const { onUpdate } = this.props
    const { price, prices } = this.state
    const selected = prices.filter(item => item.selected)

    if (onUpdate) {
      onUpdate(selected, price)
    }

    this.setState({ price: '' })
  }

  onRemove = () => {
    const { onRemove } = this.props
    const { prices } = this.state
    const selected = prices.filter(item => item.selected)

    if (onRemove) {
      onRemove(selected)
    }
  }

  renderTopTollbar() {
    return (
      <Text size={14}>
        {__i18n('SELLER.PRICING.CURRENT')}
      </Text>
    )
  }

  renderPrices() {
    const { prices } = this.state

    return (
      <div style={{ width: '100%' }}>
        {prices.map((price, index) => (
          <Price
            key={index}
            price={price}
            onSelect={this.onSelectPrice}
          />
        ))}
      </div>
    )
  }

  renderBottomToolbar() {
    const { price, prices } = this.state
    const selected = prices.filter(item => item.selected)

    return (
      <ColumnLayout align='center'>
        <Layout basis='230px' shrink={1}>
          <Input
            value={price}
            disabled={!selected.length}
            placeholder={__i18n('SELLER.PRICING.PRICE')}
            onChange={this.onChangePrice}
          />
        </Layout>
        <Layout grow={1} />
        <Layout basis='10px' />
        <Layout>
          <Button
            size='small'
            disabled={!(selected.length && price.length)}
            onClick={this.onUpdate}
          >
            {__i18n('SELLER.PRICING.UPDATE')}
          </Button>
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <Button
            size='small'
            disabled={!selected.length}
            onClick={this.onRemove}
          >
            {__i18n('LANG.BUTTONS.DELETE')}
          </Button>
        </Layout>
      </ColumnLayout>
    )
  }

  render() {
    return (
      <div className={styles()}>
        <RowLayout>
          <Layout>
            <Toolbar>
              {this.renderTopTollbar()}
            </Toolbar>
          </Layout>
          <Layout grow={1}>
            {this.renderPrices()}
          </Layout>
          <Layout>
            <Toolbar>
              {this.renderBottomToolbar()}
            </Toolbar>
          </Layout>
        </RowLayout>
      </div>
    )
  }
}

export default UpdatePrice
