import React from 'react'
import { StyleSheet } from 'quantum'
import { CloseIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    paddingLeft: '10px',
    fontSize: '16px',
    textAlign: 'left',
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: '38px',
    width: '100%',
    borderRadius: '5px',
    boxSizing: 'border-box',
    '& div:first-child': {
      flexGrow: 1,
    },
    '& div:last-child svg': {
      position: 'relative',
      top: '3px',
      right: '5px',
    },
    '&:hover': {
      background: '#ececec',
    },
  },
})

const Country = ({ country, onRemove }) => (
  <div className={styles()}>
    <div>
      {country.country}
    </div>
    <div onClick={() => onRemove && onRemove(country)}>
      <CloseIcon width={16} fill='#b71c1c' />
    </div>
  </div>
)

export default Country
