import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#d3e0e5',
    width: '100%',
    padding: '10px',
    minHeight: '50px',
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'center',
  },
})

const Toolbar = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Toolbar
