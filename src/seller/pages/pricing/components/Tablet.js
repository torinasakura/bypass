import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { AddPrice, UpdatePrice } from './editor'

const Tablet = ({ countries, prices, onAdd, onRemove }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('SELLER.PRICING.HEAD')}
          </Title>
        </Layout>
        <Layout basis='470px'>
          <ColumnLayout justify='center'>
            <Layout basis='10px' />
            <Layout basis='50%' shrink={1}>
              <AddPrice
                countries={countries}
                onAdd={onAdd}
              />
            </Layout>
            <Layout basis='20px' />
            <Layout basis='50%' shrink={1}>
              <UpdatePrice
                prices={prices}
                onUpdate={onAdd}
                onRemove={onRemove}
              />
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
