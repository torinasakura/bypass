/* eslint-disable */

module.exports = {
  LOCALE:  'cn',
  BILLING: {
    PM:            {
      LOAD_TEXT: '手续费 <strong>0%</strong>.\
      付款处理不受任何限制。'
    },
    GIFT:          {
      LOAD_TEXT:  '礼品码生成与兑换均不受任何限制。',
      PLCHLDR:    '输入金额, 如10.88',
      ENTER_CODE: '输入您的礼品码',
      READY:      '您价值 <b>$${amount}</b>的礼品码已兑换完成！'
    },
    WM:            {
      LOAD_TEXT: '手续费<b>4%</b>."充值"后点击"确认"将资金存入您的账户。'
    },
    BTC:           {
      LOAD_TEXT:     '手续费<b>3%</b>.\
      请您在比特币系统<b>1</b> 个确认交易后，点击"确认"将资金存入您的账户。',
      CREATE_WALLET: '您还未创建比特币地址, 请点击"创建钱包" 按键.'
    },
    TABLE:         {
      COLS:  {
        INDEX:    '索引',
        DIR:      '交易方向',
        TIME:     '添加时间',
        CMNT:     '描述',
        AMOUNT:   '交易金额, $',
        BALANCE:  '余额, $',
        COUNT:    '数量.',
        ORDER_ID: '订单ID'
      },
      CLEAR: '清除'
    },
    LOAD_UNAV:     '请求的充值方式无法使用.',
    IF_TROUBLE:    '付款失败？',
    NEW_TICKET:    '新建私信',
    WALLET:        '钱包',
    COMMENT:       '评论',
    PRESS_CHECK:   '支付完成后，请点击 "确认"。',
    NO_PROTECT:    '受保护支付未处理。',
    POINT_ADDRESS: '要完成支付，请使用以下地址：',
    WAIT:          '请等待...',
    TYPE:          '类型',
    AMOUNT:        '金额',
    COURSE:        '汇率',
    BATCH:         '批号',
    CHECK:         '确认'
  },
  SEARCH:  {
    PAGEINFO:      '找到卡片 <strong>${count}</strong>, 您的限额 <strong>${limit}</strong>',
    SELECT_SEARCH: '从您保存的列表中选择搜索',
    NOT_FLAG:      '反选',
    TEXT_INPUT:    '文本输入',
    ZIP_MASK: '通过屏蔽',
    ZIP_MASK_LEGEND: '<strong>*</strong> &minus; 零以上字符',
    ZIP_MASK_LEGEND1: '<strong>.</strong> &minus;任何单字符',
    FAST_BUY:      {
      CARD_SELECTED: '已选卡片',
      TOTAL_COAST:   '总费用',
      AFTER_BUY:     '购买后',
      SET_CHECK:     '检测',
      GO_ORDER:      '添加到历史订单',
      SHOW_CVS:      'CVS显示',
      FAST:          '快速购买'
    },
    ASIDE:         {
      BIN:      'BIN码',
      BRAND:    '品牌',
      TYPE:     '类型',
      LEVEL:    '等级',
      BANK:     '银行',
      COUNTRY:  '国家',
      STATE:    '州',
      CITY:     '城市',
      CITY_PLC: '输入城市名称',
      SEARCH:   '搜索',
      ZIP:      '邮编',
      ZIP_PLC1: '邮编',
      ZIP_PLC2: '输入邮编',
      ZIP_PLC3: '输入邮编屏蔽项',
      ADDRESS:  '地址',
      SELLER:   '卖家',
      BASE:     '卡库',
      LOAD:     '卡库更新',
      PERIOD:   '过期日',
      CRITERIA: '您的搜索条件'
    }
  },
  CART:    {
    SUMMARY: '已选择卡片: <strong>${count}</strong> 总费用: <strong>${summ}</strong>'
  },
  CHECK:   {
    FORMAT_LABEL:     '输入格式',
    FORMAT_PLCHLDR:   'E.g. %num%|%m%|%y%|%cvv2%',
    FORMAT_TOOLTIP:   '请正确输入格式，应包括一组标签。请在输入栏输入信息。必填信息： 卡号，月， 年，以及CVV2号。',
    SAVE_TPL_LABEL:   '保存格式',
    LOAD_TPL_LABEL:   '加载格式',
    SELECT_TPL_LABEL: '从已保存列表中选择格式',
    LEGEND_LABEL:     '<b>%num%</b> &mdash; 卡号, <b>%m%</b> &mdash; 月, <b>%y%</b> &mdash; 年,\
    <b>%cvv2%</b> &mdash; CVV2, <b>%zip%</b> &mdash; 邮编, <b>%addr%</b> &mdash; 地址',
    FORM_LABEL:       '卡片信息输入栏',
    HINT_LABEL:       '提示',
    AREA_PLCHLDR:     '例如 444444444444444|12|2015|333',
    INSTRUCT:         '按照已选格式添加卡片，点击“预览”。您将在下方看见确认信息。若有错误，将红色高亮显示。若您的卡片超过限制，请从历史记录中删除。',
    CARD_ADDED:       '已添加卡片',
    PREVIEW:          '预览'
  },
  MAIN:    {
    GO_TO:         '去网页',
    ADVERTISEMENT: '广告',
    CARD:          '卡片'
  },
  MISC:    {
    CHANGE_DOMAIN1: '新域名1',
    CHANGE_DOMAIN2: '新域名2',
    TIMEOUT:        '检测时间: <b>${t.m}</b> 分. <b>${t.s}</b> 秒.',
    HOW_UP_TIMEOUT: '如何提高检测时间?',
    STEP2STEP:      {
      ICHECK: '我点击了“确认”。',
      IWAIT:  '我等待了来自比特币的一份确认。',
      IWRITE: '我正确填写了评论。'
    }
  },
  NEWS:    {
    BACK_TO_LIST: '回到新闻',
    LIKE:         '赞',
    DISLIKE:      '踩',
    MARK_ALL:     '标记为已读',
    TYPES:        {
      OPINION: '您的意见',
      BASE:    '新卡库',
      SITE:    '新选项'
    }
  },
  ORDERS:  {
    CHECKING:  '检测中',
    CHECKED:   '卡片已检测',
    UNCHECKED: '卡片未检测',
    TABLE:     {
      ADD_TIME:      '购买时间',
      COUNT:         '总数',
      VALID:         '有效',
      UNOPEN:         '未开封',
      ORDER_COMMENT: '订单评论',
      PRICE:         '价格, $'
    },
    BY_BIN:    'BIN码所属国家'
  },
  PROFILE: {
    CHECK_SYS_LABEL:          '检测系统',
    SELECT_PROVIDER:          '选择检测系统',
    CSV_OUTPUT_LABEL:         'CSV 输出选项',
    SELECT_CSV_FIELDS:        '在CSV中选择所需字段',
    AVAIL_FIELD:              '可用字段',
    ACTIVE_FIELD:             '有效字段',
    UP:                       '向上',
    DOWN:                     '向下',
    DELIMETER:                '分隔符',
    CSV_LEGEND_TEXT:          '已选&nbsp;<b>${count}</b>/<b>${max_csv_field}</b> 字段. 需选择 <b>${max_csv_field}</b>字段已显示。',
    CSV_OUTPUT_SHARED_LABEL:  'CSV显示选项(高级)',
    CSV_OUTPUT_SHARED_LEGEND: '输入最大<b>16</b> 字段',
    STATISTIC:                {
      TEXT:            '数据',
      WASTED_AMOUNT:   '利润',
      BUYED:           '已购买卡 (数量)',
      WASTED_BY_CARD:  '已购买卡 ($)',
      CHECKED_COUNT:   '已检测卡(数量.)',
      CHECK_AMOUNT:    '已检测卡 ($)',
      RETURNED_COUNT:  '无效卡 (数量)',
      RETURNED_AMOUNT: '无效卡 ($)',
      RATING:          '评级',
      STATUS:          '状态',
      SEARCH_LIMIT:    '搜索限数',
      CHECK_TIMEOUT:   '检测时间',
      NOT_IN_RATING:   '无评级',
      M:               '分',
      S:               '秒'
    },
    SHOW_OPTS:                {
      TEXT:         '展示选项',
      SHOW_INVALID: '显示无效卡',
      SHOW_CONFIRM: '请求确认',
      SHOW_ADV:     '显示广告'
    },
    FAST_BUY:                 {
      TEXT: '快速购买'
    },
    SEARCH:                   {
      TEXT:           '搜索显示选项',
      COLUMNS_LEGEND: '选择必要字段，以查看搜索表。',
      LEGEND:         '已选 <b>${count}</b>/<b>16</b>字段.。需要选择 <b>16</b> 字段来显示.'
    },
    ITEMS_PER_PAGE:           {
      TEXT:         '每页数量',
      SEARCH_TABLE: '搜索表'
    }
  },
  STAT:    {
    ID:       'ID',
    DATE:     '日期',
    COUNT:    '编号',
    VALID:    '有效',
    LOAD:     '更新',
    BASES:    '卡库',
    BASE:     '卡库',
    SELLERS:  '卖家',
    SELLERSS: '卖家',
    SUMM:     '总数',
    NAME:     '姓名',
    SELLER:   '卖家',
    DYN:      {
      TEXT:         '动态有效',
      SELECT_BASE:  '选择卡库',
      QUICK_SEARCH: '快速搜索...',
      SELECT_LOAD:  '选择更新',
      TIME_UNIT:    {
        WEEK:   '周',
        MONTH:  '月',
        SEASON: '季'
      },
      LEGEND:       '选择您感兴趣的最大更新值 <b>${maxSelected}</b> 。'
    },
    STAT:     {
      TEXT:            '数据',
      LOAD_TOOLTIP:    '来自 ${time_formatted}, 有效 ${valid}%',
      BASE_TOOLTIP:    '卡库 ${name}, 有效 ${valid}%',
      SELLERS_TOOLTIP: '卖家 ${姓名}, 有效 ${valid}%',
      SHOW_ALL:        '显示全部',
      SHOW_ALLS:       '显示全部l'
    }
  },
  TICKETS: {
    CREATE:          '新建私信',
    OPEN:            '打开',
    CLOSED:          '关闭',
    IN_PROGRESS:     '处理中',
    YOU:             '您',
    SUBJECT:         '标题',
    NEW_MSG_PLCHLDR: '在此处输入信息',
    SCHEDULE:        '管理员，来自<b>9:00</b> — <b>21:00</b> UTC+3<br>\
      支持，来自<b>0:00</b> — <b>24:00</b> UTC+3<br>\
      支持系统需要最多 <b>10</b>分钟进行回应.'
  },
  LOGIN:   {
    PASW_PLC:  '密码',
    YOUR_PASW: '您的密码',
    ATTENTION: '请妥善保管您的密码！如果丢失，您将无法恢复账户！ '
  },
  FOOTER:  {
    NEED_HELP: '需要帮助?',
    HELP: '幫助',
    CHECK_SYS: {
      TEXT: '检测系统'
    },
    SUPPORT:   {
      TEXT: '工作人员'
    },
    STATUS:    {
      WORK:     '在线',
      NOT_WORK: '离线',
      PART:     '部分在线',
      HOLYDAY:  '休假'
    },
    UPDATE:    '版本'
  },
  HEADER:  {},
  NAV:     {
    MAIN:     '主页',
    CARDS:    {
      TEXT:   '卡片',
      SEARCH: '搜索',
      CART:   '购物车',
      ORDERS: '历史订单',
      STAT:   '数据'
    },
    CHECK:    {
      TEXT:    '检测',
      CHECK:   '检测',
      HISTORY: '历史'
    },
    BILLING:  {
      TEXT:    '账单',
      BALANCE: '充值',
      MAIN:    '交易记录'
    },
    NEWS:     '新闻',
    TICKETS:  '私信',
    FAQ:      '常见问题',
    PROFILE:  '档案',
    REGISTER: '注册'
  },
  COM:     {
    EMPTY:       '空',
    _EMPTY_:       '<空>',
    SET_DEFAULT: '设置默认',
    SORT:        '排序',
    SELECT:      '选择...',
    ADD_TO_CART: '加入购物车',
    ADD_NOTE:    '添加评论',
    MANAGERS:    {
      OWNER:   '所有者',
      ADMIN:   '管理员',
      SUPPORT: '支持'
    },
    COUNT:       '项目数',
    SETUP:       '启动',
    WRONG_TIME:  '支持服务暂时不可用。您会根据工作时间表，收到回复。'
  },
  VALID:   {
    LOW: '低'
  },
  LANG:    {
    ERRORS:  {
      ERROR:                  '错误',
      AUTH_ERROR:             '授权错误',
      UNKNOW_ERROR:           '未知错误',
      NOTHING_SELECT:         '未选择任何选项',
      COMMON:                 '操作失败，请稍后再试。',
      CARD_CHECK_TIMEOUT:     '审核超时',
      INCORRECT_PASSWORD:     '密码错误',
      INCORRECT_REQUEST: '请求错误',
      SEARCH_SYSTEM_PROBLEM:  '搜索系统错误',
      BTC_INCORRECT_RESPONSE: '服务器反应错误',
      SEARCH_EXISTING:        '该搜索已存在',
      SEARCH_NAME_EMPTY:      '搜索名称不能为空！',
      404:                    '未找到页面',
      500:                    '服务器错误',
      CHECK_CORRECT:          '检查信息是否正确',
      RESTRICTED_ACCESS:      '访问受限',
      INVALID_PARAMS:         '无效选项',
      POOR_PARAMS:            '选项不足',
      NOT_SPECIFIED_FORMAT:   '未指定格式',
      EMPTY_CARDS_DATA:       '未指定卡信息',
      FILL_AND_TRY:           '请将卡信息填入输入栏',
      MIN_DATA_FORMAT:        '必填信息：卡号，月，年，CVV2号',
      SHORT_TEXT:             '文本太短',
      EMPTY_VALUE:            '值域不能为空。',
      SHORT_VALUES_LIST:      '未选择足够的值',

      INCORRECT_FORMAT:   '错误格式',
      INCORRECT_GIFTCODE: '错误礼品码',

      SAVED_SEARCH_LIMIT: '已保存搜索限数',

      GENERAL: {
        'GENERAL ERROR':        '未处理的错误',
        'NO PARAM':             '所要求选项有一项丢失',
        'BAD VALIDATION':       '一个选项无法验证',
        'ALREADY EXISTS':       '您尝试创建的对象已经存在',
        'TIMEOUT':              '注册受限，稍后再试。',
        'NO ACCESS':            '访问受限',
        'UNKNOWN ACTION':       '系统无法识别此次行为',
        'EMPTY PARAM':          '所要求选项有一项为空',
        'NOTIFY_CHECK_TIMEOUT': '服务器升级错误。请检查您的网络连接。'
      },

      PAYMENT: {
        'DUPLICATE RECORD':           '钱包错误。该钱包已存在。',
        'BAD ADDRESS':                '比特币连接错误。请稍后再试。',
        'CANT CONNECT (NEW_ADDRESS)': '比特币连接错误。请稍后再试。',
        'CANT CONNECT (RAWADDR)':     '比特币连接错误。请稍后再试。',
        'BAD RESPONSE (BLOCK)':       '比特币连接错误。请稍后再试。',
        'CANT CONNECT (BLOCK)':       '比特币连接错误。请稍后再试。',
        'EMPTY ADDRESS':              '创建钱包，再次尝试。',
        'EMPTY AMOUNT AFTER FEE':     '您支付的金额过小。',
        'EMPTY AMOUNT':               '您支付的金额过小。',
        'CANT CONNECT (COURSE)':      '比特币连接错误。请稍后再试。',
        'PENDING':                    '交易已登记，但未收到足够确认信息。',
        'NO PAYMENT':                 '本钱包未收到任何付款。',
        'CANT CONNECT (WM)':          'Webmoney 连接失败，请稍后再试。',

        'CANT CONNECT (PM)': 'Perfect Money 连接失败，请稍后再试。',

        NOT_ENROLL: '支付未处理'
      },

      CARDS: {
        ALREADY_CHECKED:   '已检测的卡',
        ALREADY_BUYED:     '已购买的卡',
        BALANCE_ISSUE:     '余额不足！',
        NOTHING_BUYED:     '未购买任何物品！',
        IS_NO_ADDED:       '未添加卡',
        OVER_LIMITED:      '很抱歉，您无法添加新卡。新卡已超过限制。',
        SMALL_BALANCE:     '余额不足',
        INCORRECT_DATA:    '请检查已输入信息是否正确',
        NEED_TO_OPEN_CARD: '支票支付前，您需要开通卡片'
      }
    },
    SERVICE: {
      MESSAGE:             '信息',
      CASH_NUMBER_PRETEXT: '完成该支付，输入本地址',
      PAYMENT_SUCCESS:     '支付成功完成!',
      EMPTY_TABLE:         '空',
      FAST_BUY:            '快速购买',

      DELETE_CARDS_CONFIRM:  '您确定要删除卡？',
      DELETE_ORDERS_CONFIRM: '您确定要删除订单？',

      CHECK_ORDER_CONFIRM: '您确定要检测订单？',
      NOT_MONEY_BACK:      '我们不对特价卡进行退款',

      CARDS_BUYED: '已购买卡片： ',
      READ_FAQ:    '请您阅读常见问题FAQ',

      THANK4PURCHASE: '感谢您的购买！',
      EDIT_TITLE:     '编辑标题',

      LOAD_SEARCH:       '加载搜索',
      ENTER_SEARCH_NAME: '输入搜索名称',
      NOTE:              '评价',
      EMPTY_NOTE:        '评价空白',
      CARDS_CHECK:       '卡片检测',
      ATTENTION:         '警告',

      SEARCH_RESULT:      '搜索结果',
      EXTERNAL_SEARCH_RESULT: '第三方搜索结果',
      TICKETS:            '私信',
      FAQ:                '常见问题',
      DELETE_CARDS_TITLE: '删除卡片',
      ORDERS_DELETE:      '订单删除',
      ORDERS_CHECK:       '订单检测',
      LOAD_BALANCE:       '充值余额',
      LOAD_OVER:          '用来充值',
      PROFILE:            '档案',
      NEWS:               '新闻',

      CREATE_PURSE: '创建钱包',

      PROCESSING_QUERY: '您的请求正在处理',

      OPERATION_STATUS: '操作状态',

      REPEAT_OVER: '重复',

      BACK_TO_ORDERS:  '回到历史订单',
      BACK_TO_TICKETS: '回到私信',

      ORDER_SHOW: '订单',
      CSV_VIEW:   'CSV预览',
      CSV_FORMAT: 'CSV格式',
      NEW_TICKET: '新私信',

      CSV_CHANGE_SUCCESS: "CSV格式成功修改.",

      NULL_CARDS: '打开了0个卡片',

      CHECKING:   '审核中',
      VALID:      '有效',
      INVALID:    '无效',
      UNCHECKED:  '未检测',
      NOCASHBACK: '不可退款',

      CHECK_SYSTEM:                '检测系统',
      CHECK_SYSTEM_CHANGE_SUCCESS: '检测系统修改成功. 您的新系统： ',

      VIEW_OPTIONS:                '查看选项',
      VIEW_OPTIONS_CHANGE_SUCCESS: '查看选项修改成功。',

      SEARCH_OPTIONS:                '搜索选项',
      SEARCH_OPTIONS_CHANGE_SUCCESS: '搜索选项修改成功。',

      MONTH:             ['一月.', '二月.', '三月.', '四月.', '五月', '六月.', '七月.', '八月.', '九月.', '十月.', '十一月.', '十二月.'],
      DELETED_CARDS:     '已删除卡号： ',
      CARD_DELETED:      '卡已删除',
      EMPTY_TPL:         '模版列表为空',
      TPL_SUCCESS:       '模版保存成功',
      TPL_ALREADY_EXIST: '模版已存在',
      LOAD_TPL:          '加载模版',
      EMPTY_SELECTION:   '未选择卡片',
      TRANSACT:          '模版编号',

      INTYPES: {
        BTC:       '比特币',
        FAKE:      'Internal',
        WM:        'Webmoney',
        PM:        'PerfectMoney',
        UNDEFINED: '未定义'
      },

      PROFILE_STATUS: {
        '0': '匿名',
        '1': '受信任',
        '2': '已验证'
      },

      ALL_BASE:              '全部卡库',
      GIFTCODE_SUCCESS:      '礼品码已成功兑换',
      GIFTCODE_HEADER:       '您的礼品码',
      GIFTCODE_REDEEM:       '兑换礼品码',
      SEARCH_SAVED:          '搜索保存成功！',
      TICKET_DELETE_CONFIRM: '已读私信将被删除。您确定吗？',
      NO_TICKETS_DELETED:    '无已关闭或已读私信',

      ORDERS_LIST:      '订单',
      BACK_TO_SEARCH:   '回到卡片搜索 ‘',
      CART:             '购物车',
      BILLING:          '账单',
      LOW:              '低',
      COUNT:            '件.',
      ADD_TO_CHECK:     '添加卡去检测',
      CHECK_RESULT:     '检测结果',
      EMPTY:            '空',
      CREATE_GIFT_CODE: '创建礼品码',
      STAT_HEADER:      '数据',
      QUANT:            '数量',
      VALID_COUNT:      '有效',
      BUYED:            '已购买',
      CLOSED:           '已关闭',
      CHECKED:          '已检测',
      M:                '分',
      S:                '秒',
      H:                '小时',
      M_AGO:            '分钟以前',
      S_AGO:            '秒以前',
      H_AGO:            '小时以前',
      PARAGRAPH:        '§',
      ADV:              '广告',
      checkAllText:     '全选',
      uncheckAllText:   '取消全部选择',
      QUICK_SEARCH:     '快速搜索'

    },
    TABLE:   {
      COLS:          {
        ID:           'ID',
        INDEX:        '索引',
        NUMBER:       '编号',
        EXP:          '有效期',
        HOLDER:       '卡主',
        LEVEL:        '等级',
        TYPE:         '类型',
        BANK:         '银行',
        ZIP:          '邮编',
        ADDRESS:      '地址',
        CITY:         '城市',
        STATE_CODE:   '州',
        COUNTRY_CODE: '国家',
        EMAIL:        'Email',
        PHONE:        '电话',
        LOAD_VALID:   '有效率, %',
        SELLER:       '卖家',
        BASE:         '卡库',
        ADD_TIME:     '添加时间',
        SELLER_PRICE: '价格, $',
        'CVV2':       'CVV2',
        'M':          '月',
        'Y':          '年',
        DATE:         '日期',
        PRICE_ITEM:   '价格',
        PROVIDER:     '供应者',
        STATUS:       '状态',
        RESPONSE:     '回复码',
        COMMENT:      '评价',
        THEME:        '主题',
        OPENED:       '打开',
        FROM:         '来自',
        TO:           '致',
        CARDS_ANOTHER: '卡（第三方）'
      },
      lengthMenu:    '显示 _MENU_',
      CARDS_sInfo:   '找到卡片 <strong>_TOTAL_</strong>, 从_START_ 到 _END_显示',
      ORDERS_sInfo:  "找到订单<strong>_TOTAL_</strong>, 从_START_ 到 _END_显示",
      TRANS_sInfo:   '找到交易<strong>_TOTAL_</strong>, 从_START_ 到 _END_显示',
      TICKETS_sInfo: '找到标签<strong>_TOTAL_</strong>, 从_START_ 到 _END_显示',
      ELEMENTS:      ' 元素' //'30元素', '50元素', '100元素'
    },
    BUTTONS: {
      CANCEL:       '取消',
      DELETE:       '删除',
      SAVE:         '保存',
      APPLY:        '应用',
      CREATE:       '创建',
      SELECT_ALL:   '全选',
      CHECK:        '检测',
      LOAD:         '充值',
      REDEEM:       '兑换',
      SEE:          '查看',
      BUY:          '购买',
      OPEN:         '打开',
      CHECK_CARDS:  '检测卡片',
      SHOW_CSV:     '在CSV中显示',
      EDIT_COMMENT: '编辑评价',
      PAY_CHECK:    '检测(不可退款)',
      DOWNLOAD_ALL: '全部下载',
      DOWNLOAD:     '下载',
      CLEAR:        '清除',
      SEND:         '发送',
      ENTER:        '开始',
      SAVE_SEARCH:  '保存搜索',
      LOAD_SEARCH:  '加载搜索',
      RESET_SEARCH: '清除搜索',
      SEARCH_CARDS: '搜索',
      AGREE:        '我同意',
      SHOW:         '显示',
      MORE: '顯示更多'
    }
  },
  SELLER:  {
    NAV: {
      STAT: '统计',
      DISBURSEMENT: '代垫',
      PRICING: '价格主编',
      EXIT: '注销'
    },
    SELLS: {
      HEAD: '统计 和 代垫',
      NAME: '名',
      PERCENTAGE: '比率',
      LOADED: '加载的',
      TURNOVER: '周转',
      INCORRECT: '不正确',
      INVALID: '失效',
      SOLD: '售出',
      VALID: '应用%',
      REFUND: '换回新公债',
      REFUND_CHECK: '退款（检查）',
      PROFIT: '润',
      WITHDRAW: '提款',
      BALANCE: '差额'
    },
    PAYOUT: {
      HEAD: '派彩',
      SUM: '总数',
      WALLET: '钱包',
      DATE: '日期',
      STATUS: '待遇',
      STATUS_PROCESSING: '洗印',
      STATUS_PROCESSED: '处理',
      STATUS_REJECTED: '拒绝'
    },
    PRICING: {
      HEAD: '价格主编',
      ADD: '附带',
      UPDATE: '更新',
      CURRENT: '现',
      SEARCH: '搜查国',
      PRICE: '输入价例子4.99'
    }
  }
}
