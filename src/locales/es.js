/* eslint-disable */

module.exports = {
  LOCALE: 'es',
  BILLING: {
    PM: {
      LOAD_TEXT: 'Comisión por la recarga&nbsp;<strong>0%</strong>.\
    La aceptación de los pagos se realiza sin restricción.'
    },
    GIFT: {
      LOAD_TEXT: 'Se permite crear y utilizar los códigos de regalo sin ninguna restricción.',
      PLCHLDR: 'Introduzca la cantidad, por ejemplo 10.88',
      ENTER_CODE: 'Introduzca su código de regalo',
      READY: 'Su código de regalo por el valor de  <b>$${amount}</b> ¡está listo!'
    },
    WM: {
      LOAD_TEXT: 'Comisión por la recarga <b>4%</b>. La aceptación de los pagos se realiza sin restricción.'
    },
    BTC: {
      LOAD_TEXT: ' Comisión por la recarga <b>3%</b>.\
     Hace falta <b>1</b> confirmación por la transacción en el sistema Bitcoin para transferir el dinero a su cuenta.',
      CREATE_WALLET: 'Su cuenta en el sistema Bitcoin todavía no se ha creado, pulsa el botón "Crear eWallet.".'
    },
    TABLE: {
      COLS: {
        INDEX: 'Número',
        DIR: 'Dirección',
        TIME: ' Tiempo de transacción',
        CMNT: ' Descripción',
        AMOUNT: 'Importe de la transacción, $',
        BALANCE: 'Saldo, $',
        COUNT: 'Cantidad',
        ORDER_ID: 'ID de la compra'
      },
      CLEAR: 'Borrar el contenido de la tabla'
    },
    LOAD_UNAV: 'Este modo la recarga no está disponible.',
    IF_TROUBLE: 'Si su pago aún no ha sido  transferido a su cuenta',
    NEW_TICKET: 'Crear un ticket',
    WALLET: 'eWallet',
    COMMENT: 'Comentario',
    PRESS_CHECK: 'Haga clic en el botón "Verificar" después del pago.',
    NO_PROTECT: 'No se aceptan los pagos con la protección.',
    POINT_ADDRESS: 'Para realizar pagos indique la siguiente dirección:',
    WAIT: 'Espere...',
    TYPE: 'Tipo',
    AMOUNT: 'Importe',
    COURSE: 'Tasa de cambio',
    BATCH: 'Batch',
    CHECK: 'Verificar'
  },
  SEARCH: {
    PAGEINFO: 'Tarjetas encontradas <strong>${count}</strong>, su límite es <strong>${limit}</strong>',
    SELECT_SEARCH: 'Escoja un modelo de búsqueda de la lista ',
    NOT_FLAG: 'Todos exepto los marcados ',
    TEXT_INPUT: ' Introducción de texto',
    ZIP_MASK: 'Por máscara',
    ZIP_MASK_LEGEND: '<strong>*</strong> &minus; cero (0) y más cualquier caracteres',
    ZIP_MASK_LEGEND1: '<strong>.</strong> &minus; un (1) cualquier carácter',
    FAST_BUY: {
      CARD_SELECTED: ' Tarjetas seleccionadas',
      TOTAL_COAST: 'Total',
      AFTER_BUY: 'Inmediatamente después de la compra ',
      SET_CHECK: 'Poner en la verificación',
      GO_ORDER: 'Ver las tarjetas compradas',
      SHOW_CVS: 'Mostrar en CVS',
      FAST: 'Compra rápida'
    },
    ASIDE: {
      BIN: 'BIN',
      BRAND: 'Marca',
      TYPE: 'Tipo',
      LEVEL: 'Nivel',
      BANK: 'Banco',
      COUNTRY: 'País',
      STATE: 'Estado',
      CITY: 'Ciudad',
      CITY_PLC: ' Introduzca la ciudad',
      SEARCH: 'Buscar',
      ZIP: 'ZIP',
      ZIP_PLC1: 'Código postal (ZIP)',
      ZIP_PLC2: 'Introduzca el сódigo postal (ZIP)',
      ZIP_PLC3: 'Introduzca la mascara de búsqueda para ZIP (el сódigo postal)',
      ADDRESS: 'Dirección',
      SELLER: 'Vendedor',
      BASE: 'Base',
      LOAD: 'La recarga',
      PERIOD: 'Período de validez',
      CRITERIA: 'Criterios de buscar'
    }
  },
  CART: {
    SUMMARY: 'Tarjetas seleccionadas: <strong>${count}</strong> en el importe de: <strong>${summ}</strong>'
  },
  CHECK: {
    FORMAT_LABEL: 'Formato de entrada de datos',
    FORMAT_PLCHLDR: 'Por ejemplo %num%|%m%|%y%|%cvv2%',
    FORMAT_TOOLTIP: 'Introduzca el formato correctamente. El formato debe contener un conjunto de etiquetas. Los datos de tarjetas ponga en formulario de entrada de los datos. Datos obligatorios mínimos de tarjeta: número de tarjeta, mes, año y CVV2.',
    SAVE_TPL_LABEL: 'Guardar el modelo',
    LOAD_TPL_LABEL: 'Aplicar el modelo',
    SELECT_TPL_LABEL: 'Seleccione un modelo de la lista',
    LEGEND_LABEL: '<b>%num%</b> &mdash; número de tarjeta, <b>%m%</b> &mdash; mes, <b>%y%</b> &mdash; año,\
   <b>%cvv2%</b> &mdash; CVV2, <b>%zip%</b> &mdash; zip, <b>%addr%</b> &mdash; dirección',
    FORM_LABEL: 'Campo de entrada de los datos de la tarjeta',
    HINT_LABEL: 'Sugerencia',
    AREA_PLCHLDR: 'Por ejemplo 444444444444444|12|2015|333',
    INSTRUCT: 'Escriba (ponga) los datos de las tarjetas de acuerdo al tipo de formato seleccionado y pulsa el botón “Vista previa”. En la parte inferior verá los datos de la tarjeta. Si hay unas faltas de los datos introducidos se marcan con rojo. Borre las tarjetas del historia si su límite se ha agotado.',
    CARD_ADDED: 'Tarjetas añadidas',
    PREVIEW: 'Vista previa'
  },
  MAIN: {
    GO_TO: 'Ir a la página web',
    ADVERTISEMENT: 'Publicidad',
    CARD: 'tarjetas'
  },
  MISC: {
    CHANGE_DOMAIN1: 'Dominios nuevos',
    CHANGE_DOMAIN2: 'Por favor, cambie el dominio',
    TIMEOUT: ' Tiempo de espera para revisión: <b>${t.m}</b> min. <b>${t.s}</b> seg.',
    HOW_UP_TIMEOUT: '¿Cómo puedo aumentar el tiempo de espera?',
    STEP2STEP: {
      ICHECK: 'He pulsado el botón “Verificar”.',
      IWAIT: 'He esperado a que al menos una confirmación de la recarga de Bitcoin. ',
      IWRITE: 'He rellenado el comentario correctamente.'
    }
  },
  NEWS: {
    BACK_TO_LIST: 'Volver a la lista de las noticias',
    LIKE: 'Me gusta',
    DISLIKE: 'No me gusta',
    MARK_ALL: 'Marcar todo como “Leído”',
    TYPES: {
      OPINION: 'Su opinión',
      BASE: 'Nuevo en la base',
      SITE: 'Nuevo en el sitio web'
    }
  },
  ORDERS: {
    CHECKING: 'Tarjetas en la verificación',
    CHECKED: 'Tarjetas verificadas',
    UNCHECKED: 'Tarjetas no verificadas',
    TABLE: {
      ADD_TIME: 'Tiempo de la adición',
      COUNT: 'Cantidad total',
      VALID: 'Cantidad de tarjetas válidas',
      UNOPEN: 'Undiscovered',
      ORDER_COMMENT: 'Comentarios de la compra',
      PRICE: 'Precio, $'
    },
    BY_BIN: 'El país por el BIN'
  },
  PROFILE: {
    CHECK_SYS_LABEL: 'El sistema de verificación de tarjetas ',
    SELECT_PROVIDER: 'Escoja un proveedor',
    CSV_OUTPUT_LABEL: 'Parámetros del formato de salida de CSV',
    SELECT_CSV_FIELDS: 'Seleccione los campos necesarios para ver la compra en el formato CSV',
    AVAIL_FIELD: 'Campos disponibles',
    ACTIVE_FIELD: 'Campos activos',
    UP: 'Arriba',
    DOWN: 'Abajo',
    DELIMETER: 'Separador',
    CSV_LEGEND_TEXT: 'Seleccionado &nbsp;<b>${count}</b>/<b>${max_csv_field}</b> campos. Hace falta seleccionar <b>${max_csv_field}</b> campos para visualización.',
    CSV_OUTPUT_SHARED_LABEL: 'Parámetros del formato de salida de CSV (ampliados)',
    CSV_OUTPUT_SHARED_LEGEND: 'Introduzca no más que <b>${max_csv_field}</b> campos',
    STATISTIC: {
      TEXT: 'Estadística',
      WASTED_AMOUNT: 'Fondos gastados',
      BUYED: 'Tarjetas compradas',
      WASTED_BY_CARD: 'Fondos gastados en la compra de tarjetas ',
      CHECKED_COUNT: 'Cantidad de tarjetas verificadas',
      CHECK_AMOUNT: 'Fondos gastados en la verificaciones de tarjetas',
      RETURNED_COUNT: 'Tarjetas devueltas',
      RETURNED_AMOUNT: 'El importe de las restituciones para las tarjetas',
      RATING: 'Rating',
      STATUS: 'Status',
      SEARCH_LIMIT: 'El límite de búsqueda de tarjetas',
      CHECK_TIMEOUT: 'Tiempo de espera para revisión ',
      NOT_IN_RATING: 'Fuera de rating',
      M: 'min.',
      S: 'seg.'
    },
    SHOW_OPTS: {
      TEXT: ' Opciones de visualización',
      SHOW_INVALID: 'Mostrar tarjetas no válidas',
      SHOW_CONFIRM: 'Pedir confirmación',
      SHOW_ADV: 'Mostrar publicidad'
    },
    FAST_BUY: {
      TEXT: 'Compra rápida'
    },
    SEARCH: {
      TEXT: 'Opciones de visualización de búsqueda',
      COLUMNS_LEGEND: 'Seleccione los campos necesarios para ver la tabla de búsqueda',
      LEGEND: 'Seleccionado <b>${count}</b>/<b>16</b> campos. Hace falta seleccionar <b>16</b> campos para visualización.'
    },
    ITEMS_PER_PAGE: {
      TEXT: 'Cantidad de elementos en la página',
      SEARCH_TABLE: 'Tabla de la búsqueda'
    }
  },
  STAT: {
    ID: 'ID',
    DATE: 'Fecha',
    COUNT: 'Cantidad',
    VALID: 'Válido',
    LOAD: 'Las recargas',
    BASES: 'Bases',
    BASE: 'Base',
    SELLERS: 'Vendedores',
    SELLERSS: 'Vendedores',
    SUMM: 'Total',
    NAME: 'Nombre',
    SELLER: 'Vendedor',
    DYN: {
      TEXT: 'Válido dinámico',
      SELECT_BASE: 'Seleccione la base',
      QUICK_SEARCH: 'Búsqueda rápida...',
      SELECT_LOAD: 'Seleccione la recarga de la base',
      TIME_UNIT: {
        WEEK: 'Semana',
        MONTH: 'Mes',
        SEASON: 'Trimestre'
      },
      LEGEND: 'Seleccione no más que <b>${maxSelected}</b> recargas.'
    },
    STAT: {
      TEXT: 'Válido estatico',
      LOAD_TOOLTIP: 'La recarga desde ${time_formatted}, válido ${valid}%',
      BASE_TOOLTIP: 'Base ${name}, válido ${valid}%',
      SELLERS_TOOLTIP: 'Vendedor ${name}, válido ${valid}%',
      SHOW_ALL: 'Mostrar todo',
      SHOW_ALLS: 'Mostrar todos'
    }
  },
  TICKETS: {
    CREATE: 'Crear un Ticket',
    OPEN: 'Abierto',
    CLOSED: 'Cerrado',
    IN_PROGRESS: 'En progreso',
    YOU: 'Usted',
    SUBJECT: ' El tema del ticket',
    NEW_MSG_PLCHLDR: 'Escriba su mensaje...',
    SCHEDULE: 'El administrador está disponible <b>9:00</b> — <b>21:00</b> UTC+3<br>\
     El soporte técnico está disponible <b>0:00</b> — <b>24:00</b> UTC+3<br>\
     El tiempo de respuesta del soporte técnico no más que <b>10</b> minutos.'
  },
  LOGIN: {
    PASW_PLC: 'Contraseña',
    YOUR_PASW: 'Su contraseña',
    ATTENTION: '¡Guarde su contraseña an pronto como sea posible confiable! Si pierde la contraseña, ¡será IMPOSIBLE recuperar su cuenta!'
  },
  FOOTER: {
    NEED_HELP: '¿Necesita ayuda?',
    HELP: 'Ayudar',
    CHECK_SYS: {
      TEXT: 'Sistema de verificación'
    },
    SUPPORT: {
      TEXT: 'Soporte técnico'
    },
    STATUS: {
      WORK: 'Funciona',
      NOT_WORK: 'No funciona',
      PART: 'Parcialmente',
      HOLYDAY: 'Día libre'
    },
    UPDATE: 'Actualización de'
  },
  NAV: {
    MAIN: 'Inicio',
    CARDS: {
      TEXT: 'Tarjetas',
      SEARCH: 'Buscar',
      CART: 'Cesta',
      ORDERS: 'Compras',
      STAT: 'Estadísticas'
    },
    CHECK: {
      TEXT: 'Verificación',
      CHECK: 'Verificar',
      HISTORY: 'Historial'
    },
    BILLING: {
      TEXT: 'Finanzas',
      BALANCE: 'Recarga su cuenta',
      MAIN: 'Contabilidad'
    },
    NEWS: 'Noticias',
    TICKETS: 'Tickets',
    FAQ: 'Preguntas frecuentes (FAQ)',
    PROFILE: 'Perfil',
    REGISTER: 'Registrarse'
  },
  COM: {
    EMPTY: 'Vacío',
    _EMPTY_: '<Vacío>',
    SET_DEFAULT: ' Establecer como predeterminado',
    SORT: 'Ordenar por',
    SELECT: 'Seleccionar...',
    ADD_TO_CART: 'Añadir a la cesta',
    ADD_NOTE: 'Añadir una nota',
    MANAGERS: {
      OWNER: 'Propietario',
      ADMIN: 'Administrador',
      SUPPORT: 'Soporte técnico'
    },
    COUNT: 'pieza(s)',
    SETUP: 'Configurar',
    WRONG_TIME: ' En este momento los operadores no están disponibles. Podrá recibir una respuesta en conformidad con el horario de trabajo del soporte técnico.'
  },
  VALID: {
    LOW: 'Bajo'
  },
  LANG: {
    ERRORS: {
      ERROR: 'Error',
      AUTH_ERROR: 'Error de autorización',
      UNKNOW_ERROR: 'Error desconocido.',
      NOTHING_SELECT: 'Se ha seleccionado nada ',
      COMMON: 'No se puede realizar la operación. Por favor, intente de nuevo más tarde.',
      CARD_CHECK_TIMEOUT: 'El tiempo de verificación de la tarjeta ha caducado',
      INCORRECT_PASSWORD: '¡Contraseña incorrecta!',
      INCORRECT_REQUEST: 'Solisitud de la búsqueda incorrecta',
      SEARCH_SYSTEM_PROBLEM: 'Error de la búsqueda',
      BTC_INCORRECT_RESPONSE: 'Los datos del servidor BTC no son correctos',
      SEARCH_EXISTING: 'Este modelo de la búsqueda ya existe.',
      SEARCH_NAME_EMPTY: '¡Nombre de modelo no puede estar vacío!',
      404: 'Página no encontrada',
      500: 'Error del servidor',
      CHECK_CORRECT: 'Compruebe la exactitud de los datos',
      RESTRICTED_ACCESS: "No tiene acceso a esta sección",
      INVALID_PARAMS: 'Parámetros no validos',
      POOR_PARAMS: 'Parámetros no son suficientes',
      NOT_SPECIFIED_FORMAT: 'El formato no está especificado',
      EMPTY_CARDS_DATA: 'No hay datos de las tarjetas',
      FILL_AND_TRY: 'Introduzca el formato de entrada de datos y introduzca los datos de las tarjetas',
      MIN_DATA_FORMAT: 'Datos obligatorios mínimos de tarjeta: número de tarjeta, mes, año y CVV2.',
      SHORT_TEXT: ' Su mensaje es muy corto',
      EMPTY_VALUE: " El campo no puede estar vacío",
      SHORT_VALUES_LIST: 'Ha elegido muy pocos valores',

      INCORRECT_FORMAT: 'Formato es incorrecto',
      INCORRECT_GIFTCODE: 'Código es incorrecto',

      SAVED_SEARCH_LIMIT: 'El límite para guardar modelos de búsquedas se ha agotado ',

      GENERAL: {
        'GENERAL ERROR': 'Un error descnocido',
        'NO PARAM': 'Falta uno de los parámetros obligatorios',
        'BAD VALIDATION': 'Uno de los parámetros no puede pasar el proceso de validación',
        'ALREADY EXISTS': 'El objeto que desea crear ya existe ',
        'TIMEOUT': 'La cantidad de registros es limitado. Por favor, intente de nuevo más tarde. ',
        'NO ACCESS': 'No tiene acceso a estos datos',
        'UNKNOWN ACTION': 'La acción desconocida',
        'EMPTY PARAM': 'Uno de los parámetros obligatorios está vacío',
        'NOTIFY_CHECK_TIMEOUT': 'Error al actualizar los datos del servidor. Compruebe su conexión a la red'
      },

      PAYMENT: {
        'DUPLICATE RECORD': 'Error al crear eWallet. eWallet ya existe.',
        'BAD ADDRESS': 'Hay unas problemas en la interacción con Bitcoin. Por favor, intente  de nuevo  más tarde.',
        'CANT CONNECT (NEW_ADDRESS)': 'Hay unas problemas en la interacción con Bitcoin. Por favor, intente de nuevo  más tarde. ',
        'CANT CONNECT (RAWADDR)': 'Hay unas problemas en la interacción con Bitcoin. Por favor, intente de nuevo  más tarde.',
        'BAD RESPONSE (BLOCK)': 'Hay unas problemas en la interacción con Bitcoin. Por favor, intente de nuevo  más tarde.',
        'CANT CONNECT (BLOCK)': 'Hay unas problemas en la interacción con Bitcoin. Por favor, intente de nuevo  más tarde.',
        'EMPTY ADDRESS': 'Por favor, primero cree eWallet (monedero electrónico), y después inténtelo de nuevo.',
        'EMPTY AMOUNT AFTER FEE': 'Ha enviado una cantidad muy pequeña de dinero.',
        'EMPTY AMOUNT': 'Ha enviado una cantidad muy pequeña de dinero.',
        'CANT CONNECT (COURSE)': ' Hay unas problemas en la interacción con Bitcoin. Por favor, intente de nuevo  más tarde.',
        'PENDING': 'Hay una transacción de la recarga, pero tiene muy poco cantidad de confirmaciones.',
        'NO PAYMENT': 'No hay pagos nuevos para este eWallet.',
        'CANT CONNECT (WM)': 'Hay unas problemas en la interacción con Webmoney. Por favor, intente de nuevo  más tarde.',

        'CANT CONNECT (PM)': ' Hay unas problemas en la interacción con Perfect Money. Por favor, intente de nuevo  más tarde.',

        NOT_ENROLL: 'El pago no se ha transferido'
      },

      CARDS: {
        ALREADY_CHECKED: 'Los datos de la tarjeta han sido verificados.',
        ALREADY_BUYED: '¡Todas las tarjetas seleccionadas ya están compradas!',
        BALANCE_ISSUE: 'Hay unas problemas con el saldo!',
        NOTHING_BUYED: 'No se ha comrado nada!',
        IS_NO_ADDED: 'No hay tarjetas añadidas',
        OVER_LIMITED: 'El límite para añadir las tarjetas nuevos se ha agotado',
        SMALL_BALANCE: 'Fondos insuficientes',
        INCORRECT_DATA: 'Compruebe si ha introducido los datos correctamente',
        NEED_TO_OPEN_CARD: 'Antes de hacer una verificación pagada hace falta abrir las tarjetas'
      }
    },
    SERVICE: {
      MESSAGE: 'Mensaje',
      CASH_NUMBER_PRETEXT: 'Para realizar los pagos indique esta dirección',
      PAYMENT_SUCCESS: '¡Pago realizado con éxito!',
      EMPTY_TABLE: 'La lista está vacío',
      FAST_BUY: 'Compra rápida',

      DELETE_CARDS_CONFIRM: '¿Está seguro que desea eliminar las tarjetas?',
      DELETE_ORDERS_CONFIRM: '¿Está seguro que desea eliminar las compras?',


      CHECK_ORDER_CONFIRM: '¿Está seguro que desea poner la compra en verificación?',
      NOT_MONEY_BACK: 'No devolvemos el dinero para tarjetas no válidas',

      CARDS_BUYED: 'Tarjetas compradas: ',
      READ_FAQ: 'La respuesta a esta pregunta se puede encontrar en Preguntas Frecuentes (FAQ)',

      THANK4PURCHASE: '¡Gracias por su compra!',
      EDIT_TITLE: 'Editar el título',

      LOAD_SEARCH: 'Aplicar el modelo',
      ENTER_SEARCH_NAME: 'Introduzca el nombre del modelo',
      NOTE: 'Nota',
      EMPTY_NOTE: 'No hay notas',
      CARDS_CHECK: 'Verificación de las tarjetas',
      ATTENTION: 'Atención!',

      SEARCH_RESULT: 'Resultados de la búsqueda',
      EXTERNAL_SEARCH_RESULT: 'Resultados de la búsqueda en otras tiendas',
      TICKETS: 'Tickets',
      FAQ: 'Preguntas frecuentes',
      DELETE_CARDS_TITLE: 'Eliminación de las tarjetas',
      ORDERS_DELETE: 'Eliminación de las compras',
      ORDERS_CHECK: 'Verificación de las compras',
      LOAD_BALANCE: 'Recarga su cuenta',
      LOAD_OVER: 'Recarga su cuenta por ',
      PROFILE: 'Perfil',
      NEWS: 'Noticias',

      CREATE_PURSE: 'Crear eWallet (monedero electrónico)',

      PROCESSING_QUERY: 'Su petición se está procesando',

      OPERATION_STATUS: 'Estado de la operación',

      REPEAT_OVER: 'Inténtelo de nuevo dentro de',

      BACK_TO_ORDERS: 'Volver a la lista de las compras',
      BACK_TO_TICKETS: 'Volver a la lista de tickets',

      ORDER_SHOW: 'Ver su compra',
      CSV_VIEW: 'CSV',
      CSV_FORMAT: 'Formato CSV',
      NEW_TICKET: 'Ticket nuevo',

      CSV_CHANGE_SUCCESS: "El formato CSV ha sido modificado con éxito.",

      NULL_CARDS: 'Se han abierto 0 tarjetas',

      CHECKING: 'En proceso de verificación',
      VALID: 'Válida',
      INVALID: 'No válida',
      UNCHECKED: 'No ha sido verificada',
      NOCASHBACK: 'Sin devolución de dinero',

      CHECK_SYSTEM: 'Sistema de verificación',
      CHECK_SYSTEM_CHANGE_SUCCESS: 'El cambio del sistema de verificación se ha realizado con éxito. Su nuevo sistema de verificación es: ',

      VIEW_OPTIONS: 'Opciones de visualización',
      VIEW_OPTIONS_CHANGE_SUCCESS: 'El cambio de las opciones de visualización se ha realizado con éxito.',

      SEARCH_OPTIONS: 'Visualización de búsqueda',
      SEARCH_OPTIONS_CHANGE_SUCCESS: 'El cambio de la visualización de búsqueda se ha realizado con éxito.',

      MONTH: ['enero', 'feb.', 'marzo', 'abr.', 'mayo', 'jun.', 'jul.', 'agosto', 'sept.', 'oct.', 'nov.', 'dic.'],
      DELETED_CARDS: 'Cantidad de tarjetas borradas: ',
      CARD_DELETED: 'Las tarjetas han sido borradas',
      EMPTY_TPL: 'La lista de modelos está vació',
      TPL_SUCCESS: 'El modelo se ha guardado con éxito',
      TPL_ALREADY_EXIST: 'Este modelo ya existe',
      LOAD_TPL: 'Aplicar el modelo',
      EMPTY_SELECTION: 'No hay tarjetas seleccionadas',
      TRANSACT: 'Transacción №',

      INTYPES: {
        BTC: 'Bitcoin',
        FAKE: 'Interno',
        WM: 'Webmoney',
        PM: 'PerfectMoney',
        UNDEFINED: 'Desconocido'
      },

      PROFILE_STATUS: {
        '0': 'Anónimo',
        '1': 'De confianza',
        '2': 'Verificado'
      },

      ALL_BASE: 'Todas las bases',
      GIFTCODE_SUCCESS: 'El código de regalo ha sido utilizado con éxito!',
      GIFTCODE_HEADER: 'Sus códigos de regalo',
      GIFTCODE_REDEEM: 'Utilizar el código de regalo',
      SEARCH_SAVED: '¡La búsqueda se ha guardada con éxito!',
      TICKET_DELETE_CONFIRM: 'Se van a borrar los tickets cerrados y leídos. ¿Desea continuar esta operación?',
      NO_TICKETS_DELETED: 'No hay ningún ticket cerrado o leído',

      ORDERS_LIST: 'Las compras',
      BACK_TO_SEARCH: 'Volver a la búsqueda de tarjetas',
      CART: 'Cesta',
      BILLING: 'Contabilidad',
      LOW: 'Bajo',
      COUNT: 'pieza(s)',
      ADD_TO_CHECK: 'Añadir tarjetas para verificación',
      CHECK_RESULT: 'Resultados de la verificación de tarjetas',
      EMPTY: 'Vacío',
      CREATE_GIFT_CODE: 'Crear un código de regalo',
      STAT_HEADER: 'Estadísticas',
      QUANT: 'Cantidad',
      VALID_COUNT: 'Válido',
      BUYED: 'Tarjetas compradas',
      CLOSED: 'Cerrados',
      CHECKED: 'Verificados',
      M: 'min.',
      S: 'seg.',
      H: 'h.',
      M_AGO: 'min. atrás',
      S_AGO: 'seg. atrás',
      H_AGO: 'h. atrás',
      PARAGRAPH: '§',
      ADV: 'Publicidad',
      checkAllText: 'Seleccionar todo',
      uncheckAllText: 'Deseleccionar todo',
      QUICK_SEARCH: 'Búsqueda rápida...'
    },
    TABLE: {
      COLS: {
        ID: 'ID',
        INDEX: 'Número',
        NUMBER: 'Numero de tarjeta',
        EXP: 'Exp',
        HOLDER: 'Titular',
        LEVEL: 'Nivel',
        TYPE: 'Tipo',
        BANK: 'Banco',
        ZIP: 'ZIP',
        ADDRESS: 'Dirección',
        CITY: 'Ciudad',
        STATE_CODE: 'Estado',
        COUNTRY_CODE: 'País',
        EMAIL: 'Email',
        PHONE: 'Teléfono',
        LOAD_VALID: 'Válido, %',
        SELLER: 'Vendedor',
        BASE: 'Base',
        ADD_TIME: 'Añadido',
        SELLER_PRICE: 'Precio, $',
        'CVV2': 'CVV2',
        'M': 'Mes',
        'A': 'Año',
        DATE: 'Fecha de verificación',
        PRICE_ITEM: 'Precio',
        PROVIDER: 'Proveedor',
        STATUS: 'Status',
        RESPONSE: 'Código de la respuesta',
        COMMENT: 'Comentario',
        THEME: 'Tema',
        OPENED: 'Abierto',
        FROM: 'De',
        TO: 'Para quien',
        CARDS_ANOTHER: 'Tarjetas (de terceros)'
      },
      lengthMenu: 'Mostrar _MENU_',
      CARDS_sInfo: 'Tarjetas encontradas <strong>_TOTAL_</strong>, mostrado de _START_ a _END_',
      ORDERS_sInfo: "Pedidos encontrados <strong>_TOTAL_</strong>, mostrado de _START_ a _END_",
      TRANS_sInfo: 'Transacciones encontradas <strong>_TOTAL_</strong>, mostrado de _START_ a _END_',
      TICKETS_sInfo: 'Tickets encontrados<strong>_TOTAL_</strong>, mostrado de _START_ a _END_',
      ELEMENTS: ' elementos' //'30 elementos', '50 elementos', '100 elementos'
    },
    BUTTONS: {
      CANCEL: 'Cancelar',
      DELETE: 'Borrar',
      SAVE: 'Guardar',
      APPLY: 'Aplicar',
      CREATE: 'Crear',
      SELECT_ALL: 'Seleccionar todo',
      CHECK: 'Verificar',
      LOAD: 'Recargar saldo',
      REDEEM: 'Aprovechar',
      SEE: 'Ver',
      BUY: 'Comprar',
      OPEN: 'Abrir',
      CHECK_CARDS: 'Verificar las tarjetas',
      SHOW_CSV: 'Ver en formato CSV',
      EDIT_COMMENT: 'Editar el comentario',
      PAY_CHECK: 'Verificación pagada',
      DOWNLOAD_ALL: 'Descargar todo',
      DOWNLOAD: 'Descargar',
      CLEAR: 'Limpiar',
      SEND: 'Enviar',
      ENTER: 'Entrar',
      SAVE_SEARCH: 'Guardar el modelo',
      LOAD_SEARCH: 'Aplicar el modelo',
      RESET_SEARCH: 'Borrar el formulario',
      SEARCH_CARDS: 'Encontrar',
      AGREE: 'Acepto',
      SHOW: 'Mostrar',
      MORE: 'Mostrar más'
    }
  },
  SELLER: {
    NAV: {
      STAT: 'Estadística',
      DISBURSEMENT: 'Desembolso',
      PRICING: 'Editor de precio',
      EXIT: 'Cerrar sesión'
    },
    SELLS: {
      HEAD: 'Estadísticas y desembolso',
      NAME: 'Nombre',
      PERCENTAGE: 'Porcentaje',
      LOADED: 'Cargado',
      TURNOVER: 'Volumen de negocios',
      INCORRECT: 'Incorrecto',
      INVALID: 'Inválido',
      SOLD: 'Vendido',
      VALID: 'Válido, %',
      REFUND: 'Reembolso',
      REFUND_CHECK: 'Reembolso (cheque)',
      PROFIT: 'Lucro',
      WITHDRAW: 'Retirada',
      BALANCE: 'Equilibrar'
    },
    PAYOUT: {
      HEAD: 'Los pagos',
      SUM: 'Suma',
      WALLET: 'Billetera',
      DATE: 'Fecha',
      STATUS: 'Estado',
      STATUS_PROCESSING: 'Tratamiento',
      STATUS_PROCESSED: 'Procesada',
      STATUS_REJECTED: 'Rechazado'
    },
    PRICING: {
      HEAD: 'Editor de precio',
      ADD: 'Añadir',
      UPDATE: 'Actualización',
      CURRENT: 'Corriente',
      SEARCH: 'Países de la búsqueda',
      PRICE: 'Anote el precio por ejemplo 4.99'
    }
  }
}
