/* eslint-disable */

module.exports = {
  LOCALE: 'ru',
  BILLING: {
    PM: {
      LOAD_TEXT: 'Комиссия пополнения <strong>0%</strong>.\
      Прием платежей производится без ограничений.'
    },
    GIFT: {
      LOAD_TEXT: 'Подарочные коды создаются и погашаются без ограничений.',
      PLCHLDR: 'Введите сумму, например 10.88',
      ENTER_CODE: 'Введите ваш подарочный код',
      READY: '<span>Ваш подарочный код на сумму <b>$${amount}</b> готов!</span>'
    },
    WM: {
      LOAD_TEXT: 'Комиссия пополнения <b>4%</b>. Прием платежей производится без ограничений.'
    },
    BTC: {
      LOAD_TEXT: 'Комиссия пополнения <b>3%</b>.\
      Необходимо <b>1</b> подтверждение транзакции в системе Bitcoin для зачисления средств на ваш счет.',
      CREATE_WALLET: 'Ваш адрес в системе Bitcoin еще не создан, нажмите на кнопку "Создать кошелек".'
    },
    TABLE: {
      COLS: {
        INDEX: 'Номер',
        DIR: 'Направление',
        TIME: 'Время транзакции',
        CMNT: 'Описание',
        AMOUNT: 'Сумма транзакции, $',
        BALANCE: 'Баланс, $',
        COUNT: 'Количество',
        ORDER_ID: 'ID покупки'
      },
      CLEAR: 'Очистить таблицу'
    },
    LOAD_UNAV: 'Данный вид пополнения недоступен.',
    IF_TROUBLE: 'В случае, если ваш платеж не был зачислен',
    NEW_TICKET: 'Сформировать тикет',
    WALLET: 'Кошелек',
    COMMENT: 'Комментарий',
    PRESS_CHECK: 'Нажмите на кнопку "Проверить" после совершения платежа.',
    NO_PROTECT: 'Платежи с протекцией не зачисляются.',
    POINT_ADDRESS: 'Для совершения платежей указывайте этот адрес:',
    WAIT: 'Подождите...',
    TYPE: 'Тип',
    AMOUNT: 'Сумма',
    COURSE: 'Курс',
    BATCH: 'Батч',
    CHECK: 'Проверить'
  },
  SEARCH: {
    PAGEINFO: 'Найдено карт <strong>${count}</strong>, ваш лимит <strong>${limit}</strong>',
    SELECT_SEARCH: 'Выберите поиск из списка сохраненных',
    NOT_FLAG: 'Все кроме отмеченных',
    TEXT_INPUT: 'Текстовый ввод',
    ZIP_MASK: 'По маске',
    ZIP_MASK_LEGEND: '<strong>*</strong> &minus; 0 и более любых символов',
    ZIP_MASK_LEGEND1: '<strong>.</strong> &minus; 1 любой символ',
    FAST_BUY: {
      CARD_SELECTED: 'Выбрано карт',
      TOTAL_COAST: 'Общая стоимость',
      AFTER_BUY: 'Сразу после покупки',
      SET_CHECK: 'Поставить на проверку',
      GO_ORDER: 'Перейти в покупку',
      SHOW_CVS: 'Показать в CVS',
      FAST: 'Быстрая'
    },
    ASIDE: {
      BIN: 'BIN',
      BRAND: 'Бренд',
      TYPE: 'Тип',
      LEVEL: 'Уровень',
      BANK: 'Банк',
      COUNTRY: 'Страна',
      STATE: 'Штат',
      CITY: 'Город',
      CITY_PLC: 'Введите город',
      SEARCH: 'Поиск',
      ZIP: 'ZIP',
      ZIP_PLC1: 'Почтовый индекс',
      ZIP_PLC2: 'Введите ZIP',
      ZIP_PLC3: 'Введите маску поиска для ZIP',
      ADDRESS: 'Адрес',
      SELLER: 'Продавец',
      BASE: 'База',
      LOAD: 'Пополнение',
      PERIOD: 'Период действия',
      CRITERIA: 'Критерии поиска'
    }
  },
  CART: {
    SUMMARY: 'Выбрано карт: <strong>${count}</strong> на сумму: <strong>${summ}</strong>'
  },
  CHECK: {
    FORMAT_LABEL: 'Формат ввода',
    FORMAT_PLCHLDR: 'Например %num%|%m%|%y%|%cvv2%',
    FORMAT_TOOLTIP: 'Введите формат корректно, формат должен содержать набор тегов, сами данные помещайте в форме ввода. Минимальные обязательные данные карты: номер карты, месяц, год и CVV2.',
    SAVE_TPL_LABEL: 'Сохранить формат',
    LOAD_TPL_LABEL: 'Загрузить формат',
    SELECT_TPL_LABEL: 'Выберите формат из списка сохраненных',
    LEGEND_LABEL: '<b>%num%</b> &mdash; номер карты, <b>%m%</b> &mdash; месяц, <b>%y%</b> &mdash; год,\
    <b>%cvv2%</b> &mdash; CVV2, <b>%zip%</b> &mdash; zip, <b>%addr%</b> &mdash; адрес',
    FORM_LABEL: 'Форма ввода данных карт',
    HINT_LABEL: 'Подсказка',
    AREA_PLCHLDR: 'Например 444444444444444|12|2015|333',
    INSTRUCT: 'Вставьте карты согласно выбранному формату и нажмите на кнопку "Предпросмотр", в нижнем блоке вы увидите распознанные данные. Если в введенных данных были ошибки они будут подсвечены красным цветом. Удалите карты из истории если у вас закончился лимит.',
    CARD_ADDED: 'Добавлено карт',
    PREVIEW: 'Предпросмотр'
  },
  MAIN: {
    GO_TO: 'Перейти на сайт',
    ADVERTISEMENT: 'Реклама',
    CARD: 'карт'
  },
  MISC: {
    CHANGE_DOMAIN1: 'Новые домены',
    CHANGE_DOMAIN2: 'Пожалуйста, смените домен',
    TIMEOUT: 'Таймаут на проверку: <b>${t.m}</b> мин. <b>${t.s}</b> сек.',
    HOW_UP_TIMEOUT: 'Как увеличить таймаут?',
    STEP2STEP: {
      ICHECK: 'Я нажал на кнопку "Проверить".',
      IWAIT: 'Я дождался 1 подтверждения при пополнении Bitcoin.',
      IWRITE: 'Я правильно заполнил комментарий.'
    }
  },
  NEWS: {
    BACK_TO_LIST: 'Вернуться к списку новостей',
    LIKE: 'Нравится',
    DISLIKE: 'Не нравится',
    MARK_ALL: 'Отметить все как «Прочитанные»',
    TYPES: {
      OPINION: 'Ваше мнение',
      BASE: 'Новое в базе',
      SITE: 'Новое на сайте'
    }
  },
  ORDERS: {
    CHECKING: 'Проверяется карт',
    CHECKED: 'Проверено карт',
    UNCHECKED: 'Не проверено карт',
    TABLE: {
      ADD_TIME: 'Время добавления',
      COUNT: 'Всего',
      VALID: 'Валидных',
      UNOPEN: 'Неоткрытых',
      ORDER_COMMENT: 'Примечание для покупки',
      PRICE: 'Цена, $'
    },
    BY_BIN: 'Cтрана по BIN'
  },
  PROFILE: {
    CHECK_SYS_LABEL: 'Система проверки карт',
    SELECT_PROVIDER: 'Выберите провайдера',
    CSV_OUTPUT_LABEL: 'Параметры вывода CSV',
    SELECT_CSV_FIELDS: 'Выберите необходимые поля для просмотра покупки в CSV-виде',
    AVAIL_FIELD: 'Доступные поля',
    ACTIVE_FIELD: 'Активные поля',
    UP: 'Вверх',
    DOWN: 'Вниз',
    DELIMETER: 'Разделитель',
    CSV_LEGEND_TEXT: 'Выбрано&nbsp;<b>${count}</b>/<b>${max_csv_field}</b> полей. Необходимо выбрать <b>${max_csv_field}</b> полей для отображения.',
    CSV_OUTPUT_SHARED_LABEL: 'Параметры вывода CSV (расширенные)',
    CSV_OUTPUT_SHARED_LEGEND: 'Введите не более <b>${max_csv_field}</b> полей',
    STATISTIC: {
      TEXT: 'Статистика',
      WASTED_AMOUNT: 'Потрачено средств',
      BUYED: 'Куплено карт',
      WASTED_BY_CARD: 'Потрачено на карты',
      CHECKED_COUNT: 'Количество проверенных карт',
      CHECK_AMOUNT: 'Проверок выполнено на сумму',
      RETURNED_COUNT: 'Возвращенно карт',
      RETURNED_AMOUNT: 'Сумма возвратов за карты',
      RATING: 'Рейтинг',
      STATUS: 'Статус',
      SEARCH_LIMIT: 'Лимит поиска, карт',
      CHECK_TIMEOUT: 'Таймаут на проверку',
      NOT_IN_RATING: 'Вне рейтинга',
      M: 'мин.',
      S: 'сек.'
    },
    SHOW_OPTS: {
      TEXT: 'Опции отображения',
      SHOW_INVALID: 'Показывать невалидные карты',
      SHOW_CONFIRM: 'Запрашивать подтверждение',
      SHOW_ADV: 'Показывать рекламу'
    },
    FAST_BUY: {
      TEXT: 'Быстрая покупка'
    },
    SEARCH: {
      TEXT: 'Параметры отображения поиска',
      COLUMNS_LEGEND: 'Выберите необходимые поля для просмотра таблицы поиска',
      LEGEND: 'Выбрано <b>${count}</b>/<b>16</b> полей. Необходимо выбрать <b>16</b> полей для отображения.'
    },
    ITEMS_PER_PAGE: {
      TEXT: 'Количество элементов на странице',
      SEARCH_TABLE: 'Таблица поиска'
    }
  },
  STAT: {
    ID: 'ID',
    DATE: 'Дата',
    COUNT: 'Количество',
    VALID: 'Валид',
    LOAD: 'Пополнения',
    BASES: 'Базы',
    BASE: 'База',
    SELLERS: 'Продавцы',
    SELLERSS: 'Продавцов',
    SUMM: 'Всего',
    NAME: 'Имя',
    SELLER: 'Продавец',
    DYN: {
      TEXT: 'Динамический валид',
      SELECT_BASE: 'Выберите базу',
      QUICK_SEARCH: 'Быстрый поиск...',
      SELECT_LOAD: 'Выберите пополнение',
      TIME_UNIT: {
        WEEK: 'Неделя',
        MONTH: 'Месяц',
        SEASON: 'Квартал'
      },
      LEGEND: 'Выберите не более <b>${maxSelected}</b> интересующих вас пополнений.'
    },
    STAT: {
      TEXT: 'Статистический валид',
      LOAD_TOOLTIP: 'Пополнение от ${time_formatted}, валид ${valid}%',
      BASE_TOOLTIP: 'База ${name}, валид ${valid}%',
      SELLERS_TOOLTIP: 'Продавец ${name}, валид ${valid}%',
      SHOW_ALL: 'Показать все',
      SHOW_ALLS: 'Показать всех'
    }
  },
  TICKETS: {
    CREATE: 'Создать тикет',
    OPEN:'Открыт',
    CLOSED:'Закрыт',
    IN_PROGRESS: 'В работе',
    YOU: 'Вы',
    SUBJECT: 'Тема тикета',
    NEW_MSG_PLCHLDR: 'Введите текст вашего сообщения...',
    SCHEDULE:  'Управляющий <b>9:00</b> — <b>21:00</b> UTC+3<br>\
      Техподдержка <b>0:00</b> — <b>24:00</b> UTC+3<br>\
      Время реагирования службы поддержки не более <b>10</b> минут.'
  },
  LOGIN: {
    PASW_PLC: 'Пароль',
    YOUR_PASW: 'Ваш пароль',
    ATTENTION: 'Храните ваш пароль как можно надежнее! В случае его утери, восстановить аккаунт будет НЕВОЗМОЖНО!'
  },
  FOOTER: {
    NEED_HELP: 'Нужна помощь?',
    HELP: 'Помощь',
    CHECK_SYS: {
      TEXT: 'Система проверки'
    },
    SUPPORT: {
      TEXT: 'Техподдержка'
    },
    STATUS: {
      WORK: 'Работает',
      NOT_WORK: 'Не работает',
      PART: 'Частично',
      HOLYDAY: 'Выходной'
    },
    UPDATE: 'Обновление от'
  },
  NAV: {
    MAIN: 'Главная',
    CARDS: {
      TEXT: 'Карты',
      SEARCH: 'Поиск',
      CART: 'Корзина',
      ORDERS: 'Покупки',
      STAT: 'Статистика'
    },
    CHECK: {
      TEXT: 'Проверка',
      CHECK: 'Проверить',
      HISTORY: 'История'
    },
    BILLING: {
      TEXT: 'Финансы',
      BALANCE: 'Пополнить баланс',
      MAIN: 'Бухгалтерия'
    },
    NEWS: 'Новости',
    TICKETS: 'Тикеты',
    FAQ: 'FAQ',
    PROFILE: 'Профиль',
    REGISTER: 'Регистрация'
  },
  COM: {
    EMPTY: 'Пусто',
    _EMPTY_: '<Пусто>',
    SET_DEFAULT: 'Установить по умолчанию',
    SORT: 'Сортировка',
    SELECT: 'Выбрать...',
    ADD_TO_CART: 'В корзину',
    ADD_NOTE: 'Добавить примечание',
    MANAGERS: {
      OWNER: 'Владелец',
      ADMIN: 'Управляющий',
      SUPPORT: 'Техподдержка'
    },
    COUNT: 'шт.',
    SETUP: 'настроить',
    WRONG_TIME: 'В данный момент операторы недоступны, вы сможете получить ответ согласно графику работы техподдержки.'
  },
  VALID: {
    LOW: 'Низкий'
  },
  LANG: {
    ERRORS : {
      ERROR                 : 'Ошибка',
      AUTH_ERROR            : 'Ошибка авторизации',
      UNKNOW_ERROR          : 'Неизвестная ошибка.',
      NOTHING_SELECT        : 'Ничего не выбрано',
      COMMON                : 'Не удалось выполнить операцию, пожалуйста попробуйте повторить запрос позже',
      CARD_CHECK_TIMEOUT    : 'Время для проверки карты истекло',
      INCORRECT_PASSWORD    : 'Неверный пароль!',
      INCORRECT_REQUEST: 'Некорректный запрос',
      SEARCH_SYSTEM_PROBLEM : 'Не удалось инициализировать систему поиска',
      BTC_INCORRECT_RESPONSE: 'Некорректные данные от сервера BTC',
      SEARCH_EXISTING       : 'Данный поиск уже существует.',
      SEARCH_NAME_EMPTY     : 'Имя поиска не может быть пустым!',
      404                   : 'Страница не найдена',
      500                   : 'Ошибка сервера',
      CHECK_CORRECT         : 'Проверьте корректность данных',
      RESTRICTED_ACCESS     : "У вас нет доступа к этому разделу",
      INVALID_PARAMS        : 'Недопустимые параметры',
      POOR_PARAMS           : 'Недостаточно параметров',
      NOT_SPECIFIED_FORMAT  : 'Не задан формат',
      EMPTY_CARDS_DATA      : 'Не заданы карты',
      FILL_AND_TRY          : 'Заполните формат ввода и данные карт',
      MIN_DATA_FORMAT       : 'Минимальные обязательные данные карты: номер карты, месяц, год и CVV2.',
      SHORT_TEXT            : 'Слишком короткое сообщение',
      EMPTY_VALUE           : "Значение не может быть пустым",
      SHORT_VALUES_LIST     : 'Слишком мало значений выбрано',

      INCORRECT_FORMAT: 'Неверный формат',
      INCORRECT_GIFTCODE: 'Неверный код',

      SAVED_SEARCH_LIMIT: 'Превышен лимит сохранения поисков',

      GENERAL: {
        'GENERAL ERROR'       : 'необработанная ошибка',
        'NO PARAM'            : 'нет одного из обязательных параметров',
        'BAD VALIDATION'      : 'какой-то из параметров не может пройти валидацию',
        'ALREADY EXISTS'      : 'объект, который вы хотите создать, уже существует',
        'TIMEOUT'             : 'Количество регистраций ограничено, повторите попытку чуть позже.',
        'NO ACCESS'           : 'у вас нет доступа к этим данным',
        'UNKNOWN ACTION'      : 'система не может понять, какое действие вы хотите совершить',
        'EMPTY PARAM'         : 'один из обязательных параметров пустой',
        'NOTIFY_CHECK_TIMEOUT': 'Не удалось обновить данные с сервера. Проверьте ваше сетевое подключение'
      },

      PAYMENT: {
        'DUPLICATE RECORD'          : 'Ошибка при создании кошелька, кошелек уже существует.',
        'BAD ADDRESS'               : 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'CANT CONNECT (NEW_ADDRESS)': 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'CANT CONNECT (RAWADDR)'    : 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'BAD RESPONSE (BLOCK)'      : 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'CANT CONNECT (BLOCK)'      : 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'EMPTY ADDRESS'             : 'Пожалуйста, сначала создайте кошелек, а затем повторите попытку.',
        'EMPTY AMOUNT AFTER FEE'    : 'Вы отправили слишком малую сумму.',
        'EMPTY AMOUNT'              : 'Вы отправили слишком малую сумму.',
        'CANT CONNECT (COURSE)'     : 'Проблемы при взаимодействии с Bitcoin. Повторите попытку позже.',
        'PENDING'                   : 'Транзакция на пополнение есть, но малое число подтверждений.',
        'NO PAYMENT'                : 'Нет неучтенных платежей по данному кошельку.',
        'CANT CONNECT (WM)'         : 'Проблемы при взаимодействии с Webmoney. Повторите попытку позже.',

        'CANT CONNECT (PM)': 'Проблемы при взаимодействии с Perfect Money. Повторите попытку позже.',

        NOT_ENROLL: 'Платеж не был зачислен'
      },

      CARDS: {
        ALREADY_CHECKED: 'Данные карты уже прошли проверку.',
        ALREADY_BUYED  : 'Все выбранные вами карты уже куплены!',
        BALANCE_ISSUE  : 'Проблемы с балансом!',
        NOTHING_BUYED  : 'Ничего не куплено!',
        IS_NO_ADDED    : 'Карты не добавлены',
        OVER_LIMITED   : 'Превышен лимит на добавление новых карт',
        SMALL_BALANCE  : 'Недостаточно средств',
        INCORRECT_DATA : 'Проверьте правильность введенных данных',
        NEED_TO_OPEN_CARD: 'Перед платной проверкой необходимо открыть карты'
      }
    },
    SERVICE: {
      MESSAGE            : 'Сообщение',
      CASH_NUMBER_PRETEXT: 'Для совершения платежей указывайте этот адрес',
      PAYMENT_SUCCESS    : 'Платеж успешно выполнен!',
      EMPTY_TABLE        : 'Список пуст',
      FAST_BUY           : 'Быстрая покупка',

      DELETE_CARDS_CONFIRM : 'Вы уверены, что хотите удалить карты?',
      DELETE_ORDERS_CONFIRM: 'Вы уверены, что хотите удалить покупки?',

      CHECK_ORDER_CONFIRM : 'Вы уверены, что хотите поставить покупку на проверку?',
      NOT_MONEY_BACK      : 'Мы не возвращаем деньги за невалидные карты',

      CARDS_BUYED: 'Куплено карт: ',
      READ_FAQ   : 'Ответ на этот вопрос вы найдете в FAQ',

      THANK4PURCHASE: 'Спасибо за покупку!',
      EDIT_TITLE    : 'Редактировать название',

      LOAD_SEARCH      : 'Загрузить поиск',
      ENTER_SEARCH_NAME: 'Введите название',
      NOTE             : 'Примечание',
      EMPTY_NOTE       : 'Примечание отсутствует',
      CARDS_CHECK      : 'Проверка карт',
      ATTENTION        : 'Предупреждение',

      SEARCH_RESULT     : 'Результаты поиска',
      EXTERNAL_SEARCH_RESULT: 'Результаты стороннего поиска',
      TICKETS           : 'Тикеты',
      FAQ               : 'Часто задаваемые вопросы',
      DELETE_CARDS_TITLE: 'Удаление карт',
      ORDERS_DELETE     : 'Удаление покупок',
      ORDERS_CHECK      : 'Проверка покупок',
      LOAD_BALANCE      : 'Пополнить баланс',
      LOAD_OVER         : 'Пополнение через ',
      PROFILE           : 'Профиль',
      NEWS              : 'Новости',

      CREATE_PURSE: 'Создать кошелек',

      PROCESSING_QUERY: 'Ваш запрос обрабатывается',

      OPERATION_STATUS: 'Статус операции',

      REPEAT_OVER: 'Попробуйте повторить попытку через ',

      BACK_TO_ORDERS : 'Вернуться к списку покупок',
      BACK_TO_TICKETS: 'Вернуться к списку тикетов',

      ORDER_SHOW: 'Просмотр покупки',
      CSV_VIEW  : 'CSV-вид',
      CSV_FORMAT: 'CSV-формат',
      NEW_TICKET: 'Новый тикет',

      CSV_CHANGE_SUCCESS: "CSV-формат успешно изменен.",

      NULL_CARDS: 'Открылось 0 карт',

      CHECKING  : 'Проверяется',
      VALID     : 'Валидна',
      INVALID   : 'Не валидна',
      UNCHECKED : 'Не проверена',
      NOCASHBACK: 'Невозвратная',

      CHECK_SYSTEM               : 'Система проверки',
      CHECK_SYSTEM_CHANGE_SUCCESS: 'Смена системы проверки прошла успешно. Ваш новая система проверки: ',

      VIEW_OPTIONS               : 'Опции отображения',
      VIEW_OPTIONS_CHANGE_SUCCESS: 'Опции отображения успешно изменены.',

      SEARCH_OPTIONS               : 'Отображение поиска',
      SEARCH_OPTIONS_CHANGE_SUCCESS: 'Отображение поиска успешно изменено',

      MONTH            : ['янв.', 'фев.', 'мар.', 'апр.', 'мая', 'июн.', 'июл.', 'авг.', 'сен.', 'окт.', 'ноя.', 'дек.'],
      DELETED_CARDS    : 'Количество удаленных карт: ',
      CARD_DELETED     : 'Карты удалены',
      EMPTY_TPL        : 'Список форматов пуст',
      TPL_SUCCESS      : 'Формат успешно сохранен',
      TPL_ALREADY_EXIST: 'Данный формат уже существует',
      LOAD_TPL         : 'Загрузить формат',
      EMPTY_SELECTION  : 'Карты не выбраны',
      TRANSACT         : 'Транзакция №',

      INTYPES: {
        BTC      : 'Bitcoin',
        FAKE     : 'Внутренний',
        WM       : 'Webmoney',
        PM       : 'PerfectMoney',
        UNDEFINED: 'Неизвестно'
      },

      PROFILE_STATUS: {
        '0': 'Аноним',
        '1': 'Доверенный',
        '2': 'Проверенный'
      },

      ALL_BASE: 'Все базы',
      GIFTCODE_SUCCESS: 'Код успешно погашен!',
      GIFTCODE_HEADER: 'Ваши подарочные коды',
      GIFTCODE_REDEEM: 'Погасить подарочный код',
      SEARCH_SAVED: 'Поиск успешно сохранен!',
      TICKET_DELETE_CONFIRM: 'Будут удалены закрытые и прочитанные тикеты. Вы уверены?',
      NO_TICKETS_DELETED: 'Нет ни одного закрытого и прочитанного тикета',

      ORDERS_LIST: 'Покупки',
      BACK_TO_SEARCH: 'Вернуться к поиску карт',
      CART: 'Корзина',
      BILLING: 'Бухгалтерия',
      LOW: 'Низкий',
      COUNT: 'шт.',
      ADD_TO_CHECK: 'Добавить карты на проверку',
      CHECK_RESULT: 'Результаты проверки карт',
      EMPTY: 'Пусто',
      CREATE_GIFT_CODE: 'Создать подарочный код',
      STAT_HEADER: 'Статистика',
      QUANT: 'Количество',
      VALID_COUNT: 'Валид',
      BUYED: 'Купленные',
      CLOSED: 'Закрытые',
      CHECKED: 'Проверенные',
      M: 'мин.',
      S: 'сек.',
      H: 'ч.',
      M_AGO: 'мин. назад',
      S_AGO: 'сек. назад',
      H_AGO: 'ч. назад',
      PARAGRAPH: '§',
      ADV: 'Реклама',
      checkAllText: 'Выбрать все',
      uncheckAllText: 'Снять все',
      QUICK_SEARCH: 'Быстрый поиск...',
    },
    TABLE: {
      COLS: {
        ID: 'ID',
        INDEX: 'Номер',
        NUMBER: 'Номер карты',
        EXP: 'Эксп',
        HOLDER: 'ФИО',
        LEVEL: 'Уровень',
        TYPE: 'Тип',
        BANK: 'Банк',
        ZIP: 'ЗИП',
        ADDRESS: 'Адрес',
        CITY: 'Город',
        STATE_CODE: 'Штат',
        COUNTRY_CODE: 'Страна',
        EMAIL: 'Email',
        PHONE: 'Телефон',
        LOAD_VALID: 'Валид, %',
        SELLER: 'Продавец',
        BASE: 'База',
        ADD_TIME: 'Добавлено',
        SELLER_PRICE: 'Цена, $',
        'CVV2': 'CVV2',
        'M': 'Месяц',
        'Y': 'Год',
        DATE: 'Время проверки',
        PRICE_ITEM: 'Цена',
        PROVIDER: 'Провайдер',
        STATUS: 'Статус',
        RESPONSE: 'Код ответа',
        COMMENT: 'Примечание',
        THEME: 'Тема',
        OPENED: 'Открыт',
        FROM: 'От кого',
        TO: 'Кому',
        CARDS_ANOTHER: 'Карты (сторонние)'
      },
      lengthMenu: 'Показывать _MENU_',
      CARDS_sInfo: 'Найдено карт <strong>_TOTAL_</strong>, показано с _START_ по _END_',
      ORDERS_sInfo: "Найдено ордеров <strong>_TOTAL_</strong>, показано с _START_ по _END_",
      TRANS_sInfo: 'Найдено транзакций <strong>_TOTAL_</strong>, показано с _START_ по _END_',
      TICKETS_sInfo: 'Найдено тикетов <strong>_TOTAL_</strong>, показано с _START_ по _END_',
      ELEMENTS: ' элементов' //'30 элементов', '50 элементов', '100 элементов'
    },
    BUTTONS: {
      CANCEL    : 'Отмена',
      DELETE    : 'Удалить',
      SAVE      : 'Сохранить',
      APPLY     : 'Применить',
      CREATE    : 'Создать',
      SELECT_ALL: 'Выделить все',
      CHECK     : 'Проверить',
      LOAD: 'Пополнить',
      REDEEM: 'Погасить',
      SEE: 'Посмотреть',
      BUY: 'Купить',
      OPEN: 'Открыть',
      CHECK_CARDS: 'Проверить карты',
      SHOW_CSV: 'Посмотреть в CSV',
      EDIT_COMMENT: 'Редактировать примечание',
      PAY_CHECK: 'Проверить платно',
      DOWNLOAD_ALL: 'Скачать все',
      DOWNLOAD: 'Скачать',
      CLEAR: 'Очистить',
      SEND: 'Отправить',
      ENTER:'Войти',
      SAVE_SEARCH: 'Сохранить поиск',
      LOAD_SEARCH: 'Загрузить поиск',
      RESET_SEARCH: 'Очистить форму',
      SEARCH_CARDS: 'Найти',
      AGREE: 'Я понимаю',
      SHOW: 'Показать',
      MORE: 'Показать ещё'
    }
  },
  SELLER: {
    NAV: {
      STAT: 'Статистика',
      DISBURSEMENT: 'Выплаты',
      PRICING: 'Редактор цен',
      EXIT: 'Выход'
    },
    SELLS: {
      HEAD: 'Статистика и выплаты',
      NAME: 'Имя',
      PERCENTAGE: 'Процент выплат',
      LOADED: 'Загружено',
      TURNOVER: 'В обороте',
      INCORRECT: 'Некорректных',
      INVALID: 'Невалидных',
      SOLD: 'Продано',
      VALID: 'Валид, %',
      REFUND: 'Возврат',
      REFUND_CHECK: 'Возврат (проверки)',
      PROFIT: 'Прибыль',
      WITHDRAW: 'Выведено',
      BALANCE: 'Баланс'
    },
    PAYOUT: {
      HEAD: 'Выплаты',
      SUM: 'Сумма',
      WALLET: 'Кошелек',
      DATE: 'Дата',
      STATUS: 'Статус',
      STATUS_PROCESSING: 'Обрабатывается',
      STATUS_PROCESSED: 'Обработан',
      STATUS_REJECTED: 'Отказано'
    },
    PRICING: {
      HEAD: 'Редактор цен',
      ADD: 'Добавить',
      UPDATE: 'Обновить',
      CURRENT: 'Текущие',
      SEARCH: 'Искать страны',
      PRICE: 'Введите цену, например 4.99'
    }
  }
}
