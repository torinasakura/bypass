/* eslint-disable */

module.exports = {
  LOCALE:  'en',
  BILLING: {
    PM:            {
      LOAD_TEXT: 'Balance top-up fee <strong>0%</strong>.\
      Payments are processed without restrictions.'
    },
    GIFT:          {
      LOAD_TEXT:  'Gift codes are created and redeemed without restrictions.',
      PLCHLDR:    'Enter the amount, e.g. 10.88',
      ENTER_CODE: 'Enter your gift code',
      READY:      'Your gift code for the amount of <b>$${amount}</b> is ready!'
    },
    WM:            {
      LOAD_TEXT: 'Balance top-up fee <b>4%</b>. Payments are processed without restrictions.'
    },
    BTC:           {
      LOAD_TEXT:     'Balance top-up fee <b>3%</b>.\
      You need to confirm <b>1</b> transaction in the Bitcoin system to deposit money to your account.',
      CREATE_WALLET: 'Your Bitcoin address has not been created yet, press the "Create wallet" button.'
    },
    TABLE:         {
      COLS:  {
        INDEX:    'Index',
        DIR:      'Direction',
        TIME:     'Add time',
        CMNT:     'Description',
        AMOUNT:   'Transaction amount, $',
        BALANCE:  'Balance, $',
        COUNT:    'Pcs.',
        ORDER_ID: 'Order ID'
      },
      CLEAR: 'Clear'
    },
    LOAD_UNAV:     'The requested top-up method is unavailable.',
    IF_TROUBLE:    'Is payment failed?',
    NEW_TICKET:    'Create new ticket',
    WALLET:        'Wallet',
    COMMENT:       'Comment',
    PRESS_CHECK:   'Press "Verify" after the payment is completed.',
    NO_PROTECT:    'Protected payments are not processed.',
    POINT_ADDRESS: 'To make a payment use the following address:',
    WAIT:          'Wait...',
    TYPE:          'Type',
    AMOUNT:        'Amount',
    COURSE:        'Course',
    BATCH:         'Batch',
    CHECK:         'Verify'
  },
  SEARCH:  {
    PAGEINFO:      'Cards found <strong>${count}</strong>, your limit <strong>${limit}</strong>',
    SELECT_SEARCH: 'Select a search from your saved list',
    NOT_FLAG:      'All except selected',
    TEXT_INPUT:    'Text input',
    ZIP_MASK: 'By mask',
    ZIP_MASK_LEGEND: '<strong>*</strong> &minus; zero and more characters',
    ZIP_MASK_LEGEND1: '<strong>.</strong> &minus; any signle character',
    FAST_BUY:      {
      CARD_SELECTED: 'Cards selected',
      TOTAL_COAST:   'Total cost',
      AFTER_BUY:     'After the purchase',
      SET_CHECK:     'Check',
      GO_ORDER:      'Go to order',
      SHOW_CVS:      'Show in CVS',
      FAST:          'Quick order'
    },
    ASIDE:         {
      BIN:      'BIN',
      BRAND:    'Brand',
      TYPE:     'Type',
      LEVEL:    'Level',
      BANK:     'Bank',
      COUNTRY:  'Country',
      STATE:    'State',
      CITY:     'City',
      CITY_PLC: 'Enter city name',
      SEARCH:   'Search',
      ZIP:      'ZIP Code',
      ZIP_PLC1: 'Zip Code',
      ZIP_PLC2: 'Enter ZIP Code',
      ZIP_PLC3: 'Enter ZIP Сode mask',
      ADDRESS:  'Address',
      SELLER:   'Seller',
      BASE:     'Base',
      LOAD:     'Load',
      PERIOD:   'Expiration date',
      CRITERIA: 'Search criteria'
    }
  },
  CART:    {
    SUMMARY: 'Cards selected: <strong>${count}</strong> Total cost: <strong>${summ}</strong>'
  },
  CHECK:   {
    FORMAT_LABEL:     'Input format',
    FORMAT_PLCHLDR:   'E.g. %num%|%m%|%y%|%cvv2%',
    FORMAT_TOOLTIP:   'Enter the format correctly. The format must contain a set of tags. Please enter the information into the input field. The minimum required information: card number, month, year and CVV2.',
    SAVE_TPL_LABEL:   'Save format',
    LOAD_TPL_LABEL:   'Load format',
    SELECT_TPL_LABEL: 'Choose a format from your saved list',
    LEGEND_LABEL:     '<b>%num%</b> &mdash; card number, <b>%m%</b> &mdash; month, <b>%y%</b> &mdash; year,\
    <b>%cvv2%</b> &mdash; CVV2, <b>%zip%</b> &mdash; zip, <b>%addr%</b> &mdash; address',
    FORM_LABEL:       'Card information input field',
    HINT_LABEL:       'Hint',
    AREA_PLCHLDR:     'E.g. 444444444444444|12|2015|333',
    INSTRUCT:         'Add cards according to the selected format and press the "Preview" button. Below you will see the recognized information. If there are any mistakes, they will be highlighted with red color. If your limit is exceeded, please delete cards from your history',
    CARD_ADDED:       'Cards added',
    PREVIEW:          'Preview'
  },
  MAIN:    {
    GO_TO:         'Go to website',
    ADVERTISEMENT: 'Advertisement',
    CARD:          'cards'
  },
  MISC:    {
    CHANGE_DOMAIN1: 'New domains',
    CHANGE_DOMAIN2: 'Please change the domain',
    TIMEOUT:        'Check timeout: <b>${t.m}</b> min. <b>${t.s}</b> sec.',
    HOW_UP_TIMEOUT: 'How to increase timeout limit?',
    STEP2STEP:      {
      ICHECK: 'I pressed "Verify".',
      IWAIT:  'I have waited for 1 confirmation on Bitcoin.',
      IWRITE: 'I wrote the comment correctly.'
    }
  },
  NEWS:    {
    BACK_TO_LIST: 'Back to news',
    LIKE:         'Like',
    DISLIKE:      'Dislike',
    MARK_ALL:     'Mark all read',
    TYPES:        {
      OPINION: 'Your opinion',
      BASE:    'New in base',
      SITE:    'New options'
    }
  },
  ORDERS:  {
    CHECKING:  'Checking',
    CHECKED:   'Cards checked',
    UNCHECKED: 'Cards not checked',
    TABLE:     {
      ADD_TIME:      'Add time',
      COUNT:         'Total',
      VALID:         'Valid',
      UNOPEN:        'Unopened',
      ORDER_COMMENT: 'Order comment',
      PRICE:         'Price, $'
    },
    BY_BIN:    'Country by BIN'
  },
  PROFILE: {
    CHECK_SYS_LABEL:          'Check system',
    SELECT_PROVIDER:          'Select provider',
    CSV_OUTPUT_LABEL:         'CSV output options',
    SELECT_CSV_FIELDS:        'Select required fields to show in CSV',
    AVAIL_FIELD:              'Available fields',
    ACTIVE_FIELD:             'Active fields',
    UP:                       'Up',
    DOWN:                     'Down',
    DELIMETER:                'Delimeter',
    CSV_LEGEND_TEXT:          'Selected&nbsp;<b>${count}</b>/<b>${max_csv_field}</b> fields. Need to select <b>${max_csv_field}</b> fields to show.',
    CSV_OUTPUT_SHARED_LABEL:  'CSV output options(advanced)',
    CSV_OUTPUT_SHARED_LEGEND: 'Enter maximum <b>${max_csv_field}</b> fields',
    STATISTIC:                {
      TEXT:            'Statistics',
      WASTED_AMOUNT:   'Profit',
      BUYED:           'Card bought (pcs.)',
      WASTED_BY_CARD:  'Card bought ($)',
      CHECKED_COUNT:   'Checked cards (pcs.)',
      CHECK_AMOUNT:    'Checked cards ($)',
      RETURNED_COUNT:  'Returned cards (pcs.)',
      RETURNED_AMOUNT: 'Returned cards ($)',
      RATING:          'Rating',
      STATUS:          'Status',
      SEARCH_LIMIT:    'Search limit',
      CHECK_TIMEOUT:   'Check timeout',
      NOT_IN_RATING:   'Out of rating',
      M:               'min.',
      S:               'sec.'
    },
    SHOW_OPTS:                {
      TEXT:         'Show options',
      SHOW_INVALID: 'Show dead cards',
      SHOW_CONFIRM: 'Request confirmation',
      SHOW_ADV:     'Show advertisement'
    },
    FAST_BUY:                 {
      TEXT: 'Quick order'
    },
    SEARCH:                   {
      TEXT:           'Search show options',
      COLUMNS_LEGEND: 'Select necessary fields to see search table',
      LEGEND:         'Selected <b>${count}</b>/<b>16</b> fields. Need to select <b>16</b> fields to show.'
    },
    ITEMS_PER_PAGE:           {
      TEXT:         'Number of items per page',
      SEARCH_TABLE: 'Search table'
    }
  },
  STAT:    {
    ID:       'ID',
    DATE:     'Date',
    COUNT:    'Number',
    VALID:    'Valid',
    LOAD:     'Loads',
    BASES:    'Bases',
    BASE:     'Base',
    SELLERS:  'Sellers',
    SELLERSS: 'Sellers',
    SUMM:     'Total',
    NAME:     'Name',
    SELLER:   'Seller',
    DYN:      {
      TEXT:         'Dynamic valid',
      SELECT_BASE:  'Select base',
      QUICK_SEARCH: 'Quick search...',
      SELECT_LOAD:  'Select load',
      TIME_UNIT:    {
        WEEK:   'Week',
        MONTH:  'Month',
        SEASON: 'Season'
      },
      LEGEND:       'Select maximum <b>${maxSelected}</b> loads you are interested in.'
    },
    STAT:     {
      TEXT:            'Statistic valid',
      LOAD_TOOLTIP:    'Load from ${time_formatted}, valid ${valid}%',
      BASE_TOOLTIP:    'Base ${name}, valid ${valid}%',
      SELLERS_TOOLTIP: 'Seller ${name}, valid ${valid}%',
      SHOW_ALL:        'Show all',
      SHOW_ALLS:       'Show all'
    }
  },
  TICKETS: {
    CREATE:          'Create a new ticket',
    OPEN:            'Open',
    CLOSED:          'Closed',
    IN_PROGRESS:     'In progress',
    YOU:             'You',
    SUBJECT:         'Subject',
    NEW_MSG_PLCHLDR: 'Enter your message here...',
    SCHEDULE:        'Manager <b>9:00</b> — <b>21:00</b> UTC+3<br>\
      Support <b>0:00</b> — <b>24:00</b> UTC+3<br>\
      Support system responses in maximum <b>10</b> minutes.'
  },
  LOGIN:   {
    PASW_PLC:  'Password',
    YOUR_PASW: 'Your password',
    ATTENTION: 'Keep your password safe! In case of loss it will be IMPOSSIBLE to restore your account!'
  },
  FOOTER:  {
    NEED_HELP: 'Need help?',
    HELP: 'Help',
    CHECK_SYS: {
      TEXT: 'Check system'
    },
    SUPPORT:   {
      TEXT: 'Support system'
    },
    STATUS:    {
      WORK:     'Online',
      NOT_WORK: 'Offline',
      PART:     'Partly',
      HOLYDAY:  'Offline'
    },
    UPDATE:    'Version'
  },
  NAV:     {
    MAIN:     'Main',
    CARDS:    {
      TEXT:   'Cards',
      SEARCH: 'Search',
      CART:   'Cart',
      ORDERS: 'Orders',
      STAT:   'Statistics'
    },
    CHECK:    {
      TEXT:    'Check',
      CHECK:   'Check',
      HISTORY: 'History'
    },
    BILLING:  {
      TEXT:    'Billing',
      BALANCE: 'Top-up balance',
      MAIN:    'Transactions'
    },
    NEWS:     'News',
    TICKETS:  'Tickets',
    FAQ:      'FAQ',
    PROFILE:  'Profile',
    REGISTER: 'Registration'
  },
  COM:     {
    EMPTY:       'Empty',
    _EMPTY_:       '<Empty>',
    SET_DEFAULT: 'Set default',
    SORT:        'Sort',
    SELECT:      'Select...',
    ADD_TO_CART: 'Add to cart',
    ADD_NOTE:    'Add comment',
    MANAGERS:    {
      OWNER:   'Owner',
      ADMIN:   'Administrator',
      SUPPORT: 'Support'
    },
    COUNT:       'items',
    SETUP:       'Setup',
    WRONG_TIME:  'The support service is unavailable at the moment. You will receive the response in accordance with the working timetable.'
  },
  VALID:   {
    LOW: 'Low'
  },
  LANG:    {
    ERRORS:  {
      ERROR:                  'Error',
      AUTH_ERROR:             'Authorization error',
      UNKNOW_ERROR:           'Unknown error.',
      NOTHING_SELECT:         'Nothing is selected',
      COMMON:                 'Operation failed, please try again later',
      CARD_CHECK_TIMEOUT:     'The check is timed out',
      INCORRECT_PASSWORD:     'Incorrect password!',
      INCORRECT_REQUEST: 'Incorrect request!',
      SEARCH_SYSTEM_PROBLEM:  'Search system error',
      BTC_INCORRECT_RESPONSE: 'Incorrect server response',
      SEARCH_EXISTING:        'The search already exists.',
      SEARCH_NAME_EMPTY:      'Search name cannot be empty!',
      404:                    'Page not found',
      500:                    'Server error',
      CHECK_CORRECT:          'Check whether the information is correct',
      RESTRICTED_ACCESS:      'Restricted access',
      INVALID_PARAMS:         'Invalid options',
      POOR_PARAMS:            'Not enough options',
      NOT_SPECIFIED_FORMAT:   'Format not specified',
      EMPTY_CARDS_DATA:       'Cards information not specified',
      FILL_AND_TRY:           'Fill in the input field and cards information',
      MIN_DATA_FORMAT:        'Minimum required information: card number, month, year and CVV2',
      SHORT_TEXT:             'The text is too short',
      EMPTY_VALUE:            'Value field cannot be empty',
      SHORT_VALUES_LIST:      'Not enough values selected',

      INCORRECT_FORMAT:   'Incorrect format',
      INCORRECT_GIFTCODE: 'Incorrect gift code',

      SAVED_SEARCH_LIMIT: 'Saved searches limit exceeded',

      GENERAL: {
        'GENERAL ERROR':        'Unprocessed error',
        'NO PARAM':             'One of the required options is missing',
        'BAD VALIDATION':       'One of the options cannot be validated',
        'ALREADY EXISTS':       'The object you are trying to create already exists',
        'TIMEOUT':              'The registration is limited, try again later.',
        'NO ACCESS':            'Restricted access',
        'UNKNOWN ACTION':       'System cannot recognize the action',
        'EMPTY PARAM':          'One of the required options is empty',
        'NOTIFY_CHECK_TIMEOUT': 'Server update failure. Check your network connection'
      },

      PAYMENT: {
        'DUPLICATE RECORD':           'Wallet error. The wallet already exists.',
        'BAD ADDRESS':                'Bitcoin connection failure. Try again later.',
        'CANT CONNECT (NEW_ADDRESS)': 'Bitcoin connection failure. Try again later.',
        'CANT CONNECT (RAWADDR)':     'Bitcoin connection failure. Try again later.',
        'BAD RESPONSE (BLOCK)':       'Bitcoin connection failure. Try again later.',
        'CANT CONNECT (BLOCK)':       'Bitcoin connection failure. Try again later.',
        'EMPTY ADDRESS':              'Create a wallet and try again.',
        'EMPTY AMOUNT AFTER FEE':     'The amount you sent is too small.',
        'EMPTY AMOUNT':               'The amount you sent is too small.',
        'CANT CONNECT (COURSE)':      'Bitcoin connection failure. Try again later.',
        'PENDING':                    'Transaction is registered, but not enough confirmations received.',
        'NO PAYMENT':                 'No payments received on this wallet.',
        'CANT CONNECT (WM)':          'Webmoney connection failure. Try again later.',

        'CANT CONNECT (PM)': 'Perfect Money connection failure. Try again later.',

        NOT_ENROLL: 'Payment was not processed'
      },

      CARDS: {
        ALREADY_CHECKED:   'The selected cards have been checked already.',
        ALREADY_BUYED:     'The selected cards have been purchased already!',
        BALANCE_ISSUE:     'Balance problems!',
        NOTHING_BUYED:     'Nothing is bought!',
        IS_NO_ADDED:       'Cards not added',
        OVER_LIMITED:      'Sorry, you cannot add a new card. New cards limit exceeded',
        SMALL_BALANCE:     'Not enough balance',
        INCORRECT_DATA:    'Check whether the entered information is correct',
        NEED_TO_OPEN_CARD: 'You need to open cards before paying with check'
      }
    },
    SERVICE: {
      MESSAGE:             'Message',
      CASH_NUMBER_PRETEXT: 'To perform the payment enter this address',
      PAYMENT_SUCCESS:     'Payment completed successfully!',
      EMPTY_TABLE:         'Empty',
      FAST_BUY:            'Quick order',

      DELETE_CARDS_CONFIRM:  'Are you sure you want to delete cards?',
      DELETE_ORDERS_CONFIRM: 'Are you sure you want to delete orders?',

      CHECK_ORDER_CONFIRM: 'Are you sure you want to check the order?',
      NOT_MONEY_BACK:      'We do not refund dead cards',

      CARDS_BUYED: 'Cards purchased: ',
      READ_FAQ:    'The answer to this question can be found in FAQ',

      THANK4PURCHASE: 'Thank you for your purchase!',
      EDIT_TITLE:     'Edit title',

      LOAD_SEARCH:       'Load search',
      ENTER_SEARCH_NAME: 'Enter search name',
      NOTE:              'Comment',
      EMPTY_NOTE:        'Comment empty',
      CARDS_CHECK:       'Cards check',
      ATTENTION:         'Warning',

      SEARCH_RESULT:      'Search result',
      EXTERNAL_SEARCH_RESULT: 'External search result',
      TICKETS:            'Tickets',
      FAQ:                'FAQ',
      DELETE_CARDS_TITLE: 'Delete cards',
      ORDERS_DELETE:      'Delete purchases',
      ORDERS_CHECK:       'Purchases check',
      LOAD_BALANCE:       'Top-up balance',
      LOAD_OVER:          'Top-up with',
      PROFILE:            'Profile',
      NEWS:               'News',

      CREATE_PURSE: 'Create wallet',

      PROCESSING_QUERY: 'Your request is in process',

      OPERATION_STATUS: 'Operation status',

      REPEAT_OVER: 'Repeat in ',

      BACK_TO_ORDERS:  'Back to order',
      BACK_TO_TICKETS: 'Back to tickets',

      ORDER_SHOW: 'Order',
      CSV_VIEW:   'CSV-view',
      CSV_FORMAT: 'CSV-format',
      NEW_TICKET: 'New ticket',

      CSV_CHANGE_SUCCESS: "CSV-format changed successfully.",

      NULL_CARDS: '0 cards opened',

      CHECKING:   'Checking',
      VALID:      'Valid',
      INVALID:    'Dead',
      UNCHECKED:  'Not checked',
      NOCASHBACK: 'Non-refundable',

      CHECK_SYSTEM:                'Check system',
      CHECK_SYSTEM_CHANGE_SUCCESS: 'The check system was changed successfully. Your new check system: ',

      VIEW_OPTIONS:                'View options',
      VIEW_OPTIONS_CHANGE_SUCCESS: 'The view options were changed successfully.',

      SEARCH_OPTIONS:                'Search options',
      SEARCH_OPTIONS_CHANGE_SUCCESS: 'The search options were changed successfully.',

      MONTH:             ['jan.', 'feb.', 'mar.', 'apr.', 'may', 'jun.', 'jul.', 'aug.', 'sep.', 'oct.', 'nov.', 'dec.'],
      DELETED_CARDS:     'Deleted cards number: ',
      CARD_DELETED:      'Cards deleted',
      EMPTY_TPL:         'Format list empty',
      TPL_SUCCESS:       'Format saved successfully',
      TPL_ALREADY_EXIST: 'Format already exists',
      LOAD_TPL:          'Load format',
      EMPTY_SELECTION:   'Cards not selected',
      TRANSACT:          'Transaction №',

      INTYPES: {
        BTC:       'Bitcoin',
        FAKE:      'Internal',
        WM:        'Webmoney',
        PM:        'PerfectMoney',
        UNDEFINED: 'Undefined'
      },

      PROFILE_STATUS: {
        '0': 'Anonymous',
        '1': 'Trusted',
        '2': 'Verified'
      },

      ALL_BASE:              'All bases',
      GIFTCODE_SUCCESS:      'Gift code redeemed successfully!',
      GIFTCODE_HEADER:       'Your gift codes',
      GIFTCODE_REDEEM:       'Redeem gift code',
      SEARCH_SAVED:          'Search saved successfully!',
      TICKET_DELETE_CONFIRM: 'Closed and read tickets will be deleted. Are you sure?',
      NO_TICKETS_DELETED:    'No closed or read tickets',

      ORDERS_LIST:      'Orders',
      BACK_TO_SEARCH:   'Back to cards search',
      CART:             'Cart',
      BILLING:          'Transactions',
      LOW:              'Low',
      COUNT:            'pcs.',
      ADD_TO_CHECK:     'Add cards to check',
      CHECK_RESULT:     'Check history',
      EMPTY:            'Empty',
      CREATE_GIFT_CODE: 'Create gift code',
      STAT_HEADER:      'Statistics',
      QUANT:            'Number',
      VALID_COUNT:      'Valid',
      BUYED:            'Purchased',
      CLOSED:           'Closed',
      CHECKED:          'Checked',
      M:                'min.',
      S:                'sec.',
      H:                'h.',
      M_AGO:            'min. ago',
      S_AGO:            'sec. ago',
      H_AGO:            'h. ago',
      PARAGRAPH:        '§',
      ADV:              'Advertisement',
      checkAllText:     'Check all',
      uncheckAllText:   'Uncheck all',
      QUICK_SEARCH:     'Quick search...'

    },
    TABLE:   {
      COLS:          {
        ID:           'ID',
        INDEX:        'Index',
        NUMBER:       'Number',
        EXP:          'Exp',
        HOLDER:       'Holder name',
        LEVEL:        'Level',
        TYPE:         'Type',
        BANK:         'Bank',
        ZIP:          'ZIP Code',
        ADDRESS:      'Address',
        CITY:         'City',
        STATE_CODE:   'State',
        COUNTRY_CODE: 'Country',
        EMAIL:        'Email',
        PHONE:        'Phone',
        LOAD_VALID:   'Valid, %',
        SELLER:       'Seller',
        BASE:         'Base',
        ADD_TIME:     'Add time',
        SELLER_PRICE: 'Price, $',
        'CVV2':       'CVV2',
        'M':          'Month',
        'Y':          'Year',
        DATE:         'Check time',
        PRICE_ITEM:   'Price',
        PROVIDER:     'Provider',
        STATUS:       'Status',
        RESPONSE:     'Response code',
        COMMENT:      'Comment',
        THEME:        'Subject',
        OPENED:       'Open',
        CLOSED:       'Undiscovered',
        FROM:         'From',
        TO:           'To',
        CARDS_ANOTHER: 'Cards (other)'
      },
      lengthMenu:    'Show _MENU_',
      CARDS_sInfo:   'Cards found <strong>_TOTAL_</strong>, shown from _START_ to _END_',
      ORDERS_sInfo:  "Orders found <strong>_TOTAL_</strong>, shown from _START_ to _END_",
      TRANS_sInfo:   'Transactions found <strong>_TOTAL_</strong>, shown from _START_ to _END_',
      TICKETS_sInfo: 'Tickets found <strong>_TOTAL_</strong>, shown from _START_ to _END_',
      ELEMENTS:      ' elements' //'30 elements', '50 elements', '100 elements'
    },
    BUTTONS: {
      CANCEL:       'Cancel',
      DELETE:       'Delete',
      SAVE:         'Save',
      APPLY:        'Apply',
      CREATE:       'Create',
      SELECT_ALL:   'Select all',
      CHECK:        'Check',
      LOAD:         'Top-up',
      REDEEM:       'Redeem',
      SEE:          'View',
      BUY:          'Buy',
      OPEN:         'Open',
      CHECK_CARDS:  'Check',
      SHOW_CSV:     'Show in CSV',
      EDIT_COMMENT: 'Edit comment',
      PAY_CHECK:    'Check (No refund)',
      DOWNLOAD_ALL: 'Download all',
      DOWNLOAD:     'Download',
      CLEAR:        'Clear',
      SEND:         'Send',
      ENTER:        'Enter',
      SAVE_SEARCH:  'Save search',
      LOAD_SEARCH:  'Load search',
      RESET_SEARCH: 'Clear search',
      SEARCH_CARDS: 'Search',
      AGREE:        'I agree',
      SHOW:         'Show',
      MORE: 'Load more'
    }
  },
  SELLER:  {
    NAV: {
      STAT: 'Stats',
      DISBURSEMENT: 'Disbursement',
      PRICING: 'Price editor',
      EXIT: 'Logout'
    },
    SELLS: {
      HEAD: 'Stats & Disbursement',
      NAME: 'Name',
      PERCENTAGE: 'Percentage',
      LOADED: 'Loaded',
      TURNOVER: 'Turnover',
      INCORRECT: 'Incorrect',
      INVALID: 'Invalid',
      SOLD: 'Sold',
      VALID: 'Valid, %',
      REFUND: 'Refund',
      REFUND_CHECK: 'Refund (check)',
      PROFIT: 'Profit',
      WITHDRAW: 'Withdrawal',
      BALANCE: 'Balance'
    },
    PAYOUT: {
      HEAD: 'Payouts',
      SUM: 'Sum',
      WALLET: 'Wallet',
      DATE: 'Date',
      STATUS: 'Status',
      STATUS_PROCESSING: 'Processing',
      STATUS_PROCESSED: 'Processed',
      STATUS_REJECTED: 'Rejected'
    },
    PRICING: {
      HEAD: 'Price editor',
      ADD: 'Add',
      UPDATE: 'Update',
      CURRENT: 'Current',
      SEARCH: 'Search Countries',
      PRICE: 'Enter price for example 4.99'
    }
  }
}
