module.exports = {
  ru: require('./ru'),
  en: require('./en'),
  es: require('./es'),
  cn: require('./cn'),
}
