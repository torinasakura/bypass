import viewportGetter from 'react-media-queries/lib/viewportGetter'
import { Map } from 'immutable'

export function getOrientation() {
  if (window.matchMedia) {
    return window.matchMedia('(orientation: portrait)').matches ? 'Portrait' : 'Landscape'
  }

  return window.innerHeight > window.innerWidth ? 'Portrait' : 'Landscape'
}

export function getViewport() {
  const { viewport: { width } } = viewportGetter()

  if (width < 768) {
    return {
      type: 'Mobile',
      orientation: getOrientation(),
    }
  } else if (width >= 768 && width <= 1024) {
    return {
      type: 'Tablet',
      orientation: getOrientation(),
    }
  }

  return {
    type: 'Desktop',
  }
}

export function createViewportListener({ dispatch, getState }) {
  return () => {
    const viewport = getViewport()
    const current = getState().viewport

    if (viewport.type !== current.type || viewport.orientation !== current.orientation) {
      dispatch({
        type: 'VIEWPORT_CHANGE',
        viewport,
      })
    }
  }
}

export function createReducer(initialState, reducers = {}, nested) {
  return (state = initialState, { type, ...payload }) => {
    const handler = reducers[type]
    const newState = handler ? handler(state, payload) : state

    if (nested) {
      const nestedState = Object.keys(nested).reduce((result, key) => ({
        ...result,
        [key]: nested[key](newState.get(key), { type, ...payload }),
      }), {})

      return Map.isMap(newState) ? newState.merge(nestedState) : { ...newState, ...nestedState }
    }

    return newState
  }
}
