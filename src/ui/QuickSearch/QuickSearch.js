import React from 'react'
import { StyleSheet } from 'quantum'
import { SearchIcon } from '../icons'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    paddingRight: '14px',
    marginRight: '10px',
    '& input': {
      transition: 'all 0.3s',
      boxSizing: 'content-box',
      padding: '2px 6px',
      border: '1px solid #e0e0e0',
      fontSize: '12px',
      lineHeight: '20px',
      height: 'auto',
      width: '100%',
      outline: 'none',
    },
    '& svg': {
      position: 'absolute',
      top: '6px',
      right: '5px',
    },
  },
})

const QuickSearch = ({ value = '', onChange }) => (
  <span className={styles()}>
    <input
      value={value}
      placeholder={__i18n('LANG.SERVICE.QUICK_SEARCH')}
      onChange={onChange}
    />
    <SearchIcon fill='#9e9e9e' />
  </span>
)

export default QuickSearch
