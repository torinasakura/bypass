import React from 'react'
import { StyleSheet } from 'quantum'
import Spinner from './Spinner'
import Block from './Block'

const styles = StyleSheet.create({
  self: {
    position: 'fixed',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: 'rgba(0, 0, 0, 0.5)',
    zIndex: '9999',
    textAlign: 'center',
    display: 'none',
  },
  show: {
    display: 'block',
  },
})

const Loader = ({ show }) => (
  <div className={styles({ show })}>
    <Spinner>
      <Block />
      <Block delay />
      <Block delay2x />
    </Spinner>
  </div>
)

export default Loader
