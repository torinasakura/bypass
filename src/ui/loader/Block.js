import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#37474f',
    border: '3px solid #37474f',
    float: 'left',
    height: '100px',
    marginLeft: '8px',
    width: '27px',
    opacity: '.1',
    animation: 'bounceG 1.3s infinite linear',
    transform: 'scale(0.7)',
    animationDelay: '0s',
  },
  delay: {
    animationDelay: '.13s',
  },
  delay2x: {
    animationDelay: '.26s',
  },
  small: {
    width: '6px',
    height: '12px',
    border: 0,
    marginLeft: '1px',
  },
})

const Block = ({ delay, delay2x, small }) => (
  <div className={styles({ delay, delay2x, small })} />
)

export default Block
