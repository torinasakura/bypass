import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '140px',
    position: 'absolute',
    left: '50%',
    top: '50%',
    margin: '-53px 0 0 -70px',
  },
  local: {
    top: 0,
    left: 0,
    margin: 0,
  },
})

const Spinner = ({ children, local }) => (
  <div className={styles({ local })}>
    {children}
  </div>
)

export default Spinner
