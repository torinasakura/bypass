import React from 'react'

const Text = ({ children, size }) => (
  <span style={{ fontSize: size }}>
    {children}
  </span>
)

export default Text
