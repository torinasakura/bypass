import React from 'react'
import Icon from './Icon'

const CartIcon = ({ fill = '#ffffff', ...props }) => (
  <Icon originalWidth={30} originalHeight={30} fill={fill} {...props}>
    <g>
      <path
        fill='none'
        stroke={fill}
        strokeWidth='3'
        strokeLinejoin='round'
        strokeLinecap='round'
        d='m 27,3 -4,1 -5,14 -12,0 -3,-11 19,0'
      />
      <circle r='3' id='circle6' cy='24' cx='7' fill={fill} stroke={fill} />
      <circle r='3' id='circle8' cy='24' cx='17' fill={fill} stroke={fill} />
    </g>
  </Icon>
)

export default CartIcon
