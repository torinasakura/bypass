import React from 'react'
import Icon from './Icon'

const CheckboxIcon = props => (
  <Icon originalWidth={16} originalHeight={16} {...props}>
    <rect
      x='1'
      y='1'
      rx='4'
      width='14'
      height='14'
      fill='#f0f0f0'
      stroke='#b5b5b5'
      strokeWidth='1'
    />
  </Icon>
)

export default CheckboxIcon
