import ru from './Ru'
import en from './En'
import es from './Es'
import cn from './Cn'

export {
  ru,
  en,
  es,
  cn,
}
