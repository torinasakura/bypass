import React from 'react'
import Icon from '../Icon'

const Cn = props => (
  <Icon originalWidth={30} originalHeight={30} {...props}>
    <defs>
      <path
        id='s'
        fill='#ffde00'
        d='M0,-1 0.587785,0.809017 -0.951057,-0.309017H0.951057L-0.587785,0.809017z'
      />
    </defs>
    <rect width='30' height='30' fill='#de2910' />
    <use xlinkHref='#s' transform='translate(5,5) scale(3)' />
    <use xlinkHref='#s' transform='translate(10,2) rotate(23.036243)' />
    <use xlinkHref='#s' transform='translate(12,4) rotate(45.869898)' />
    <use xlinkHref='#s' transform='translate(12,7) rotate(69.945396)' />
    <use xlinkHref='#s' transform='translate(10,9) rotate(20.659808)' />
  </Icon>
)

export default Cn
