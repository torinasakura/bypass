import React from 'react'
import Icon from '../Icon'

const Ru = props => (
  <Icon originalWidth={512} originalHeight={512} {...props}>
    <g fillRule='evenodd' strokeWidth='1pt'>
      <path fill='#fff' d='M0 0h512.005v512H0z' />
      <path fill='#01017e' d='M0 170.667h512.005V512H0z' />
      <path fill='#fe0101' d='M0 341.333h512.005V512H0z' />
    </g>
  </Icon>
)

export default Ru
