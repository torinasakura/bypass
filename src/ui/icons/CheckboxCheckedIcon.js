import React from 'react'
import Icon from './Icon'

const CheckboxCheckedIcon = props => (
  <Icon originalWidth={16} originalHeight={16} {...props}>
    <rect
      x='1'
      y='1'
      rx='4'
      width='14'
      height='14'
      fill='#f0f0f0'
      stroke='#b5b5b5'
      strokeWidth='1'
    />
    <polyline
      fill='none'
      strokeWidth='2'
      stroke='#b5b5b5'
      points='3,7 6,11 13,5'
    />
  </Icon>
)

export default CheckboxCheckedIcon
