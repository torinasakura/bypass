import React from 'react'
import Icon from './Icon'

const PlusCircleIcon = props => (
  <Icon originalWidth={18} originalHeight={18} {...props}>
    <g>
      <path
        d='M9 1c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM9
          15c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6z'
      />
      <path
        d='M12.010 8h-2.010v-2.010c0-0.547-0.443-0.99-0.99-0.99h-0.020c-0.547 0-0.99
          0.443-0.99 0.99v2.010h-2.010c-0.547 0-0.99 0.443-0.99 0.99v0.020c0 0.547 0.443
          0.99 0.99 0.99h2.010v2.010c0 0.547 0.443 0.99 0.99 0.99h0.020c0.547 0 0.99-0.443
          0.99-0.99v-2.010h2.010c0.547 0 0.99-0.443 0.99-0.99v-0.020c-0-0.547-0.443-0.99-0.99-0.99z'
      />
    </g>
  </Icon>
)

export default PlusCircleIcon
