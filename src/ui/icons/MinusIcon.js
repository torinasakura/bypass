import React from 'react'
import Icon from './Icon'

const MinusIcon = props => (
  <Icon originalWidth={24} originalHeight={24} {...props}>
    <g>
      <path
        d='M3 11h18q0.414 0 0.707 0.293t0.293 0.707-0.293 0.707-0.707
          0.293h-18q-0.414 0-0.707-0.293t-0.293-0.707 0.293-0.707 0.707-0.293z'
      />
    </g>
  </Icon>
)

export default MinusIcon
