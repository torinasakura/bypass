import React from 'react'
import Icon from './Icon'
import { rotate } from './utils'

const ArrowIcon = ({ right, down, left, ...props }) => (
  <Icon originalWidth={32} originalHeight={32} {...props}>
    <g transform={rotate({ right, down, left }, 32, 32)}>
      <path
        d='M30.054 23.768l-2.964 2.946q-0.339 0.339-0.804 0.339t-0.804-0.339l-9.482-9.482-9.482 9.482q-0.339
          0.339-0.804 0.339t-0.804-0.339l-2.964-2.946q-0.339-0.339-0.339-0.813t0.339-0.813l13.25-13.232q0.339-0.339
          0.804-0.339t0.804 0.339l13.25 13.232q0.339 0.339 0.339 0.813t-0.339 0.813z'
      />
    </g>
  </Icon>
)

export default ArrowIcon
