import React from 'react'
import Icon from './Icon'

const NodeExpandedIcon = props => (
  <Icon originalWidth={16} originalHeight={16} {...props}>
    <g>
      <rect x='1' y='1' rx='4' width='14' height='14' fill='#f0f0f0' stroke='#b5b5b5' strokeWidth='1' />
      <line x1='2' y1='8' x2='14' y2='8' stroke='#b5b5b5' strokeWidth='2' />
    </g>
  </Icon>
)

export default NodeExpandedIcon
