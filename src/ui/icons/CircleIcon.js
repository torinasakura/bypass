import React from 'react'
import Icon from './Icon'

const CircleIcon = ({ green, ...props }) => (
  <Icon originalWidth={16} originalHeight={16} {...props}>
    <circle
      cx='8'
      cy='8'
      r='6'
      fill='none'
      strokeWidth='3'
      stroke={green ? '#8bc34a' : '#b71c1c'}
    />
  </Icon>
)

export default CircleIcon
