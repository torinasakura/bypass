import React, { Component } from 'react'

class Icon extends Component {
  static SIZE = {
    width: 13,
    height: 13,
  }

  static defaultProps = {
    fill: '#8c8c8c',
  }

  getSize(width, height, ratio) {
    if (!width && !height) return Icon.SIZE
    if (width && height) return { width, height }

    return {
      ...width && { width, height: (width / ratio) },
      ...height && { width: (height * ratio), height },
    }
  }

  render() {
    const { originalWidth, originalHeight, width, height, fill, children, viewBox, onClick } = this.props
    const size = this.getSize(width, height, originalWidth / originalHeight)

    return (
      <svg {...size} fill={fill} viewBox={viewBox || `0 0 ${originalWidth} ${originalHeight}`} onClick={onClick}>
        {children}
      </svg>
    )
  }
}

export default Icon
