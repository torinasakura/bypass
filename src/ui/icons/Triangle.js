import React from 'react'
import Icon from './Icon'

const transforms = {
  down: 'rotate(180 16 12)',
}

const direction = values => {
  const target = Object.keys(values).filter(key => values[key]).shift()

  if (target) {
    return transforms[target]
  }

  return null
}

const Triangle = ({ down, ...props }) => (
  <Icon originalWidth={32} originalHeight={24} {...props}>
    <g transform={direction({ down })}>
      <path d='M11.992 8l-11.992 11.992h23.984l-11.992-11.992z' />
    </g>
  </Icon>
)

export default Triangle
