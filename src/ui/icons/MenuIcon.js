import React from 'react'
import Icon from './Icon'

const MenuIcon = props => (
  <Icon originalWidth={24} originalHeight={24} {...props}>
    <g transform='rotate(90 12 12)'>
      <path d='M0 0h24v24H0z' fill='none' />
      <path
        d='M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9
          2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z'
      />
    </g>
  </Icon>
)

export default MenuIcon
