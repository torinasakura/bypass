import React from 'react'
import { StyleSheet } from 'quantum'
import { CheckboxIcon, CheckboxCheckedIcon } from '../icons'

let uniqId = 0

const uniq = () => uniqId++

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
    verticalAlign: 'sub',
    marginRight: '5px',
    height: '16px',
    width: '16px',
    transition: 'background .15s linear',
    cursor: 'pointer',
    '& input': {
      display: 'none',
    },
  },
})

const Checkbox = ({ checked = false, id = `checkbox-${uniq()}`, onChange }) => (
  <label htmlFor={id} className={styles()}>
    <input
      id={id}
      type='checkbox'
      checked={checked}
      onChange={onChange}
    />
    {checked ? <CheckboxCheckedIcon width={16} /> : <CheckboxIcon width={16} />}
  </label>
)

export default Checkbox
