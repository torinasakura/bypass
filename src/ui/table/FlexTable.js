/* eslint-disable no-underscore-dangle */
import React from 'react'
import cn from 'classnames'
import { FlexTable as BaseFlexTable } from 'react-virtualized'
import { Cell as HeaderCell } from './header'
import { Row } from './body'
import './Table.css'

class FlexTable extends BaseFlexTable {
  static propTypes = {
    ...BaseFlexTable.propTypes,
    children: () => {},
  }

  _getColumnWidth(width) {
    if (typeof width === 'string') {
      return width
    }

    return `${width}px`
  }

  /**
   * Determines the flex-shrink, flex-grow, and width values for a cell (header or column).
   */
  _getFlexStyleForColumn(column) {
    const flexValue = `${column.props.flexGrow} ${column.props.flexShrink} ${this._getColumnWidth(column.props.width)}`

    const style = {
      flex: flexValue,
      msFlex: flexValue,
      WebkitFlex: flexValue,
    }

    if (column.props.maxWidth) {
      style.maxWidth = column.props.maxWidth
    }

    if (column.props.minWidth) {
      style.minWidth = column.props.minWidth
    }

    return style
  }

  _getRenderedHeaderRow() {
    const { children, disableHeader } = this.props
    const { scrollbarWidth } = this.state
    const items = disableHeader ? [] : React.Children.toArray(children)

    const columns = items.map((column, index) =>
      this._createHeader(column, index)
    )

    if (scrollbarWidth > 0) {
      columns.push((
        <div
          key='Header-ColScrollbar'
          className='FlexTable__headerColumn'
          style={this._getFlexStyleForColumn({ props: { flexGrow: 0, flexShrink: 0, width: `${scrollbarWidth}px` } })}
        >
          <HeaderCell>
            {'\u00A0'}
          </HeaderCell>
        </div>
      ))
    }

    return columns
  }

  _createRow(rowIndex) {
    const {
      children,
      onRowClick,
      rowClassName,
      rowGetter,
    } = this.props

    const { scrollbarWidth } = this.state

    const rowClass = rowClassName instanceof Function ? rowClassName(rowIndex) : rowClassName
    const rowData = rowGetter(rowIndex)

    const a11yProps = {}

    if (onRowClick) {
      a11yProps['aria-label'] = 'row'
      a11yProps.role = 'row'
      a11yProps.tabIndex = 0
      a11yProps.onClick = event => onRowClick(event, rowIndex)
    }

    return (
      <Row
        {...a11yProps}
        key={rowIndex}
        className={cn('FlexTable__row', rowClass)}
        style={{
          height: this._getRowHeight(rowIndex),
          paddingRight: scrollbarWidth,
        }}
        rowData={rowData}
        columns={children}
        rowIndex={rowIndex}
      />
    )
  }
}

export default FlexTable
