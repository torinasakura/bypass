import React from 'react'
import { StyleSheet } from 'quantum'
import Cell from './Cell'

const styles = StyleSheet.create({
  self: {
    flexGrow: 1,
  },
})

const NoRowsRenderer = () => (
  <div className='FlexTable__row' style={{ height: 30, borderTop: '1px solid #e0e0e0' }}>
    <div className={`${styles()} FlexTable__rowColumn`}>
      <Cell justify='center'>
        {__i18n('LANG.SERVICE.EMPTY_TABLE')}
      </Cell>
    </div>
  </div>
)

export default NoRowsRenderer
