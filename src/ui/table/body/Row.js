import React, { Children, Component } from 'react'
import cn from 'classnames'

class Row extends Component {
  static defaultProps = {
    style: {},
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.style.paddingRight !== this.props.style.paddingRight
          || nextProps.rowData !== this.props.rowData
  }

  getColumnWidth(width) {
    if (typeof width === 'string') {
      return width
    }

    return `${width}px`
  }

  getFlexStyleForColumn(column) {
    const flexValue = `${column.props.flexGrow} ${column.props.flexShrink} ${this.getColumnWidth(column.props.width)}`

    const style = {
      flex: flexValue,
      msFlex: flexValue,
      WebkitFlex: flexValue,
    }

    if (column.props.maxWidth) {
      style.maxWidth = column.props.maxWidth
    }

    if (column.props.minWidth) {
      style.minWidth = column.props.minWidth
    }

    return style
  }

  renderColumn(column, columnIndex, rowData, rowIndex) {
    const {
      cellClassName,
      cellDataGetter,
      columnData,
      dataKey,
      cellRenderer,
      ...options,
    } = column.props

    const cellData = cellDataGetter(dataKey, rowData, columnData, columnIndex, rowIndex)
    const renderedCell = cellRenderer(cellData, dataKey, rowData, rowIndex, columnData, columnIndex, options)

    const style = this.getFlexStyleForColumn(column)

    return (
      <div
        key={`Row${rowIndex}-Col${columnIndex}`}
        className={cn('FlexTable__rowColumn', cellClassName)}
        style={style}
      >
        {renderedCell}
      </div>
    )
  }

  renderColumns(columns, rowData, rowIndex) {
    return Children.toArray(columns).map(
      (column, columnIndex) => this.renderColumn(
        column,
        columnIndex,
        rowData,
        rowIndex
      )
    )
  }

  render() {
    const { columns, rowData, rowIndex, ...props } = this.props

    return (
      <div {...props}>
        {this.renderColumns(columns, rowData, rowIndex)}
      </div>
    )
  }
}

export default Row
