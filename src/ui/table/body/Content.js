import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
})

const Content = ({ children }) => (
  <span className={styles()}>
    {children}
  </span>
)

export default Content
