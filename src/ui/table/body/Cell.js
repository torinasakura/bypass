import React from 'react'
import { StyleSheet } from 'quantum'
import { connect, hover as hoverState } from 'bypass/ui/state'
import { Tooltip } from '../../tooltip'
import Content from './Content'

const styles = StyleSheet.create({
  self: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    fontSize: '12px',
    background: '#ffffff',
    padding: '2px 7px',
    cursor: 'pointer',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  oddRow: {
    background: '#eceff1',
  },
  selected: {
    background: '#e5e19a',
  },
  'justify=center': {
    justifyContent: 'center',
  },
  'justify=end': {
    justifyContent: 'flex-end',
  },
  strong: {
    fontWeight: 'bold',
  },
})

const Cell = ({ children, oddRow, justify, strong, selected, tooltip, hover, onMouseEnter, onMouseLeave }) => (
  <div
    className={styles({ oddRow, justify, selected, strong })}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    <Content>
      {children}
    </Content>
    {tooltip && hover ? (
      <Tooltip align='top' offset='10px 0'>
        {tooltip}
      </Tooltip>
    ) : null}
  </div>
)

export default connect([hoverState], Cell)
