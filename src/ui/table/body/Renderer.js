import React from 'react'
import Cell from './Cell'

const defaultTooltipDataGetter = cellData => cellData

const Renderer = (cellData, dataKey, rowData, rowIndex, columnData, columnIndex, options = {}) => {
  const children = cellData == null ? '\u00A0' : String(cellData)

  const tooltipDataGetter = options.tooltipDataGetter || defaultTooltipDataGetter
  const tooltip = options.tooltip ? tooltipDataGetter(cellData, dataKey, rowData) : null

  return (
    <Cell
      {...options}
      tooltip={tooltip}
      oddRow={(rowIndex % 2) !== 0}
      selected={rowData.get('selected')}
    >
      {children}
    </Cell>
  )
}

export default Renderer
