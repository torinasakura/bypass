import React, { Component } from 'react'
import { AutoSizer, InfiniteLoader } from 'react-virtualized'
import shallowCompare from 'react-addons-shallow-compare'
import NoRowsRenderer from './body/NoRowsRenderer'
import FlexTable from './FlexTable'

const STATUS_LOADING = 1
const STATUS_LOADED = 2

class Table extends Component {
  static defaultProps = {
    headerHeight: 26,
    rowHeight: 40,
    total: 0,
  }

  constructor(props, context) {
    super(props, context)

    const loadedRowsMap = {}

    for (let i = 0; i <= props.list.size; i++) {
      loadedRowsMap[i] = STATUS_LOADED
    }

    this.state = {
      loadedRowsMap,
    }

    this.isRowLoaded = this.isRowLoaded.bind(this)
    this.loadMoreRows = this.loadMoreRows.bind(this)
    this.onRowClick = this.onRowClick.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list !== this.props.list) {
      const { loadedRowsMap } = this.state

      for (let i = this.props.list.size; i <= nextProps.list.size; i++) {
        loadedRowsMap[i] = STATUS_LOADED
      }

      this.setState({ loadedRowsMap })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  onRowClick(event, index) {
    event.stopPropagation()
    event.preventDefault()

    const { list, onRowClick } = this.props

    if (onRowClick) {
      onRowClick(list.get(index))
    }
  }

  loadMoreRows({ startIndex, stopIndex }) {
    const { onLoad } = this.props
    const { loadedRowsMap } = this.state

    for (let i = startIndex; i <= stopIndex; i++) {
      loadedRowsMap[i] = STATUS_LOADING
    }

    this.setState({ loadedRowsMap })

    return onLoad(startIndex)
  }

  isRowLoaded(index) {
    const { loadedRowsMap } = this.state
    return !!loadedRowsMap[index]
  }

  render() {
    const { total, list, children, minWidth, ...props } = this.props

    const rowGetter = index => list.get(index)
    const rowsCount = list.size

    return (
      <InfiniteLoader
        isRowLoaded={this.isRowLoaded}
        loadMoreRows={this.loadMoreRows}
        rowsCount={total}
      >
        {({ onRowsRendered, registerChild }) => (
          <AutoSizer>
            {({ width, height }) => (
              <FlexTable
                {...props}
                ref={registerChild}
                noRowsRenderer={NoRowsRenderer}
                onRowsRendered={onRowsRendered}
                onRowClick={this.onRowClick}
                rowGetter={rowGetter}
                rowsCount={rowsCount}
                height={height}
                width={minWidth > width ? minWidth : width}
              >
                {children}
              </FlexTable>
            )}
          </AutoSizer>
        )}
      </InfiniteLoader>
    )
  }
}

export default Table
