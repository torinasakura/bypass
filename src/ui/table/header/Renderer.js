import React from 'react'
import Cell from './Cell'

const Renderer = ({ label }) => (
  <Cell>
    {label}
  </Cell>
)

export default Renderer
