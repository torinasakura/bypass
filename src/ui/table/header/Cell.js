import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontWeight: 'normal',
    background: '#eceff1',
    textAlign: 'left',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    padding: '4px 7px 2px 7px',
    fontSize: '12px',
    width: '100%',
    borderBottom: '1px solid #e0e0e0',
    display: 'flex',
    alignItems: 'center',
    minHeight: '24px',
  },
  'justify=center': {
    justifyContent: 'center',
  },
  'justify=end': {
    justifyContent: 'flex-end',
  },
})

const Cell = ({ children, justify }) => (
  <span className={styles({ justify })}>
    {children}
  </span>
)

export default Cell
