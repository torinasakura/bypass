import { PropTypes } from 'react'
import { FlexColumn } from 'react-virtualized'
import { Renderer as HeaderRenderer } from './header'
import { Renderer as BodyRenderer } from './body'

class Column extends FlexColumn {
  static propTypes = {
    ...FlexColumn.propTypes,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }

  static defaultProps = {
    ...FlexColumn.defaultProps,
    headerRenderer: HeaderRenderer,
    cellRenderer: BodyRenderer,
  }
}

export default Column
