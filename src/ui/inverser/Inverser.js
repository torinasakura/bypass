import React from 'react'
import { StyleSheet } from 'quantum'
import { Checkbox } from '../checkbox'

const styles = StyleSheet.create({
  self: {
    padding: '6px',
    fontSize: '14px',
    color: '#263238',
    boxShadow: 'inset 0 1px 1px rgba(0, 0, 0, 0.075)',
    transition: 'border-color ease-in-out .15s, box-shadow ease-in-out .15s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    marginBottom: '10px',
    textAlign: 'left',
    cursor: 'pointer',
    width: '100%',
    boxSizing: 'border-box',
  },
})

const Inverser = ({ value = false, message, onChange }) => {
  const handleChange = event => {
    event.preventDefault()

    if (onChange) {
      onChange(!value)
    }
  }

  return (
    <div className={styles()} onClick={handleChange}>
      <Checkbox checked={value} /> {message || __i18n('SEARCH.NOT_FLAG')}
    </div>
  )
}

export default Inverser
