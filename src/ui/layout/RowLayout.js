import React, { Component } from 'react'
import VendorPrefix from 'react-vendor-prefix'

class RowLayout extends Component {
  getStyles() {
    const { style, justify, align, scrollY } = this.props

    return VendorPrefix.prefix({
      styles: {
        ...style,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
        minHeight: '100%',
        maxHeight: '100%',
        width: '100%',
        justifyContent: justify,
        alignItems: align,
        overflowY: scrollY ? 'auto' : null,
      },
    }).styles
  }

  render() {
    const { children } = this.props

    return (
      <div style={this.getStyles()}>
        {children}
      </div>
    )
  }
}

export default RowLayout
