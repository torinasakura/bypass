import React, { Component } from 'react'
import VendorPrefix from 'react-vendor-prefix'

class ColumnLayout extends Component {
  getStyles() {
    const { style, justify, align } = this.props

    return VendorPrefix.prefix({
      styles: {
        ...style,
        display: 'flex',
        flexDirection: 'row',
        overflow: 'hidden',
        width: '100%',
        justifyContent: justify,
        alignItems: align,
      },
    }).styles
  }

  render() {
    const { children } = this.props

    return (
      <div style={this.getStyles()}>
        {children}
      </div>
    )
  }
}

export default ColumnLayout
