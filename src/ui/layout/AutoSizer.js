/* eslint-disable no-underscore-dangle */
import React, { Children, cloneElement } from 'react'
import { AutoSizer as BaseAutoSizer } from 'react-virtualized'

class AutoSizer extends BaseAutoSizer {
  static propTypes = {}

  _onScroll() {}

  renderContent() {
    const { children } = this.props
    const { width, height } = this.state

    if (!width || !height || !children) {
      return null
    }

    return (
      <div style={{ position: 'absolute', width, height }}>
        {this.renderChildren()}
      </div>
    )
  }

  renderChildren() {
    const { children } = this.props
    const { width, height } = this.state
    const { props: { style } } = Children.only(children)

    return cloneElement(children, { style: { ...style, width, height } })
  }

  render() {
    const outerStyle = { position: 'relative' }

    return (
      <div
        ref={this._setRef}
        onScroll={this._onScroll}
        style={outerStyle}
      >
        {this.renderContent()}
      </div>
    )
  }
}

export default AutoSizer
