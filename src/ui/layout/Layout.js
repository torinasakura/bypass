import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import VendorPrefix from 'react-vendor-prefix'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    overflow: 'hidden',
    '-ms-overflow-style': '-ms-autohiding-scrollbar',
  },
  scroll: {
    overflow: 'auto',
  },
  scrollX: {
    overflowX: 'auto',
  },
  scrollY: {
    overflowY: 'auto',
  },
})

class Layout extends Component {
  static defaultProps = {
    grow: 0,
    shrink: 0,
    basis: 'auto',
  }

  onTouchMove(event) {
    event.stopPropagation()
  }

  getStyles() {
    const { grow, shrink, basis } = this.props

    return VendorPrefix.prefix({
      styles: {
        flex: `${grow} ${shrink} ${basis}`,
      },
    }).styles
  }

  render() {
    const { children, scroll, scrollX, scrollY, touch } = this.props

    return (
      <div
        style={this.getStyles()}
        className={styles({ scroll, scrollX, scrollY, touch })}
        onTouchMove={touch && this.onTouchMove}
      >
        {children}
      </div>
    )
  }
}

export default Layout
