import React, { Children, cloneElement } from 'react'

const List = ({ children }) => (
  <div>
    {Children.map(children, (child, index) => cloneElement(child, { gray: index % 2 === 0 }))}
  </div>
)

export default List
