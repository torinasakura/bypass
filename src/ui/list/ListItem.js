import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from '../layout'

const styles = StyleSheet.create({
  self: {
    lineHeight: '42px',
    paddingLeft: '37px',
    borderBottom: '1px solid #e5e5e5',
  },
  gray: {
    background: '#f0f0f0',
  },
})

const ListItem = ({ title, gray, children }) => (
  <div className={styles({ gray })}>
    <ColumnLayout>
      <Layout basis='50%'>
        {title}
      </Layout>
      <Layout basis='50%'>
        {children}
      </Layout>
    </ColumnLayout>
  </div>
)

export default ListItem
