import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import { List } from './list'

const styles = StyleSheet.create({
  self: {
    overflow: 'hidden',
  },
})

class Multiselect extends Component {
  static defaultProps = {
    value: [],
    items: [],
    textKey: 'text',
    valueKey: 'value',
  }

  constructor(props) {
    super(props)
    this.state = {
      items: props.items,
      value: props.value,
      filter: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ value: nextProps.value })
    }
  }

  onFilter = ({ target }) => {
    const { items, textKey } = this.props
    const value = target.value

    if (!value || value.length === 0) {
      this.setState({
        filter: '',
        items: this.props.items,
      })
    }

    this.setState({
      filter: value,
      items: items.filter(item => new RegExp(value, 'i').test(item[textKey])),
    })
  }

  onSelect = item => {
    const { valueKey, onChange } = this.props
    const { value } = this.state
    const id = item[valueKey]

    if (value.includes(id)) {
      value.splice(value.indexOf(id), 1)
    } else {
      value.push(id)
    }

    this.setState({ value })

    if (onChange) {
      onChange(value)
    }
  }

  onSelectAll = () => {
    const { valueKey, items, onChange } = this.props

    const value = items.map(item => item[valueKey])

    this.setState({ value })

    if (onChange) {
      onChange(value)
    }
  }

  onUnselectAll = () => {
    const { onChange } = this.props

    this.setState({ value: [] })

    if (onChange) {
      onChange([])
    }
  }

  render() {
    const { textKey, valueKey } = this.props
    const { items, value, filter } = this.state

    return (
      <div className={styles()}>
        <List
          items={items}
          value={value}
          filter={filter}
          textKey={textKey}
          valueKey={valueKey}
          onFilter={this.onFilter}
          onSelect={this.onSelect}
          onSelectAll={this.onSelectAll}
          onUnselectAll={this.onUnselectAll}
        />
      </div>
    )
  }
}

export default Multiselect
