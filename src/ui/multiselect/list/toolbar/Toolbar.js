import React from 'react'
import { StyleSheet } from 'quantum'
import Filter from './Filter'
import Selection from './Selection'

const styles = StyleSheet.create({
  self: {
    background: '#d3e0e5',
    padding: '5px 10px 10px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
})

const Toolbar = ({ filter, onFilter, onSelect, onUnselect }) => (
  <div className={styles()}>
    <Filter
      value={filter}
      onChange={onFilter}
    />
    <Selection
      onSelect={onSelect}
      onUnselect={onUnselect}
    />
  </div>
)

export default Toolbar
