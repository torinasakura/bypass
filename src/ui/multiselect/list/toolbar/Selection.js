import React from 'react'
import { StyleSheet } from 'quantum'
import { Link } from '../../../link'
import { CloseIcon, CheckIcon } from '../../../icons'

const styles = StyleSheet.create({
  self: {
    marginTop: '5px',
    '& svg': {
      position: 'relative',
      top: '4px',
    },
  },
})

const Selection = ({ onSelect, onUnselect }) => (
  <div className={styles()}>
    <Link
      color='blue'
      decorated
      onClick={onSelect}
    >
      <CheckIcon
        width={18}
        fill='#4caf50'
      />
      {__i18n('LANG.SERVICE.checkAllText')}
    </Link>
    {'\u00A0'}
    <Link
      color='blue'
      decorated
      onClick={onUnselect}
    >
      <CloseIcon
        width={18}
        fill='#b71c1c'
      />
      {__i18n('LANG.SERVICE.uncheckAllText')}
    </Link>
  </div>
)

export default Selection
