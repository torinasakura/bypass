import React from 'react'
import { StyleSheet } from 'quantum'
import { SearchIcon } from '../../../icons'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    paddingRight: '14px',
    marginRight: '20px',
    flexGrow: 1,
    '& input': {
      transition: 'all 0.3s',
      boxSizing: 'content-box',
      padding: '2px 6px',
      border: '1px solid #e0e0e0',
      fontSize: '12px',
      borderRadius: '5px',
      height: 'auto',
      marginTop: '5px',
      width: '100%',
    },
    '& svg': {
      position: 'absolute',
      top: '9px',
      right: '5px',
    },
  },
})

const Filter = ({ value = '', onChange }) => (
  <span className={styles()}>
    <input
      value={value}
      placeholder={__i18n('LANG.SERVICE.QUICK_SEARCH')}
      onChange={onChange}
    />
    <SearchIcon fill='#9e9e9e' />
  </span>
)

export default Filter
