import React from 'react'
import { StyleSheet } from 'quantum'
import Item from './Item'

const styles = StyleSheet.create({
  self: {
    paddingTop: '4px',
    height: '200px',
    overflowY: 'auto',
  },
})

const Items = ({ valueKey, textKey, items = [], value = [], onSelect }) => (
  <div className={styles()}>
    {items.map((item, index) => (
      <Item
        key={index}
        selected={value.includes(item[valueKey])}
        onClick={onSelect.bind(null, item)}
      >
        {item[textKey]}
      </Item>
    ))}
  </div>
)

export default Items
