import React from 'react'
import { StyleSheet } from 'quantum'
import { Checkbox } from '../../../checkbox'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    color: '#546e7a',
    cursor: 'pointer',
    width: '100%',
    margin: '0 0 4px 0',
    textAlign: 'left',
    padding: '0 10px',
  },
})

const Item = ({ children, selected, onClick }) => (
  <div className={styles()} onClick={onClick}>
    <Checkbox checked={selected} onChange={onClick} /> {children}
  </div>
)

export default Item
