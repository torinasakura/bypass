import React from 'react'
import { StyleSheet } from 'quantum'
import { Items } from './items'
import { Toolbar } from './Toolbar'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    background: '#ffffff',
  },
})

const List = ({ textKey, valueKey, items, value, filter, onFilter, onSelect, onSelectAll, onUnselectAll }) => (
  <div className={styles()}>
    <Toolbar
      filter={filter}
      onFilter={onFilter}
      onSelect={onSelectAll}
      onUnselect={onUnselectAll}
    />
    <Items
      items={items}
      value={value}
      textKey={textKey}
      valueKey={valueKey}
      onSelect={onSelect}
    />
  </div>
)

export default List
