import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    lineHeight: '24px',
    paddingLeft: '6px',
    fontSize: '14px',
    boxSizing: 'border-box',
    outline: 'none',
    '&:invalid': {
      boxShadow: '0 0 1px 0 #b71c1c',
      borderColor: '#b71c1c',
    },
  },
  fill: {
    width: '100%',
  },
  login: {
    transition: 'all 0.3s',
    borderRadius: '2px 0 0 2px',
    padding: '.5rem',
    fontSize: '.8rem',
    fontWeight: 400,
    letterSpacing: '.2px',
    color: '#9e9e9e',
    border: 'none',
  },
  disabled: {
    opacity: 0.6,
  },
})

const Input = ({ fill = true, type = 'text', disabled, login, value, onChange, ...props }) => (
  <input
    {...props}
    type={type}
    value={value}
    disabled={disabled}
    className={styles({ fill, login, disabled })}
    onChange={onChange}
  />
)

export default Input
