import React, { Component } from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    paddingLeft: 0,
    background: 'white',
    boxSizing: 'border-box',
    display: 'inline-flex',
    flexDirection: 'row',
    '& input': {
      content: '#c9c9c9',
      border: 'none',
      padding: 0,
      lineHeight: '24px',
      fontSize: '14px',
      color: '#333333',
      fontWeight: 'bold',
      textAlign: 'center',
      outline: 'none',
    },
    '& input:first-child': {
      width: '27px',
      marginRight: '5px',
      marginLeft: '0px',
    },
    '& input:last-child': {
      width: '48px',
    },
  },
})

const DateInput = ({ month, year, onChangeMonth, onChangeYear }) => (
  <span className={styles()}>
    <input
      type='text'
      maxLength='2'
      pattern='[0-9.]+'
      value={month}
      onChange={onChangeMonth}
    />
    <span>
      /
    </span>
    <input
      type='text'
      maxLength='4'
      pattern='[0-9.]+'
      value={year}
      onChange={onChangeYear}
    />
  </span>
)

class DateInputContainer extends Component {
  static defaultProps = {
    value: {
      month: '',
      year: '',
    },
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      value: this.formatFromShort(props.value),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ value: this.formatFromShort(nextProps.value) })
    }
  }

  onChangeMonth = ({ target }) => {
    const { onChange } = this.props
    const { value } = this.state

    const state = {
      value: { ...value, month: target.value },
    }

    this.setState(state)

    if (onChange) {
      onChange(this.formatToShort(state.value))
    }
  }

  onChangeYear = ({ target }) => {
    const { onChange } = this.props
    const { value } = this.state

    const state = {
      value: { ...value, year: target.value },
    }

    this.setState(state)

    if (onChange) {
      onChange(this.formatToShort(state.value))
    }
  }

  formatToShort({ month, year }) {
    return { m: month, y: year }
  }

  formatFromShort({ m, y }) {
    return { month: m, year: y }
  }

  render() {
    const { value } = this.state

    return (
      <DateInput
        month={value.month}
        year={value.year}
        onChangeMonth={this.onChangeMonth}
        onChangeYear={this.onChangeYear}
      />
    )
  }
}

export default DateInputContainer
