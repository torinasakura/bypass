import React from 'react'
import { StyleSheet } from 'quantum'
import { getmmss } from 'bypass/app/utils/utils'
import { connect, hover as hoverState } from '../state'
import { Condition } from '../condition'
import { Tooltip } from '../tooltip'

const styles = StyleSheet.create({
  self: {
    background: 'rgba(236, 239, 241, 0.5)',
    padding: '12px',
    fontSize: '14px',
    textAlign: 'right',
    lineHeight: '20px',
    width: '100%',
    '& i': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  },
  white: {
    background: '#ffffff',
  },
  'align=center': {
    textAlign: 'center',
  },
})

const getMessage = timeout => {
  const { m, s } = getmmss(timeout)

  return __i18n('MISC.TIMEOUT')
          .replace('${t.m}', m)
          .replace('${t.s}', s)
}

const ReadFaq = connect([hoverState], ({ hover, onMouseEnter, onMouseLeave }) => (
  <i
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    ({__i18n('MISC.HOW_UP_TIMEOUT')})
    <Condition match={hover}>
      <Tooltip align='top' offset='10px 30px' message>
        {__i18n('LANG.SERVICE.READ_FAQ')} §6.4.
      </Tooltip>
    </Condition>
  </i>
))

const HowUpTimeout = ({ timeout = 0, white, align }) => {
  if (!timeout) {
    return null
  }

  return (
    <div className={styles({ white, align })}>
      <div dangerouslySetInnerHTML={{ __html: getMessage(timeout) }} />
      <ReadFaq />
    </div>
  )
}

export default HowUpTimeout
