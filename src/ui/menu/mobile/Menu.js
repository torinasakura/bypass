import React, { PropTypes, Component } from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    height: '100%',
    backgroundColor: '#37474f',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
})

class Menu extends Component {
  static childContextTypes = {
    router: PropTypes.object,
  }

  getChildContext() {
    return this.props.context || {}
  }

  render() {
    return (
      <div className={styles()}>
        {this.props.children}
      </div>
    )
  }
}

export default Menu
