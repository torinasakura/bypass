import React, { Component, Children, cloneElement } from 'react'
import { styles } from './Item'

class RootItem extends Component {
  renderChildren() {
    const { children } = this.props

    return Children.map(children, child => cloneElement(child, { sub: true }))
  }

  render() {
    const { text } = this.props

    return (
      <div>
        <a className={styles()}>
          {text}
        </a>
        {this.renderChildren()}
      </div>
    )
  }
}

export default RootItem
