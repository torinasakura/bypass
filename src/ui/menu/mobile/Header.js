import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    height: '60px',
    width: '100%',
    background: '#263238',
    boxShadow: 'inset -1px 0 1px 0 #37474f',
  },
})

const Header = () => (
  <div className={styles()} />
)

export default Header
