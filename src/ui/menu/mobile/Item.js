import React from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'react-router'

export const styles = StyleSheet.create({
  self: {
    width: '100%',
    padding: '15px 10px 14px 20px',
    display: 'block',
    backgroundColor: '#37474f',
    borderBottom: '1px solid #546e7a',
    boxShadow: '0 1px 0 0 #556e79, inset 10px 0 0 0 #546e7a',
    fontSize: '16px',
    letterSpacing: '0.5px',
    color: '#ffffff',
    textDecoration: 'none',
  },
  active: {
    boxShadow: '0 1px 0 0 #556e79, inset 10px 0 0 0 #fdd835 !important',
    color: '#fdd835',
  },
  sub: {
    boxShadow: 'none',
  },
})

const Item = ({ to, children, sub, onClick }) => (
  <Link
    to={to}
    className={styles({ sub })}
    onClick={onClick}
  >
    {children}
  </Link>
)

export default Item
