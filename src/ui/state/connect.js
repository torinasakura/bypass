import { Component, createElement } from 'react'

function processEvents(events) {
  return Object.keys(events).reduce((result, event) => ({
    ...result,
    [event]: (result[event] || []).concat(events[event]),
  }), {})
}

function combineConnectors(result, { state, ...events }) {
  return {
    state: {
      ...result.state,
      ...state,
    },
    events: {
      ...result.events,
      ...processEvents(events),
    },
  }
}

const connect = (connectors = [], target) => {
  class StateConnector extends Component {
    constructor(props, context) {
      super(props, context)

      const { state, events } = connectors.reduce(combineConnectors.bind(this), { state: {}, events: {} })
      this.state = state
      this.events = Object.keys(events).reduce((res, event) => ({
        ...res,
        [event]: this.processEvent.bind(this, events[event]),
      }), {})
    }

    processEvent(handlers, event) {
      const state = handlers.reduce((result, handler) => ({
        ...result,
        ...handler(event, this.state),
      }), {})

      this.setState(state)
    }

    render() {
      const { children, ...props } = this.props

      return createElement(target, {
        ...this.state,
        ...this.events,
        ...props,
      }, children)
    }
  }

  return StateConnector
}

export default connect
