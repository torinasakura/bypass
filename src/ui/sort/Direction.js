import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from '../icons'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    background: '#f5f5f5',
    border: '1px solid #9e9e9e',
    cursor: 'pointer',
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxSizing: 'border-box',
    width: '25px',
    height: '24px',
    '& svg': {
      fill: '#000000',
      position: 'relative',
      top: '-1px',
    },
  },
  down: {
    '& svg': {
      transform: 'rotate(180deg)',
      top: '2px',
    },
  },
  active: {
    background: '#546e7a',
    '& svg': {
      fill: '#ffffff',
    },
  },
})

const Item = ({ active, down, onSelect }) => (
  <span className={styles({ active, down })} onClick={onSelect}>
    <ArrowIcon
      width={16}
    />
  </span>
)

const Direction = ({ direction = 'asc', onChange }) => (
  <span>
    <Item
      down
      active={direction === 'desc'}
      onSelect={() => onChange && onChange('desc')}
    />
    <Item
      active={direction === 'asc'}
      onSelect={() => onChange && onChange('asc')}
    />
  </span>
)

export default Direction
