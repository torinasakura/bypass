import React from 'react'
import { StyleSheet } from 'quantum'
import { Select } from '../select'
import Direction from './Direction'

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
    display: 'inline-flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
})

const Sort = ({ value = {}, columns = [], onChange }) => (
  <div className={styles()}>
    {__i18n('COM.SORT')}
    <Select
      value={value.order}
      valueKey='column'
      textKey='column'
      options={columns}
      onChange={event => onChange && onChange(event.target.value, value.direction)}
    />
    <Direction
      direction={value.direction}
      onChange={direction => onChange && onChange(value.order, direction)}
    />
  </div>
)

export default Sort
