import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import { Column } from './column'
import { Controls } from './controls'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
  },
  'direction=column': {
    flexDirection: 'row',
  },
  'direction=row': {
    flexDirection: 'column',
  },
})

class TwoTowers extends Component {
  static defaultProps = {
    direction: 'column',
    available: [],
    selected: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      selected: props.selected,
      available: this.getAvailable(props.available, props.selected),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selected !== this.props.selected) {
      this.setState({
        selected: nextProps.selected,
        available: this.getAvailable(nextProps.available, nextProps.selected),
      })
    }
  }

  onSelectAvailable = (field) => {
    this.setState({ type: 'available', active: field })
  }

  onSelectSelected = (field) => {
    this.setState({ type: 'selected', active: field })
  }

  onMoveFrom = () => {
    const { selected, active } = this.state
    const { onChange } = this.props

    selected.push(active)

    if (onChange) {
      onChange(selected)
    }

    this.setState({ active: null, type: null })
  }

  onMoveTo = () => {
    const { selected, active } = this.state
    const { onChange } = this.props

    const index = selected.indexOf(active)

    selected.splice(index, 1)

    if (onChange) {
      onChange(selected)
    }

    this.setState({ active: null, type: null })
  }

  onTurnUp = () => {
    const { selected, active } = this.state
    const { onChange } = this.props

    const index = selected.indexOf(active)

    selected.splice(index, 1)
    selected.splice(index - 1, 0, active)

    if (onChange) {
      onChange(selected)
    }
  }

  onTurnDown = () => {
    const { selected, active } = this.state
    const { onChange } = this.props

    const index = selected.indexOf(active)

    selected.splice(index, 1)
    selected.splice(index + 1, 0, active)

    if (onChange) {
      onChange(selected)
    }
  }

  getAvailable(available, selected) {
    return available.filter(item => !selected.includes(item))
  }

  canMoveFrom() {
    return this.state.type === 'available'
  }

  canMoveTo() {
    return this.state.type === 'selected'
  }

  canTurnUp() {
    const { selected, active, type } = this.state

    if (!active || type === 'available') {
      return false
    }

    return selected.indexOf(active) !== 0
  }

  canTurnDown() {
    const { selected, active, type } = this.state

    if (!active || type === 'available') {
      return false
    }

    return selected.indexOf(active) !== selected.length - 1
  }

  render() {
    const { direction } = this.props
    const { active, available, selected } = this.state

    return (
      <div className={styles({ direction })}>
        <Column
          direction={direction}
          fields={available}
          selected={active}
          title={__i18n('PROFILE.AVAIL_FIELD')}
          onSelect={this.onSelectAvailable}
        />
        <Controls
          direction={direction}
          canMoveFrom={this.canMoveFrom()}
          canMoveTo={this.canMoveTo()}
          canTurnUp={this.canTurnUp()}
          canTurnDown={this.canTurnDown()}
          onMoveFrom={this.onMoveFrom}
          onMoveTo={this.onMoveTo}
          onTurnUp={this.onTurnUp}
          onTurnDown={this.onTurnDown}
        />
        <Column
          direction={direction}
          fields={selected}
          selected={active}
          title={__i18n('PROFILE.ACTIVE_FIELD')}
          onSelect={this.onSelectSelected}
        />
      </div>
    )
  }
}

export default TwoTowers
