import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    lineHeight: '25px',
    transition: 'all 0.3s',
    userSelect: 'none',
    '&:hover': {
      background: '#c8d3dc !important',
      cursor: 'pointer',
    },
  },
  selected: {
    background: '#c8d3dc !important',
    cursor: 'pointer',
  },
  'direction=row': {
    textAlign: 'left',
    display: 'inline-block',
    width: '50%',
    float: 'left',
    paddingLeft: '10px',
    boxSizing: 'border-box',
  },
})

const Field = ({ name, selected, direction, onSelect }) => (
  <div
    className={styles({ selected, direction })}
    onClick={() => onSelect(name)}
  >
    {name}
  </div>
)

export default Field
