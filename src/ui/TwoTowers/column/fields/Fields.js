import React from 'react'
import { StyleSheet } from 'quantum'
import Field from './Field'

const styles = StyleSheet.create({
  self: {
    fontSize: '12px',
    background: '#ffffff',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  'direction=column': {
    height: '430px',
  },
  'direction=row': {
    height: '150px',
  },
})

const Fields = ({ fields = [], direction, selected, onSelect, onMove }) => (
  <div className={styles({ direction })}>
    {fields.map((field, index) => (
      <Field
        key={index}
        name={field}
        direction={direction}
        selected={selected === field}
        onMove={onMove}
        onSelect={onSelect}
      />
    ))}
  </div>
)

export default Fields
