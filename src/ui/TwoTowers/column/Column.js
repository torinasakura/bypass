import React from 'react'
import { StyleSheet } from 'quantum'
import Title from './Title'
import { Fields } from './fields'

const styles = StyleSheet.create({
  self: {
    color: '#5f644e',
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    overflow: 'hidden',
    background: '#ffffff',
  },
  'direction=column': {
    flexGrow: 1,
  },
})

const Column = ({ direction, title, fields, selected, onSelect }) => (
  <div className={styles({ direction })}>
    <Title>
      {title}
    </Title>
    <Fields
      fields={fields}
      selected={selected}
      direction={direction}
      onSelect={onSelect}
    />
  </div>
)

export default Column
