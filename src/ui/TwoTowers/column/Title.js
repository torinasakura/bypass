import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#d3e0e5',
    fontSize: '12px',
    lineHeight: '30px',
    width: '100%',
  },
})

const Title = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Title
