import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from '../../icons'
import Button from './Button'

const styles = StyleSheet.create({
  self: {},
  'direction=column': {
    display: 'flex',
    flexDirection: 'column',
    '& button:last-child svg': {
      position: 'relative',
      left: '-2px',
    },
    '& button': {
      margin: '5px 0',
    },
  },
  'direction=row': {
    display: 'flex',
    flexDirection: 'row',
    '& button:last-child svg': {
      position: 'relative',
      left: '-2px',
    },
    '& button': {
      margin: '0 5px',
      transform: 'rotate(90deg)',
    },
  },
})

const Move = ({ direction, canMoveFrom, canMoveTo, onMoveFrom, onMoveTo }) => (
  <div className={styles({ direction })}>
    <Button
      rounded
      disabled={!canMoveFrom}
      onClick={onMoveFrom}
    >
      <ArrowIcon right width={9} />
    </Button>
    <Button
      rounded
      disabled={!canMoveTo}
      onClick={onMoveTo}
    >
      <ArrowIcon left width={9} />
    </Button>
  </div>
)

export default Move
