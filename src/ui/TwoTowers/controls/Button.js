import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #a1a1a1',
    color: '#a1a1a1',
    background: '#ffffff',
    borderRadius: '5px',
    outline: 'none',
    fontSize: '12px',
    lineHeight: '22px',
    cursor: 'pointer',
    '& svg': {
      fill: '#a1a1a1',
    },
  },
  rounded: {
    borderRadius: '50%',
    width: '30px',
    height: '30px',
  },
  disabled: {
    opacity: 0.4,
    pointerEvents: 'none',
    cursor: 'initial',
  },
})

const Button = ({ children, disabled, rounded, onClick }) => (
  <button
    className={styles({ disabled, rounded })}
    disabled={disabled}
    onClick={!disabled && onClick}
  >
    {children}
  </button>
)

export default Button
