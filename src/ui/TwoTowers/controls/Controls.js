import React from 'react'
import { StyleSheet } from 'quantum'
import Move from './Move'
import Turn from './Turn'

const styles = StyleSheet.create({
  self: {},
  'direction=column': {
    margin: '0 20px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  'direction=row': {
    margin: '10px 0',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})

const Controls = ({
  direction, canMoveFrom, canMoveTo, canTurnUp, canTurnDown,
  onMoveFrom, onMoveTo, onTurnUp, onTurnDown,
}) => (
  <div className={styles({ direction })}>
    <Move
      direction={direction}
      canMoveFrom={canMoveFrom}
      canMoveTo={canMoveTo}
      onMoveFrom={onMoveFrom}
      onMoveTo={onMoveTo}
    />
    <Turn
      direction={direction}
      canTurnUp={canTurnUp}
      canTurnDown={canTurnDown}
      onTurnUp={onTurnUp}
      onTurnDown={onTurnDown}
    />
  </div>
)

export default Controls
