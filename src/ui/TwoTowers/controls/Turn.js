import React from 'react'
import { StyleSheet } from 'quantum'
import { Triangle } from '../../icons'
import Button from './Button'

const styles = StyleSheet.create({
  self: {},
  'direction=column': {
    display: 'flex',
    flexDirection: 'column',
    '& button:last-child svg': {
      position: 'relative',
      top: '2px',
    },
    '& button': {
      margin: '5px 0',
    },
  },
  'direction=row': {
    display: 'flex',
    flexDirection: 'row',
    '& button:last-child svg': {
      position: 'relative',
      top: '2px',
    },
    '& button': {
      margin: '0 5px',
    },
  },
})

const Turn = ({ direction, canTurnUp, canTurnDown, onTurnUp, onTurnDown }) => (
  <div className={styles({ direction })}>
    <Button
      disabled={!canTurnUp}
      onClick={onTurnUp}
    >
      {__i18n('PROFILE.UP')} <Triangle />
    </Button>
    <Button
      disabled={!canTurnDown}
      onClick={onTurnDown}
    >
      {__i18n('PROFILE.DOWN')} <Triangle down />
    </Button>
  </div>
)

export default Turn
