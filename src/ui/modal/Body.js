import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#e0e0e0',
    padding: '15px',
    width: '100%',
    fontSize: '14px',
    boxSizing: 'border-box',
    overflowY: 'auto',
    wordWrap: 'break-word',
  },
  white: {
    background: '#ffffff',
  },
  alignCenter: {
    textAlign: 'center',
  },
})

const Body = ({ children, white, alignCenter }) => (
  <div className={styles({ white, alignCenter })}>
    {children}
  </div>
)

export default Body
