import React from 'react'
import { StyleSheet } from 'quantum'
import { CloseIcon } from '../icons'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    cursor: 'pointer',
    borderRadius: '50%',
    width: '30px',
    height: '30px',
    display: 'inline-flex',
    transition: 'opacity 0.3s linear',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: '-15px',
    top: '-15px',
    '& svg': {
      opacity: 0.5,
    },
  },
})

const Close = ({ onClick }) => (
  <span className={styles()} onClick={onClick}>
    <CloseIcon width={18} fill='#b71c1c' />
  </span>
)

export default Close
