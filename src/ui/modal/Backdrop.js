import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 4,
    background: '#000000',
    opacity: 0,
  },
  show: {
    opacity: 0.5,
    transition: 'opacity .15s linear',
  },
})

const Backdrop = ({ onClick }) => (
  <div
    className={styles({ show: true })}
    onClick={onClick}
  />
)

export default Backdrop
