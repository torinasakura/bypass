import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#e0e0e0',
    textAlign: 'center',
    padding: '15px',
    width: '100%',
    boxSizing: 'border-box',
  },
})

const Header = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Header
