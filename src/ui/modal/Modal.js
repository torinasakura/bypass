import React from 'react'
import storeShape from 'react-redux/lib/utils/storeShape'
import { StyleSheet } from 'quantum'
import Backdrop from './Backdrop'
import Container from './Container'
import Close from './Close'

const styles = StyleSheet.create({
  self: {
    overflow: 'hidden',
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 5,
    outline: 0,
  },
})

const shouldScale = ({ getState }) => {
  const viewport = getState && getState().viewport

  if (!viewport) {
    return false
  }

  return viewport.type === 'Mobile' && viewport.orientation === 'Portrait'
}

const Modal = ({ children, fill, onClose }, { store }) => (
  <div className={styles()}>
    <Backdrop onClick={onClose} />
    <Container fill={fill} scale={shouldScale(store)}>
      <Close onClick={onClose} />
      {children}
    </Container>
  </div>
)

Modal.contextTypes = {
  store: storeShape,
}

export default Modal
