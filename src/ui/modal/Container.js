import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: '0 auto',
    flexDirection: 'column',
    margin: '5% auto',
    position: 'relative',
    maxWidth: '480px',
    background: '#ffffff',
    maxHeight: '80%',
    zIndex: 5,
  },
  scale: {
    transform: 'scale(0.9)',
  },
})

const Container = ({ children, scale }) => (
  <div className={styles({ scale })}>
    {children}
  </div>
)

export default Container
