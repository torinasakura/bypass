import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#e0e0e0',
    padding: '0 15px 10px 15px',
    width: '100%',
    boxSizing: 'border-box',
  },
  white: {
    background: '#ffffff',
  },
  alignCenter: {
    textAlign: 'center',
  },
})

const Footer = ({ children, white, alignCenter }) => (
  <div className={styles({ white, alignCenter })}>
    {children}
  </div>
)

export default Footer
