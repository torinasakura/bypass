import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from '../layout'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    minHeight: '100%',
    display: 'flex',
    padding: '0',
  },
})

const Wrapper = ({ children }) => (
  <div className={styles()}>
    <ColumnLayout>
      <Layout basis='24px' />
      <Layout grow={1} shrink={1}>
        {children}
      </Layout>
      <Layout basis='24px' />
    </ColumnLayout>
  </div>
)

export default Wrapper
