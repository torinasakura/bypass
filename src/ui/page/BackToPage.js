import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from '../icons'
import { Link } from '../link'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    lineHeight: '16px',
  },
  center: {
    textAlign: 'center',
    padding: '2px 0 4px 0',
  },
})

const BackToPage = ({ children, to, large, center }) => (
  <div className={styles({ center })}>
    <Link to={to} color='gray' large={large}>
      <ArrowIcon left fill='#9e9e9e' width={large ? 12 : 10} /> {children}
    </Link>
  </div>
)

export default BackToPage
