import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    width: '100%',
    minHeight: '100%',
    overflow: 'hidden',
  },
  indent: {
    padding: '0 24px',
  },
})

const Container = ({ children, indent }) => (
  <div className={styles({ indent })}>
    {children}
  </div>
)

export default Container
