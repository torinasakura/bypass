import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#f5f5f5',
    width: '100%',
    minHeight: '100%',
    display: 'flex',
    padding: '0 24px',
  },
})

const DesktopContainer = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default DesktopContainer
