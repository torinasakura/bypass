import React from 'react'
import { StyleSheet } from 'quantum'
import { Link as RouterLink } from 'react-router'

const styles = StyleSheet.create({
  self: {
    color: '#000000',
    fontSize: '14px',
    textDecoration: 'none',
    cursor: 'pointer',
  },
  decorated: {
    textDecoration: 'underline',
  },
  dashed: {
    borderBottom: '1px dashed #000000',
  },
  small: {
    fontSize: '12px',
  },
  large: {
    fontSize: '17px',
  },
  bold: {
    fontWeight: 'bold',
  },
  'color=gray': {
    color: '#9e9e9e',
    '&:hover': {
      color: '#000000',
    },
  },
  'color=blue': {
    color: '#546e7a',
    borderBottomColor: '#546e7a',
  },
  'color=lightBlue': {
    color: '#03a9f4',
    borderBottomColor: '#03a9f4',
  },
})

const Link = ({ children, to, href, target, decorated, dashed, small, bold, large, color, onClick, ...props }) => {
  if (to) {
    return (
      <RouterLink
        {...props}
        to={to}
        className={styles({ decorated, dashed, small, bold, large, color })}
      >
        {children}
      </RouterLink>
    )
  }

  if (href) {
    return (
      <a
        href={href}
        target={target}
        className={styles({ decorated, dashed, small, bold, large, color })}
      >
        {children}
      </a>
    )
  }

  return (
    <span
      className={styles({ decorated, dashed, small, bold, large, color })}
      onClick={onClick}
    >
      {children}
    </span>
  )
}

export default Link
