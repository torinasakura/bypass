import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    color: '#000000',
    fontSize: '14px',
    fontWeight: 'bold',
    '& span:last-child': {
      verticalAlign: 'text-top',
      fontSize: '11px',
      fontWeight: 'normal',
      lineHeight: '8px',
    },
  },
  green: {
    color: '#4caf50',
  },
})

const Price = ({ children = '', green }) => {
  const [int, flt] = children.split('.')

  if (!(int && flt)) {
    return null
  }

  return (
    <div className={styles({ green })}>
      <span>{int}</span>.<span>{flt}</span>
    </div>
  )
}

export default Price
