import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    overflow: 'hidden',
  },
})

const Collapse = ({ expanded, children }) => {
  if (!expanded) {
    return null
  }

  return (
    <div className={styles()}>
      {children}
    </div>
  )
}

export default Collapse
