import React from 'react'
import { StyleSheet } from 'quantum'
import { Header } from './header'
import Collapse from './Collapse'

const styles = StyleSheet.create({
  self: {
    backgroundColor: '#ffffff',
    border: '1px solid #cccccc',
    borderTopWidth: '0px',
    boxShadow: '0 1px 1px rgba(0, 0, 0, 0.05)',
  },
  first: {
    borderTopWidth: '1px',
  },
})

const Panel = ({ title, expanded, first, children, onToggle }) => (
  <div className={styles({ first })}>
    <Header expanded={expanded} onClick={onToggle}>
      {title}
    </Header>
    <Collapse expanded={expanded}>
      {children}
    </Collapse>
  </div>
)

export default Panel
