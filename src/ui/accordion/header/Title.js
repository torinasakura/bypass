import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    color: '#546e7a',
    fontWeight: 'bold',
    fontSize: '14px',
    lineHeight: '20px',
    display: 'inline-flex',
    alignItems: 'center',
    width: '100%',
  },
})

const Title = ({ children }) => (
  <span className={styles()}>
    {children}
  </span>
)

export default Title
