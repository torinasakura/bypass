import React from 'react'
import { StyleSheet } from 'quantum'
import { PlusIcon, MinusIcon } from '../../icons'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: '3px',
  },
})

const Expander = ({ expanded }) => (
  <span className={styles()}>
    {expanded
      ? <MinusIcon width={14} fill='#546e7a' />
      : <PlusIcon width={14} fill='#546e7a' />}
  </span>
)

export default Expander
