import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from '../../layout'
import Expander from './Expander'
import Title from './Title'

const styles = StyleSheet.create({
  self: {
    padding: '5px 15px 5px 15px',
    cursor: 'pointer',
    overflow: 'hidden',
  },
  expanded: {
    borderBottom: '1px solid #cccccc',
  },
})

const Header = ({ expanded, children, onClick }) => (
  <div
    className={styles({ expanded })}
    onClick={onClick}
  >
    <ColumnLayout>
      <Layout basis='22px'>
        <Expander expanded={expanded} />
      </Layout>
      <Layout grow={1} shrink={1}>
        <Title>
          {children}
        </Title>
      </Layout>
    </ColumnLayout>
  </div>
)

export default Header
