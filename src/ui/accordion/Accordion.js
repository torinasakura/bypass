import React, { Component, Children, cloneElement } from 'react'

class Accordion extends Component {
  constructor(props) {
    super(props)

    this.state = {
      expanded: this.getInitialTab(),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (Children.count(nextProps.children) !== Children.count(this.props.children)) {
      this.setState({ expanded: null })
    }
  }

  onToggle(index) {
    const { expanded } = this.state

    this.setState({
      expanded: index === expanded ? null : index,
    })
  }

  getInitialTab() {
    const { hash } = window.location
    const matches = hash.match(/tab=([^&]*)/)

    if (matches && matches[1]) {
      return parseInt(matches[1], 10)
    }

    return null
  }

  renderChildren() {
    const { children } = this.props
    const { expanded } = this.state

    return Children.map(children, (child, index) =>
      cloneElement(child, {
        first: index === 0,
        expanded: index === expanded,
        onToggle: this.onToggle.bind(this, index),
      })
    )
  }

  render() {
    return (
      <div>
        {this.renderChildren()}
      </div>
    )
  }
}

export default Accordion
