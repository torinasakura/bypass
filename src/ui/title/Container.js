import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    textAlign: 'center',
    margin: '20px 0',
    width: '100%',
  },
  'size=medium': {
    margin: '10px 0',
  },
  'size=small': {
    fontSize: '17px',
    margin: 0,
    padding: '12px 0',
    background: '#eceff1',
  },
  white: {
    background: '#ffffff',
  },
})

const Container = ({ children, size, white }) => (
  <div className={styles({ size, white })}>
    {children}
  </div>
)


export default Container
