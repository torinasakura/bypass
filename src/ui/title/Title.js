import React from 'react'
import { StyleSheet } from 'quantum'
import Container from './Container'

const styles = StyleSheet.create({
  self: {
    fontSize: '36px',
    color: '#37474f',
    fontWeight: 400,
    margin: 0,
    overflow: 'hidden',
  },
  'size=medium': {
    fontSize: '24px',
  },
  'size=small': {
    fontSize: '17px',
  },
  'offset=right': {
    marginRight: '-150px',
  },
  'offset=left': {
    marginLeft: '-150px',
  },
})

const Title = ({ children, size, offset, white }) => (
  <Container size={size} white={white}>
    <h1 className={styles({ size, offset })}>
      {children}
    </h1>
  </Container>
)

export default Title
