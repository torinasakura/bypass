import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#fff',
    width: '100%',
    minHeight: '100%',
    overflowY: 'auto',
  },
})

const Content = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Content
