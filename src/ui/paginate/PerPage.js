import React from 'react'
import { StyleSheet } from 'quantum'
import { Select } from '../select'

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
  },
})

const items = [
  {
    value: 30,
    text: `30 ${__i18n('LANG.TABLE.ELEMENTS')}`,
  },
  {
    value: 50,
    text: `50 ${__i18n('LANG.TABLE.ELEMENTS')}`,
  },
  {
    value: 100,
    text: `100 ${__i18n('LANG.TABLE.ELEMENTS')}`,
  },
]

const PerPage = ({ value, onChange }) => (
  <div className={styles()}>
    {__i18n('LANG.TABLE.lengthMenu').replace(' _MENU_', '')}
    <Select
      options={items}
      valueKey='value'
      textKey='text'
      value={value}
      onChange={event => onChange && onChange(0, event.target.value)}
    />
  </div>
)

export default PerPage
