/* eslint-disable eqeqeq */
const numberOfPages = (count, pageSize) => Math.ceil(count / pageSize)

const range = (count, pageSize) => {
  const end = numberOfPages(count, pageSize)
  const result = []

  for (let i = 0; i < end; i++) {
    result.push(i)
  }

  return result
}

const isStartInterval = (position, currentPage) =>
  ((position - currentPage) == -2) && (currentPage > 3)

const isEndInterval = (position, currentPage, count, pageSize) =>
  ((position - currentPage) == 3) && ((numberOfPages(count, pageSize) - currentPage) > 4)

const canRenderPosition = (position, count, pageSize, currentPage) => (
  position == 0 ||
  position == numberOfPages(count, pageSize) - 1 ||
  (position >= currentPage - 2 && position <= currentPage + 2)
)

export {
  numberOfPages,
  range,
  isStartInterval,
  isEndInterval,
  canRenderPosition,
}
