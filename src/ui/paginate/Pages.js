/* eslint-disable eqeqeq */
import React from 'react'
import { StyleSheet } from 'quantum'
import { range, isStartInterval, isEndInterval, canRenderPosition } from './utils'
import Page from './Page'

const styles = StyleSheet.create({
  self: {},
})

const Pages = ({ count = 0, pageSize = 10, offset = 0, onChange }) => {
  const currentPage = +offset / +pageSize

  return (
    <span className={styles()}>
      {range(count, pageSize).map((n, i) => {
        if (isStartInterval(n, currentPage)) {
          return <span key={i}>...</span>
        }

        if (canRenderPosition(n, count, pageSize, currentPage)) {
          return (
            <Page
              key={i}
              current={n == currentPage}
              onClick={() => onChange && onChange(pageSize * n, pageSize)}
            >
              {n + 1}
            </Page>
          )
        }

        if (isEndInterval(n, currentPage, count, pageSize)) {
          return <span key={i}>...</span>
        }

        return null
      })}
    </span>
  )
}

export default Pages
