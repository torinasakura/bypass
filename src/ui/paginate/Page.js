import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    padding: '0',
    margin: '0 5px',
    fontSize: '14px',
    borderRadius: '50%',
    border: '2px solid #9e9e9e',
    width: '29px',
    height: '29px',
    lineHeight: '26px',
    color: '#9e9e9e',
    textAlign: 'center',
    cursor: 'pointer',
    transition: 'all 0.3s',
    boxSizing: 'border-box',
    display: 'inline-block',
    '&:hover': {
      color: '#37474f',
      background: '#ffffff',
      border: '2px solid #37474f',
    },
  },
  current: {
    color: '#ffffff',
    background: '#37474f',
  },
})

const Page = ({ children, current, onClick }) => (
  <span
    className={styles({ current })}
    onClick={onClick}
  >
    {children}
  </span>
)

export default Page
