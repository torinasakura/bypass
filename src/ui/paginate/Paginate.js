import React from 'react'
import { StyleSheet } from 'quantum'
import Pages from './Pages'
import PerPage from './PerPage'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
})

const Paginate = ({ pageSize, onChange, ...props }) => (
  <div className={styles()}>
    <Pages
      {...props}
      pageSize={pageSize}
      onChange={onChange}
    />
    <PerPage
      value={pageSize}
      onChange={onChange}
    />
  </div>
)

export default Paginate
