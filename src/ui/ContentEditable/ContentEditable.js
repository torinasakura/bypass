import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { isEmpty } from 'ramda'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    padding: '3px 10px',
    overflowY: 'auto',
    resize: 'none',
    background: '#fff',
    color: '#666',
    fontSize: '13px',
    lineHeight: '20px',
    height: '90px',
    outline: 'none',
    '&:before': {
      content: 'attr(data-ph)',
      color: '#999',
      fontSize: '14px',
    },
    '&:focus:before': {
      content: '""',
    },
  },
  large: {
    height: '170px',
  },
})

class ContentEditable extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      showPlaceholder: isEmpty(props.html),
    }
  }

  componentDidUpdate() {
    const el = ReactDOM.findDOMNode(this)
    if (this.props.html !== el.innerHTML) {
      el.innerHTML = this.props.html
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.showPlaceholder !== this.state.showPlaceholder) {
      return true
    }

    return nextProps.html !== ReactDOM.findDOMNode(this).innerHTML
  }

  onChange = () => {
    const html = ReactDOM.findDOMNode(this).innerHTML
    if (this.props.onChange && html !== this.lastHtml) {
      this.props.onChange({
        target: {
          value: html,
        },
      })
    }

    this.lastHtml = html
  }

  onBlur = () => {
    const html = ReactDOM.findDOMNode(this).innerHTML

    this.setState({
      showPlaceholder: isEmpty(html),
    })

    this.onChange()
  }

  onFocus = () => {
    this.setState({ showPlaceholder: false })
    this.onChange()
  }

  render() {
    const { html, large, placeholder, ...props } = this.props
    const { showPlaceholder } = this.state

    return (
      <div
        {...props}
        contentEditable
        className={styles({ large })}
        onInput={this.onChange}
        onBlur={this.onBlur}
        onFocus={this.onFocus}
        onKeyUp={this.onChange}
        dangerouslySetInnerHTML={{ __html: html }}
        data-ph={showPlaceholder ? placeholder : null}
      />
    )
  }
}

export default ContentEditable
