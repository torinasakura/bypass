import React from 'react'
import { StyleSheet } from 'quantum'

const optionGetter = (key, option) => {
  if (typeof key === 'string') {
    return option[key]
  }

  return key(option)
}

const styles = StyleSheet.create({
  self: {
    borderRadius: '2px',
    border: '2px solid #9e9e9e',
    height: '25px',
    outline: 'none',
    margin: '0 10px',
    width: '150px',
    background: '#f5f5f5',
    fontSize: '13px',
  },
})

const Select = ({ value, options, valueKey, textKey, onChange }) => (
  <select className={styles()} value={value} onChange={onChange}>
    {options.map(option => (
      <option key={optionGetter(valueKey, option)} value={optionGetter(valueKey, option)}>
        {optionGetter(textKey, option)}
      </option>
    ))}
  </select>
)

export default Select
