import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    height: '50px',
    backgroundColor: '#263238',
    boxShadow: '0 2px 4px rgba(0, 0, 0, 0.24)',
    padding: '0 24px',
    display: 'flex',
  },
  small: {
    height: '48px',
    padding: 0,
  },
})

const Navigation = ({ children, small }) => (
  <div className={styles({ small })}>
    {children}
  </div>
)

export default Navigation
