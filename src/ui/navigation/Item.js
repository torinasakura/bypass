import React, { cloneElement } from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'react-router'
import { connect, hover as hoverState } from 'bypass/ui/state'

const styles = StyleSheet.create({
  self: {
    fontSize: '15px',
    letterSpacing: '0.2px',
    color: '#ffffff',
    display: 'inline-flex',
    alignItems: 'center',
    height: '100%',
    padding: '0 15px',
    cursor: 'pointer',
    position: 'relative',
    textDecoration: 'none',
    '& svg': {
      fill: '#ffffff',
    },
  },
  hover: {
    color: '#fdd835',
    boxShadow: 'inset 0 -3px 0 0 #fdd835',
    transition: 'all linear .1s',
    '& svg': {
      fill: '#fdd835',
    },
  },
  active: {
    color: '#fdd835',
    boxShadow: 'inset 0 -3px 0 0 #fdd835',
    transition: 'all linear .1s',
    '& svg': {
      fill: '#fdd835',
    },
  },
  withouthBorder: {
    boxShadow: 'none',
  },
})

const Item = ({ children, to, menu, hover, withouthBorder, onClick, onMouseEnter, onMouseLeave }) => {
  if (to) {
    return (
      <Link
        to={to}
        className={styles({ hover, withouthBorder })}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        {children}
      </Link>
    )
  }

  return (
    <span
      className={styles({ hover, withouthBorder })}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {children}
      {menu && hover ? cloneElement(menu, { onMouseEnter, onMouseLeave }) : null}
    </span>
  )
}

export default connect([hoverState], Item)
