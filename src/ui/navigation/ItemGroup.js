import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
    height: '100%',
  },
  fill: {
    flexGrow: 1,
  },
  'justify=center': {
    justifyContent: 'center',
  },
  'justify=space-around': {
    justifyContent: 'space-around',
  },
  'justify=space-between': {
    justifyContent: 'space-between',
  },
  limit: {
    maxWidth: '1200px',
  },
  smallLimit: {
    maxWidth: '400px',
  },
})

const ItemGroup = ({ children, fill, justify, justifyCenter, limit, smallLimit }) => (
  <div className={styles({ fill, justify, justifyCenter, limit, smallLimit })}>
    {children}
  </div>
)

export default ItemGroup
