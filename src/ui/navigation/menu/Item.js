import React, { Component, PropTypes } from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'react-router'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    padding: '12px',
    background: '#37474f',
    boxShadow: 'inset 0 -1px 0 0 #e0e0e0',
    fontSize: '14px',
    letterSpacing: '0.3px',
    color: '#ffffff',
    cursor: 'pointer',
    transition: 'all 0.3s',
    display: 'block',
    textDecoration: 'none',
    '& svg': {
      fill: '#ffffff',
    },
    '&:hover': {
      color: '#fdd835',
      background: '#546e7a',
      '& svg': {
        fill: '#fdd835',
      },
    },
  },
})

class Item extends Component {
  static childContextTypes = {
    router: PropTypes.object,
  }

  getChildContext() {
    return this.props.context
  }

  render() {
    const { children, to, onMouseEnter, onMouseLeave, onClose } = this.props

    return (
      <Link
        to={to}
        className={styles()}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onClick={onClose}
      >
        <ArrowIcon right width={10} />
        {'\u00A0'}
        {children}
      </Link>
    )
  }
}

export default Item
