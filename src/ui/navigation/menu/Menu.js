import React from 'react'
import { StyleSheet } from 'quantum'
import { Layer } from '../../layer'

const styles = StyleSheet.create({
  self: {
    display: 'block',
    background: 'black',
    width: '180px',
  },
})

const Menu = ({ children, onMouseEnter, onMouseLeave }, context) => (
  <Layer>
    <div
      className={styles()}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {React.Children.map(children, child => React.cloneElement(child, { context, onClose: onMouseLeave }))}
    </div>
  </Layer>
)

Menu.contextTypes = {
  router: React.PropTypes.object,
}

export default Menu
