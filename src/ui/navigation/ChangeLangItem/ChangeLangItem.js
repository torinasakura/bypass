import React from 'react'
import cookie from 'js-cookie'
import { connect, toggle } from 'bypass/ui/state'
import { Tooltip } from '../../tooltip'
import Item from '../Item'
import Flag from './Flag'
import Locale from './Locale'

const ChangeLangItem = ({ toggled, replace = true, onClick }) => {
  const locale = window.LOCALE || cookie.get('locale')

  const onClose = event => {
    event.preventDefault()

    if (onClick) {
      onClick()
    }
  }

  return (
    <Item withouthBorder onClick={onClick}>
      <Flag locale={locale} /> {locale.toUpperCase()}
      {toggled ? (<Tooltip onClose={onClose} offset='5px 0'>
        <Locale locale='ru' replace={replace} />
        <Locale locale='en' replace={replace} />
        <Locale locale='es' replace={replace} />
        <Locale locale='cn' replace={replace} />
      </Tooltip>) : null}
    </Item>
  )
}

export default connect([toggle], ChangeLangItem)
