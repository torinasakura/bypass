import React, { createElement } from 'react'
import { StyleSheet } from 'quantum'
import { Flags } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    width: '15px',
    height: '15px',
    display: 'inline-flex',
    verticalAlign: 'middle',
    borderRadius: '50%',
    overflow: 'hidden',
    marginRight: '5px',
  },
})

const Flag = ({ locale }) => {
  if (!Flags[locale]) {
    return null
  }

  return (
    <span className={styles()}>
      {createElement(Flags[locale], { width: 15 })}
    </span>
  )
}

export default Flag
