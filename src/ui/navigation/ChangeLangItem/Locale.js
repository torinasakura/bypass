import React from 'react'
import { StyleSheet } from 'quantum'
import cookie from 'js-cookie'
import { connect, hover as hoverState } from 'bypass/ui/state'
import Flag from './Flag'

const styles = StyleSheet.create({
  self: {
    fontSize: '15px',
    cursor: 'pointer',
    marginBottom: '5px',
    display: 'flex',
    alignItems: 'center',
    '& div': {
      color: '#ffffff',
      display: 'inline-block',
    },
  },
  hover: {
    '& div': {
      color: '#fdd835',
    },
  },
})

const changeLocale = (locale, replace) => {
  cookie.set('locale', locale)

  if (replace) {
    location.replace(`${location.origin}/${locale}/${location.hash}`)
  } else {
    window.location.reload()
  }
}

const Locale = ({ locale = '', replace, hover, onMouseEnter, onMouseLeave }) => (
  <div
    className={styles({ hover })}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    onClick={changeLocale.bind(this, locale, replace)}
  >
    <Flag locale={locale} />
    <div>{locale.toUpperCase()}</div>
  </div>
)

export default connect([hoverState], Locale)
