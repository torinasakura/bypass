import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: 0,
    outline: 0,
    display: 'inline-block',
    textAlign: 'center',
    cursor: 'pointer',
    transition: 'all 0.3s',
    color: '#ffffff',
    padding: '10px 20px',
    fontSize: '18px',
    fontFamily: 'PT Sans',
    background: '#546e7a',
    margin: 0,
  },
  disabled: {
    opacity: '0.6',
  },
  fill: {
    width: '100%',
  },
  'type=white': {
    background: '#ffffff',
    color: '#000000',
    '&:hover': {
      color: '#1a1a1a',
      background: '#e6e6e6',
    },
  },
  'type=navy': {
    '&:hover': {
      background: '#3f535c',
    },
  },
  'type=light': {
    background: '#f2f2f2',
    color: '#000000',
    '&:hover': {
      color: '#1a1a1a',
      background: 'gainsboro',
    },
  },
  'type=green': {
    background: '#8bc34a',
    '&:hover': {
      background: '#71a436',
    },
  },
  'type=red': {
    background: '#b71c1c',
    '&:hover': {
      background: '#8b1515',
    },
  },
  'size=small': {
    fontSize: '14px',
  },
  'size=large': {
    padding: '14px 87px',
  },
  'size=medium': {
    padding: '12px 20px',
  },
  'size=xsmall': {
    fontSize: '14px',
    padding: '5px 10px',
  },
})

const Button = ({ disabled, type = 'navy', size, children, fill, large, onClick, ...props }) => (
  <button
    {...props}
    disabled={disabled}
    className={styles({ disabled, type, fill, large, size })}
    onClick={!disabled && onClick}
  >
    {children}
  </button>
)

export default Button
