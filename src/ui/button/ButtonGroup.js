import React, { Children, cloneElement } from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
})

const ButtonGroup = ({ value, children, onChange }) => (
  <div className={styles()}>
    {Children.map(children, child => cloneElement(child, {
      fill: true,
      type: child.props.value === value ? 'navy' : 'white',
      onClick: onChange && onChange.bind(null, child.props.value),
    }))}
  </div>
)

export default ButtonGroup
