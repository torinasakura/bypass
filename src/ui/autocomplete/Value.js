import React from 'react'
import { StyleSheet } from 'quantum'
import { CloseIcon } from '../icons'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    background: '#d3e0e5',
    fontSize: '13px',
    padding: '5px 8px',
    marginTop: '8px',
    marginRight: '8px',
    border: '1px solid #8DBAD0',
    display: 'inline-block',
    '& svg': {
      position: 'relative',
      top: '2px',
      cursor: 'pointer',
    },
  },
})

const Value = ({ children, onRemove }) => (
  <div className={styles()}>
    {children} <CloseIcon fill='#b71c1c' onClick={onRemove} />
  </div>
)

export default Value
