import React from 'react'
import { StyleSheet } from 'quantum'
import { Layer } from '../layer'
import Pointer from './Pointer'

const aligments = {
  top: 'bc-tc',
  right: 'ml-mr',
  bottom: 'tc-bc',
  left: 'mr-ml',
}

const styles = StyleSheet.create({
  self: {
    padding: '8px 12px',
    background: '#546e7a',
    position: 'relative',
    color: '#ffffff',
  },
  message: {
    fontSize: '12px',
    maxWidth: '300px',
  },
})

const Tooltip = ({ children, align = 'bottom', offset, message, onClose }) => (
  <Layer
    offset={offset}
    align={aligments[align]}
    onOutsideClick={onClose}
  >
    <div className={styles({ align, message })}>
      <Pointer align={align} />
      {children}
    </div>
  </Layer>
)

export default Tooltip
