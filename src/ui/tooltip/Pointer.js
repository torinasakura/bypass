import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    width: 0,
    height: 0,
    borderColor: 'transparent',
    borderStyle: 'solid',
  },
  'align=bottom': {
    left: '50%',
    top: '-5px',
    marginLeft: '-5px',
    borderWidth: '0 5px 5px',
    borderBottomColor: '#546e7a',
  },
  'align=top': {
    left: '50%',
    bottom: '-5px',
    marginLeft: '-5px',
    borderWidth: '5px 5px 0 5px',
    borderTopColor: '#546e7a',
  },
  'align=right': {
    top: '50%',
    left: '-5px',
    marginTop: '-5px',
    borderWidth: '5px 5px 5px 0',
    borderRightColor: '#546e7a',
  },
  'align=left': {
    top: '50%',
    right: '-5px',
    marginTop: '-5px',
    borderWidth: '5px 0 5px 5px',
    borderLeftColor: '#546e7a',
  },
})

const Pointer = ({ align = 'bottom' }) => (
  <div className={styles({ align })} />
)

export default Pointer
