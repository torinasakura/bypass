import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    width: '100%',
    border: '2px solid #a1a1a1',
    resize: 'none',
    margin: '0',
    padding: '8px 6px 10px 6px',
    fontSize: '14px',
    boxSizing: 'border-box',
  },
  gray: {
    background: '#e7e7e7',
  },
  rounded: {
    borderRadius: '5px',
  },
  disabled: {
    opacity: 0.6,
  },
})

const Textarea = ({ value, disabled, gray, rounded, onChange, ...props }) => (
  <textarea
    {...props}
    className={styles({ gray, rounded, disabled })}
    value={value}
    disabled={disabled}
    onChange={onChange}
  />
)

export default Textarea
