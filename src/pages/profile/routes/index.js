import { createViewport } from 'bypass/app/utils'
import { load, clear } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'profile',
    onEnter() {
      dispatch(load())
    },
    onLeave() {
      dispatch(clear())
    },
    getComponent(location, callback) {
      System.import('../containers/Profile').then(Profile => callback(null, createViewport(Profile)))
    },
  }]
}
