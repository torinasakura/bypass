import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { getFormatTpl, getmmss } from 'bypass/app/utils/utils'
import * as actions from '../constants'

const initialState = fromJS({
  available_search_column: [],
  provider: [],
  columns: [],
  max_csv_field: 0,
  statistic: {
    csv_format_advanced: '',
    csv_format: {
      data: [],
      delim: [],
    },
    pagination: {},
    fast_buy: {},
    search_col: [],
    check_timeout: {},
  },
  showOpts: {},
  fastBuy: {},
})

export default createReducer(initialState, {
  [actions.load]: (state, { statistic, ...result }) => {
    const csvFormat = JSON.parse(statistic.csv_format)
    const csvFormatAdvanced = getFormatTpl(csvFormat.data, csvFormat.delim)
    const checkTimeout = getmmss(statistic.check_timeout)

    const showOpts = ['show_adv', 'show_confirm', 'show_invalid'].reduce((res, key) => ({
      ...res,
      [key]: !!parseInt(statistic[key], 10),
    }), {})

    const fastBuy = ['check', 'csv', 'open', 'redirect'].reduce((res, key) => ({
      ...res,
      [key]: !!parseInt(statistic.fast_buy[key], 10),
    }), {})

    return state.merge(fromJS({
      ...result,
      showOpts,
      fastBuy,
      statistic: {
        ...statistic,
        csv_format: csvFormat,
        csv_format_advanced: csvFormatAdvanced,
        check_timeout: checkTimeout,
      },
    }))
  },

  [actions.clear]: (state) => state.merge(initialState),

  [actions.changeProvider]: (state, { provider }) => state.setIn(['statistic', 'check_provider_id'], provider),

  [actions.changeCsvFormatShared]: (state, { format }) => state.setIn(['statistic', 'csv_format_advanced'], format),

  [actions.changeShowOption]: (state, { option, value }) => state.setIn(['showOpts', option], value),

  [actions.changeFastBuyOption]: (state, { option, value }) => state.setIn(['fastBuy', option], value),

  [actions.changePagination]: (state, { value }) => state.setIn(['statistic', 'pagination', 'search'], value),

  [actions.changeCsvSettings]: (state, { fields }) => state.setIn(['statistic', 'csv_format', 'data'], fromJS(fields)),

  [actions.changeCsvDelim]: (state, { delim }) => state.setIn(['statistic', 'csv_format', 'delim'], delim),

  [actions.changeSearchCols]: (state, { cols }) => state.setIn(['statistic', 'search_col'], fromJS(cols)),
})
