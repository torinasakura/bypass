import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    textAlign: 'center',
    '& p': {
      margin: '0 0 10px',
    },
  },
  gray: {
    background: '#f0f0f0',
  },
  green: {
    background: '#eceff1',
  },
})

const Content = ({ children, padding, gray, green }) => (
  <div className={styles({ gray, green })} style={{ padding }}>
    {children}
  </div>
)

export default Content
