import React from 'react'
import { Accordion } from 'bypass/ui/accordion'
import { Title } from 'bypass/ui/title'
import Content from 'bypass/ui/content/Content'
import Provider from './provider/Desktop'
import CsvSettings from './CsvSettings/Desktop'
import CsvShared from './CsvShared/Desktop'
import Stat from './Stat'
import ShowOpts from './ShowOpts/Desktop'
import Search from './Search/Desktop'
import FastBuy from './FastBuy/Desktop'
import PerPage from './PerPage/Desktop'

const Profile = (props) => (
  <Content>
    <Title>{__i18n('NAV.PROFILE')}</Title>
    <div style={{ margin: '0 auto', width: 600 }}>
      <Accordion>
        <Provider {...props} />
        <CsvSettings {...props} />
        <CsvShared {...props} />
        <Stat {...props} />
        <ShowOpts {...props} />
        <FastBuy {...props} />
        <Search {...props} />
        <PerPage {...props} />
      </Accordion>
    </div>
  </Content>
)

export default Profile
