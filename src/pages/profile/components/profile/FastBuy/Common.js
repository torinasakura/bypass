import React from 'react'
import { List, ListItem } from 'bypass/ui/list'
import { Checkbox } from 'bypass/ui/checkbox'

const Common = ({ fastBuy, onChangeCheck, onChangeCsv, onChangeOpen, onChangeRedirect }) => (
  <List>
    <ListItem title={`${__i18n('SEARCH.FAST_BUY.SET_CHECK')}:`}>
      <strong>
        <Checkbox
          checked={fastBuy.check}
          onChange={onChangeCheck}
        />
      </strong>
    </ListItem>
    <ListItem title={`${__i18n('SEARCH.FAST_BUY.GO_ORDER')}:`}>
      <strong>
        <Checkbox
          checked={fastBuy.redirect}
          onChange={onChangeRedirect}
        />
      </strong>
    </ListItem>
    <ListItem title={`${__i18n('LANG.BUTTONS.OPEN')}:`}>
      <strong>
        <Checkbox
          checked={fastBuy.open}
          onChange={onChangeOpen}
        />
      </strong>
    </ListItem>
    <ListItem title={`${__i18n('SEARCH.FAST_BUY.SHOW_CVS')}:`}>
      <strong>
        <Checkbox
          checked={fastBuy.csv}
          onChange={onChangeCsv}
        />
      </strong>
    </ListItem>
  </List>
)

export default Common
