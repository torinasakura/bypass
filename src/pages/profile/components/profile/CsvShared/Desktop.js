/* eslint-disable max-len */
import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { Button } from 'bypass/ui/button'
import { Input } from 'bypass/ui/input'
import { ContentContainer } from '../../container'
import AvailableFields from './AvailableFields'
import MaxFields from './MaxFields'

const Desktop = ({ columns, csvFormatAdvanced, maxCsvField, onChangeCsvFormatShared, onSaveCsvFormatShared, ...props }) => (
  <Panel title={__i18n('PROFILE.CSV_OUTPUT_SHARED_LABEL')} {...props}>
    <ContentContainer padding='10px' gray>
      <Input value={csvFormatAdvanced} onChange={onChangeCsvFormatShared} />
    </ContentContainer>
    <ContentContainer padding='0 0 10px 0' gray>
      <MaxFields maxCsvField={maxCsvField} />
    </ContentContainer>
    <ContentContainer padding='10px'>
      <AvailableFields columns={columns} />
    </ContentContainer>
    <ContentContainer padding='35px' gray>
      <Button large type='lightnessNavy' disabled={!csvFormatAdvanced.length} onClick={onSaveCsvFormatShared}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Desktop
