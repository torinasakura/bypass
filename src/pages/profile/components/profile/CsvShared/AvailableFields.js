import React from 'react'

const AvailableFields = ({ align = 'left', columns }) => {
  const lastIndex = columns.length - 1

  return (
    <div style={{ textAlign: align }}>
      <p>{`${__i18n('PROFILE.AVAIL_FIELD')}:`}</p>
      <p>
        {columns.map((column, index) => (
          <b key={index}>
            {`%${column}%${index !== lastIndex ? ', ' : ''}`}
          </b>
        ))}
      </p>
    </div>
  )
}

export default AvailableFields
