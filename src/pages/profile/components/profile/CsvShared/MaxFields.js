import React from 'react'

const getHtml = maxCsvField =>
  __i18n('PROFILE.CSV_OUTPUT_SHARED_LEGEND').replace(/\$\{max_csv_field\}/ig, maxCsvField)

const MaxFields = ({ maxCsvField }) => (
  <p dangerouslySetInnerHTML={{ __html: getHtml(maxCsvField) }} />
)

export default MaxFields
