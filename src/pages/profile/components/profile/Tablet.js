import React from 'react'
import { Accordion } from 'bypass/ui/accordion'
import { Title } from 'bypass/ui/title'
import Content from 'bypass/ui/content/Content'
import Provider from './provider/Tablet'
import CsvSettings from './CsvSettings/Desktop'
import CsvShared from './CsvShared/Desktop'
import Stat from './Stat'
import FastBuy from './FastBuy/Desktop'
import ShowOpts from './ShowOpts/Desktop'
import Search from './Search/Desktop'
import PerPage from './PerPage/Tablet'

const Profile = (props) => (
  <Content>
    <Title size='medium'>{__i18n('NAV.PROFILE')}</Title>
    <div style={{ margin: '0 auto', width: 600 }}>
      <Accordion>
        <Provider {...props} />
        <CsvSettings {...props} />
        <CsvShared {...props} />
        <Stat {...props} />
        <ShowOpts {...props} />
        <FastBuy {...props} />
        <Search {...props} />
        <PerPage {...props} />
      </Accordion>
    </div>
  </Content>
)

export default Profile
