import React from 'react'

const Legend = ({ maxLength, dataLength }) => {
  const legend = __i18n('PROFILE.CSV_LEGEND_TEXT')
      .replace(/\$\{max_csv_field\}/ig, maxLength)
      .replace('${count}', dataLength)

  return (
    <span className='legend' dangerouslySetInnerHTML={{ __html: legend }} />
  )
}

export default Legend
