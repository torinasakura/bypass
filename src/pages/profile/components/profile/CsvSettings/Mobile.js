/* eslint-disable max-len */
import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { Button } from 'bypass/ui/button'
import { Input } from 'bypass/ui/input'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { TwoTowers } from 'bypass/ui/TwoTowers'
import { ContentContainer } from '../../container'
import Legend from './Legend'

const Desktop = ({ columns, csvFormat, maxCsvField, onChangeCsvSettings, onChangeCsvDelim, onSaveCsvSettings, ...props }) => (
  <Panel title={__i18n('PROFILE.CSV_OUTPUT_LABEL')} {...props}>
    <ContentContainer padding='10px' gray>
      {__i18n('PROFILE.SELECT_CSV_FIELDS')}
    </ContentContainer>
    <ContentContainer padding='10px' gray>
      <TwoTowers
        direction='row'
        available={columns}
        selected={csvFormat.data}
        maxLength={maxCsvField}
        onChange={onChangeCsvSettings}
      />
    </ContentContainer>
    <ContentContainer padding='10px 49px' gray>
      <ColumnLayout>
        <Layout>
          <label>{`${__i18n('PROFILE.DELIMETER')}:`}</label>
        </Layout>
        <Layout basis='15px' />
        <Layout>
          <Input
            fill={false}
            size='2'
            maxLength='1'
            value={csvFormat.delim}
            onChange={onChangeCsvDelim}
          />
        </Layout>
      </ColumnLayout>
    </ContentContainer>
    <ContentContainer padding='10px' green>
      <Legend
        maxLength={maxCsvField}
        dataLength={csvFormat.data.length}
      />
    </ContentContainer>
    <Button fill type='lightnessNavy' onClick={onSaveCsvSettings}>
      {__i18n('LANG.BUTTONS.SAVE')}
    </Button>
  </Panel>
)

export default Desktop
