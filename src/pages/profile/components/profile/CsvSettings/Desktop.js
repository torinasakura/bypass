/* eslint-disable max-len */
import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { Button } from 'bypass/ui/button'
import { Input } from 'bypass/ui/input'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { TwoTowers } from 'bypass/ui/TwoTowers'
import { ContentContainer } from '../../container'
import Legend from './Legend'

const Desktop = ({ columns, csvFormat, maxCsvField, onChangeCsvSettings, onChangeCsvDelim, onSaveCsvSettings, ...props }) => (
  <Panel title={__i18n('PROFILE.CSV_OUTPUT_LABEL')} {...props}>
    <ContentContainer padding='10px' gray>
      {__i18n('PROFILE.SELECT_CSV_FIELDS')}
    </ContentContainer>
    <ContentContainer padding='10px 50px' gray>
      <TwoTowers
        available={columns}
        selected={csvFormat.data}
        maxLength={maxCsvField}
        onChange={onChangeCsvSettings}
      />
    </ContentContainer>
    <ContentContainer padding='10px 49px' gray>
      <ColumnLayout>
        <Layout>
          <label>{`${__i18n('PROFILE.DELIMETER')}:`}</label>
        </Layout>
        <Layout basis='15px' />
        <Layout>
          <Input
            fill={false}
            size='2'
            maxLength='1'
            value={csvFormat.delim}
            onChange={onChangeCsvDelim}
          />
        </Layout>
      </ColumnLayout>
    </ContentContainer>
    <ContentContainer padding='10px' gray>
      <Legend
        maxLength={maxCsvField}
        dataLength={csvFormat.data.length}
      />
    </ContentContainer>
    <ContentContainer padding='35px' gray>
      <Button large type='lightnessNavy' onClick={onSaveCsvSettings}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Desktop
