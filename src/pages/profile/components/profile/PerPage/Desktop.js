import React from 'react'
import { Button } from 'bypass/ui/button'
import { Panel } from 'bypass/ui/accordion'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Select } from 'bypass/ui/select'
import { ContentContainer } from '../../container'

const Desktop = ({ pagination, onChangePagination, onSavePagination, ...props }) => (
  <Panel title={__i18n('PROFILE.ITEMS_PER_PAGE.TEXT')} {...props}>
    <ContentContainer gray padding='8px 0'>
      <ColumnLayout>
        <Layout basis='65px' />
        <Layout grow={1}>
          {`${__i18n('PROFILE.ITEMS_PER_PAGE.SEARCH_TABLE')}:`}
        </Layout>
        <Layout grow={1}>
          <Select
            options={[{ count: '30' }, { count: '50' }, { count: '100' }]}
            value={pagination.search}
            valueKey='count'
            textKey='count'
            onChange={onChangePagination}
          />
        </Layout>
      </ColumnLayout>
    </ContentContainer>
    <ContentContainer padding='35px'>
      <Button large type='lightnessNavy' onClick={onSavePagination}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Desktop
