import React from 'react'
import { Button } from 'bypass/ui/button'
import { Panel } from 'bypass/ui/accordion'
import { Select } from 'bypass/ui/select'
import { ContentContainer } from '../../container'

const Mobile = ({ pagination, onChangePagination, onSavePagination, ...props }) => (
  <Panel title={__i18n('PROFILE.ITEMS_PER_PAGE.TEXT')} {...props}>
    <ContentContainer padding='15px 0' green>
      {`${__i18n('PROFILE.ITEMS_PER_PAGE.SEARCH_TABLE')}:`}
    </ContentContainer>
    <ContentContainer padding='0 0 15px 0' green>
      <Select
        options={[{ count: '30' }, { count: '50' }, { count: '100' }]}
        value={pagination.search}
        valueKey='count'
        textKey='count'
        onChange={onChangePagination}
      />
    </ContentContainer>
    <ContentContainer>
      <Button fill type='lightnessNavy' onClick={onSavePagination}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Mobile
