import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { Button } from 'bypass/ui/button'
import { TwoTowers } from 'bypass/ui/TwoTowers'
import { ContentContainer } from '../../container'
import Legend from './Legend'

const Mobile = ({ availableSearchColumn, searchCol, onChangeSearchCols, onSaveSearchCols, ...props }) => (
  <Panel title={__i18n('PROFILE.SEARCH.TEXT')} {...props}>
    <ContentContainer padding='10px' gray>
      {__i18n('PROFILE.SEARCH.TEXT')}
    </ContentContainer>
    <ContentContainer padding='10px' gray>
      <TwoTowers
        direction='row'
        available={availableSearchColumn}
        selected={searchCol}
        maxLength={16}
        onChange={onChangeSearchCols}
      />
    </ContentContainer>
    <ContentContainer padding='10px' green>
      <Legend searchColLength={searchCol.length} />
    </ContentContainer>
    <Button
      fill
      type='lightnessNavy'
      disabled={searchCol.length < 16}
      onClick={onSaveSearchCols}
    >
        {__i18n('LANG.BUTTONS.SAVE')}
    </Button>
  </Panel>
)

export default Mobile
