import React from 'react'

const Legend = ({ searchColLength }) => {
  const legend = __i18n('PROFILE.SEARCH.LEGEND').replace('${count}', searchColLength)

  return (
    <span className='legend' dangerouslySetInnerHTML={{ __html: legend }} />
  )
}

export default Legend
