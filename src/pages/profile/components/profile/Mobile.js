import React from 'react'
import { Accordion } from 'bypass/ui/accordion'
import { Title } from 'bypass/ui/title'
import Content from 'bypass/ui/content/Content'
import Provider from './provider/Mobile'
import CsvSettings from './CsvSettings/Mobile'
import CsvShared from './CsvShared/Mobile'
import Stat from './Stat'
import ShowOpts from './ShowOpts/Mobile'
import Search from './Search/Mobile'
import FastBuy from './FastBuy/Mobile'
import PerPage from './PerPage/Mobile'

const Profile = (props) => (
  <Content>
    <Title>{__i18n('NAV.PROFILE')}</Title>
    <Accordion>
      <Provider {...props} />
      <CsvSettings {...props} />
      <CsvShared {...props} />
      <Stat {...props} />
      <ShowOpts {...props} />
      <FastBuy {...props} />
      <Search {...props} />
      <PerPage {...props} />
    </Accordion>
  </Content>
)

export default Profile
