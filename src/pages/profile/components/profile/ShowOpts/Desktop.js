import React from 'react'
import { Button } from 'bypass/ui/button'
import { Panel } from 'bypass/ui/accordion'
import { ContentContainer } from '../../container'
import Common from './Common'

const Desktop = ({ onSaveShowOptions, ...props }) => (
  <Panel title={__i18n('PROFILE.SHOW_OPTS.TEXT')} {...props}>
    <Common {...props} />
    <ContentContainer padding='35px'>
      <Button large type='lightnessNavy' onClick={onSaveShowOptions}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Desktop
