import React from 'react'
import { Button } from 'bypass/ui/button'
import { Panel } from 'bypass/ui/accordion'
import Common from './Common'

const Mobile = ({ onSaveShowOptions, ...props }) => (
  <Panel title={__i18n('PROFILE.SHOW_OPTS.TEXT')} {...props}>
    <Common {...props} />
    <Button fill type='lightnessNavy' onClick={onSaveShowOptions}>
      {__i18n('LANG.BUTTONS.SAVE')}
    </Button>
  </Panel>
)

export default Mobile
