import React from 'react'
import { List, ListItem } from 'bypass/ui/list'
import { Checkbox } from 'bypass/ui/checkbox'

const Common = ({ showOpts, onChangeShowAdv, onChangeShowConfirm, onChangeShowInvalid }) => (
  <List>
    <ListItem title={`${__i18n('PROFILE.SHOW_OPTS.SHOW_INVALID')}:`}>
      <strong>
        <Checkbox
          checked={showOpts.show_invalid}
          onChange={onChangeShowInvalid}
        />
      </strong>
    </ListItem>
    <ListItem title={`${__i18n('PROFILE.SHOW_OPTS.SHOW_CONFIRM')}:`}>
      <strong>
        <Checkbox
          checked={showOpts.show_confirm}
          onChange={onChangeShowConfirm}
        />
      </strong>
    </ListItem>
    <ListItem title={`${__i18n('PROFILE.SHOW_OPTS.SHOW_ADV')}:`}>
      <strong>
        <Checkbox
          checked={showOpts.show_adv}
          onChange={onChangeShowAdv}
        />
      </strong>
    </ListItem>
  </List>
)

export default Common
