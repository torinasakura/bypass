import React from 'react'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Panel } from 'bypass/ui/accordion'
import { Select } from 'bypass/ui/select'
import { Button } from 'bypass/ui/button'
import { ContentContainer } from '../../container'

const Desktop = ({ providers = [], providerId, onChangeProvider, onSaveProvider, ...props }) => (
  <Panel title={__i18n('PROFILE.CHECK_SYS_LABEL')} {...props}>
    <ContentContainer gray padding='8px 0'>
      <ColumnLayout>
        <Layout basis='65px' />
        <Layout grow={1}>
          {`${__i18n('PROFILE.SELECT_PROVIDER')}:`}
        </Layout>
        <Layout grow={1}>
          <Select
            options={providers}
            value={providerId}
            valueKey='provider_id'
            textKey={option => `${option.provider} ($${option.price})`}
            onChange={onChangeProvider}
          />
        </Layout>
      </ColumnLayout>
    </ContentContainer>
    <ContentContainer padding='35px'>
      <Button large type='lightnessNavy' onClick={onSaveProvider}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Desktop
