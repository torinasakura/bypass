import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { Select } from 'bypass/ui/select'
import { Button } from 'bypass/ui/button'
import { ContentContainer } from '../../container'

const Mobile = ({ providers = [], providerId, onChangeProvider, onSaveProvider, ...props }) => (
  <Panel title={__i18n('PROFILE.CHECK_SYS_LABEL')} {...props}>
    <ContentContainer padding='15px 0' green>
      {`${__i18n('PROFILE.SELECT_PROVIDER')}:`}
    </ContentContainer>
    <ContentContainer padding='0 0 15px 0' green>
      <Select
        options={providers}
        value={providerId}
        valueKey='provider_id'
        textKey={option => `${option.provider} ($${option.price})`}
        onChange={onChangeProvider}
      />
    </ContentContainer>
    <ContentContainer>
      <Button fill type='lightnessNavy' onClick={onSaveProvider}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </ContentContainer>
  </Panel>
)

export default Mobile
