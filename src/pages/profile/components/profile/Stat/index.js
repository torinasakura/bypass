/* eslint-disable */
import React from 'react'
import { Panel } from 'bypass/ui/accordion'
import { List, ListItem } from 'bypass/ui/list'

const Stat = ({ statistic, ...props }) => {
  return (
    <Panel title={__i18n('PROFILE.STATISTIC.TEXT')} {...props}>
      <List>
        <ListItem title={__i18n('PROFILE.STATISTIC.WASTED_AMOUNT') + ':'}>
          <strong>{statistic.profit}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.BUYED') + ':'}>
          <strong>{statistic.card_count}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.WASTED_BY_CARD') + ':'}>
          <strong>{statistic.card_sum}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.CHECKED_COUNT') + ':'}>
          <strong>{statistic.check_count}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.CHECKED_COUNT') + ':'}>
          <strong>{statistic.check_count}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.CHECK_AMOUNT') + ':'}>
          <strong>{statistic.check_sum}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.RETURNED_COUNT') + ':'}>
          <strong>{statistic.card_refund_count}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.RETURNED_AMOUNT') + ':'}>
          <strong>{statistic.card_refund_sum}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.RATING') + ':'}>
          <strong>
            {statistic.rating === 0 ? '<' + __i18n('PROFILE.STATISTIC.NOT_IN_RATING') + '>' : statistic.rating}
          </strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.STATUS') + ':'}>
          <strong>{__i18n('LANG.SERVICE.PROFILE_STATUS')[statistic.status]}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.SEARCH_LIMIT') + ':'}>
          <strong>{statistic.search_limit}</strong>
        </ListItem>
        <ListItem title={__i18n('PROFILE.STATISTIC.CHECK_TIMEOUT') + ':'}>
          <strong>
            {statistic.check_timeout.m + ' '}
            <i>
              {__i18n('PROFILE.STATISTIC.M')}
            </i>
            {' ' + statistic.check_timeout.s + ' '}
            <i>
              {__i18n('PROFILE.STATISTIC.S')}
            </i>
          </strong>
        </ListItem>
      </List>
    </Panel>
  )
}

export default Stat
