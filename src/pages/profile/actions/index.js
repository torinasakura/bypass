import React from 'react'
import { open } from 'bypass/app/actions/modal'
import * as actions from '../constants'

const toRemoteBooleanValue = value => (value ? 1 : 0)

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('profile')

    return dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}

export function changeProvider(provider) {
  return {
    type: actions.changeProvider,
    provider,
  }
}

export function saveProvider() {
  return async (dispatch, getState, { post }) => {
    const profile = getState().profile

    const body = {
      type: 'provider',
      provider_id: profile.getIn(['statistic', 'check_provider_id']),
    }

    const { result } = await post('profile/save', { body })

    const provider = profile.get('provider')
                            .find(item => item.get('provider_id') === result.check_provider_id)
                            .get('provider')

    const props = {
      header: __i18n('LANG.SERVICE.CHECK_SYSTEM'),
      body: (
        <span>
          {__i18n('LANG.SERVICE.CHECK_SYSTEM_CHANGE_SUCCESS')}
          <strong>
            {provider}
          </strong>
        </span>
      ),
    }

    dispatch(open('common/Message', props))
  }
}

export function changeCsvFormatShared(format) {
  return {
    type: actions.changeCsvFormatShared,
    format,
  }
}

export function saveCsvFormatShared() {
  return async (dispatch, getState, { post }) => {
    const profile = getState().profile

    const body = {
      type: 'csv',
      mode: 2,
      data: profile.getIn(['statistic', 'csv_format_advanced']),
    }

    const { error } = await post('profile/save', { body })

    if (error) {
      dispatch(open('common/Message', {
        header: __i18n('LANG.ERRORS.INCORRECT_FORMAT'),
        body: __i18n('LANG.ERRORS.ERROR'),
      }))
    } else {
      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.CSV_FORMAT'),
        body: __i18n('LANG.SERVICE.CSV_CHANGE_SUCCESS'),
      }))
    }
  }
}

export function changeShowOption(option, value) {
  return {
    type: actions.changeShowOption,
    option,
    value,
  }
}

export function saveShowOptions() {
  return async (dispatch, getState, { post }) => {
    const showOpts = getState().profile.get('showOpts')

    const body = {
      type: 'show',
      show_adv: toRemoteBooleanValue(showOpts.get('show_adv')),
      show_confirm: toRemoteBooleanValue(showOpts.get('show_confirm')),
      show_invalid: toRemoteBooleanValue(showOpts.get('show_invalid')),
    }

    await post('profile/save', { body })

    dispatch(open('common/Message', {
      header: __i18n('LANG.SERVICE.VIEW_OPTIONS'),
      body: __i18n('LANG.SERVICE.VIEW_OPTIONS_CHANGE_SUCCESS'),
    }))
  }
}

export function changeFastBuyOption(option, value) {
  return {
    type: actions.changeFastBuyOption,
    option,
    value,
  }
}

export function saveFastBuy() {
  return async (dispatch, getState, { post }) => {
    const options = getState().profile.get('fastBuy').toJS()

    const body = {
      type: 'fast_buy',
      format: JSON.stringify(options),
    }

    await post('profile/save', { body })

    dispatch(open('common/Message', {
      header: __i18n('LANG.SERVICE.VIEW_OPTIONS'),
      body: __i18n('LANG.SERVICE.VIEW_OPTIONS_CHANGE_SUCCESS'),
    }))
  }
}

export function changePagination(value) {
  return {
    type: actions.changePagination,
    value,
  }
}

export function savePagination() {
  return async (dispatch, getState, { post }) => {
    const search = getState().profile.getIn(['statistic', 'pagination', 'search'])

    const body = {
      type: 'pagination',
      search,
    }

    await post('profile/save', { body })

    dispatch(open('common/Message', {
      header: __i18n('LANG.SERVICE.VIEW_OPTIONS'),
      body: __i18n('LANG.SERVICE.VIEW_OPTIONS_CHANGE_SUCCESS'),
    }))
  }
}

export function changeCsvSettings(fields) {
  return {
    type: actions.changeCsvSettings,
    fields,
  }
}

export function changeCsvDelim(delim) {
  return {
    type: actions.changeCsvDelim,
    delim,
  }
}

export function saveCsvSettings() {
  return async (dispatch, getState, { post }) => {
    const csvFormat = getState().profile.getIn(['statistic', 'csv_format']).toJS()

    const body = {
      type: 'csv',
      mode: 1,
      data: JSON.stringify(csvFormat),
    }

    const { error } = await post('profile/save', { body })

    if (error) {
      dispatch(open('common/Message', {
        header: __i18n('LANG.ERRORS.INCORRECT_FORMAT'),
        body: __i18n('LANG.ERRORS.ERROR'),
      }))
    } else {
      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.CSV_FORMAT'),
        body: __i18n('LANG.SERVICE.CSV_CHANGE_SUCCESS'),
      }))
    }
  }
}

export function changeSearchCols(cols) {
  return {
    type: actions.changeSearchCols,
    cols,
  }
}

export function saveSearchCols() {
  return async (dispatch, getState, { post }) => {
    const searchCols = getState().profile.getIn(['statistic', 'search_col']).toJS()

    const body = {
      type: 'search_col',
      mode: 1,
      format: JSON.stringify(searchCols),
    }

    const { error } = await post('profile/save', { body })

    if (error) {
      dispatch(open('common/Message', {
        header: __i18n('LANG.ERRORS.CHECK_CORRECT'),
        body: __i18n('LANG.ERRORS.ERROR'),
      }))
    } else {
      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.SEARCH_OPTIONS'),
        body: __i18n('LANG.SERVICE.SEARCH_OPTIONS_CHANGE_SUCCESS'),
      }))
    }
  }
}
