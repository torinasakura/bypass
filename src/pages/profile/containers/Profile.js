import { connect } from 'react-redux'
import * as Profile from '../components/profile'
import {
  changeProvider, saveProvider, changeCsvFormatShared,
  saveCsvFormatShared, changeShowOption, saveShowOptions,
  changeFastBuyOption, saveFastBuy, changePagination, savePagination,
  changeCsvSettings, changeCsvDelim, saveCsvSettings,
  changeSearchCols, saveSearchCols,
} from '../actions'

const connector = connect(
  state => ({
    providers: state.profile.get('provider').toJS(),
    providerId: state.profile.getIn(['statistic', 'check_provider_id']),
    columns: state.profile.get('columns').toJS(),
    csvFormat: state.profile.getIn(['statistic', 'csv_format']).toJS(),
    csvFormatAdvanced: state.profile.getIn(['statistic', 'csv_format_advanced']),
    maxCsvField: state.profile.get('max_csv_field'),
    statistic: state.profile.get('statistic').toJS(),
    showOpts: state.profile.get('showOpts').toJS(),
    fastBuy: state.profile.get('fastBuy').toJS(),
    pagination: state.profile.getIn(['statistic', 'pagination']).toJS(),
    availableSearchColumn: state.profile.get('available_search_column').toJS(),
    searchCol: state.profile.getIn(['statistic', 'search_col']).toJS(),
  }),
  dispatch => ({
    onChangeProvider: ({ target }) => {
      dispatch(changeProvider(target.value))
    },
    onSaveProvider: () => {
      dispatch(saveProvider())
    },
    onChangeCsvFormatShared: ({ target }) => {
      dispatch(changeCsvFormatShared(target.value))
    },
    onSaveCsvFormatShared: () => {
      dispatch(saveCsvFormatShared())
    },
    onChangeShowAdv: ({ target }) => {
      dispatch(changeShowOption('show_adv', target.checked))
    },
    onChangeShowConfirm: ({ target }) => {
      dispatch(changeShowOption('show_confirm', target.checked))
    },
    onChangeShowInvalid: ({ target }) => {
      dispatch(changeShowOption('show_invalid', target.checked))
    },
    onSaveShowOptions: () => {
      dispatch(saveShowOptions())
    },
    onChangeCheck: ({ target }) => {
      dispatch(changeFastBuyOption('check', target.checked))
    },
    onChangeCsv: ({ target }) => {
      dispatch(changeFastBuyOption('csv', target.checked))
    },
    onChangeOpen: ({ target }) => {
      dispatch(changeFastBuyOption('open', target.checked))
    },
    onChangeRedirect: ({ target }) => {
      dispatch(changeFastBuyOption('redirect', target.checked))
    },
    onSaveFastBuy: () => {
      dispatch(saveFastBuy())
    },
    onChangePagination: ({ target }) => {
      dispatch(changePagination(target.value))
    },
    onSavePagination: () => {
      dispatch(savePagination())
    },
    onChangeCsvSettings: fields => {
      dispatch(changeCsvSettings(fields))
    },
    onChangeCsvDelim: ({ target }) => {
      dispatch(changeCsvDelim(target.value))
    },
    onSaveCsvSettings: () => {
      dispatch(saveCsvSettings())
    },
    onChangeSearchCols: cols => {
      dispatch(changeSearchCols(cols))
    },
    onSaveSearchCols: () => {
      dispatch(saveSearchCols())
    },
  }),
)

export const Desktop = connector(Profile.Desktop)

export const Tablet = connector(Profile.Tablet)

export const Mobile = connector(Profile.Mobile)
