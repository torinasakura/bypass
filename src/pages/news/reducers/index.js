import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { actions } from './../actions'

const initialState = fromJS({
  news: [],
  offset: 0,
  perpage: 40,
  total: 0,
  detail: {
    news: {
      add_time: 0,
      news: '',
      news_id: '',
      subject: '',
      type: '',
    },
    vote: {
      n: 0,
      p: 0,
    },
  },
})

export default createReducer(initialState, {
  [actions.load]: (state, { news, offset, perpage, total }) => state.merge({
    news: state.get('news').concat(fromJS(news)),
    total: parseInt(total, 10),
    offset,
    perpage,
  }),

  [actions.clear]: (state) => state.merge(initialState),

  [actions.loadDetail]: (state, { news, vote }) => {
    const listIndex = state.get('news').findIndex(item => item.get('news_id') === news.news_id)

    if (listIndex !== -1) {
      return state.set('detail', fromJS({ news, vote }))
                  .setIn(['news', listIndex, 'cust_id'], '999')
    }

    return state.set('detail', fromJS({ news, vote }))
  },

  [actions.vote]: (state, vote) => state.setIn(['detail', 'vote'], fromJS(vote)),
})
