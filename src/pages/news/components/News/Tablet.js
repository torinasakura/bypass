import React from 'react'
import { Column } from 'bypass/ui/table'
import { Title } from 'bypass/ui/title'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { Container } from 'bypass/ui/page'
import { type, time, subject, detail } from '../column'
import List from './List'
import Toolbar from '../toolbar/Toolbar'

const Tablet = ({ total, news, onLoad, onRowClick, onMarkAsProcessed }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('NAV.NEWS')}
          </Title>
        </Layout>
        <Layout grow={1} shrink={1}>
          <List
            news={news}
            total={total}
            onLoad={onLoad}
            onRowClick={onRowClick}
          >
            <Column {...type} />
            <Column {...time} />
            <Column {...subject} />
            <Column {...detail} />
          </List>
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <Toolbar onMarkAsProcessed={onMarkAsProcessed} />
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
