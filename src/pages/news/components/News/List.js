import React from 'react'
import { Table } from 'bypass/ui/table'

const List = ({ children, total, news, rowHeight, onLoad, onRowClick }) => (
  <Table
    list={news}
    total={total}
    headerHeight={0}
    rowHeight={rowHeight}
    onLoad={onLoad}
    onRowClick={onRowClick}
  >
    {children}
  </Table>
)

export default List
