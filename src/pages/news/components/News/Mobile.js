import React from 'react'
import { Column } from 'bypass/ui/table'
import { Title } from 'bypass/ui/title'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { Container } from 'bypass/ui/page'
import { type, timeAndSubject, detail } from '../column'
import List from './List'
import Toolbar from '../toolbar/Toolbar'

const Mobile = ({ total, news, onLoad, onRowClick, onMarkAsProcessed }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small'>
            {__i18n('NAV.NEWS')}
          </Title>
        </Layout>
        <Layout grow={1} shrink={1}>
          <List
            news={news}
            total={total}
            rowHeight={50}
            onLoad={onLoad}
            onRowClick={onRowClick}
          >
            <Column {...type} />
            <Column {...timeAndSubject} />
            <Column {...detail} />
          </List>
        </Layout>
        <Layout>
          <Toolbar onMarkAsProcessed={onMarkAsProcessed} />
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
