import React from 'react'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { Container } from 'bypass/ui/page'
import { Header } from './header'
import { Body } from './body'
import { Toolbar } from './toolbar'
import Content from './Content'
import Back from './Back'

const Mobile = ({ news, vote, onVote, onUnvote }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout scrollY grow={1} shrink={1}>
          <Content>
            <Back small />
            <Header
              subject={news.subject}
              time={news.add_time}
            />
            <Body {...news} />
          </Content>
        </Layout>
        <Layout>
          <Toolbar
            {...vote}
            fill
            size='small'
            onVote={onVote}
            onUnvote={onUnvote}
          />
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
