import React from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'bypass/ui/link'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    width: '100%',
    textAlign: 'center',
    padding: '12px',
  },
  small: {
    padding: '4px',
  },
})

const Back = ({ small }) => (
  <div className={styles({ small })}>
    <Link to='/news' large color='gray'>
      <ArrowIcon left fill='#9e9e9e' width={12} /> {__i18n('NEWS.BACK_TO_LIST')}
    </Link>
  </div>
)

export default Back
