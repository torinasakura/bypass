import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { HeartIcon, HeartBrokenIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    background: '#eceff1',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    '& svg': {
      position: 'relative',
      top: '2px',
      margin: '0 2px',
    },
  },
})

const Toolbar = ({ n = '', p = '', fill, size, onVote, onUnvote }) => (
  <div className={styles()}>
    <Button type='green' fill={fill} size={size} onClick={onVote}>
      {__i18n('NEWS.LIKE')} <HeartIcon fill='#ffffff' width={17} /> {p}
    </Button>
    <Button type='red' fill={fill} size={size} onClick={onUnvote}>
      {__i18n('NEWS.DISLIKE')} <HeartBrokenIcon fill='#ffffff' width={16} /> {n}
    </Button>
  </div>
)

export default Toolbar
