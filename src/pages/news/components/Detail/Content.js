import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    flexDirection: 'column',
    width: '100%',
    overflowX: 'hidden',
  },
})

const Content = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Content
