import React from 'react'
import { StyleSheet } from 'quantum'
import { timeConverter } from 'bypass/app/utils/utils'
import Title from './Title'
import Time from './Time'

const styles = StyleSheet.create({
  self: {
    background: '#eceff1',
    width: '100%',
    textAlign: 'center',
    padding: '5px 0 8px 0',
  },
})

const Header = ({ subject, time }) => (
  <div className={styles()}>
    <Title>{subject}</Title>
    <Time>{timeConverter(time, true, true)}</Time>
  </div>
)

export default Header
