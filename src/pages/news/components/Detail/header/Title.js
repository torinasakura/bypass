import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontSize: '24px',
    color: '#000000',
  },
})

const Title = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Title
