import React from 'react'
import { StyleSheet } from 'quantum'
import { ClockIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
    color: '#9e9e9e',
    marginTop: '5px',
  },
})

const Time = ({ children }) => (
  <div className={styles()}>
    <ClockIcon /> {children}
  </div>
)

export default Time
