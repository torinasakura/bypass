import React from 'react'
import { Wrapper, Container } from 'bypass/ui/page'
import { Header } from './header'
import { Body } from './body'
import { Toolbar } from './toolbar'
import Back from './Back'

const Desktop = ({ news, vote, onVote, onUnvote }) => (
  <Wrapper fill>
    <Container>
      <Back />
      <Header
        subject={news.subject}
        time={news.add_time}
      />
      <Body {...news} />
      <Toolbar
        {...vote}
        onVote={onVote}
        onUnvote={onUnvote}
      />
    </Container>
  </Wrapper>
)

export default Desktop
