import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'block',
    padding: '.3rem',
    fontSize: '17px',
    textAlign: 'center',
  },
})

const Body = ({ news }) => (
  <div
    className={styles()}
    dangerouslySetInnerHTML={{ __html: news }}
  />
)

export default Body
