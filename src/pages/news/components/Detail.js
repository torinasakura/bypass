/* eslint-disable */
import React from 'react'
import Link from 'react-router/lib/Link'
import { timeConverter } from 'bypass/app/utils/utils'
import BaseUpdate from './BaseUpdate'
import { DesktopContainer } from 'bypass/ui/page'

/**
 * @media only screen and (max-width: 667px)
 *  .NewsItemContainer
 *    .newsItemBody
 *      font-size: 13px
 */

class newsItem extends React.Component {
  onVote(vote) {
    const { news, onVote } = this.props

    if (onVote) {
      onVote(news.news_id, vote)
    }
  }

  render() {
    const { news, vote } = this.props

    return (
      <DesktopContainer>
      <article className='appContainer-woInfinity NewsItemContainer'>
        <Link className='newsItemNav' to='/news'>{__i18n('NEWS.BACK_TO_LIST')}</Link>
        <header className='newsItemHeader'>
          <h3 className='newsItemHeader-title'>{news.subject}</h3>
          <span className='newsItemHeader-date icon'>
            <span className='clock' />
            {timeConverter(news.add_time, true, true)}
          </span>
        </header>
	      <div className='newsItem'>
	      {news.type == 'base_update'
          ? <BaseUpdate loads={news.news}/>
          : <main className='newsItemBody' dangerouslySetInnerHTML={{__html: news.news}}/>
        }
		      </div>
        <footer className='newsItemFooter'>
          <a className='button-green' onClick={this.onVote.bind(this, 'p')}>
            {__i18n('NEWS.LIKE')}
            <span className='icon like'>{vote.p ? vote.p : ''}</span>
          </a>
          <a className='button-red' onClick={this.onVote.bind(this, 'n')}>
            {__i18n('NEWS.DISLIKE')}
            <span className='icon dislike'>{vote.n ? vote.n : ''}</span>
          </a>
        </footer>
      </article>
      </DesktopContainer>
    )
  }
}

export default newsItem
