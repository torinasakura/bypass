import React from 'react'
import { StyleSheet } from 'quantum'
import { PlusCircleIcon, QuestionIcon, DataIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    background: '#546e7a',
    borderTop: '1px solid #ffffff',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
})

const icons = {
  future: (
    <QuestionIcon width={17} fill='#f5f5f5' />
  ),
  base_update: (
    <DataIcon height={17} fill='#f5f5f5' />
  ),
  soft_update: (
    <PlusCircleIcon width={17} fill='#f5f5f5' />
  ),
}

const type = {
  width: '40px',
  dataKey: 'type',
  flexShrink: 0,
  cellRenderer: cellData => (
    <div className={styles()}>
      {icons[cellData]}
    </div>
  ),
}

export default type
