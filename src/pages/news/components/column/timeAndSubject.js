import React from 'react'
import { StyleSheet } from 'quantum'
import { formatDate } from 'bypass/app/utils/utils'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #f5f5f5',
    borderLeft: 0,
    borderRight: 0,
    fontSize: '17px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: '0 12px',
    cursor: 'pointer',
    background: '#e7ecf0',
    '& div:first-child': {
      fontSize: '14px',
      color: '#000000',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      width: '100%',
    },
    '& div:last-child': {
      fontSize: '13px',
      color: '#9e9e9e',
    },
  },
  viewed: {
    background: '#ffffff',
  },
})

export const timeAndSubject = {
  width: 'auto',
  flexGrow: 1,
  dataKey: 'timeAndSubject',
  cellRenderer: (cellData, dataKey, rowData) => (
    <div className={styles({ viewed: rowData.get('cust_id') })}>
      <div>
        {rowData.get('subject')}
      </div>
      <div>
        {formatDate(new Date(rowData.get('add_time') * 1000), true, true)}
      </div>
    </div>
  ),
}

export default timeAndSubject
