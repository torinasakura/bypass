import type from './type'
import time from './time'
import subject from './subject'
import timeAndSubject from './timeAndSubject'
import detail from './detail'

export {
  type,
  time,
  subject,
  timeAndSubject,
  detail,
}
