import React from 'react'
import { StyleSheet } from 'quantum'
import { formatDate } from 'bypass/app/utils/utils'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #f5f5f5',
    borderLeft: 0,
    color: '#546e7a',
    fontSize: '16px',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: '0 12px',
    cursor: 'pointer',
    background: '#e7ecf0',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  viewed: {
    background: '#ffffff',
  },
})

const time = {
  width: '200px',
  dataKey: 'add_time',
  cellRenderer: (cellData, dataKey, rowData) => (
    <div className={styles({ viewed: rowData.get('cust_id') })}>
      {formatDate(new Date(cellData * 1000), true, true)}
    </div>
  ),
}

export default time
