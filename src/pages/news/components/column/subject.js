import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #f5f5f5',
    borderLeft: 0,
    borderRight: 0,
    fontSize: '17px',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: '0 12px',
    cursor: 'pointer',
    background: '#e7ecf0',
  },
  viewed: {
    background: '#ffffff',
  },
})

const subject = {
  flexGrow: 1,
  width: 'auto',
  dataKey: 'subject',
  cellRenderer: (cellData, dataKey, rowData) => (
    <div className={styles({ viewed: rowData.get('cust_id') })}>
      {cellData}
    </div>
  ),
}

export default subject
