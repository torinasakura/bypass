import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    borderTop: '1px solid #ffffff',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    cursor: 'pointer',
    border: '1px solid #f5f5f5',
    borderLeft: 0,
    background: '#e7ecf0',
  },
  viewed: {
    background: '#ffffff',
  },
})

const detail = {
  width: '18px',
  dataKey: 'detail',
  flexShrink: 0,
  cellRenderer: (cellData, dataKey, rowData) => (
    <div className={styles({ viewed: rowData.get('cust_id') })}>
      <ArrowIcon right width={11} />
    </div>
  ),
}

export default detail
