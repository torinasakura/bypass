import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    textAlign: 'right',
  },
  offset: {
    padding: '20px 0',
  },
})

const Toolbar = ({ offset = false, onMarkAsProcessed }) => (
  <div className={styles({ offset })}>
    <Button size='small' type='light' fill={!offset} onClick={onMarkAsProcessed}>
      {__i18n('NEWS.MARK_ALL')}
    </Button>
  </div>
)

export default Toolbar
