import { connect } from 'react-redux'
import { load, show, markAllNewsAsProcessed } from '../actions'
import * as News from '../components/News'

const connector = connect(
  state => ({
    total: state.news.get('total'),
    news: state.news.get('news'),
  }),
  dispatch => ({
    onLoad: (offset) => {
      dispatch(load(offset))
    },
    onRowClick: (news) => {
      dispatch(show(news))
    },
    onMarkAsProcessed: () => {
      dispatch(markAllNewsAsProcessed())
    },
  }),
)

export const Desktop = connector(News.Desktop)

export const Tablet = connector(News.Tablet)

export const Mobile = connector(News.Mobile)
