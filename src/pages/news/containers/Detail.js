import { connect } from 'react-redux'
import { vote } from '../actions'
import * as Detail from '../components/Detail/index'

const connector = connect(
  state => ({
    news: state.news.getIn(['detail', 'news']).toJS(),
    vote: state.news.getIn(['detail', 'vote']).toJS(),
  }),
  dispatch => ({
    onVote: () => {
      dispatch(vote('p'))
    },
    onUnvote: () => {
      dispatch(vote('n'))
    },
  })
)

export const Desktop = connector(Detail.Desktop)

export const Tablet = connector(Detail.Tablet)

export const Mobile = connector(Detail.Mobile)
