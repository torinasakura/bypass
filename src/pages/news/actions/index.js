import { push } from 'redux-router'

export const actions = {
  load: '@@bypass/news/LOAD',
  clear: '@@bypass/news/CLEAR',
  loadDetail: '@@bypass/news/LOAD_DETAIL',
  vote: '@@bypass/news/VOTE',
}

export function load(offset = 0) {
  return async (dispatch, getState, { get }) => {
    const perpage = getState().news.get('perpage')

    const { result } = await get('news', { body: { offset, perpage }, loader: offset === 0 })

    return dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}

export function show(news) {
  return push(`/news/${news.get('news_id')}`)
}

export function loadDetail(id) {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('news/view', { body: { news_id: id } })

    return dispatch({
      type: actions.loadDetail,
      ...result,
    })
  }
}

export function vote(value) {
  return async (dispatch, getState, { get }) => {
    const id = getState().news.getIn(['detail', 'news', 'news_id'])

    const { result } = await get('news/vote', { body: { news_id: id, vote: value } })

    return dispatch({
      type: actions.vote,
      ...result,
    })
  }
}

export function markAllNewsAsProcessed() {
  return async (dispatch, getState, { get }) => {
    await get('/news/viewed', { loader: false })

    dispatch(clear())
    dispatch(load())
  }
}
