import { createViewport } from 'bypass/app/utils'
import { load, loadDetail } from '../actions'


export default function getRoutes({ dispatch, getState }) {
  return [{
    path: 'news',
    onEnter() {
      if (!getState().news.get('news').size) {
        dispatch(load())
      }
    },
    getComponent(location, callback) {
      System.import('../containers/News')
            .then(News => callback(null, createViewport(News)))
    },
  },
  {
    path: 'news/:id',
    onEnter({ params }) {
      dispatch(loadDetail(params.id))
    },
    getComponent(location, callback) {
      System.import('../containers/Detail')
            .then(Detail => callback(null, createViewport(Detail)))
    },
  }]
}
