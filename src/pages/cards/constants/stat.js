export const loadStatic = '@@bypass/cards/stat/LOAD_STATIC'
export const loadDynamic = '@@bypass/cards/stat/LOAD_DYNAMIC'
export const selectBase = '@@bypass/cards/stat/SELECT_BASE'
export const selectUpdate = '@@bypass/cards/stat/SELECT_UPDATE'
export const changePeriod = '@@bypass/cards/stat/CHANGE_PERIOD'
export const show = '@@bypass/cards/stat/SHOW'
export const toggleControls = '@@bypass/cards/stat/TOGGLE_CONTROLS'
export const clearSelection = '@@bypass/cards/stat/CLEAR_SELECTION'
