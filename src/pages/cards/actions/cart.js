/* eslint-disable no-use-before-define */
import config from 'bypass/app/config'
import { open, close } from 'bypass/app/actions/modal'
import { runCheck, showCsv, checkAndShowCsv } from './order'
import * as actions from '../constants/cart'

const buyMessages = {
  0: __i18n('LANG.ERRORS.CARDS.NOTHING_BUYED'),
  '-2': __i18n('LANG.ERRORS.CARDS.BALANCE_ISSUE'),
  '-3': __i18n('LANG.ERRORS.CARDS.ALREADY_BUYED'),
}

export function loadStat(loader = true) {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('card/cart/stat', { loader })

    dispatch({
      type: actions.loadStat,
      ...result,
    })
  }
}

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('card/cart')

    dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function select(row) {
  return {
    type: actions.select,
    row,
  }
}

export function selectAll() {
  return {
    type: actions.selectAll,
  }
}

export function unselect() {
  return {
    type: actions.unselect,
  }
}

export function remove() {
  return async (dispatch, getState, { post }) => {
    const cards = getState().cards.cart.get('cards')
                                       .filter(card => card.get('selected'))
                                       .map(card => card.get('card_id'))
                                       .toJS()

    const body = {
      action: 'delete',
      card_id: cards,
    }

    await post('card/cart/edit', { body })

    dispatch(load())
  }
}

export function buy(cards = [], options = {}) {
  return async (dispatch, getState, { post }) => {
    const selected = getState().cards.cart.get('cards')
                                       .filter(card => card.get('selected'))
                                       .map(card => card.get('card_id'))
                                       .toJS()

    const body = {
      card_id: cards && cards.length ? cards.map(card => card.card_id) : selected,
    }

    const { result } = await post('card/buy', { body, hideLoader: false })

    dispatch(waitBuyById(result, cards, options))
  }
}

export function fastBuy() {
  return async (dispatch, getState) => {
    const purchase = getState().cards.cart.get('toPurchase')
    const options = getState().cards.cart.get('fast_buy').toJS()

    dispatch(open('cards/FastBuy', {
      ...purchase,
      options,
    }))
  }
}

export function waitBuyById(buyId, cards, options) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('card/buy', { body: { buy_id: buyId }, hideLoader: false })

    if (config.cart.error_codes.includes(result.count)) {
      dispatch(load())
      dispatch(loadStat())

      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.MESSAGE'),
        body: buyMessages[result.count],
      }))

      return
    }

    if (result.count == -1) { // eslint-disable-line eqeqeq
      setTimeout(() => {
        dispatch(waitBuyById(buyId, cards, options))
      }, config.cart.buy_interval)

      return
    }

    dispatch(close())
    dispatch(load())
    dispatch(loadStat())

    if (options.redirect) {
      window.location.hash = `/cards/orders/${result.order_id}`
    } else {
      window.location.hash = '/cards/orders'
    }

    if (options.check && options.csv) {
      dispatch(checkAndShowCsv(result.order_id, cards))
    } else if (options.check) {
      dispatch(runCheck(result.order_id, cards.map(card => card.card_id)))
    } else if (options.csv) {
      dispatch(showCsv(cards))
    }
  }
}
