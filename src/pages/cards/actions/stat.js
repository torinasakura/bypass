import { open } from 'bypass/app/actions/modal'
import * as actions from '../constants/stat'

export function loadStatic() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('statistic')

    dispatch({
      type: actions.loadStatic,
      ...result,
    })
  }
}

export function openDetail(type) {
  return open(`cards/${type}`)
}

export function loadDynamic() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('statistic/object_list')

    dispatch({
      type: actions.loadDynamic,
      ...result,
    })
  }
}

export function toggleControls() {
  return {
    type: actions.toggleControls,
  }
}

export function clearSelection() {
  return {
    type: actions.clearSelection,
  }
}

export function selectBase(items) {
  return {
    type: actions.selectBase,
    items,
  }
}

export function selectUpdate(item) {
  return {
    type: actions.selectUpdate,
    item,
  }
}

export function changePeriod(period) {
  return {
    type: actions.changePeriod,
    period,
  }
}

export function show() {
  return async (dispatch, getState, { get }) => {
    const { period, update } = getState().cards.stat.get('dynamic').toJS()

    const body = {
      period,
      load_id: update.filter(item => item.selected).map(item => item.load_id),
    }

    const { result } = await get('statistic/dynamic', { body })

    dispatch({
      type: actions.show,
      result,
    })
  }
}
