/* eslint-disable no-use-before-define */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
import { open, close } from 'bypass/app/actions/modal'
import * as actions from '../constants/order'

export function load(orderId) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('card/order', { body: { order_id: orderId } })

    dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function select(row) {
  return {
    type: actions.select,
    row,
  }
}

export function selectAll() {
  return {
    type: actions.selectAll,
  }
}

export function check() {
  return async (dispatch, getState) => {
    const { show_confirm } = getState().config.options
    const orderId = getState().router.params.id
    const cards = getState().cards.order.get('cards')
                                .filter(card => card.get('selected'))
                                .map(card => card.get('card_id'))
                                .toJS()

    if (!show_confirm) {
      dispatch(runCheck(orderId, cards))
    }

    dispatch(open('common/Confirm', {
      header: __i18n('LANG.SERVICE.ORDERS_CHECK'),
      body: __i18n('LANG.SERVICE.CHECK_ORDER_CONFIRM'),
      confirm: __i18n('LANG.BUTTONS.CHECK'),
      cancel: __i18n('LANG.BUTTONS.CANCEL'),
      onConfirm: () => dispatch(runCheck(orderId, cards)),
    }))
  }
}

export function runCheck(orderId, cards, onCloseChecking) {
  return async (dispatch, getState, { post }) => {
    const body = {
      order_id: orderId,
      card_id: cards,
    }

    await post('check/add', { body })

    dispatch({
      type: actions.checking,
      statuses: {
        checking: 0,
        checked: 0,
        unchecked: 0,
      },
    })

    const props = {}

    if (onCloseChecking) {
      props.onClose = onCloseChecking
    }

    dispatch(open('cards/Checking', props))

    dispatch(waitCheck(body))
  }
}

export function waitCheck(params) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('check/view', { body: params, loader: false })

    const statuses = result.reduce((data, card) => {
      if (card.returned == 0) data.unchecked++
      if (card.returned == 1) data.checking++
      if (card.returned == 2 || card.returned == 3) data.checked++

      return data
    }, { checking: 0, checked: 0, unchecked: 0 })

    dispatch({
      type: actions.checking,
      statuses,
    })

    const complete = result.every(card => [2, 3, 4].includes(+card.returned))

    if (!complete) {
      setTimeout(() => {
        dispatch(waitCheck(params))
      }, 15000)
    }
  }
}

export function showCsv(cards) {
  return async (dispatch, getState) => {
    const selected = getState().cards.order.get('cards')
                                  .filter(card => card.get('selected'))
                                  .toJS()

    dispatch(open('cards/OrderCsv', { cards: cards || selected }))
  }
}

export function checkAndShowCsv(orderId, cards) {
  return async dispatch => {
    const cardIds = cards.map(card => card.card_id)
    const runAfterCheck = () => dispatch(showCsv(cards))

    dispatch(runCheck(orderId, cardIds, runAfterCheck))
  }
}

export function editComment() {
  return async (dispatch, getState) => {
    const orderId = getState().router.params.id
    const cards = getState().cards.order.get('cards').filter(card => card.get('selected')).toJS()
    const [card] = cards.filter(({ comment }) => comment)

    dispatch(open('common/EditComment', {
      comment: card ? card.comment : '',
      onSave: comment => {
        dispatch(saveComment(comment, cards, orderId))
        dispatch(close())
      },
    }))
  }
}

export function saveComment(comment, cards, orderId) {
  return async (dispatch, getState, { post }) => {
    const body = {
      card_id: cards.map(card => card.card_id),
      order_id: orderId,
      comment,
    }

    await post('card/edit', { body })

    dispatch(load(orderId))
  }
}

export function openCard() {
  return async (dispatch, getState, { post }) => {
    const orderId = getState().router.params.id
    const cards = getState().cards.order.get('cards')
                                .filter(card => card.get('selected'))
                                .map(card => card.get('card_id'))
                                .toJS()

    const { result } = await post('card/order/open', { body: { card_id: cards } })

    if (result == 0) {
      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.MESSAGE'),
        body: __i18n('LANG.SERVICE.NULL_CARDS'),
      }))
    } else if (result > 0) {
      dispatch(load(orderId))
    }
  }
}
