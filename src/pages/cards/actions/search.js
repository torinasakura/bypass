/* eslint-disable no-use-before-define */
/* eslint-disable no-shadow */
import { Map } from 'immutable'
import config from 'bypass/app/config'
import { formatSearch, reduceSearch } from 'bypass/app/utils/utils'
import { open, close } from 'bypass/app/actions/modal'
import { hide } from 'bypass/app/actions/loader'
import * as actions from '../constants'
import { loadStat } from './cart'

export function presearch() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('card/presearch', { loader: false })

    dispatch({
      type: actions.presearch,
      presearch: result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}

export function showFilter(type = '') {
  return open(`cards/${type.substr(0, 1).toUpperCase()}${type.substr(1)}`)
}

export function showSearch() {
  return {
    type: actions.showSearch,
  }
}

export function hideSearch() {
  return {
    type: actions.hideSearch,
  }
}

export function clearSearch() {
  return {
    type: actions.clearSearch,
  }
}

export function changeFilter(filter, value) {
  return {
    type: actions.changeFilter,
    filter,
    value,
  }
}

export function inverseFilter(filter, value) {
  return {
    type: actions.inverseFilter,
    filter,
    value,
  }
}

export function searchById(searchId) {
  return async (dispatch, getState, { post }) => {
    const { perpage } = getState().cards.search.get('result').toJS()
    const { order, direction } = getState().cards.search.getIn(['search', 'sort'], new Map()).toJS()

    const body = {
      search_id: searchId,
      perpage,
      order,
      direction,
    }

    const { result } = await post('card/search', { body, hideLoader: false })

    if (result.count === -1) {
      setTimeout(() => dispatch(searchById(searchId)), config.search.request_interval)
      return
    } else if (result.count === -2) {
      dispatch(search())
      return
    }

    dispatch(hide())

    dispatch({
      type: actions.search,
      search: result,
    })
  }
}

export function search() {
  return async (dispatch, getState, { post }) => {
    const params = getState().cards.search.get('search').toJS()

    const body = {
      search_string: JSON.stringify(formatSearch(reduceSearch(params))),
    }

    const { result } = await post('card/search', { body, hideLoader: false })

    setTimeout(() => dispatch(searchById(result)), config.search.request_interval)
  }
}

export function load(offset = 0) {
  return async (dispatch, getState, { post }) => {
    const { search_id, perpage } = getState().cards.search.get('result').toJS()
    const { order, direction } = getState().cards.search.getIn(['search', 'sort'], new Map()).toJS()

    const body = {
      search_id,
      perpage,
      offset,
      order,
      direction,
    }

    const { result } = await post('card/search', { body })

    dispatch({
      type: actions.loadSearch,
      search: result,
    })
  }
}

export function loadPage(offset, perpage) {
  return async (dispatch, getState, { post }) => {
    const { search_id } = getState().cards.search.get('result').toJS()
    const { order, direction } = getState().cards.search.getIn(['search', 'sort'], new Map()).toJS()

    const body = {
      search_id,
      perpage,
      offset,
      order,
      direction,
    }

    const { result } = await post('card/search', { body })

    dispatch({
      type: actions.loadSearchPage,
      search: result,
    })
  }
}

export function changeSort(order, direction) {
  return async (dispatch, getState) => {
    const { perpage } = getState().cards.search.get('result').toJS()

    dispatch(changeFilter('sortOrder', order))
    dispatch(changeFilter('sortDirection', direction))

    dispatch(loadPage(0, perpage))
  }
}

export function selectSearch() {
  return open('cards/SelectSearch')
}

export function saveSearch() {
  return async (dispatch, getState, { post }) => {
    const search = getState().cards.search.get('search').toJS()

    if (!search.search_id) {
      dispatch(open('cards/SaveSearch'))
      return
    }

    const body = {
      type: 'search',
      search_id: search.search_id,
      value: JSON.stringify(formatSearch(search, true)),
    }

    const { result } = await post('search/edit', { body })

    dispatch({
      type: actions.refreshSearch,
      result,
    })
  }
}

export function applySearch(searchId) {
  return async dispatch => {
    dispatch(close())

    dispatch({
      type: actions.applySearch,
      searchId,
    })
  }
}

export function setDefaultSearch(searchId) {
  return async (dispatch, getState, { post }) => {
    const body = {
      type: 'default',
      search_id: searchId,
      value: 1,
    }

    const { result } = await post('search/edit', { body })

    dispatch({
      type: actions.refreshSearch,
      result,
    })
  }
}

export function toggleDefaultSearch() {
  return async (dispatch, getState, { post }) => {
    const { def, search_id } = getState().cards.search.get('search').toJS()
    const value = def == '1' ? 0 : 1 // eslint-disable-line eqeqeq

    const body = {
      type: 'default',
      search_id,
      value,
    }

    await post('search/edit', { body })

    dispatch({
      type: actions.updateDefaultSearch,
      searchId: search_id,
      value,
    })
  }
}

export function removeSearch(searchId) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('search/delete', { body: { search_id: searchId } })

    dispatch({
      type: actions.refreshSearch,
      result,
    })
  }
}

export function createSearch(name, isDefault) {
  return async (dispatch, getState, { post }) => {
    const search = getState().cards.search.get('search').toJS()

    const body = {
      name,
      url: 'search/add',
      default: isDefault ? 1 : 0,
      search: JSON.stringify(formatSearch(search)),
    }

    const { result } = await post('search/add', { body })

    dispatch(open('common/Message', {
      header: __i18n('LANG.SERVICE.MESSAGE'),
      body: __i18n('LANG.SERVICE.SEARCH_SAVED'),
    }))

    dispatch({
      type: actions.createSearch,
      result,
      name,
    })

    const [created] = result.filter(item => item.search_name === name)

    if (created && created.search_id) {
      dispatch({
        type: actions.applySearch,
        searchId: created.search_id,
      })
    }
  }
}

export function select(row) {
  return {
    type: actions.selectSearchRow,
    row,
  }
}

export function selectAll() {
  return {
    type: actions.selectSearchRows,
  }
}

export function unselect() {
  return {
    type: actions.unselectCards,
  }
}

export function addCards(row) {
  return async (dispatch, getState, { post }) => {
    const { search_id, list } = getState().cards.search.get('result').toJS()

    const cards = row && row.get ? [row.get('card_id')] : list.filter(item => item.selected)
                                                   .map(item => item.card_id)

    const body = {
      search_id,
      action: 'add',
      card_id: cards,
    }

    await post('card/cart/edit', { body })

    dispatch(loadStat())
    dispatch(unselect())
  }
}

export function complete(field, query = '') {
  return async (dispatch, getState, { post }) => {
    if (query.length < 2) {
      return
    }

    const body = {
      type: field,
      value: query,
    }

    const { result } = await post('card/complete', { body, loader: false })

    dispatch({
      type: actions.complete,
      field,
      result,
    })
  }
}

export function fastBuy() {
  return async (dispatch, getState) => {
    const options = getState().cards.search.getIn(['presearch', 'fast_buy']).toJS()
    const selected = getState().cards.search.getIn(['result', 'list'])
                                            .filter(card => card.get('selected'))
                                            .toJS()

    const finalPrice = (price, addPrice = 0) => {
      if (!price) return null

      return parseFloat(price) + parseFloat(addPrice)
    }

    const purchase = selected.reduce((result, card) => ({
      count: result.count + 1,
      price: result.price + finalPrice(card.seller_price, card.add_price),
    }), { count: 0, price: 0 })

    dispatch(open('cards/FastBuy', {
      ...purchase,
      cards: selected,
      options,
    }))
  }
}
