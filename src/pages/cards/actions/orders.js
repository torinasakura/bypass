/* eslint-disable no-use-before-define */
/* eslint-disable camelcase */
import { downloadFile } from 'bypass/app/utils/utils'
import { hide } from 'bypass/app/actions/loader'
import { open, close } from 'bypass/app/actions/modal'
import config from 'bypass/app/config'
import * as actions from '../constants/orders'

export function load(offset = 0) {
  return async (dispatch, getState, { get }) => {
    const perpage = getState().tickets.get('perpage')

    const { result } = await get('card/list', { body: { offset, perpage } })

    dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function loadOrder(orderId) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('card/order', { body: { order_id: orderId } })

    dispatch({
      type: actions.loadOrder,
      ...result,
    })
  }
}

export function select(row) {
  return {
    type: actions.select,
    row,
  }
}

export function selectAll() {
  return {
    type: actions.selectAll,
  }
}

export function unselect() {
  return {
    type: actions.unselect,
  }
}

export function checkDownload(downloadId) {
  return async (dispatch, getState, { post }) => {
    const body = {
      download_id: downloadId,
    }

    const { result } = await post('card/download/view', { body, hideLoader: false })

    if (result.status != 1) { // eslint-disable-line eqeqeq
      setTimeout(() => {
        dispatch(checkDownload(downloadId))
      }, 5000)

      return
    }

    dispatch(hide())

    if (!result.url) {
      dispatch(open('common/Errors', {
        header: __i18n('LANG.ERRORS.ERROR'),
        body: __i18n('LANG.SERVICE.EMPTY_TABLE'),
      }))

      return
    }

    downloadFile(`${config.api_url}${result.url}`)
  }
}

export function download() {
  return async (dispatch, getState, { post }) => {
    const orders = getState().cards.orders.get('orders')
    const selected = orders.filter(order => order.get('selected'))
    const target = selected.size === 0 ? orders : selected

    const body = {
      order_id: target.map(order => order.get('order_id')).toJS(),
    }

    const { result } = await post('card/download/add', { body, hideLoader: false })

    if (!result) {
      dispatch(hide())
      return
    }

    dispatch(checkDownload(result))
  }
}

export function editComment() {
  return open('cards/EditOrderComment')
}

export function saveComment(comment) {
  return async (dispatch, getState, { post }) => {
    const orders = getState().cards.orders.get('orders')
    const selected = orders.filter(order => order.get('selected'))

    const body = {
      order_id: selected.map(order => order.get('order_id')).toJS(),
      comment,
    }

    const { result } = await post('card/order/edit', { body })

    if (result) {
      dispatch(close())
      dispatch(load())
    }
  }
}

export function runRemove() {
  return async (dispatch, getState, { post }) => {
    const orders = getState().cards.orders.get('orders')
    const selected = orders.filter(order => order.get('selected'))

    const body = {
      order_id: selected.map(order => order.get('order_id')).toJS(),
    }

    await post('card/delete', { body, hideLoader: false })

    dispatch(load())
  }
}

export function remove() {
  return async (dispatch, getState) => {
    const { show_confirm } = getState().config.options

    if (!show_confirm) {
      dispatch(runRemove())
    }

    dispatch(open('common/Confirm', {
      header: __i18n('LANG.SERVICE.ORDERS_DELETE'),
      body: __i18n('LANG.SERVICE.DELETE_ORDERS_CONFIRM'),
      confirm: __i18n('LANG.BUTTONS.DELETE'),
      cancel: __i18n('LANG.BUTTONS.CANCEL'),
      onConfirm: () => {
        dispatch(runRemove())
        dispatch(close())
      },
    }))
  }
}
