import { combineReducers } from 'redux'
import search from './search'
import cart from './cart'
import orders from './orders'
import order from './order'
import stat from './stat'

export default combineReducers({
  search,
  cart,
  orders,
  order,
  stat,
})
