import { fromJS } from 'immutable'
import { getCvsData } from 'bypass/app/utils/utils'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants/order'

const initialState = fromJS({
  total: 0,
  checkTimeout: 0,
  cards: [],
  checking: {
    checking: 0,
    checked: 0,
    unchecked: 0,
  },
  csvFormat: {},
  csvData: [],
})

export default createReducer(initialState, {
  [actions.load]: (state, { data, check_timeout, show, format }) => {
    const cards = show === '0' ? data.filter(card => card.returned != 3) : data // eslint-disable-line eqeqeq
    const csvFormat = JSON.parse(format)
    const csvData = getCvsData(cards, csvFormat.data)

    return state.merge(fromJS({
      checkTimeout: check_timeout,
      total: cards.length,
      csvFormat,
      csvData,
      cards,
    }))
  },

  [actions.select]: (state, { row }) => {
    const index = state.get('cards')
                       .findIndex(item => item === row)

    if (index !== -1) {
      const updated = state.setIn(['cards', index, 'selected'], !row.get('selected'))

      return updated.set(
        'hasSelected',
        updated.get('cards')
               .some(item => item.get('selected'))
      )
    }

    return state
  },

  [actions.selectAll]: state => {
    const selected = !state.get('allSelected')

    return state.set('cards', state.get('cards').map(card => card.set('selected', selected)))
                .set('allSelected', selected)
                .set('hasSelected', selected)
  },

  [actions.checking]: (state, { statuses }) => state.mergeIn(['checking'], fromJS(statuses)),
})
