import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants/cart'

const initialState = fromJS({
  stat: {
    count: 0,
    price: '0.00',
  },
  total: 0,
  cards: [],
  search_col: [],
  fast_buy: {},
  check_timeout: 0,
  selected: null,
  toPurchase: {},
})

const finalPrice = (price, addPrice = 0) => {
  if (!price) return null

  return parseFloat(price) + parseFloat(addPrice)
}

const calculateStat = (result, card) => ({
  count: result.count + 1,
  price: result.price + finalPrice(card.get('seller_price'), card.get('add_price')),
})

export default createReducer(initialState, {
  [actions.load]: (state, { data, fast_buy, search_col, check_timeout }) =>
    state.merge(fromJS({
      fast_buy,
      cards: data,
      total: data.length,
      checkTimeout: check_timeout,
      search_col: search_col.filter(column => column !== 'brand'),
      toPurchase: {
        count: 0,
        price: 0,
      },
    })),

  [actions.loadStat]: (state, { price, count }) => state.mergeIn(['stat'], fromJS({ price, count })),

  [actions.select]: (state, { row }) => {
    const index = state.get('cards')
                       .findIndex(item => item.get('card_id') === row.get('card_id'))

    if (index !== -1) {
      const updated = state.setIn(['cards', index, 'selected'], !row.get('selected'))

      const selected = updated.get('cards').filter(card => card.get('selected'))

      const toPurchase = selected.reduce(calculateStat, { count: 0, price: 0 })

      return updated.set('toPurchase', toPurchase)
                    .set('hasSelected', selected.size > 0)
    }

    return state
  },

  [actions.selectAll]: state => {
    const selected = !state.get('allSelected')
    const cards = state.get('cards').map(card => card.set('selected', selected))
    const toPurchase = selected ? cards.reduce(calculateStat, { count: 0, price: 0 }) : { count: 0, price: 0 }

    return state.set('cards', cards)
                .set('allSelected', selected)
                .set('hasSelected', selected)
                .set('toPurchase', toPurchase)
  },
})
