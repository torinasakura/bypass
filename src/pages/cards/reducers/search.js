/* eslint-disable no-param-reassign */
import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { toObject, timeConverter, parseBins, unformatSearch, SearchState } from 'bypass/app/utils/utils'
import * as actions from '../constants'

const initialState = fromJS({
  showSearch: false,
  presearch: {
    sort_column: [],
  },
  search: {
    ...SearchState,
  },
  result: {
    show: false,
    list: [],
    offset: 0,
    perpage: 50,
    total: 0,
    search_col: [],
  },
})

const getLoadText = load => [
  timeConverter(load.add_time, false, true),
  `(${load.available} ${__i18n('COM.COUNT')})`,
  (load.valid ? `(${parseFloat(load.valid).toFixed(2)}%)` : ''),
].join(' ')

const getValidText = item => [
  item.name,
  (item.valid ? `(${parseFloat(item.valid).toFixed(2)}%)` : ''),
].join(' ')

export default createReducer(initialState, {
  [actions.presearch]: (state, { presearch }) => {
    const data = Object.keys(presearch).reduce((result, key) => {
      if (key === 'search') {
        result[key] = presearch[key].map(item => {
          try {
            return {
              ...item,
              ...JSON.parse(item.search_string),
            }
          } catch (error) {
            return initialState.get('search').merge(fromJS(item))
          }
        })
      }

      if (key === 'sort_column') {
        result[key] = presearch[key].map(column => ({ column }))
      }

      if (!(presearch[key].map && presearch[key].data)) {
        return result
      }

      const values = toObject(presearch[key].map, presearch[key].data)

      if (key === 'load') {
        result[key] = values.map(item => ({ ...item, text: getLoadText(item) }))
      } else if (key === 'base') {
        result[key] = values.map(item => ({ ...item, text: getValidText(item) }))
      } else if (key === 'seller') {
        result[key] = values.map(item => ({ ...item, text: getValidText(item) }))
      } else {
        result[key] = values
      }

      return result
    }, presearch)

    const [search] = data.search.filter(searchItem => searchItem.def == 1) // eslint-disable-line eqeqeq

    if (search) {
      const currentSearch = {
        ...unformatSearch(search),
        search_id: search.search_id,
        search_name: search.search_name,
        def: search.def,
      }

      return state.set('presearch', fromJS(data))
                  .set('search', initialState.get('search').merge(currentSearch))
    }

    return state.set('presearch', fromJS(data))
  },

  [actions.changeFilter]: (state, { filter, value }) => {
    if (filter === 'bin') {
      return state.setIn(['search', filter, 'value'], fromJS(parseBins(value)))
    } else if (filter === 'valid') {
      return state.mergeIn(['search', 'valid'], fromJS({
        ...value,
        touched: !(value.from == 0 && value.to == 100), // eslint-disable-line eqeqeq
      }))
    } else if (filter === 'zipMask') {
      return state.setIn(['search', 'zip', 'mask'], value)
    } else if (filter === 'sortOrder') {
      return state.setIn(['search', 'sort', 'order'], value)
                  .setIn(['search', 'sort', 'touched'], true)
    } else if (filter === 'sortDirection') {
      return state.setIn(['search', 'sort', 'direction'], value)
                  .setIn(['search', 'sort', 'touched'], true)
    } else if (filter === 'dates') {
      return state.mergeIn(['search', 'dates'], fromJS({
        ...value,
        touched: true,
      }))
    }

    return state.setIn(['search', filter, 'value'], fromJS(value))
  },

  [actions.inverseFilter]: (state, { filter, value }) => state.setIn(['search', filter, 'inversed'], value),

  [actions.showSearch]: state => state.set('showSearch', true),

  [actions.hideSearch]: state => state.set('showSearch', false),

  [actions.clearSearch]: state => state.set('search', initialState.get('search')),

  [actions.search]: (state, { search }) => {
    const count = parseInt(search.count, 10)
    const limit = parseInt(search.limit, 10)
    const total = count > limit ? limit : count

    return state.set('showSearch', false)
                .mergeIn(['result'], fromJS({
                  ...search,
                  show: true,
                  total,
                  list: search.data,
                  search_col: search.search_col.filter(column => column !== 'brand'),
                }))
  },

  [actions.loadSearch]: (state, { search }) => {
    const list = state.getIn(['result', 'list'])
    const count = parseInt(search.count, 10)
    const limit = parseInt(search.limit, 10)
    const total = count > limit ? limit : count

    return state.mergeIn(['result'], fromJS({
      ...search,
      checkTimeout: search.check_timeout,
      total,
      list: list.concat(fromJS(search.data)),
      search_col: search.search_col.filter(column => column !== 'brand'),
      allSelected: false,
    }))
  },

  [actions.loadSearchPage]: (state, { search }) => {
    const count = parseInt(search.count, 10)
    const limit = parseInt(search.limit, 10)
    const total = count > limit ? limit : count

    const data = search.data.map((item, index) => ({
      ...item,
      index: search.offset + index + 1,
    }))

    return state.mergeIn(['result'], fromJS({
      ...search,
      checkTimeout: search.check_timeout,
      list: fromJS(data),
      search_col: search.search_col.filter(column => column !== 'brand'),
      total,
      allSelected: false,
    }))
  },

  [actions.selectSearchRow]: (state, { row }) => {
    const index = state.getIn(['result', 'list'])
                       .findIndex(item => item.get('card_id') === row.get('card_id'))

    if (index !== -1) {
      const updated = state.setIn(['result', 'list', index, 'selected'], !row.get('selected'))

      return updated.setIn(
        ['result', 'hasSelected'],
        updated.getIn(['result', 'list'])
               .some(item => item.get('selected'))
      ).setIn(
        ['result', 'allSelected'],
        false
      )
    }

    return state
  },

  [actions.selectSearchRows]: state => {
    const selected = !state.getIn(['result', 'allSelected'])

    return state.setIn(['result', 'list'], state.getIn(['result', 'list']).map(item => item.set('selected', selected)))
                .setIn(['result', 'allSelected'], selected)
                .setIn(['result', 'hasSelected'], selected)
  },

  [actions.unselectCards]: state =>
    state
      .setIn(['result', 'allSelected'], false)
      .setIn(['result', 'hasSelected'], false)
      .setIn(
        ['result', 'list'],
        state.getIn(['result', 'list'])
             .map(item => item.set('selected', false))
      ),

  [actions.complete]: (state, { field, result }) => {
    let data = result

    if (field === 'address') {
      data = result.map(address => ({ ...address, label: address.address, value: address.address }))
    } else if (field === 'city') {
      data = result.map(city => ({ ...city, label: city.city, value: city.city }))
    } else if (field === 'zip') {
      data = result.map(zip => ({ ...zip, label: zip.zip, value: zip.zip }))
    } else if (field === 'bank') {
      data = result.map(bank => ({ ...bank, label: bank.bank, value: bank.bank }))
    }

    return state.setIn(['presearch', field, 'value'], fromJS(data))
  },

  [actions.clear]: state => state.merge(initialState),

  [actions.createSearch]: (state, { result }) => {
    const parsed = result.map(item => {
      try {
        return {
          ...item,
          ...JSON.parse(item.search_string),
        }
      } catch (error) {
        return initialState.get('search').merge(fromJS(item))
      }
    })

    return state.setIn(['presearch', 'search'], fromJS(parsed))
  },

  [actions.updateDefaultSearch]: (state, { value }) => state.setIn(['search', 'def'], value),

  [actions.applySearch]: (state, { searchId }) => {
    const search = state.getIn(['presearch', 'search']).find(item => item.get('search_id') === searchId)

    if (search) {
      const data = search.toJS()

      const current = {
        ...unformatSearch(data),
        search_id: data.search_id,
        search_name: data.search_name,
        def: data.def,
      }

      return state.set('search', initialState.get('search').merge(current))
    }

    return state
  },

  [actions.refreshSearch]: (state, { result }) => {
    const search = result.map(item => {
      try {
        return {
          ...item,
          ...JSON.parse(item.search_string),
        }
      } catch (error) {
        return initialState.get('search').merge(fromJS(item))
      }
    })

    return state.setIn(['presearch', 'search'], fromJS(search))
  },
})
