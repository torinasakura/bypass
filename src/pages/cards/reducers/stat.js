import { fromJS } from 'immutable'
import { indexBy, sortBy } from 'lodash'
import { timeConverter, prepareStat } from 'bypass/app/utils/utils'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants/stat'

const loadsColor = [
  '#37474f',
  '#000000',
  '#e99444',
  '#d15b41',
  '#557eaa',

  '#fb1d45',
  '#3fca14',
  '#e042bc',
  '#75ebea',
  '#c2f6f7',
]

const initialState = fromJS({
  static: {},
  dynamic: {
    period: 'week',
    showControls: true,
  },
})

export default createReducer(initialState, {
  [actions.loadStatic]: (state, { load, base, seller }) => {
    const stat = { seller }

    stat.load = load.map(loadItem => {
      const [baseTarget] = base.filter(baseItem => baseItem.base_id === loadItem.base_id)

      return {
        ...loadItem,
        time_formatted: timeConverter(loadItem.add_time, 0, 1),
        base_name: baseTarget ? baseTarget.name : '',
      }
    })

    stat.base = base.reduce((result, baseItem) => {
      const [sellerTarget] = seller.filter(sellerItem => sellerItem.seller_id === baseItem.seller_id)

      if (!sellerTarget) {
        return result
      }

      return result.concat([{
        ...baseItem,
        seller_name: sellerTarget.name,
      }])
    }, [])

    stat.limitedLoad = stat.load.slice(0, 10)
    stat.limitedBase = stat.base.slice(0, 10)
    stat.limitedSellers = stat.seller.splice(0, 10)

    const mapAvailable = item => {
      if (item) {
        return +item.available
      }

      return undefined
    }

    stat.maxHLoad = Math.max.apply(null, stat.limitedLoad.map(mapAvailable))
    stat.maxHSellers = Math.max.apply(null, stat.limitedSellers.map(mapAvailable))
    stat.maxHBase = Math.max.apply(null, stat.limitedBase.map(mapAvailable))

    return state.set('static', stat)
  },

  [actions.loadDynamic]: (state, { base, update }) => {
    const stat = {}

    stat.baseById = indexBy(base, 'base_id')
    stat.loadById = indexBy(update, 'load_id')

    stat.base = sortBy(base, item => parseInt(item.base_id, 10)).reverse()

    stat.update = sortBy(update, 'add_time').reverse().map((item) => {
      const time = timeConverter(item.add_time)
      const valid = item.valid ? parseFloat(item.valid).toFixed(2) : null
      const validFormatted = valid ? `(${valid}%)` : ''

      return {
        ...item,
        valid,
        add_time_formatted: time,
        name: `${time} (${item.available} ${__i18n('COM.COUNT')}) ${validFormatted}`,
        base: stat.baseById[item.base_id].name,
      }
    })

    return state.mergeIn(['dynamic'], fromJS(stat))
  },

  [actions.selectBase]: (state, { items }) => {
    const all = items.length === state.getIn(['dynamic', 'base']).size
    const clear = items.length === 0

    const base = state.getIn(['dynamic', 'base']).map(item => {
      if (all) {
        return item.set('selected', true)
      }

      if (clear) {
        return item.set('selected', false)
      }

      if (!items.includes(item.get('base_id'))) {
        return item
      }

      return item.set('selected', !item.get('selected'))
    })

    const update = state.getIn(['dynamic', 'update']).map(item => {
      if (items.includes(item.get('base_id'))) {
        return item.set('selected', false)
      }

      return item
    })

    return state.mergeIn(['dynamic'], {
      base,
      update,
      canShow: update.filter(item => item.get('selected')).size > 0,
    })
  },

  [actions.selectUpdate]: (state, { item }) => {
    const update = state.getIn(['dynamic', 'update']).map(load => {
      if (load.get('load_id') === item) {
        return load.set('selected', !load.get('selected'))
      }

      return load
    })

    return state.mergeIn(['dynamic'], {
      update,
      canShow: update.filter(updatedItem => updatedItem.get('selected')).size > 0,
    })
  },

  [actions.changePeriod]: (state, { period }) => state.setIn(['dynamic', 'period'], period),

  [actions.show]: (state, { result }) => {
    const baseById = state.getIn(['dynamic', 'baseById']).toJS()
    const loadById = state.getIn(['dynamic', 'loadById']).toJS()
    const loadId = state.getIn(['dynamic', 'update'])
                        .filter(item => item.get('selected'))
                        .map(item => item.get('load_id'))
                        .toJS()

    const data = prepareStat(result, loadId, baseById, loadsColor, loadById)

    return state.setIn(['dynamic', 'data'], data)
                .setIn(['dynamic', 'showControls'], false)
  },

  [actions.toggleControls]: state =>
    state.setIn(['dynamic', 'showControls'], !state.getIn(['dynamic', 'showControls'])),

  [actions.clearSelection]: state => {
    const base = state.getIn(['dynamic', 'base']).map(item => item.set('selected', false))
    const update = state.getIn(['dynamic', 'update']).map(item => item.set('selected', false))

    return state.mergeIn(['dynamic'], {
      base,
      update,
      data: {},
      canShow: false,
    })
  },
})
