import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants/orders'

const initialState = fromJS({
  orders: [],
  total: 0,
  perpage: 30,
  timeout: 1800,
  checkTimeout: 0,
  detail: {
    checkTimeout: 0,
    cards: [],
  },
})

export default createReducer(initialState, {
  [actions.load]: (state, { data, count, offset, perpage, timeout, check_timeout }) => {
    const orders = offset > 0 ? state.get('orders').concat(data) : data

    return state.merge(fromJS({
      offset,
      perpage,
      timeout,
      orders,
      total: parseInt(count, 10),
      checkTimeout: check_timeout,
    }))
  },

  [actions.select]: (state, { row }) => {
    const index = state.get('orders')
                       .findIndex(item => item.get('order_id') === row.get('order_id'))

    if (index !== -1) {
      const updated = state.setIn(['orders', index, 'selected'], !row.get('selected'))

      return updated.set(
        'hasSelected',
        updated.get('orders')
               .some(item => item.get('selected'))
      )
    }

    return state
  },

  [actions.selectAll]: state => {
    const selected = !state.get('allSelected')

    return state.set('orders', state.get('orders').map(card => card.set('selected', selected)))
                .set('allSelected', selected)
                .set('hasSelected', selected)
  },

  [actions.loadOrder]: (state, { data, check_timeout, show }) => {
    const cards = show === '0' ? data.filter(card => card.returned != 3) : data // eslint-disable-line eqeqeq

    return state.mergeIn(['order'], fromJS({
      checkTimeout: check_timeout,
      total: cards.length,
      cards,
    }))
  },
})
