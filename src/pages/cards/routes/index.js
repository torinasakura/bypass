import { createViewport } from 'bypass/app/utils'
import { presearch, showSearch, clear as clearSearch } from '../actions/search'
import { load as loadCart, loadStat } from '../actions/cart'
import { load as loadOrders } from '../actions/orders'
import { load as loadOrder } from '../actions/order'
import { loadStatic, loadDynamic } from '../actions/stat'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'cards',
    childRoutes: [{
      path: 'search',
      onEnter() {
        dispatch(presearch())
        dispatch(loadStat(false))
        setTimeout(() => dispatch(showSearch()), 300)
      },
      onLeave() {
        dispatch(clearSearch())
      },
      getComponent(location, callback) {
        System.import('../containers/Search')
              .then(Search => callback(null, createViewport(Search)))
      },
    },
    {
      path: 'cart',
      onEnter() {
        dispatch(loadCart())
      },
      getComponent(location, callback) {
        System.import('../containers/Cart')
              .then(Cart => callback(null, createViewport(Cart)))
      },
    },
    {
      path: 'orders',
      onEnter() {
        dispatch(loadOrders())
      },
      getComponent(location, callback) {
        System.import('../containers/Orders')
              .then(Orders => callback(null, createViewport(Orders)))
      },
    },
    {
      path: 'orders/:id',
      onEnter({ params }) {
        dispatch(loadOrder(params.id))
      },
      getComponent(location, callback) {
        System.import('../containers/Order')
              .then(Order => callback(null, createViewport(Order)))
      },
    },
    {
      path: 'stat',
      childRoutes: [{
        path: 'stat',
        onEnter() {
          dispatch(loadStatic())
        },
        getComponent(location, callback) {
          System.import('../containers/StatStatic')
                .then(StatStatic => callback(null, createViewport(StatStatic)))
        },
      },
      {
        path: 'dyn',
        onEnter() {
          dispatch(loadDynamic())
        },
        getComponent(location, callback) {
          System.import('../containers/StatDynamic')
                .then(StatDynamic => callback(null, createViewport(StatDynamic)))
        },
      }],
    }],
  }]
}
