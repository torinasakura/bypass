import { connect } from 'react-redux'
import { select, selectAll, check, showCsv, editComment, openCard } from '../actions/order'
import * as Order from '../components/Orders/Detail'

const connector = connect(
  state => ({
    checkTimeout: state.cards.order.get('checkTimeout'),
    hasSelected: state.cards.order.get('hasSelected'),
    allSelected: state.cards.order.get('allSelected'),
    cards: state.cards.order.get('cards'),
    total: state.cards.order.get('total'),
    orderId: state.router.params.id,
  }),
  dispatch => ({
    onSelect: (row) => {
      dispatch(select(row))
    },
    onSelectAll: () => {
      dispatch(selectAll())
    },
    onCheck: () => {
      dispatch(check())
    },
    onShowCsv: () => {
      dispatch(showCsv())
    },
    onEditComment: () => {
      dispatch(editComment())
    },
    onOpen: () => {
      dispatch(openCard())
    },
  }),
)

export const Desktop = connector(Order.Desktop)

export const Tablet = connector(Order.Tablet)

export const Mobile = connector(Order.Mobile)
