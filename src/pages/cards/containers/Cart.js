import { connect } from 'react-redux'
import { select, selectAll, buy, fastBuy, remove } from '../actions/cart'
import * as Cart from '../components/Cart'

const connector = connect(
  state => ({
    cards: state.cards.cart.get('cards'),
    total: state.cards.cart.get('total'),
    columns: state.cards.cart.get('search_col'),
    hasSelected: state.cards.cart.get('hasSelected'),
    allSelected: state.cards.cart.get('allSelected'),
    checkTimeout: state.cards.cart.get('checkTimeout'),
    toPurchase: state.cards.cart.get('toPurchase'),
  }),
  dispatch => ({
    onSelect: (row) => {
      dispatch(select(row))
    },
    onSelectAll: () => {
      dispatch(selectAll())
    },
    onBuy: () => {
      dispatch(buy())
    },
    onFastBuy: () => {
      dispatch(fastBuy())
    },
    onRemove: () => {
      dispatch(remove())
    },
  }),
)

export const Desktop = connector(Cart.Desktop)

export const Tablet = connector(Cart.Tablet)

export const Mobile = connector(Cart.Mobile)
