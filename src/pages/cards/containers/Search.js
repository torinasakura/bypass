import { connect } from 'react-redux'
import * as Search from '../components/Search'
import {
  showFilter, search, changeSort, clearSearch, showSearch,
  hideSearch, load, loadPage, select, selectAll, addCards,
  fastBuy, saveSearch, selectSearch, toggleDefaultSearch,
} from '../actions/search'


const connector = connect(
  state => ({
    showSearch: state.cards.search.get('showSearch'),
    search: state.cards.search.getIn(['search']).toJS(),
    total: state.cards.search.getIn(['result', 'total']),
    limit: state.cards.search.getIn(['result', 'limit']),
    list: state.cards.search.getIn(['result', 'list']),
    count: state.cards.search.getIn(['result', 'count']),
    perpage: state.cards.search.getIn(['result', 'perpage']),
    offset: state.cards.search.getIn(['result', 'offset']),
    checkTimeout: state.cards.search.getIn(['result', 'checkTimeout']),
    showResult: state.cards.search.getIn(['result', 'show']),
    hasSelected: state.cards.search.getIn(['result', 'hasSelected']),
    allSelected: state.cards.search.getIn(['result', 'allSelected']),
    columns: state.cards.search.getIn(['result', 'search_col']).toJS(),
    sortColumns: state.cards.search.getIn(['presearch', 'sort_column']).toJS(),
    cart: state.cards.cart.get('stat').toJS(),
  }),
  dispatch => ({
    onShowSearch: () => {
      dispatch(showSearch())
    },
    onHideSearch: () => {
      dispatch(hideSearch())
    },
    onShowFilter: type => {
      dispatch(showFilter(type))
    },
    onSearch: () => {
      dispatch(search())
    },
    onClearSearch: () => {
      dispatch(clearSearch())
    },
    onLoad: (offset) => {
      dispatch(load(offset))
    },
    onLoadPage: (offset, perpage) => {
      dispatch(loadPage(offset, perpage))
    },
    onSelect: (row) => {
      dispatch(select(row))
    },
    onSelectAll: () => {
      dispatch(selectAll())
    },
    onAddToCart: row => {
      dispatch(addCards(row))
    },
    onFastBuy: () => {
      dispatch(fastBuy())
    },
    onSaveSearch: () => {
      dispatch(saveSearch())
    },
    onLoadSearch: () => {
      dispatch(selectSearch())
    },
    onToggleDefault: () => {
      dispatch(toggleDefaultSearch())
    },
    onChangeSort: (order, direction) => {
      dispatch(changeSort(order, direction))
    },
  }),
)

export const Desktop = connector(Search.Desktop)

export const Tablet = connector(Search.Tablet)

export const Mobile = connector(Search.Mobile)
