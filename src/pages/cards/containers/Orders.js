import { connect } from 'react-redux'
import { select, selectAll, download, editComment, remove } from '../actions/orders'
import * as Orders from '../components/Orders/List'

const connector = connect(
  state => ({
    orders: state.cards.orders.get('orders'),
    total: state.cards.orders.get('total'),
    hasSelected: state.cards.orders.get('hasSelected'),
    allSelected: state.cards.orders.get('allSelected'),
    checkTimeout: state.cards.orders.get('checkTimeout'),
  }),
  dispatch => ({
    onSelect: (row) => {
      dispatch(select(row))
    },
    onSelectAll: () => {
      dispatch(selectAll())
    },
    onDownload: () => {
      dispatch(download())
    },
    onEditComment: () => {
      dispatch(editComment())
    },
    onRemove: () => {
      dispatch(remove())
    },
  }),
)

export const Desktop = connector(Orders.Desktop)

export const Tablet = connector(Orders.Tablet)

export const Mobile = connector(Orders.Mobile)
