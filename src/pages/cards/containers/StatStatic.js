import { connect } from 'react-redux'
import { openDetail } from '../actions/stat'
import * as Stat from '../components/Stat/Static'

const connector = connect(
  state => ({
    stat: state.cards.stat.get('static'),
  }),
  dispatch => ({
    onOpenDetailBase: () => {
      dispatch(openDetail('BaseStat'))
    },
    onOpenDetailLoad: () => {
      dispatch(openDetail('LoadStat'))
    },
    onOpenDetailSeller: () => {
      dispatch(openDetail('SellerStat'))
    },
  }),
)

export const Desktop = connector(Stat.Desktop)

export const Tablet = connector(Stat.Tablet)

export const Mobile = connector(Stat.Mobile)
