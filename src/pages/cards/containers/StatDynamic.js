import { connect } from 'react-redux'
import { selectBase, selectUpdate, changePeriod, show, toggleControls, clearSelection } from '../actions/stat'
import * as Stat from '../components/Stat/Dynamic'

const connector = connect(
  state => ({
    stat: state.cards.stat.get('dynamic').toJS(),
  }),
  dispatch => ({
    onSelectBase: items => {
      dispatch(selectBase(items))
    },
    onSelectUpdate: item => {
      dispatch(selectUpdate(item))
    },
    onChangePeriod: period => {
      dispatch(changePeriod(period))
    },
    onShow: () => {
      dispatch(show())
    },
    onToggleControls: () => {
      dispatch(toggleControls())
    },
    onClearSelection: () => {
      dispatch(clearSelection())
    },
  }),
)

export const Desktop = connector(Stat.Desktop)

export const Tablet = connector(Stat.Tablet)

export const Mobile = connector(Stat.Mobile)
