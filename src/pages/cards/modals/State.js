import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Multiselect } from 'bypass/ui/multiselect'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const State = ({ items, value, onChange, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.STATE')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser />
      <Multiselect
        items={items}
        value={value}
        textKey='state'
        valueKey='state_code'
        onChange={onChange}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'state'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'state', 'value'], new List()).toJS(),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('state', value))
    },
  })
)(State)
