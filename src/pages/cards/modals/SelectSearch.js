/* eslint-disable eqeqeq */
import React from 'react'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import { applySearch, removeSearch, setDefaultSearch } from '../actions/search'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { CloseIcon, FavoriteIcon } from 'bypass/ui/icons'
import { Text } from 'bypass/ui/text'

const styles = StyleSheet.create({
  self: {
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    padding: '3px',
    margin: '15px 15px 20px 15px',
    overflowY: 'auto',
    background: '#ffffff',
    display: 'block',
    maxHeight: '300px',
    '& div': {
      paddingLeft: '10px',
      lineHeight: '38px',
      fontSize: '16px',
      textAlign: 'left',
      cursor: 'pointer',
      display: 'flex',
      flexDirection: 'row',
      '& div:first-child': {
        flexGrow: 1,
      },
      '& svg': {
        position: 'relative',
        top: '10px',
        right: '5px',
      },
      '&:hover': {
        background: '#ececec',
      },
    },
  },
})

const Searches = ({ items = [], onSelect, onRemove, onSetDefault }) => (
  <div className={styles()}>
    {items.map(search => (
      <div key={search.search_id}>
        <div onClick={() => onSelect(search.search_id)}>
          {search.search_name}
        </div>
        <div onClick={() => onSetDefault(search.search_id)}>
          <FavoriteIcon
            width={16}
            checked={search.def == 1}
            fill={search.def == 1 ? '#fdd835' : null}
          />
        </div>
        <div onClick={() => onRemove(search.search_id)}>
          <CloseIcon width={16} fill='#b71c1c' />
        </div>
      </div>
    ))}
  </div>
)

const SelectSearch = ({ searches = [], onSelect, onRemove, onSetDefault, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('CHECK.LOAD_TPL_LABEL')}
      </Text>
    </Header>
    <Body alignCenter>
      <Searches
        items={searches}
        onSelect={onSelect}
        onRemove={onRemove}
        onSetDefault={onSetDefault}
      />
    </Body>
  </Modal>
)

export default connect(
  state => ({
    searches: state.cards.search.getIn(['presearch', 'search']).toJS(),
  }),
  dispatch => ({
    onSelect: searchId => {
      dispatch(applySearch(searchId))
    },
    onRemove: searchId => {
      dispatch(removeSearch(searchId))
    },
    onSetDefault: searchId => {
      dispatch(setDefaultSearch(searchId))
    },
  }),
)(SelectSearch)
