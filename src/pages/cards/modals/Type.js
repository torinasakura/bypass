import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Multiselect } from 'bypass/ui/multiselect'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Type = ({ items, value, inversed, onInverse, onChange, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.TYPE')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser
        value={inversed}
        onChange={onInverse}
      />
      <Multiselect
        items={items}
        value={value}
        textKey='type'
        valueKey='type_id'
        onChange={onChange}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'type'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'type', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'type', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('type', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('type', value))
    },
  })
)(Type)
