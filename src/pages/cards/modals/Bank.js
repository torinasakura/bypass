import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter, complete } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Autocomplete } from 'bypass/ui/autocomplete'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Bank = ({ items, value, inversed, onInverse, onChange, onComplete, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.BANK')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser
        value={inversed}
        onChange={onInverse}
      />
      <Autocomplete
        items={items}
        itemKey='value'
        value={value}
        onChange={onChange}
        onType={onComplete}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'bank', 'value'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'bank', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'bank', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('bank', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('bank', value))
    },
    onComplete: value => {
      dispatch(complete('bank', value))
    },
  })
)(Bank)
