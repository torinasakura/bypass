/* eslint-disable arrow-body-style */
/* eslint-disable eqeqeq */
import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import _ from 'lodash'
import { getCvsData, selectText } from 'bypass/app/utils/utils'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'
import { Link } from 'bypass/ui/link'

const styles = StyleSheet.create({
  self: {
    padding: '5px',
    width: '100%',
    border: 0,
    background: '#ffffff',
    margin: '20px 0 10px 0',
    boxSizing: 'border-box',
  },
})

class OrderCsv extends Component {
  onSelectAll = () => {
    selectText(findDOMNode(this.refs.cards))
  }

  getFormatShow() {
    const { format } = this.props
    let { delim } = this.props

    if (delim.toJS) {
      delim = delim.toJS()
    }

    return _.isArray(delim)
      ?
      format.map((c, i) => {
        return `%${c}%${delim[i] ? delim[i] : ''}`
      }).join('')
      :
      format.map(c => `%${c}%`).join(delim)
  }

  getCards() {
    const { delim, format, cards } = this.props

    return getCvsData(cards, format).map(card => {
      return _.isArray(delim)
        ?
        _.map(card, (it, k) => {
          return (it ? it : '') + (delim[k] ? delim[k] : '') // eslint-disable-line no-unneeded-ternary
        }).join('')
        :
        _.map(card, (it, k, arr) => {
          return (it ? it : '') + (k != _.size(arr) - 1 ? delim : '') // eslint-disable-line no-unneeded-ternary
        }).join('')
    })
  }

  render() {
    const { onClose } = this.props

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('LANG.SERVICE.CSV_VIEW')}
          </Text>
        </Header>
        <Body alignCenter>
          <div>
            <strong>
              {this.getFormatShow()}
            </strong>
          </div>
          <div>
            <Link color='lightBlue' target='_blank' href='#/profile?tab=1'>
              ({__i18n('COM.SETUP')})
            </Link>
          </div>
          <div ref='cards' className={styles()}>
            {this.getCards().map((card, idx) => <div key={idx}>{card}</div>)}
          </div>
        </Body>
        <Footer alignCenter>
          <Button onClick={this.onSelectAll}>
            {__i18n('LANG.BUTTONS.SELECT_ALL')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    delim: state.cards.order.getIn(['csvFormat', 'delim']),
    format: state.cards.order.getIn(['csvFormat', 'data']).toJS(),
  }),
)(OrderCsv)
