import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSearch } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Input } from 'bypass/ui/input'
import { Text } from 'bypass/ui/text'

class SaveSearch extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      name: '',
      isDefault: false,
    }
  }

  onChangeName = ({ target }) => {
    this.setState({ name: target.value })
  }

  onChangeDefault = isDefault => {
    this.setState({ isDefault })
  }

  onSave = () => {
    const { name, isDefault } = this.state
    const { onCreate } = this.props

    if (onCreate) {
      onCreate(name, isDefault)
    }
  }

  render() {
    const { onClose } = this.props
    const { name, isDefault } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('LANG.BUTTONS.SAVE_SEARCH')}
          </Text>
        </Header>
        <Body alignCenter>
          <RowLayout>
            <Layout>
              <Input
                value={name}
                onChange={this.onChangeName}
              />
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Inverser
                value={isDefault}
                message={__i18n('COM.SET_DEFAULT')}
                onChange={this.onChangeDefault}
              />
            </Layout>
          </RowLayout>
        </Body>
        <Footer alignCenter>
          <Button
            disabled={name.length === 0}
            onClick={this.onSave}
          >
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  null,
  dispatch => ({
    onCreate: (name, isDefault) => {
      dispatch(createSearch(name, isDefault))
    },
  })
)(SaveSearch)
