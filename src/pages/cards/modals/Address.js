import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter, complete } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Autocomplete } from 'bypass/ui/autocomplete'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Address = ({ items, value, inversed, onInverse, onChange, onComplete, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.ADDRESS')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser
        value={inversed}
        onChange={onInverse}
      />
      <Autocomplete
        items={items}
        itemKey='label'
        value={value}
        onChange={onChange}
        onType={onComplete}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'address', 'value'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'address', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'address', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('address', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('address', value))
    },
    onComplete: value => {
      dispatch(complete('address', value))
    },
  })
)(Address)
