import React, { Component } from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter, complete } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Autocomplete } from 'bypass/ui/autocomplete'
import { Textarea } from 'bypass/ui/textarea'
import { Inverser } from 'bypass/ui/inverser'
import { Input } from 'bypass/ui/input'
import { Button, ButtonGroup } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

class Zip extends Component {
  static defaultProps = {
    value: [],
    mask: '',
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      type: props.mask && props.mask.length > 0 ? 'mask' : 'search',
      inputValue: this.getInputValue(props.value),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ inputValue: this.getInputValue(nextProps.value) })
    }
  }

  onChangeType = type => {
    this.setState({ type })
  }

  onInputKeyUp = ({ target, key }) => {
    const { value, onChange } = this.props
    const values = target.value.split('\n')
                               .filter(zip => !!zip.length)

    if ((key === 'Enter' && values.length > 0) || (key === 'Backspace' && values.length !== value.length)) {
      onChange(values.map(zip => ({
        zip,
        zip_id: zip,
        label: zip,
        value: zip,
      })))
    }
  }

  onOnChangeInput = ({ target }) => {
    this.setState({ inputValue: target.value })
  }

  onChangeMask = ({ target }) => {
    const { onChangeMask } = this.props

    if (onChangeMask) {
      onChangeMask(target.value)
    }
  }

  getInputValue(value = []) {
    const values = value.map(item => item.label)

    return `${values.join('\n')}${values.length > 0 ? '\n' : ''}`
  }

  renderFilter() {
    const { type } = this.state

    if (type === 'search') {
      return this.renderSearch()
    } else if (type === 'input') {
      return this.renderInput()
    }

    return this.renderMask()
  }

  renderSearch() {
    const { value, items, onChange, onComplete } = this.props

    return (
      <Autocomplete
        items={items}
        itemKey='label'
        value={value}
        placeholder={__i18n('SEARCH.ASIDE.ZIP')}
        onType={onComplete}
        onChange={onChange}
      />
    )
  }

  renderInput() {
    const { inputValue } = this.state

    return (
      <Textarea
        rows={4}
        value={inputValue}
        onKeyUp={this.onInputKeyUp}
        onChange={this.onOnChangeInput}
      />
    )
  }

  renderMask() {
    const { mask } = this.props

    return (
      <div style={{ width: '100%', textAlign: 'left' }}>
        <Input
          fill
          value={mask}
          onChange={this.onChangeMask}
        />
        <div> &nbsp; </div>
        <div dangerouslySetInnerHTML={{ __html: __i18n('SEARCH.ZIP_MASK_LEGEND') }} />
        <div dangerouslySetInnerHTML={{ __html: __i18n('SEARCH.ZIP_MASK_LEGEND1') }} />
      </div>
    )
  }

  renderTabs() {
    const { value, mask } = this.props
    const { type } = this.state

    return (
      <ButtonGroup value={type} onChange={this.onChangeType}>
        <Button size='small' value='search' disabled={mask.length > 0}>
          {__i18n('SEARCH.ASIDE.SEARCH')}
        </Button>
        <Button size='small' value='input' disabled={mask.length > 0}>
          {__i18n('SEARCH.TEXT_INPUT')}
        </Button>
        <Button size='small' value='mask' disabled={value.length > 0}>
          {__i18n('SEARCH.ZIP_MASK')}
        </Button>
      </ButtonGroup>
    )
  }

  render() {
    const { inversed, onInverse, onClose } = this.props

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('SEARCH.ASIDE.CITY')}
          </Text>
        </Header>
        <Body alignCenter>
          <RowLayout>
            <Layout>
              {this.renderTabs()}
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Inverser
                value={inversed}
                onChange={onInverse}
              />
            </Layout>
            <Layout>
              {this.renderFilter()}
            </Layout>
          </RowLayout>
        </Body>
        <Footer alignCenter>
          <Button onClick={onClose}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'zip', 'value'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'zip', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'zip', 'inversed']),
    mask: state.cards.search.getIn(['search', 'zip', 'mask']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('zip', value))
    },
    onChangeMask: value => {
      dispatch(changeFilter('zipMask', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('zip', value))
    },
    onComplete: value => {
      dispatch(complete('zip', value))
    },
  })
)(Zip)
