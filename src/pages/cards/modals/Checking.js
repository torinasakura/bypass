import React from 'react'
import { connect } from 'react-redux'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Text } from 'bypass/ui/text'

const Checking = ({ checking, checked, unchecked, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.CARDS_CHECK')}
      </Text>
    </Header>
    <Body alignCenter>
      <RowLayout>
        <Layout>
          <ColumnLayout justify='center'>
            <Layout basis='160px'>
              {__i18n('ORDERS.CHECKING')}:
            </Layout>
            <Layout>
              <strong>
                {checking}
              </strong>
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='5px' />
        <Layout>
          <ColumnLayout justify='center'>
            <Layout basis='160px'>
              {__i18n('ORDERS.CHECKED')}:
            </Layout>
            <Layout>
              <strong>
                {checked}
              </strong>
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='5px' />
        <Layout>
          <ColumnLayout justify='center'>
            <Layout basis='160px'>
              {__i18n('ORDERS.UNCHECKED')}:
            </Layout>
            <Layout>
              <strong>
                {unchecked}
              </strong>
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='25px' />
      </RowLayout>
    </Body>
  </Modal>
)

export default connect(
  state => ({
    checking: state.cards.order.getIn(['checking', 'checking']),
    checked: state.cards.order.getIn(['checking', 'checked']),
    unchecked: state.cards.order.getIn(['checking', 'unchecked']),
  }),
)(Checking)
