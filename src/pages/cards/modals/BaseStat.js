import React from 'react'
import { connect } from 'react-redux'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { Text } from 'bypass/ui/text'
import Table from '../components/Stat/Static/base/Table'

const BaseStat = ({ data = [], onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.BASE')}
      </Text>
    </Header>
    <Body alignCenter>
      <Table
        bordered
        data={data}
      />
    </Body>
  </Modal>
)

export default connect(
  state => ({
    data: state.cards.stat.get('static').limitedBase,
  }),
)(BaseStat)
