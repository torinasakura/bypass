import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { DateInput } from 'bypass/ui/input'
import { Text } from 'bypass/ui/text'

class Period extends Component {
  static defaultProps: {
    value: {
      from: {},
      to: {},
    }
  }

  onChangeFrom = from => {
    const { value, onChange } = this.props

    onChange({ ...value, from })
  }

  onChangeTo = to => {
    const { value, onChange } = this.props

    onChange({ ...value, to })
  }

  render() {
    const { value, onClose } = this.props

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('SEARCH.ASIDE.PERIOD')}
          </Text>
        </Header>
        <Body alignCenter>
          <ColumnLayout justify='center' align='center'>
            <Layout>
              <DateInput
                value={value.from}
                onChange={this.onChangeFrom}
              />
            </Layout>
            <Layout basis='8px' />
            <Layout>
              &minus;
            </Layout>
            <Layout basis='8px' />
            <Layout>
              <DateInput
                value={value.to}
                onChange={this.onChangeTo}
              />
            </Layout>
          </ColumnLayout>
        </Body>
        <Footer alignCenter>
          <Button onClick={onClose}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    value: state.cards.search.getIn(['search', 'dates']).toJS(),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('dates', value))
    },
  })
)(Period)
