import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Multiselect } from 'bypass/ui/multiselect'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Base = ({ items, value, inversed, onInverse, onChange, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.BASE')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser
        value={inversed}
        onChange={onInverse}
      />
      <Multiselect
        items={items}
        value={value}
        textKey='text'
        valueKey='base_id'
        onChange={onChange}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'base'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'base', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'base', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('base', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('base', value))
    },
  })
)(Base)
