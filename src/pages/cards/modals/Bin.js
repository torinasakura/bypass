import React, { Component } from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Textarea } from 'bypass/ui/textarea'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

class Bin extends Component {
  static defaultProps = {
    value: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      value: props.value.join('\n'),
    }
  }

  onChange = ({ target }) => {
    const { onChange } = this.props

    this.setState({ value: target.value })

    onChange(target.value)
  }

  render() {
    const { inversed, onInverse, onClose } = this.props
    const { value } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('SEARCH.ASIDE.BIN')}
          </Text>
        </Header>
        <Body alignCenter>
          <Inverser
            value={inversed}
            onChange={onInverse}
          />
          <Textarea
            rounded
            rows='4'
            value={value}
            onChange={this.onChange}
          />
        </Body>
        <Footer alignCenter>
          <Button onClick={onClose}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'brand'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'bin', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'bin', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('bin', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('bin', value))
    },
  })
)(Bin)
