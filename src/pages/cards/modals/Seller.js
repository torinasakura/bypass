import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Multiselect } from 'bypass/ui/multiselect'
import { Inverser } from 'bypass/ui/inverser'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Seller = ({ items, value, inversed, onInverse, onChange, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('SEARCH.ASIDE.SELLER')}
      </Text>
    </Header>
    <Body alignCenter>
      <Inverser
        value={inversed}
        onChange={onInverse}
      />
      <Multiselect
        items={items}
        value={value}
        textKey='text'
        valueKey='seller_id'
        onChange={onChange}
      />
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'seller'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'seller', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'seller', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('seller', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('seller', value))
    },
  })
)(Seller)
