import React, { Component } from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter, complete } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Autocomplete } from 'bypass/ui/autocomplete'
import { Textarea } from 'bypass/ui/textarea'
import { Inverser } from 'bypass/ui/inverser'
import { Button, ButtonGroup } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

class City extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      type: 'search',
      inputValue: this.getInputValue(props.value),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ inputValue: this.getInputValue(nextProps.value) })
    }
  }

  onChangeType = type => {
    this.setState({ type })
  }

  onInputKeyUp = ({ target, key }) => {
    const { value, onChange } = this.props
    const values = target.value.split('\n')
                               .filter(city => !!city.length)

    if ((key === 'Enter' && values.length > 0) || (key === 'Backspace' && values.length !== value.length)) {
      onChange(values.map(city => ({
        city,
        city_id: city,
        label: city,
        value: city,
      })))
    }
  }

  onOnChangeInput = ({ target }) => {
    this.setState({ inputValue: target.value })
  }

  getInputValue(value = []) {
    const values = value.map(item => item.label)

    return `${values.join('\n')}${values.length > 0 ? '\n' : ''}`
  }

  renderFilter() {
    const { type } = this.state

    if (type === 'search') {
      return this.renderSearch()
    }

    return this.renderInput()
  }

  renderSearch() {
    const { value, items, onChange, onComplete } = this.props

    return (
      <Autocomplete
        items={items}
        itemKey='label'
        value={value}
        placeholder={__i18n('SEARCH.ASIDE.CITY')}
        onType={onComplete}
        onChange={onChange}
      />
    )
  }

  renderInput() {
    const { inputValue } = this.state

    return (
      <Textarea
        rows={4}
        value={inputValue}
        onKeyUp={this.onInputKeyUp}
        onChange={this.onOnChangeInput}
      />
    )
  }

  render() {
    const { inversed, onInverse, onClose } = this.props
    const { type } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('SEARCH.ASIDE.CITY')}
          </Text>
        </Header>
        <Body alignCenter>
          <RowLayout>
            <Layout>
              <ButtonGroup value={type} onChange={this.onChangeType}>
                <Button value='search'>
                  {__i18n('SEARCH.ASIDE.SEARCH')}
                </Button>
                <Button value='input'>
                  {__i18n('SEARCH.TEXT_INPUT')}
                </Button>
              </ButtonGroup>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Inverser
                value={inversed}
                onChange={onInverse}
              />
            </Layout>
            <Layout>
              {this.renderFilter()}
            </Layout>
          </RowLayout>
        </Body>
        <Footer alignCenter>
          <Button onClick={onClose}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'city', 'value'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'city', 'value'], new List()).toJS(),
    inversed: state.cards.search.getIn(['search', 'city', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('city', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('city', value))
    },
    onComplete: value => {
      dispatch(complete('city', value))
    },
  })
)(City)
