import React from 'react'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { changeFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Direction } from 'bypass/ui/sort'
import { Select } from 'bypass/ui/select'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Sort = ({ items, value = {}, onChangeOrder, onChangeDirection, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('COM.SORT')}
      </Text>
    </Header>
    <Body alignCenter>
      <ColumnLayout align='center' justify='center'>
        <Layout>
          <Select
            options={items}
            valueKey='column'
            textKey='column'
            value={value.order}
            onChange={onChangeOrder}
          />
        </Layout>
        <Layout>
          <Direction
            direction={value.direction}
            onChange={onChangeDirection}
          />
        </Layout>
      </ColumnLayout>
    </Body>
    <Footer alignCenter>
      <Button onClick={onClose}>
        {__i18n('LANG.BUTTONS.SAVE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    items: state.cards.search.getIn(['presearch', 'sort_column'], new List()).toJS(),
    value: state.cards.search.getIn(['search', 'sort']).toJS(),
  }),
  dispatch => ({
    onChangeOrder: ({ target }) => {
      dispatch(changeFilter('sortOrder', target.value))
    },
    onChangeDirection: value => {
      dispatch(changeFilter('sortDirection', value))
    },
  })
)(Sort)
