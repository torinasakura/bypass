import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeFilter, inverseFilter } from '../actions/search'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { Input } from 'bypass/ui/input'
import { Text } from 'bypass/ui/text'

class Valid extends Component {
  static defaultProps: {
    value: {
      from: 0,
      to: 0,
    }
  }

  onChangeFrom = ({ target }) => {
    const { value, onChange } = this.props

    onChange({ ...value, from: target.value })
  }

  onChangeTo = ({ target }) => {
    const { value, onChange } = this.props

    onChange({ ...value, to: target.value })
  }

  render() {
    const { value, onClose } = this.props

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('STAT.VALID')}
          </Text>
        </Header>
        <Body alignCenter>
          <ColumnLayout justify='center' align='center'>
            <Layout basis='40px'>
              <Input
                size='3'
                maxLength='3'
                pattern='^([0-9]|[1-9][0-9]|100)$'
                value={value.from}
                onChange={this.onChangeFrom}
              />
            </Layout>
            <Layout basis='8px' />
            <Layout>
              %
            </Layout>
            <Layout basis='8px' />
            <Layout>
              &minus;
            </Layout>
            <Layout basis='8px' />
            <Layout basis='40px'>
              <Input
                size='3'
                maxLength='3'
                pattern='^([0-9]|[1-9][0-9]|100)$'
                value={value.to}
                onChange={this.onChangeTo}
              />
            </Layout>
            <Layout basis='8px' />
            <Layout>
              %
            </Layout>
          </ColumnLayout>
        </Body>
        <Footer alignCenter>
          <Button onClick={onClose}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    value: state.cards.search.getIn(['search', 'valid']).toJS(),
    inversed: state.cards.search.getIn(['search', 'valid', 'inversed']),
  }),
  dispatch => ({
    onChange: value => {
      dispatch(changeFilter('valid', value))
    },
    onInverse: value => {
      dispatch(inverseFilter('valid', value))
    },
  })
)(Valid)
