import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import { buy } from '../actions/cart'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Checkbox } from 'bypass/ui/checkbox'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const styles = StyleSheet.create({
  self: {
    padding: '15px 20px',
    background: 'transparent',
    outline: 'none',
    '& span:first-child': {
      fontSize: '18px',
      color: '#8bc34a',
      margin: '0 0 15px 0',
      display: 'block',
    },
  },
})

class FastBuy extends Component {
  static defaultProps = {
    options: {},
  }

  constructor(props, context) {
    super(props, context)

    this.state = props.options
  }

  onChangeCheck = ({ target }) => {
    this.setState({ check: target.checked })
  }

  onChangeOpen = ({ target }) => {
    this.setState({ open: target.checked })
  }

  onChangeRedirect = ({ target }) => {
    this.setState({ redirect: target.checked })
  }

  onChangeCsv = ({ target }) => {
    this.setState({ csv: target.checked })
  }

  onBuy = () => {
    const { cards, onBuy } = this.props
    const { check, open, redirect, csv } = this.state

    if (onBuy) {
      onBuy(cards, { check, open, redirect, csv })
    }
  }

  renderPurchase() {
    const { count, price } = this.props

    return (
      <div>
        <div>
          <Text size={14}>
            {__i18n('SEARCH.FAST_BUY.CARD_SELECTED')}: {count}
          </Text>
        </div>
        <div>
          <Text size={14}>
            {__i18n('SEARCH.FAST_BUY.TOTAL_COAST')}: ${price}
          </Text>
        </div>
      </div>
    )
  }

  renderOptions() {
    const { check, open, redirect, csv } = this.state

    return (
      <div className={styles()}>
        <span>
          {__i18n('SEARCH.FAST_BUY.AFTER_BUY')}
        </span>
        <div>
          <ColumnLayout justify='space-around'>
            <Layout>
              <RowLayout>
                <Layout>
                  <Checkbox checked={check} onChange={this.onChangeCheck} /> {__i18n('SEARCH.FAST_BUY.SET_CHECK')}
                </Layout>
                <Layout basis='5px' />
                <Layout>
                  <Checkbox checked={open} onChange={this.onChangeOpen} /> {__i18n('LANG.BUTTONS.OPEN')}
                </Layout>
              </RowLayout>
            </Layout>
            <Layout>
              <RowLayout>
                <Layout>
                  <Checkbox checked={redirect} onChange={this.onChangeRedirect} /> {__i18n('SEARCH.FAST_BUY.GO_ORDER')}
                </Layout>
                <Layout basis='5px' />
                <Layout>
                  <Checkbox checked={csv} onChange={this.onChangeCsv} /> {__i18n('SEARCH.FAST_BUY.SHOW_CVS')}
                </Layout>
              </RowLayout>
            </Layout>
          </ColumnLayout>
        </div>
      </div>
    )
  }

  render() {
    const { onClose } = this.props

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('PROFILE.FAST_BUY.TEXT')}
          </Text>
        </Header>
        <Body alignCenter>
          {this.renderPurchase()}
          {this.renderOptions()}
        </Body>
        <Footer alignCenter>
          <Button onClick={this.onBuy}>
            {__i18n('LANG.BUTTONS.BUY')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  null,
  dispatch => ({
    onBuy: (cards, options) => dispatch(buy(cards, options)),
  }),
)(FastBuy)
