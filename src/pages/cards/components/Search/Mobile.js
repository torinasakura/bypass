import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Button } from 'bypass/ui/button'
import { Aside } from './aside'
import { List } from './list'

const Mobile = ({
  columns, list, total, search, showSearch, showResult,
  hasSelected, allSelected, cart, onToggleDefault,
  onSaveSearch, onLoadSearch, onFastBuy, onAddToCart,
  onShowFilter, onSearch, onShowSearch, onHideSearch,
  onClearSearch, onLoad, onSelect, onSelectAll,
}) => (
  <AutoSizer>
    <ColumnLayout>
      <Layout>
        <Aside
          fill
          wrapToolbar
          cart={cart}
          search={search}
          showSearch={showSearch}
          onShowFilter={onShowFilter}
          onSearch={onSearch}
          onShowSearch={onShowSearch}
          onHideSearch={onHideSearch}
          onClearSearch={onClearSearch}
          onSaveSearch={onSaveSearch}
          onLoadSearch={onLoadSearch}
          onToggleDefault={onToggleDefault}
        />
      </Layout>
      <Layout grow={1} shrink={1}>
        <Condition match={showResult}>
          <Container>
            <AutoSizer>
              <RowLayout>
                <Layout>
                  <Title size='small'>
                    {__i18n('LANG.SERVICE.SEARCH_RESULT')}
                  </Title>
                </Layout>
                <Layout touch scrollX grow={1} shrink={1}>
                  <List
                    list={list}
                    total={total}
                    columns={columns}
                    minWidth={900}
                    rowHeight={30}
                    allSelected={allSelected}
                    onLoad={onLoad}
                    onSelect={onSelect}
                    onSelectAll={onSelectAll}
                    onAddToCart={onAddToCart}
                  />
                </Layout>
                <Layout>
                  <ColumnLayout>
                    <Layout grow={1}>
                      <Button fill disabled={!hasSelected} onClick={onAddToCart}>
                        {__i18n('COM.ADD_TO_CART')}
                      </Button>
                    </Layout>
                    <Layout grow={1}>
                      <Button fill disabled={!hasSelected} onClick={onFastBuy}>
                        {__i18n('SEARCH.FAST_BUY.FAST')}
                      </Button>
                    </Layout>
                  </ColumnLayout>
                </Layout>
              </RowLayout>
            </AutoSizer>
          </Container>
        </Condition>
      </Layout>
    </ColumnLayout>
  </AutoSizer>
)

export default Mobile
