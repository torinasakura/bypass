/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
import React from 'react'
import { isNil } from 'ramda'
import { countryByBinCode, formatDate, getExp } from 'bypass/app/utils/utils'
import { Column, BodyCell } from 'bypass/ui/table'
import { CheckIcon, CloseIcon } from 'bypass/ui/icons'
import { Price } from 'bypass/ui/price'

const columnNames = {
  id: __i18n('LANG.TABLE.COLS.ID'),
  exp: __i18n('LANG.TABLE.COLS.EXP'),
  holder: __i18n('LANG.TABLE.COLS.HOLDER'),
  level: __i18n('LANG.TABLE.COLS.LEVEL'),
  type: __i18n('LANG.TABLE.COLS.TYPE'),
  bank: __i18n('LANG.TABLE.COLS.BANK'),
  zip: __i18n('LANG.TABLE.COLS.ZIP'),
  address: __i18n('LANG.TABLE.COLS.ADDRESS'),
  city: __i18n('LANG.TABLE.COLS.CITY'),
  state_code: __i18n('LANG.TABLE.COLS.STATE_CODE'),
  country_code: __i18n('LANG.TABLE.COLS.COUNTRY_CODE'),
  email: __i18n('LANG.TABLE.COLS.EMAIL'),
  phone: __i18n('LANG.TABLE.COLS.PHONE'),
  load_valid: __i18n('LANG.TABLE.COLS.LOAD_VALID'),
  seller: __i18n('LANG.TABLE.COLS.SELLER'),
  base: __i18n('LANG.TABLE.COLS.BASE'),
  add_time: __i18n('LANG.TABLE.COLS.ADD_TIME'),
  seller_price: __i18n('LANG.TABLE.COLS.SELLER_PRICE'),
  cvv2: __i18n('LANG.TABLE.COLS.CVV2'),
  m: __i18n('LANG.TABLE.COLS.M'),
  y: __i18n('LANG.TABLE.COLS.Y'),
  date: __i18n('LANG.TABLE.COLS.DATE'),
  price_item: __i18n('LANG.TABLE.COLS.PRICE_ITEM'),
  provider: __i18n('LANG.TABLE.COLS.PROVIDER'),
  status: __i18n('LANG.TABLE.COLS.STATUS'),
  response: __i18n('LANG.TABLE.COLS.RESPONSE'),
  comment: __i18n('LANG.TABLE.COLS.COMMENT'),
  theme: __i18n('LANG.TABLE.COLS.THEME'),
  opened: __i18n('LANG.TABLE.COLS.OPENED'),
  from: __i18n('LANG.TABLE.COLS.FROM'),
  to: __i18n('LANG.TABLE.COLS.TO'),
  cards_another: __i18n('LANG.TABLE.COLS.CARDS_ANOTHER'),
}

const returnedMessages = {
  0: __i18n('LANG.SERVICE.UNCHECKED'),
  1: __i18n('LANG.SERVICE.CHECKING'),
  2: __i18n('LANG.SERVICE.VALID'),
  3: __i18n('LANG.SERVICE.INVALID'),
  4: __i18n('LANG.SERVICE.NOCASHBACK'),
}

const columnsWithoutTooltip = ['load_valid', 'seller_price',
  'phone', 'email', 'exp', '_row']

const hasTooltip = col => !columnsWithoutTooltip.includes(col)

const _row = (
  <Column
    key='_row'
    label={__i18n('LANG.TABLE.COLS.INDEX')}
    width='30px'
    flexGrow={1}
    dataKey='_row'
    cellDataGetter={(dataKey, rowData, columnData, columnIndex, rowIndex) => {
      if (rowData.get('index')) {
        return rowData.get('index')
      }

      return rowIndex + 1
    }}
  />
)

const number = (
  <Column
    key='number'
    label={__i18n('LANG.TABLE.COLS.NUMBER')}
    width='100px'
    flexGrow={1}
    dataKey='number'
    tooltip
    tooltipDataGetter={(cellData, dataKey, rowData) => rowData.get('brand')}
  />
)

const exp = (
  <Column
    key='exp'
    label={__i18n('LANG.TABLE.COLS.EXP')}
    width='40px'
    flexGrow={1}
    dataKey='exp'
    cellDataGetter={(dataKey, rowData) => getExp(rowData.get(dataKey))}
  />
)

const country_code = (
  <Column
    key='country_code'
    label={__i18n('LANG.TABLE.COLS.COUNTRY_CODE')}
    width='40px'
    flexGrow={1}
    dataKey='country_code'
    cellDataGetter={(dataKey, rowData) => countryByBinCode(rowData.toJS())}
  />
)

const getPrice = (price, addPrice = 0) => {
  if (!price) {
    return null
  }

  return (parseFloat(price) + parseFloat(addPrice)).toFixed(2)
}

const seller_price = (
  <Column
    key='seller_price'
    label={__i18n('LANG.TABLE.COLS.SELLER_PRICE')}
    width='40px'
    flexGrow={1}
    dataKey='seller_price'
    cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
      <BodyCell justify='end' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
        <Price green>
          {getPrice(rowData.get(dataKey), rowData.get('add_price'))}
        </Price>
      </BodyCell>
    )}
  />
)

const add_time = (
  <Column
    key='add_time'
    label={__i18n('LANG.TABLE.COLS.ADD_TIME')}
    width='40px'
    flexGrow={1}
    dataKey='add_time'
    cellDataGetter={(dataKey, rowData) => formatDate(new Date(rowData.get(dataKey) * 1000))}
  />
)

const load_valid = (
  <Column
    key='load_valid'
    label={__i18n('LANG.TABLE.COLS.LOAD_VALID')}
    width='40px'
    flexGrow={1}
    dataKey='load_valid'
    cellDataGetter={(dataKey, rowData) => {
      const valid = rowData.get(dataKey)
      const floatValid = parseFloat(valid)

      if (isNil(valid) || valid == 0) {
        return `<${__i18n('COM.EMPTY')}>`
      } else if (floatValid > 50) {
        return floatValid.toFixed(2)
      }

      return `<${__i18n('VALID.LOW')}>`
    }}
  />
)

const email = (
  <Column
    key='email'
    label={__i18n('LANG.TABLE.COLS.EMAIL')}
    width='25px'
    flexGrow={1}
    dataKey='email'
    cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
      <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
        {cellData
          ? (<CheckIcon width={18} fill='#4caf50' />)
          : (<CloseIcon width={18} fill='#b71c1c' />)}
      </BodyCell>
    )}
  />
)

const phone = (
  <Column
    key='phone'
    label={__i18n('LANG.TABLE.COLS.PHONE')}
    width='40px'
    flexGrow={1}
    dataKey='phone'
    cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
      <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
        {cellData
          ? (<CheckIcon width={18} fill='#4caf50' />)
          : (<CloseIcon width={18} fill='#b71c1c' />)}
      </BodyCell>
    )}
  />
)

const provider = (
  <Column
    key='provider'
    label={__i18n('LANG.TABLE.COLS.INDEX')}
    width='60px'
    flexGrow={1}
    dataKey='provider'
    cellDataGetter={(dataKey, rowData) => {
      if (rowData.get(dataKey)) {
        return rowData.get(dataKey)
      }

      return __i18n('COM._EMPTY_')
    }}
  />
)

const returned = (
  <Column
    key='returned'
    label={__i18n('LANG.TABLE.COLS.STATUS')}
    width='60px'
    flexGrow={1}
    dataKey='returned'
    cellDataGetter={(dataKey, rowData) => returnedMessages[rowData.get(dataKey)]}
  />
)

const columns = {
  _row,
  number,
  exp,
  seller_price,
  country_code,
  add_time,
  load_valid,
  email,
  phone,
  provider,
  returned,
}

export default function column(name) {
  if (columns[name]) {
    return columns[name]
  }

  return (
    <Column
      key={name}
      label={columnNames[name] || '\u00A0'}
      width='40px'
      flexGrow={1}
      dataKey={name}
      tooltip={hasTooltip(name)}
    />
  )
}
