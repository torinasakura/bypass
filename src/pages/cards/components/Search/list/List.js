import React from 'react'
import { Table, Column, BodyCell, HeaderCell } from 'bypass/ui/table'
import { Checkbox } from 'bypass/ui/checkbox'
import AddToCart from './AddToCart'
import getColumn from './column'

const List = ({
  minWidth, rowHeight, total, list, columns,
  allSelected, onLoad, onSelect, onSelectAll, onAddToCart,
}) => (
  <Table
    list={list}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onLoad={onLoad}
    onRowClick={onSelect}
  >
    {columns.map(getColumn)}
    <Column
      label={'_'}
      width='70px'
      dataKey='selected'
      headerRenderer={() => (
        <HeaderCell>
          <Checkbox checked={allSelected} onChange={onSelectAll} />
        </HeaderCell>
      )}
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <AddToCart
            row={rowData}
            selected={cellData}
            onAdd={onAddToCart}
          />
        </BodyCell>
      )}
    />
  </Table>
)

export default List
