import React from 'react'
import { StyleSheet } from 'quantum'
import { Checkbox } from 'bypass/ui/checkbox'
import { CartIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    lineHeight: '25px',
    '& span:last-child svg': {
      position: 'relative',
      top: '3px',
    },
  },
})

const handleAdd = (row, onAdd, event) => {
  event.stopPropagation()

  if (onAdd) {
    onAdd(row)
  }
}

const AddToCart = ({ selected, row, onAdd }) => (
  <span className={styles()}>
    <Checkbox checked={selected} />
    <span onClick={handleAdd.bind(null, row, onAdd)}>
      <CartIcon fill='#4caf50' width={18} />
    </span>
  </span>
)

export default AddToCart
