import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Button } from 'bypass/ui/button'
import { Aside } from './aside'
import { List } from './list'

const Tablet = ({
  columns, list, total, search, showSearch, showResult,
  hasSelected, allSelected, cart, onToggleDefault, onSaveSearch,
  onLoadSearch, onFastBuy, onAddToCart, onShowFilter, onSearch,
  onShowSearch, onHideSearch, onClearSearch,
  onLoad, onSelect, onSelectAll,
}) => (
  <AutoSizer>
    <ColumnLayout>
      <Layout>
        <Aside
          fill
          cart={cart}
          search={search}
          showSearch={showSearch}
          onShowFilter={onShowFilter}
          onSearch={onSearch}
          onShowSearch={onShowSearch}
          onHideSearch={onHideSearch}
          onClearSearch={onClearSearch}
          onSaveSearch={onSaveSearch}
          onLoadSearch={onLoadSearch}
          onToggleDefault={onToggleDefault}
        />
      </Layout>
      <Layout grow={1} shrink={1}>
        <Condition match={showResult}>
          <Container>
            <AutoSizer>
              <RowLayout>
                <Layout>
                  <Title size='medium'>
                    {__i18n('LANG.SERVICE.SEARCH_RESULT')}
                  </Title>
                </Layout>
                <Layout basis='20px' />
                <Layout scrollX touch grow={1} shrink={1}>
                  <List
                    list={list}
                    total={total}
                    columns={columns}
                    rowHeight={30}
                    minWidth={980}
                    allSelected={allSelected}
                    onLoad={onLoad}
                    onSelect={onSelect}
                    onSelectAll={onSelectAll}
                    onAddToCart={onAddToCart}
                  />
                </Layout>
                <Layout basis='20px' />
                <Layout>
                  <ColumnLayout>
                    <Layout grow={1} />
                    <Layout>
                      <Button disabled={!hasSelected} onClick={onAddToCart}>
                        {__i18n('COM.ADD_TO_CART')}
                      </Button>
                    </Layout>
                    <Layout basis='15px' />
                    <Layout>
                      <Button disabled={!hasSelected} onClick={onFastBuy}>
                        {__i18n('SEARCH.FAST_BUY.FAST')}
                      </Button>
                    </Layout>
                    <Layout grow={1} />
                  </ColumnLayout>
                </Layout>
                <Layout basis='20px' />
              </RowLayout>
            </AutoSizer>
          </Container>
        </Condition>
      </Layout>
    </ColumnLayout>
  </AutoSizer>
)

export default Tablet
