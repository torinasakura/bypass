import React from 'react'
import { StyleSheet } from 'quantum'

const getMessage = (count, limit) =>
                    __i18n('SEARCH.PAGEINFO')
                      .replace('${count}', count)
                      .replace('${limit}', limit)

const styles = StyleSheet.create({
  self: {
    fontSize: '14px',
    width: '100%',
  },
})

const Info = ({ count, limit }) => (
  <div
    className={styles()}
    dangerouslySetInnerHTML={{ __html: getMessage(count, limit) }}
  />
)

export default Info
