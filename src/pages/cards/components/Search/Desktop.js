import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { HowUpTimeout } from 'bypass/ui/notice'
import { Condition } from 'bypass/ui/condition'
import { Paginate } from 'bypass/ui/paginate'
import { Button } from 'bypass/ui/button'
import { Sort } from 'bypass/ui/sort'
import { Aside } from './aside'
import { List } from './list'
import Info from './Info'

const Desktop = ({
  columns, sortColumns, list, total, limit, count, perpage,
  offset, search, showSearch, showResult, hasSelected, allSelected,
  cart, checkTimeout, onChangeSort, onLoadPage, onToggleDefault,
  onSaveSearch, onLoadSearch, onFastBuy, onAddToCart, onShowFilter,
  onSearch, onShowSearch, onHideSearch, onClearSearch,
  onLoad, onSelect, onSelectAll,
}) => (
  <AutoSizer>
    <ColumnLayout>
      <Layout>
        <Aside
          cart={cart}
          search={search}
          showSearch={showSearch}
          onShowFilter={onShowFilter}
          onSearch={onSearch}
          onShowSearch={onShowSearch}
          onHideSearch={onHideSearch}
          onClearSearch={onClearSearch}
          onSaveSearch={onSaveSearch}
          onLoadSearch={onLoadSearch}
          onToggleDefault={onToggleDefault}
        />
      </Layout>
      <Layout grow={1} shrink={1}>
        <Condition match={showResult}>
          <Container indent>
            <AutoSizer>
              <RowLayout>
                <Layout>
                  <ColumnLayout align='center'>
                    <Layout grow={1}>
                      <Title offset='right'>
                        {__i18n('LANG.SERVICE.SEARCH_RESULT')}
                        <Info count={count} limit={limit} />
                      </Title>
                    </Layout>
                    <Layout>
                      <HowUpTimeout timeout={checkTimeout} />
                    </Layout>
                  </ColumnLayout>
                </Layout>
                <Layout grow={1} shrink={1}>
                  <List
                    list={list}
                    total={perpage}
                    columns={columns}
                    rowHeight={30}
                    allSelected={allSelected}
                    onLoad={onLoad}
                    onSelect={onSelect}
                    onSelectAll={onSelectAll}
                    onAddToCart={onAddToCart}
                  />
                </Layout>
                <Layout basis='20px' />
                <Layout>
                  <ColumnLayout align='center'>
                    <Layout grow={1} shrink={1}>
                      <Paginate
                        count={total}
                        offset={offset}
                        pageSize={perpage}
                        onChange={onLoadPage}
                      />
                    </Layout>
                    <Layout>
                      <Sort
                        value={search.sort}
                        columns={sortColumns}
                        onChange={onChangeSort}
                      />
                    </Layout>
                    <Layout basis='20px' />
                    <Layout>
                      <Button disabled={!hasSelected} onClick={onAddToCart}>
                        {__i18n('COM.ADD_TO_CART')}
                      </Button>
                    </Layout>
                    <Layout basis='15px' />
                    <Layout>
                      <Button disabled={!hasSelected} onClick={onFastBuy}>
                        {__i18n('SEARCH.FAST_BUY.FAST')}
                      </Button>
                    </Layout>
                  </ColumnLayout>
                </Layout>
                <Layout basis='20px' />
              </RowLayout>
            </AutoSizer>
          </Container>
        </Condition>
      </Layout>
    </ColumnLayout>
  </AutoSizer>
)

export default Desktop
