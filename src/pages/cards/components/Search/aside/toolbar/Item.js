import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#3f535c',
    marginTop: '10px',
    width: '70px',
    height: '75px',
    display: 'inline-flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'inset 3px 0 #ffffff',
    transition: 'all 0.3s',
    fontSize: '12px',
    color: '#fdd835',
    cursor: 'pointer',
    '&:hover': {
      boxShadow: 'inset 3px 0 #fdd835',
      background: '#698998',
    },
  },
  active: {
    boxShadow: 'inset 3px 0 #fdd835',
  },
})

const Item = ({ children, active, onClick }) => (
  <div className={styles({ active })} onClick={onClick}>
    {children}
  </div>
)

export default Item
