import React from 'react'
import { StyleSheet } from 'quantum'
import { CartIcon, SearchIcon } from 'bypass/ui/icons'
import Item from './Item'

const styles = StyleSheet.create({
  self: {
    background: '#546e7a',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    zIndex: 5,
    width: '70px',
  },
})

const goToCart = () => {
  window.location.hash = '/cards/cart'
}

const Toolbar = ({ cart, showSearch, onShowSearch, onHideSearch }) => (
  <div className={styles()}>
    <Item active onClick={showSearch ? onHideSearch : onShowSearch}>
      <SearchIcon fill='#ffffff' width={30} />
    </Item>
    <Item onClick={goToCart}>
      <div>
        {cart.count}
      </div>
      <div>
        <CartIcon fill='#ffffff' width={30} />
      </div>
      <div>
        ${cart.price}
      </div>
    </Item>
  </div>
)

export default Toolbar
