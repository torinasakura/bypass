import React from 'react'
import { StyleSheet } from 'quantum'
import { Condition } from 'bypass/ui/condition'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    background: 'white',
    flexBasis: '50%',
    boxSizing: 'border-box',
    maxWidth: '50%',
    '& div': {
      color: '#37474f',
      background: '#fafafa',
      cursor: 'pointer',
      padding: '6px 5px 6px 10px',
      transition: 'all 0.3s',
      fontSize: '14px',
      margin: '5px',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    '& svg': {
      marginRight: '2px',
    },
    '&:hover div': {
      background: '#e6e6e6',
    },
  },
})

const Item = ({ children, value = [], touched, onClick }) => (
  <div className={styles()} onClick={onClick}>
    <div>
      <ArrowIcon right width={9} fill='#37474f' />
      {children}
      <Condition match={value.length > 0}>
        <span>
          {'\u00A0'}
          ({value.length})
        </span>
      </Condition>
      <Condition match={touched}>
        <span>
          {'\u00A0'}
          (1)
        </span>
      </Condition>
    </div>
  </div>
)

export default Item
