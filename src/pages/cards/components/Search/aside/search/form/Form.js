import React from 'react'
import { StyleSheet } from 'quantum'
import Item from './Item'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: '0 5px',
    marginBottom: '10px',
  },
})

const Form = ({ search, onShowFilter }) => (
  <span className={styles()}>
    <Item value={search.bin.value} onClick={onShowFilter.bind(null, 'bin')}>
      {__i18n('SEARCH.ASIDE.BIN')}
    </Item>
    <Item value={search.address.value} onClick={onShowFilter.bind(null, 'address')}>
      {__i18n('SEARCH.ASIDE.ADDRESS')}
    </Item>
    <Item value={search.brand.value} onClick={onShowFilter.bind(null, 'brand')}>
      {__i18n('SEARCH.ASIDE.BRAND')}
    </Item>
    <Item value={search.seller.value} onClick={onShowFilter.bind(null, 'seller')}>
      {__i18n('SEARCH.ASIDE.SELLER')}
    </Item>
    <Item value={search.type.value} onClick={onShowFilter.bind(null, 'type')}>
      {__i18n('SEARCH.ASIDE.TYPE')}
    </Item>
    <Item value={search.base.value} onClick={onShowFilter.bind(null, 'base')}>
      {__i18n('SEARCH.ASIDE.BASE')}
    </Item>
    <Item value={search.level.value} onClick={onShowFilter.bind(null, 'level')}>
      {__i18n('SEARCH.ASIDE.LEVEL')}
    </Item>
    <Item value={search.load.value} onClick={onShowFilter.bind(null, 'load')}>
      {__i18n('SEARCH.ASIDE.LOAD')}
    </Item>
    <Item value={search.bank.value} onClick={onShowFilter.bind(null, 'bank')}>
      {__i18n('SEARCH.ASIDE.BANK')}
    </Item>
    <Item touched={search.dates.touched} onClick={onShowFilter.bind(null, 'period')}>
      {__i18n('SEARCH.ASIDE.PERIOD')}
    </Item>
    <Item value={search.country.value} onClick={onShowFilter.bind(null, 'country')}>
      {__i18n('SEARCH.ASIDE.COUNTRY')}
    </Item>
    <Item touched={search.valid.touched} onClick={onShowFilter.bind(null, 'valid')}>
      {__i18n('STAT.VALID')}
    </Item>
    <Item value={search.state.value} onClick={onShowFilter.bind(null, 'state')}>
      {__i18n('SEARCH.ASIDE.STATE')}
    </Item>
    <Item value={search.city.value} onClick={onShowFilter.bind(null, 'city')}>
      {__i18n('SEARCH.ASIDE.CITY')}
    </Item>
    <Item value={search.zip.value} touched={search.zip.mask} onClick={onShowFilter.bind(null, 'zip')}>
      {__i18n('SEARCH.ASIDE.ZIP')}
    </Item>
    <Item touched={search.sort.touched} onClick={onShowFilter.bind(null, 'sort')}>
      {__i18n('COM.SORT')}
    </Item>
  </span>
)

export default Form
