/* eslint-disable eqeqeq */
import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { Title } from './title'
import { Form } from './form'
import Toolbar from './Toolbar'
import SearchName from './SearchName'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    zIndex: 4,
    height: '100%',
    background: '#ffffff',
    boxShadow: '4px 0 4px 0 rgba(0, 0, 0, 0.24)',
    transition: 'all 0.3s ease',
    boxSizing: 'border-box',
    paddingLeft: '70px',
    paddingBottom: '10px',
    overflowY: 'auto',
    overflowX: 'hidden',
    width: '35%',
    left: '-35%',
  },
  fill: {
    width: '100%',
    left: '-100%',
  },
  show: {
    left: '0px',
  },
})

const Search = ({
  fill, wrapToolbar, show, search, onShowFilter,
  onSearch, onHide, onClearSearch, onLoadSearch,
  onSaveSearch, onToggleDefault,
}) => (
  <div className={styles({ show, fill })}>
    <Title onHide={onHide} />
    <SearchName
      isDefault={search.def == 1}
      onToggleDefault={onToggleDefault}
    >
      {search.search_name}
    </SearchName>
    <Form
      search={search}
      onShowFilter={onShowFilter}
    />
    <Toolbar wrap={wrapToolbar}>
      <Button
        type='light'
        size='small'
        onClick={onLoadSearch}
      >
        {__i18n('LANG.BUTTONS.LOAD_SEARCH')}
      </Button>
      <Button
        type='light'
        size='small'
        onClick={onSaveSearch}
      >
        {__i18n('LANG.BUTTONS.SAVE_SEARCH')}
      </Button>
      <Button
        type='light'
        size='small'
        onClick={onClearSearch}
      >
        {__i18n('LANG.BUTTONS.RESET_SEARCH')}
      </Button>
    </Toolbar>
    <Toolbar wrap={wrapToolbar}>
      <Button
        onClick={onSearch}
      >
        {__i18n('LANG.BUTTONS.SEARCH_CARDS')}
      </Button>
    </Toolbar>
  </div>
)

export default Search
