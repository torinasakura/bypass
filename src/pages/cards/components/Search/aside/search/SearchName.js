import React from 'react'
import { StyleSheet } from 'quantum'
import { FavoriteIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    minHeight: '22px',
    '&:hover': {
      background: '#f5f5f5',
    },
  },
  hidden: {
    visibility: 'hidden',
  },
})

const SearchName = ({ children, isDefault, onToggleDefault }) => (
  <div className={styles({ hidden: !children })} onClick={onToggleDefault}>
    <span>
      {children}
    </span>
    <FavoriteIcon
      width={16}
      checked={isDefault}
      fill={isDefault ? '#fdd835' : null}
    />
  </div>
)

export default SearchName
