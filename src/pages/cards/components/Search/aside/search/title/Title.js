import React from 'react'
import { StyleSheet } from 'quantum'
import Collapse from './Collapse'
import { connect, hover as hoverState } from 'bypass/ui/state'

const styles = StyleSheet.create({
  self: {
    padding: '10px',
    color: '#000000',
    fontSize: '19px',
    textAlign: 'center',
    cursor: 'pointer',
    display: 'flex',
  },
  hover: {
    background: '#f5f5f5',
  },
})

const Title = ({ hover, onMouseEnter, onMouseLeave, onHide }) => (
  <div
    className={styles({ hover })}
    onClick={onHide}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    <div style={{ width: '100%' }}>
      {__i18n('SEARCH.ASIDE.CRITERIA')}
    </div>
    <Collapse hover={hover} />
  </div>
)

export default connect([hoverState], Title)
