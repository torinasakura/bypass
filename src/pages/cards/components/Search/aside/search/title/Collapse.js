import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #e0e0e0',
    borderRadius: '50%',
    width: '27px',
    height: '27px',
    display: 'inline-block',
    boxSizing: 'border-box',
    '& svg': {
      fill: '#e0e0e0',
      left: '-2px',
      position: 'relative',
    },
  },
  hover: {
    borderColor: '#37474f',
    '& svg': {
      fill: '#37474f',
    },
  },
})

const Collapse = ({ hover }) => (
  <span className={styles({ hover })}>
    <ArrowIcon left />
  </span>
)

export default Collapse
