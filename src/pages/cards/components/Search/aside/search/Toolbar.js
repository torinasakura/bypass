import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginTop: '0px',
    '& button': {
      flexBasis: '30%',
      margin: '0 2px 10px 2px',
    },
  },
  wrap: {
    '& button': {
      flexBasis: '100%',
      marginRight: '20px',
      marginLeft: '20px',
    },
  },
})

const Toolbar = ({ children, wrap }) => (
  <div className={styles({ wrap })}>
    {children}
  </div>
)

export default Toolbar
