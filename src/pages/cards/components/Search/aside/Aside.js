import React from 'react'
import { StyleSheet } from 'quantum'
import { Toolbar } from './toolbar'
import { Search } from './search'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
  },
})

const Aside = ({
  cart, fill, wrapToolbar, search, showSearch,
  onToggleDefault, onShowFilter, onSearch, onShowSearch,
  onHideSearch, onClearSearch, onSaveSearch, onLoadSearch,
}) => (
  <div className={styles()}>
    <Toolbar
      cart={cart}
      showSearch={showSearch}
      onShowSearch={onShowSearch}
      onHideSearch={onHideSearch}
    />
    <Search
      fill={fill}
      search={search}
      show={showSearch}
      wrapToolbar={wrapToolbar}
      onHide={onHideSearch}
      onShowFilter={onShowFilter}
      onSearch={onSearch}
      onClearSearch={onClearSearch}
      onSaveSearch={onSaveSearch}
      onLoadSearch={onLoadSearch}
      onToggleDefault={onToggleDefault}
    />
  </div>
)

export default Aside
