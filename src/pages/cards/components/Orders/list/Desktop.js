import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { HowUpTimeout } from 'bypass/ui/notice'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Desktop = ({
  orders, total, hasSelected, checkTimeout,
  onLoad, allSelected, onSelect, onSelectAll,
  onDownload, onEditComment, onRemove,
}) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout basis='10px' />
          <Layout>
            <ColumnLayout align='flex-end'>
              <Layout grow={1}>
                <Title offset='right'>
                  {__i18n('NAV.CARDS.ORDERS')}
                </Title>
              </Layout>
              <Layout>
                <HowUpTimeout timeout={checkTimeout} />
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
          <Layout grow={1} shrink={1}>
            <List
              orders={orders}
              total={total}
              rowHeight={30}
              allSelected={allSelected}
              onLoad={onLoad}
              onSelect={onSelect}
              onSelectAll={onSelectAll}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} />
              <Layout>
                <Button onClick={onDownload} disabled={!orders.size}>
                  {hasSelected ? __i18n('LANG.BUTTONS.DOWNLOAD') : __i18n('LANG.BUTTONS.DOWNLOAD_ALL')}
                </Button>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Button disabled={!hasSelected} onClick={onEditComment}>
                  {__i18n('LANG.BUTTONS.EDIT_COMMENT')}
                </Button>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Button type='red' disabled={!hasSelected} onClick={onRemove}>
                  {__i18n('LANG.BUTTONS.DELETE')}
                </Button>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
