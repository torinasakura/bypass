import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Mobile = ({
  orders, total, hasSelected, onLoad, onSelect,
  allSelected, onSelectAll, onEditComment, onRemove,
}) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small' white>
            {__i18n('NAV.CARDS.ORDERS')}
          </Title>
        </Layout>
        <Layout touch scrollX grow={1} shrink={1}>
          <List
            orders={orders}
            total={total}
            rowHeight={30}
            minWidth={900}
            allSelected={allSelected}
            onLoad={onLoad}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onEditComment}>
                {__i18n('LANG.BUTTONS.EDIT_COMMENT')}
              </Button>
            </Layout>
            <Layout grow={1}>
              <Button fill size='small' type='red' disabled={!hasSelected} onClick={onRemove}>
                {__i18n('LANG.BUTTONS.DELETE')}
              </Button>
            </Layout>
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
