import React from 'react'
import { timeConverter } from 'bypass/app/utils/utils'
import { Table, Column, BodyCell, HeaderCell } from 'bypass/ui/table'
import { Checkbox } from 'bypass/ui/checkbox'
import { Price } from 'bypass/ui/price'
import Link from 'bypass/ui/link/Link'

const List = ({ minWidth, rowHeight, total, orders, onLoad, allSelected, onSelect, onSelectAll }) => (
  <Table
    list={orders}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onLoad={onLoad}
    onRowClick={onSelect}
  >
    <Column
      label={__i18n('LANG.TABLE.COLS.INDEX')}
      width='80px'
      dataKey='index'
      cellDataGetter={(dataKey, rowData, columnData, columnIndex, rowIndex) => rowIndex + 1}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.ID')}
      width='90px'
      dataKey='order_id'
    />
    <Column
      label={__i18n('ORDERS.TABLE.ADD_TIME')}
      width='90px'
      flexGrow={1}
      dataKey='add_time'
      cellDataGetter={(dataKey, rowData) => timeConverter(rowData.get(dataKey), true, true)}
    />
    <Column
      label={__i18n('ORDERS.TABLE.COUNT')}
      width='90px'
      dataKey='count'
    />
    <Column
      label={__i18n('ORDERS.TABLE.VALID')}
      width='90px'
      dataKey='valid'
    />
    <Column
      label={__i18n('ORDERS.TABLE.UNOPEN')}
      width='90px'
      dataKey='opened'
      cellDataGetter={(dataKey, rowData) => rowData.get('count') - rowData.get('opened')}
    />
    <Column
      label={__i18n('ORDERS.TABLE.ORDER_COMMENT')}
      width='90px'
      flexGrow={1}
      dataKey='order_comment'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          {cellData || `<${__i18n('LANG.SERVICE.EMPTY_NOTE')}>`}
        </BodyCell>
      )}
    />
    <Column
      label={__i18n('ORDERS.TABLE.PRICE')}
      width='90px'
      dataKey='price'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='end' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Price green>
            {cellData}
          </Price>
        </BodyCell>
      )}
    />
    <Column
      label={__i18n('LANG.BUTTONS.CHECK')}
      width='90px'
      dataKey='check'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Link small dashed>
            {__i18n('LANG.BUTTONS.CHECK')}
          </Link>
        </BodyCell>
      )}
    />
    <Column
      label={__i18n('LANG.BUTTONS.SEE')}
      width='90px'
      dataKey='show'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Link
            small
            dashed
            color='lightBlue'
            to={`/cards/orders/${rowData.get('order_id')}`}
          >
              {__i18n('LANG.BUTTONS.SEE')}
          </Link>
        </BodyCell>
      )}
    />
    <Column
      label={'_'}
      width='40px'
      dataKey='selected'
      headerRenderer={() => (
        <HeaderCell justify='center'>
          <Checkbox checked={allSelected} onChange={onSelectAll} />
        </HeaderCell>
      )}
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Checkbox checked={cellData} />
        </BodyCell>
      )}
    />
  </Table>
)

export default List
