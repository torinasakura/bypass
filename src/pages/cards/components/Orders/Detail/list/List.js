import React from 'react'
import { Column, BodyCell, HeaderCell } from 'bypass/ui/table'
import { Checkbox } from 'bypass/ui/checkbox'
import { Table } from './table'
import getColumn from '../../../Search/list/column'

const columns = [
  '_row',
  'number', 'exp', 'cvv2',
  'holder', 'level', 'type',
  'bank', 'country_code',
  'state_code', 'city', 'zip',
  'address', 'email', 'phone',
  'provider', 'returned',
]

const List = ({ cards, minWidth, rowHeight, total, allSelected, onLoad, onSelect, onSelectAll }) => (
  <Table
    list={cards}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onLoad={onLoad}
    onRowClick={onSelect}
  >
    {columns.map(getColumn)}
    <Column
      label={'_'}
      width='50px'
      dataKey='selected'
      headerRenderer={() => (
        <HeaderCell justify='center'>
          <Checkbox checked={allSelected} onChange={onSelectAll} />
        </HeaderCell>
      )}
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Checkbox checked={cellData} />
        </BodyCell>
      )}
    />
  </Table>
)

export default List
