/* eslint-disable no-underscore-dangle */
import React from 'react'
import cn from 'classnames'
import { FlexTable as BaseFlexTable } from 'bypass/ui/table'
import Row from './Row'

class FlexTable extends BaseFlexTable {
  _createRow(rowIndex) {
    const {
      children,
      onRowClick,
      rowClassName,
      rowGetter,
    } = this.props

    const { scrollbarWidth } = this.state

    const rowClass = rowClassName instanceof Function ? rowClassName(rowIndex) : rowClassName
    const rowData = rowGetter(rowIndex)

    const a11yProps = {}

    if (onRowClick) {
      a11yProps['aria-label'] = 'row'
      a11yProps.role = 'row'
      a11yProps.tabIndex = 0
      a11yProps.onClick = event => onRowClick(event, rowIndex)
    }

    return (
      <Row
        {...a11yProps}
        key={rowIndex}
        className={cn('FlexTable__order-child-row', rowClass)}
        style={{
          height: this._getRowHeight(rowIndex) / 2,
          paddingRight: scrollbarWidth,
        }}
        rowData={rowData}
        columns={children}
        rowIndex={rowIndex}
      />
    )
  }
}

export default FlexTable
