import React from 'react'
import { BodyCell, Row } from 'bypass/ui/table'
import './Row.css'

class MultilineRow extends Row {
  getComment(rowData) {
    return rowData.get('comment') ? rowData.get('comment') : `<${__i18n('LANG.SERVICE.EMPTY_NOTE')}>`
  }

  renderCommentRow(rowData, rowIndex) {
    return (
      <div
        className='FlexTable__rowColumn'
        style={{ flex: '1 1 auto' }}
      >
        <BodyCell
          oddRow={(rowIndex % 2) !== 0}
          selected={rowData.get('selected')}
        >
          {__i18n('LANG.TABLE.COLS.COMMENT')}: {this.getComment(rowData)}
        </BodyCell>
      </div>
    )
  }

  render() {
    const { columns, rowData, rowIndex, ...props } = this.props

    return (
      <div className='FlexTable__order-row'>
        <div {...props}>
          {this.renderColumns(columns, rowData, rowIndex)}
        </div>
        <div {...props}>
          {this.renderCommentRow(rowData, rowIndex, props)}
        </div>
      </div>
    )
  }
}

export default MultilineRow
