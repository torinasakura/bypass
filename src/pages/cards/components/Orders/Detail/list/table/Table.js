import React from 'react'
import { AutoSizer, InfiniteLoader } from 'react-virtualized'
import { Table as BaseTable, NoRowsRenderer } from 'bypass/ui/table'
import FlexTable from './FlexTable'

class Table extends BaseTable {

  render() {
    const { total, list, children, minWidth, ...props } = this.props

    const rowGetter = index => list.get(index)
    const rowsCount = list.size

    return (
      <InfiniteLoader
        isRowLoaded={this.isRowLoaded}
        loadMoreRows={this.loadMoreRows}
        rowsCount={total}
      >
        {({ onRowsRendered, registerChild }) => (
          <AutoSizer>
            {({ width, height }) => (
              <FlexTable
                {...props}
                ref={registerChild}
                noRowsRenderer={NoRowsRenderer}
                onRowsRendered={onRowsRendered}
                onRowClick={this.onRowClick}
                rowGetter={rowGetter}
                rowsCount={rowsCount}
                height={height}
                width={minWidth > width ? minWidth : width}
              >
                {children}
              </FlexTable>
            )}
          </AutoSizer>
        )}
      </InfiniteLoader>
    )
  }
}

export default Table
