import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container, BackToPage } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Mobile = ({
  orderId, cards, total, hasSelected, allSelected,
  onLoad, onSelect, onSelectAll, onCheck,
  onShowCsv, onEditComment, onOpen,
}) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <BackToPage center to='/cards/orders'>
            {__i18n('LANG.SERVICE.BACK_TO_TICKETS')}
          </BackToPage>
        </Layout>
        <Layout>
          <Title white size='small'>
            {__i18n('LANG.SERVICE.ORDER_SHOW')} # {orderId}
          </Title>
        </Layout>
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            cards={cards}
            total={total}
            rowHeight={50}
            minWidth={980}
            allSelected={allSelected}
            onLoad={onLoad}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout>
          <Button fill size='small' type='light' disabled={!hasSelected} onClick={onEditComment}>
            {__i18n('LANG.BUTTONS.EDIT_COMMENT')}
          </Button>
        </Layout>
        <Layout>
          <ColumnLayout align='center'>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onCheck}>
                {__i18n('NAV.CHECK.CHECK')}
              </Button>
            </Layout>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onShowCsv}>
                CSV
              </Button>
            </Layout>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onOpen}>
                {__i18n('LANG.BUTTONS.OPEN')}
              </Button>
            </Layout>
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
