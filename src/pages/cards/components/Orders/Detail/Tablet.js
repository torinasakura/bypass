import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container, BackToPage } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { HowUpTimeout } from 'bypass/ui/notice'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Tablet = ({
  orderId, cards, total, hasSelected, allSelected,
  checkTimeout, onLoad, onSelect, onSelectAll,
  onCheck, onShowCsv, onEditComment, onOpen,
}) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout basis='10px' />
        <Layout>
          <ColumnLayout align='center'>
            <Layout basis='10px' />
            <Layout>
              <BackToPage large to='/cards/orders'>
                {__i18n('LANG.SERVICE.BACK_TO_TICKETS')}
              </BackToPage>
            </Layout>
            <Layout grow={1}>
              <Title size='medium'>
                {__i18n('LANG.SERVICE.ORDER_SHOW')} # {orderId}
              </Title>
            </Layout>
            <Layout>
              <HowUpTimeout timeout={checkTimeout} />
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='10px' />
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            cards={cards}
            total={total}
            rowHeight={50}
            minWidth={980}
            allSelected={allSelected}
            onLoad={onLoad}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout align='center'>
            <Layout grow={1} />
            <Layout>
              <Button disabled={!hasSelected} onClick={onCheck}>
                {__i18n('LANG.BUTTONS.CHECK_CARDS')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button disabled={!hasSelected} onClick={onShowCsv}>
                {__i18n('LANG.BUTTONS.SHOW_CSV')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button disabled={!hasSelected} onClick={onEditComment}>
                {__i18n('LANG.BUTTONS.EDIT_COMMENT')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button disabled={!hasSelected} onClick={onOpen}>
                {__i18n('LANG.BUTTONS.OPEN')}
              </Button>
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
