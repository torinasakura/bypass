import React from 'react'
import { StyleSheet } from 'quantum'

const getMessage = (count, price) =>
                    __i18n('CART.SUMMARY')
                      .replace('${count}', count)
                      .replace('${summ}', `$${price}`)

const styles = StyleSheet.create({
  self: {
    fontSize: '18px',
    width: '100%',
  },
})

const ToPurchase = ({ count, price }) => {
  if (!count && !price) {
    return null
  }

  return (
    <div
      className={styles()}
      dangerouslySetInnerHTML={{ __html: getMessage(count, price) }}
    />
  )
}


export default ToPurchase
