import React from 'react'
import { Table, Column, BodyCell, HeaderCell } from 'bypass/ui/table'
import { Checkbox } from 'bypass/ui/checkbox'
import getColumn from './column'

const List = ({ minWidth, rowHeight, total, cards, columns, allSelected, onSelect, onSelectAll }) => (
  <Table
    list={cards}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onRowClick={onSelect}
  >
    {columns.map(getColumn)}
    <Column
      label={'_'}
      width='50px'
      dataKey='selected'
      headerRenderer={() => (
        <HeaderCell justify='center'>
          <Checkbox checked={allSelected} onChange={onSelectAll} />
        </HeaderCell>
      )}
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Checkbox checked={cellData} />
        </BodyCell>
      )}
    />
  </Table>
)

export default List
