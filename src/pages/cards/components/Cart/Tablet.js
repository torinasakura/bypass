import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { HowUpTimeout } from 'bypass/ui/notice'
import { Button } from 'bypass/ui/button'
import ToPurchase from './ToPurchase'
import { List } from './list'

const Tablet = ({
  cards, total, columns, hasSelected, allSelected,
  toPurchase = {}, checkTimeout, onSelect,
  onSelectAll, onBuy, onFastBuy, onRemove,
}) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout basis='10px' />
        <Layout>
          <ColumnLayout align='center'>
            <Layout grow={1}>
              <Title size='medium' offset='right'>
                {__i18n('NAV.CARDS.CART')}
              </Title>
            </Layout>
            <Layout>
              <HowUpTimeout timeout={checkTimeout} />
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            cards={cards}
            total={total}
            rowHeight={30}
            minWidth={980}
            columns={columns}
            allSelected={allSelected}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <ColumnLayout align='center'>
            <Layout basis='10px' />
            <Layout grow={1}>
              <ToPurchase {...toPurchase} />
            </Layout>
            <Layout>
              <Button disabled={!hasSelected} onClick={onFastBuy}>
                {__i18n('SEARCH.FAST_BUY.FAST')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button disabled={!hasSelected} onClick={onBuy}>
                {__i18n('LANG.BUTTONS.BUY')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button type='red' disabled={!hasSelected} onClick={onRemove}>
                {__i18n('LANG.BUTTONS.DELETE')}
              </Button>
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
