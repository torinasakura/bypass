import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { HowUpTimeout } from 'bypass/ui/notice'
import { Button } from 'bypass/ui/button'
import ToPurchase from './ToPurchase'
import { List } from './list'

const Desktop = ({
  cards, total, columns, hasSelected, allSelected,
  toPurchase = {}, checkTimeout, onSelect,
  onSelectAll, onBuy, onFastBuy, onRemove,
}) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout basis='10px' />
          <Layout>
            <ColumnLayout align='center'>
              <Layout grow={1}>
                <Title offset='right'>
                  {__i18n('NAV.CARDS.CART')}
                </Title>
              </Layout>
              <Layout>
                <HowUpTimeout timeout={checkTimeout} />
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='10px' />
          <Layout grow={1} shrink={1}>
            <List
              cards={cards}
              total={total}
              rowHeight={30}
              columns={columns}
              allSelected={allSelected}
              onSelect={onSelect}
              onSelectAll={onSelectAll}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout align='center'>
              <Layout grow={1}>
                <ToPurchase {...toPurchase} />
              </Layout>
              <Layout>
                <Button disabled={!hasSelected} onClick={onFastBuy}>
                  {__i18n('SEARCH.FAST_BUY.FAST')}
                </Button>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Button disabled={!hasSelected} onClick={onBuy}>
                  {__i18n('LANG.BUTTONS.BUY')}
                </Button>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <Button type='red' disabled={!hasSelected} onClick={onRemove}>
                  {__i18n('LANG.BUTTONS.DELETE')}
                </Button>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
