import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Mobile = ({
  cards, total, columns, hasSelected, allSelected,
  onSelect, onSelectAll, onBuy, onFastBuy, onRemove,
}) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1}>
              <Title size='small' white>
                {__i18n('NAV.CARDS.CART')}
              </Title>
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout scrollX touch grow={1} shrink={1}>
          <List
            cards={cards}
            total={total}
            rowHeight={30}
            minWidth={980}
            columns={columns}
            allSelected={allSelected}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onFastBuy}>
                {__i18n('SEARCH.FAST_BUY.FAST')}
              </Button>
            </Layout>
            <Layout grow={1}>
              <Button fill size='small' disabled={!hasSelected} onClick={onBuy}>
                {__i18n('LANG.BUTTONS.BUY')}
              </Button>
            </Layout>
            <Layout grow={1}>
              <Button fill size='small' type='red' disabled={!hasSelected} onClick={onRemove}>
                {__i18n('LANG.BUTTONS.DELETE')}
              </Button>
            </Layout>
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
