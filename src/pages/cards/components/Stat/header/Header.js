import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Text } from 'bypass/ui/text'
import Links from './Links'

const styles = StyleSheet.create({
  self: {
    boxShadow: '0 3px #e0e0e0',
    background: '#eceff1',
    padding: '10px',
    width: '100%',
  },
})

const Header = () => (
  <div className={styles()}>
    <ColumnLayout align='center'>
      <Layout grow={1}>
        <Text size={19}>
          {__i18n('PROFILE.STATISTIC.TEXT')}
        </Text>
      </Layout>
      <Layout>
        <Links />
      </Layout>
    </ColumnLayout>
  </div>
)

export default Header
