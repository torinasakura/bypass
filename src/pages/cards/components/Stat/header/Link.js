import React from 'react'
import { StyleSheet } from 'quantum'
import { Link as RouterLink } from 'react-router'

const styles = StyleSheet.create({
  self: {
    border: 0,
    outline: 0,
    display: 'inline-block',
    textAlign: 'center',
    cursor: 'pointer',
    transition: 'all 0.3s',
    padding: '10px 20px',
    fontSize: '18px',
    fontFamily: 'PT Sans',
    textDecoration: 'none',
    color: '#1a1a1a',
    background: '#ffffff',
    flexGrow: 1,
    '&:hover': {
      background: '#e6e6e6',
    },
  },
  active: {
    color: '#ffffff',
    background: '#546e7a',
    '&:hover': {
      background: '#3f535c',
    },
  },
  small: {
    fontSize: '12px',
  },
})

const Link = ({ to, children, small }) => (
  <RouterLink
    to={to}
    className={styles({ small })}
    activeClassName={styles({ active: true })}
  >
    {children}
  </RouterLink>
)

export default Link
