import React from 'react'
import { StyleSheet } from 'quantum'
import Link from './Link'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
})

const Links = ({ small }) => (
  <div className={styles()}>
    <Link to='/cards/stat/stat' small={small}>
      {__i18n('STAT.STAT.TEXT')}
    </Link>
    <Link to='/cards/stat/dyn' small={small}>
      {__i18n('STAT.DYN.TEXT')}
    </Link>
  </div>
)

export default Links
