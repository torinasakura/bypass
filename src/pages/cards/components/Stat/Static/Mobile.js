import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Links } from '../header'
import { Load } from './load'
import { Base } from './base'
import { Seller } from './seller'

const Mobile = ({
  stat: { limitedLoad, limitedBase, limitedSellers },
  onOpenDetailBase, onOpenDetailLoad, onOpenDetailSeller,
}) => (
  <Container>
    <RowLayout>
      <Layout>
        <Title size='small'>
          {__i18n('PROFILE.STATISTIC.TEXT')}
        </Title>
      </Layout>
      <Layout>
        <Links small />
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='10px' />
          <Layout grow={1}>
            <Load
              data={limitedLoad}
              onDetail={onOpenDetailLoad}
            />
          </Layout>
          <Layout basis='10px' />
        </ColumnLayout>
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='10px' />
          <Layout grow={1}>
            <Base
              data={limitedBase}
              onDetail={onOpenDetailBase}
            />
          </Layout>
          <Layout basis='10px' />
        </ColumnLayout>
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='10px' />
          <Layout grow={1}>
            <Seller
              data={limitedSellers}
              onDetail={onOpenDetailSeller}
            />
          </Layout>
          <Layout basis='10px' />
        </ColumnLayout>
      </Layout>
      <Layout basis='20px' />
    </RowLayout>
  </Container>
)

export default Mobile
