import React from 'react'
import { Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Header } from '../header'
import { Load } from './load'
import { Base } from './base'
import { Seller } from './seller'

const Tablet = ({
  stat: { limitedLoad, limitedBase, limitedSellers },
  onOpenDetailBase, onOpenDetailLoad, onOpenDetailSeller,
}) => (
  <Container>
    <RowLayout>
      <Layout>
        <Header />
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='20%' />
          <Layout grow={1}>
            <Load
              data={limitedLoad}
              onDetail={onOpenDetailLoad}
            />
          </Layout>
          <Layout basis='20%' />
        </ColumnLayout>
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='20%' />
          <Layout grow={1}>
            <Base
              data={limitedBase}
              onDetail={onOpenDetailBase}
            />
          </Layout>
          <Layout basis='20%' />
        </ColumnLayout>
      </Layout>
      <Layout basis='40px' />
      <Layout>
        <ColumnLayout>
          <Layout basis='20%' />
          <Layout grow={1}>
            <Seller
              data={limitedSellers}
              onDetail={onOpenDetailSeller}
            />
          </Layout>
          <Layout basis='20%' />
        </ColumnLayout>
      </Layout>
      <Layout basis='20px' />
    </RowLayout>
  </Container>
)

export default Tablet
