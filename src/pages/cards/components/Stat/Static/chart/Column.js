import React from 'react'
import { StyleSheet } from 'quantum'
import './Column.css'

const styles = StyleSheet.create({
  self: {
    width: '20px',
    height: '230px',
    background: '#546e7a',
    position: 'relative',
    overflow: 'hidden',
    '& span': {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      zIndex: 1,
    },
  },
})

const Column = () => (
  <div className={`StatColumn ${styles()}`}>
    <span />
  </div>
)

export default Column
