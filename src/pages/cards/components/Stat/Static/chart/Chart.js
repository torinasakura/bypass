import React from 'react'
import { StyleSheet } from 'quantum'
import { connect, hover as hoverState } from 'bypass/ui/state'
import { Condition } from 'bypass/ui/condition'
import { Tooltip } from 'bypass/ui/tooltip'
import Column from './Column'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
})

const Chart = ({ tooltip, hover, onMouseEnter, onMouseLeave }) => (
  <div
    className={styles()}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    <Column />
    <Condition match={hover}>
      <Tooltip align='top' offset='10px 0'>
        {tooltip}
      </Tooltip>
    </Condition>
  </div>
)

export default connect([hoverState], Chart)
