import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { Chart } from '../chart'
import Title from '../Title'
import Table from './Table'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
})

const getTooltip = (base = {}) =>
  __i18n('STAT.STAT.BASE_TOOLTIP')
                .replace('${name}', base.name)
                .replace('${valid}', base.valid ? parseFloat(base.valid).toFixed(2) : __i18n('COM._EMPTY_'))

const Base = ({ data = [], onDetail }) => (
  <div className={styles()}>
    <Title>
      {__i18n('STAT.BASES')}
    </Title>
    <Chart
      tooltip={getTooltip(data[0])}
    />
    <Table data={data} />
    <Button fill type='light' onClick={onDetail}>
      {__i18n('STAT.STAT.SHOW_ALLS')} {__i18n('STAT.BASES')}
    </Button>
  </div>
)

export default Base
