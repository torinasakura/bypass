import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    textAlign: 'center',
    fontSize: '14px',
    margin: '10px 0',
    '& thead th': {
      borderBottom: '1px solid #e0e0e0 !important',
      lineHeight: '30px',
    },
  },
  bordered: {
    transition: 'all 0.3s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    background: '#ffffff',
  },
})

const Row = (base, index) => (
  <tr key={index}>
    <td>{index + 1}.</td>
    <td>
      {base.name}
    </td>
    <td>
      {base.seller_name}
    </td>
    <td>{base.available}</td>
    <td>
      {base.valid ? `${parseFloat(base.valid).toFixed(2)}%` : `<${__i18n('COM.EMPTY')}>`}
    </td>
  </tr>
)

const Table = ({ data, bordered }) => (
  <table className={styles({ bordered })}>
    <thead>
      <tr>
        <th />
        <th>{__i18n('STAT.NAME')}</th>
        <th>{__i18n('STAT.SELLER')}</th>
        <th>{__i18n('STAT.SUMM')}</th>
        <th>{__i18n('STAT.VALID')}</th>
      </tr>
    </thead>
    <tbody>
      {data.map(Row)}
    </tbody>
  </table>
)

export default Table
