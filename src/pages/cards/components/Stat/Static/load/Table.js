import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    textAlign: 'center',
    fontSize: '14px',
    margin: '10px 0',
    '& thead th': {
      borderBottom: '1px solid #e0e0e0 !important',
      lineHeight: '30px',
    },
  },
  bordered: {
    transition: 'all 0.3s',
    border: '2px solid #9e9e9e',
    borderRadius: '5px',
    background: '#ffffff',
  },
})

const Row = (load, index) => (
  <tr key={index}>
    <td>{index + 1}.</td>
    <td>{load.load_id}</td>
    <td>{load.time_formatted}</td>
    <td>
      {load.base_name ? load.base_name : `<${__i18n('COM.EMPTY')}>`}
    </td>
    <td>{load.available}</td>
    <td>{load.valid ? `${parseFloat(load.valid).toFixed(2)}%` : `<${__i18n('COM.EMPTY')}>`}</td>
  </tr>
)

const Table = ({ data, bordered }) => (
  <table className={styles({ bordered })}>
    <thead>
      <tr>
        <th />
        <th>{__i18n('STAT.ID')}</th>
        <th>{__i18n('STAT.DATE')}</th>
        <th>{__i18n('STAT.BASE')}</th>
        <th>{__i18n('STAT.SUMM')}</th>
        <th>{__i18n('STAT.VALID')}</th>
      </tr>
    </thead>
    <tbody>
      {data.map(Row)}
    </tbody>
  </table>
)

export default Table
