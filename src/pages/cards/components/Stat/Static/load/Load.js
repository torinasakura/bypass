import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { Chart } from '../chart'
import Title from '../Title'
import Table from './Table'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
})

const getTooltip = (load = {}) =>
  __i18n('STAT.STAT.LOAD_TOOLTIP')
                .replace('${time_formatted}', load.time_formatted)
                .replace('${valid}', load.valid ? parseFloat(load.valid).toFixed(2) : __i18n('COM._EMPTY_'))

const Load = ({ data = [], onDetail }) => (
  <div className={styles()}>
    <Title>
      {__i18n('STAT.LOAD')}
    </Title>
    <Chart
      tooltip={getTooltip(data[0])}
    />
    <Table data={data} />
    <Button fill type='light' onClick={onDetail}>
      {__i18n('STAT.STAT.SHOW_ALLS')} {__i18n('STAT.LOAD')}
    </Button>
  </div>
)

export default Load
