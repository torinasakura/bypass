import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { Chart } from '../chart'
import Title from '../Title'
import Table from './Table'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
})

const getTooltip = (seller = {}) =>
  __i18n('STAT.STAT.SELLERS_TOOLTIP')
                .replace('${name}', seller.name)
                .replace('${valid}', seller.valid ? parseFloat(seller.valid).toFixed(2) : __i18n('COM._EMPTY_'))

const Seller = ({ data = [], onDetail }) => (
  <div className={styles()}>
    <Title>
      {__i18n('STAT.SELLERS')}
    </Title>
    <Chart
      tooltip={getTooltip(data[0])}
    />
    <Table data={data} />
    <Button fill type='light' onClick={onDetail}>
      {__i18n('STAT.STAT.SHOW_ALLS')} {__i18n('STAT.SELLERSS')}
    </Button>
  </div>
)

export default Seller
