import React from 'react'
import { Wrapper, Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Header } from '../header'
import { Load } from './load'
import { Base } from './base'
import { Seller } from './seller'

const Desktop = ({
  stat: { limitedLoad, limitedBase, limitedSellers },
  onOpenDetailBase, onOpenDetailLoad, onOpenDetailSeller,
}) => (
  <Wrapper>
    <Container>
      <RowLayout>
        <Layout>
          <Header />
        </Layout>
        <Layout basis='40px' />
        <Layout>
          <ColumnLayout>
            <Layout basis='50px' />
            <Layout grow={1} shrink={1}>
              <Load
                data={limitedLoad}
                onDetail={onOpenDetailLoad}
              />
            </Layout>
            <Layout basis='20px' />
            <Layout grow={1} shrink={1}>
              <Base
                data={limitedBase}
                onDetail={onOpenDetailBase}
              />
            </Layout>
            <Layout basis='20px' />
            <Layout grow={1} shrink={1}>
              <Seller
                data={limitedSellers}
                onDetail={onOpenDetailSeller}
              />
            </Layout>
            <Layout basis='50px' />
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </Container>
  </Wrapper>
)

export default Desktop
