import React from 'react'
import Selector from './Selector'
import Items from './Items'
import Item from './Item'

class Update extends Selector {
  getItems() {
    const { update, base, filter } = this.state
    const regexp = new RegExp(filter, 'i')

    const baseSelected = base.reduce((result, item) => {
      if (item.selected) {
        result.push(item.base_id)
      }

      return result
    }, [])

    return update.filter(item => baseSelected.includes(item.base_id))
                 .filter(item => regexp.test(item.name))
  }

  onSelect(item) {
    const { onSelect } = this.props

    onSelect(item)
  }

  renderItems() {
    const items = this.getItems()

    return (
      <Items>
        {items.map((item, index) => (
          <Item
            key={index}
            selected={item.selected}
            onSelect={this.onSelect.bind(this, item.load_id)}
          >
            {item.name}
          </Item>
        ))}
      </Items>
    )
  }
}

export default Update
