import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import Header from './Header'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    overflow: 'hidden',
  },
})

class Selector extends Component {
  static defaultProps = {
    base: [],
    update: [],
    filter: '',
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      base: props.base,
      update: props.update,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.base !== this.props.base) {
      this.setState({ base: nextProps.base })
    }

    if (nextProps.update !== this.props.update) {
      this.setState({ update: nextProps.update })
    }
  }

  onFilter = ({ target }) => {
    this.setState({ filter: target.value })
  }

  getTitle() {
    return this.props.title
  }

  renderHeader() {
    const { filter } = this.state

    return (
      <Header
        title={this.getTitle()}
        filter={filter}
        onFilter={this.onFilter}
      />
    )
  }

  renderItems() {
    return null
  }

  render() {
    return (
      <div className={styles()}>
        {this.renderHeader()}
        {this.renderItems()}
      </div>
    )
  }
}

export default Selector
