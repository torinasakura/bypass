import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    height: '135px',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
})

const Items = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Items
