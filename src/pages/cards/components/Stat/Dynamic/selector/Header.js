import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { QuickSearch } from 'bypass/ui/QuickSearch'

const styles = StyleSheet.create({
  self: {
    background: '#d3e0e5',
    padding: '5px',
    fontSize: '14px',
    width: '100%',
    fontWeight: 'bold',
  },
})

const Header = ({ title, filter, onFilter }) => (
  <div className={styles()}>
    <ColumnLayout align='center'>
      <Layout grow={1} shrink={1}>
        {title}
      </Layout>
      <Layout>
        <QuickSearch
          value={filter}
          onChange={onFilter}
        />
      </Layout>
    </ColumnLayout>
  </div>
)

export default Header
