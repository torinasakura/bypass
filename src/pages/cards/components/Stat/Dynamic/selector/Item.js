import React from 'react'
import { StyleSheet } from 'quantum'
import { Checkbox } from 'bypass/ui/checkbox'

const styles = StyleSheet.create({
  self: {
    fontSize: '13px',
    color: '#333333',
    lineHeight: '30px',
    paddingLeft: '10px',
    cursor: 'pointer',
  },
  selected: {
    background: '#eceff1',
  },
  disabled: {
    opacity: 0.6,
  },
})

const Item = ({ children, selected, disabled, onSelect }) => (
  <div className={styles({ selected, disabled })} onClick={onSelect}>
    <Checkbox checked={selected} />
    <span>
      {children}
    </span>
  </div>
)

export default Item
