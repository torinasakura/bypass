/* eslint-disable camelcase */
import React from 'react'
import Selector from './Selector'
import Items from './Items'
import Item from './Item'

const ALL_ID = 'ALL'

class Base extends Selector {
  getItems() {
    const { filter } = this.state
    const regexp = new RegExp(filter, 'i')

    return this.state.base.filter(item => regexp.test(item.name))
  }

  onSelect(item) {
    const { base, allSelected } = this.state
    const { onSelect } = this.props

    if (item !== ALL_ID) {
      onSelect([item])
      return
    }

    if (!allSelected) {
      onSelect(base.map(({ base_id }) => base_id))
    } else {
      onSelect([])
    }

    this.setState({ allSelected: !allSelected })
  }

  renderItems() {
    const { allSelected } = this.state
    const items = this.getItems()

    return (
      <Items>
        <Item
          selected={allSelected}
          onSelect={this.onSelect.bind(this, ALL_ID)}
        >
          {__i18n('LANG.SERVICE.ALL_BASE')}
        </Item>
        {items.map((item, index) => (
          <Item
            key={index}
            disabled={allSelected}
            selected={!allSelected && item.selected}
            onSelect={!allSelected && this.onSelect.bind(this, item.base_id)}
          >
            {item.name}
          </Item>
        ))}
      </Items>
    )
  }
}

export default Base
