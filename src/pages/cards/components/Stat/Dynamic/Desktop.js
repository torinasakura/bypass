import React from 'react'
import { Wrapper, Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { ButtonGroup, Button } from 'bypass/ui/button'
import { Header } from '../header'
import { Controls } from './controls'
import { Base, Update } from './selector'
import { Chart } from './chart'
import Message from './Message'

const Desktop = ({
  stat: { update, base, period, data = {}, showControls, canShow },
  onClearSelection, onToggleControls, onSelectBase,
  onSelectUpdate, onChangePeriod, onShow,
}) => (
  <Wrapper>
    <Container>
      <RowLayout>
        <Layout>
          <Header />
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout>
            <Layout basis='15px' />
            <Layout grow={1}>
              <Controls
                show={showControls}
                onToggle={onToggleControls}
              >
                <RowLayout>
                  <Layout basis='10px' />
                  <Layout>
                    <ColumnLayout>
                      <Layout basis='10px' />
                      <Layout grow={1}>
                        <Base
                          title={__i18n('STAT.DYN.SELECT_BASE')}
                          base={base}
                          onSelect={onSelectBase}
                        />
                      </Layout>
                      <Layout basis='15px' />
                      <Layout grow={1}>
                        <Update
                          title={__i18n('STAT.DYN.SELECT_LOAD')}
                          base={base}
                          update={update}
                          onSelect={onSelectUpdate}
                        />
                      </Layout>
                      <Layout basis='15px' />
                      <Layout>
                        <RowLayout>
                          <Layout>
                            <ButtonGroup value={period} onChange={onChangePeriod}>
                              <Button value='week'>
                                {__i18n('STAT.DYN.TIME_UNIT.WEEK')}
                              </Button>
                              <Button value='month'>
                                {__i18n('STAT.DYN.TIME_UNIT.MONTH')}
                              </Button>
                              <Button value='season'>
                                {__i18n('STAT.DYN.TIME_UNIT.SEASON')}
                              </Button>
                            </ButtonGroup>
                          </Layout>
                          <Layout grow={1}>
                            <Message />
                          </Layout>
                          <Layout>
                            <ColumnLayout justify='center'>
                              <Layout>
                                <Button disabled={!canShow} onClick={onShow}>
                                  {__i18n('LANG.BUTTONS.SHOW')}
                                </Button>
                              </Layout>
                              <Layout basis='5px' />
                              <Layout>
                                <Button onClick={onClearSelection}>
                                  {__i18n('LANG.BUTTONS.CLEAR')}
                                </Button>
                              </Layout>
                            </ColumnLayout>
                          </Layout>
                        </RowLayout>
                      </Layout>
                      <Layout basis='10px' />
                    </ColumnLayout>
                  </Layout>
                  <Layout basis='10px' />
                </RowLayout>
              </Controls>
            </Layout>
            <Layout basis='15px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='30px' />
        <Layout>
          <ColumnLayout>
            <Layout basis='10px' />
            <Layout grow={1}>
              <RowLayout>
                <Layout>
                  <Chart
                    dynamic
                    period={period}
                    data={data.data4chart}
                    title={__i18n('LANG.SERVICE.VALID_COUNT')}
                  />
                </Layout>
                <Layout>
                  <Chart
                    small
                    period={period}
                    data={data.totalData}
                    title={__i18n('LANG.SERVICE.BUYED')}
                  />
                </Layout>
                <Layout>
                  <Chart
                    small
                    period={period}
                    data={data.closedData}
                    title={__i18n('LANG.SERVICE.CLOSED')}
                  />
                </Layout>
                <Layout>
                  <Chart
                    small
                    period={period}
                    data={data.checkedData}
                    title={__i18n('LANG.SERVICE.CHECKED')}
                  />
                </Layout>
              </RowLayout>
            </Layout>
            <Layout basis='30px' />
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </Container>
  </Wrapper>
)

export default Desktop
