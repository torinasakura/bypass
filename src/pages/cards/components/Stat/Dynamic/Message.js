import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    fontSize: '16px',
    whiteSpace: 'pre',
  },
  small: {
    fontSize: '12px',
  },
})

const Message = ({ small }) => (
  <div
    className={styles({ small })}
    dangerouslySetInnerHTML={{ __html: __i18n('STAT.DYN.LEGEND').replace('${maxSelected}', 10) }}
  />
)

export default Message
