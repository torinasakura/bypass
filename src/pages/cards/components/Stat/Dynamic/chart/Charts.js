import React from 'react'
import Highcharts from 'react-highcharts'
import { StyleSheet } from 'quantum'
import getConfig from './config'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    background: '#f5f5f5',
    '&:before': {
      position: 'absolute',
      left: '81px',
      top: '16px',
      zIndex: '1',
      background: '#d3e0e5',
      padding: '5px',
      fontWeight: 'bold',
      color: '#000000',
      content: 'attr(data-title)',
    },
  },
})

const Chart = ({ data, title, period, dynamic }) => (
  <div className={styles()}>
    <Highcharts
      data-title={title}
      config={getConfig(period, data, dynamic)}
    />
  </div>
)

export default Chart
