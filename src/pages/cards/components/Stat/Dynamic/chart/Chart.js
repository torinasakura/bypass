import React, { Component } from 'react'
import Highcharts from 'react-highcharts'
import shallowCompare from 'react-addons-shallow-compare'
import { StyleSheet } from 'quantum'
import getConfig from './config'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    position: 'relative',
    '&:before': {
      position: 'absolute',
      left: '81px',
      top: '16px',
      zIndex: '1',
      fontSize: '14px',
      background: '#d3e0e5',
      padding: '5px',
      fontWeight: 'bold',
      color: '#000000',
      content: 'attr(data-title)',
    },
  },
  small: {
    height: '200px',
  },
})

class Chart extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  render() {
    const { data, title, period, dynamic, small } = this.props

    if (!data) {
      return null
    }

    return (
      <Highcharts
        data-title={title}
        className={styles({ small })}
        config={getConfig(period, data, dynamic)}
      />
    )
  }
}

export default Chart
