import { merge } from 'lodash'

const config = {
  exporting: {
    enabled: false,
  },

  title: {
    text: '',
  },

  xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: {
      day: '%d.%m',
      week: '%d.%m',
      month: '%d.%m',
    },
    tickInterval: 24 * 3600 * 1000,
    title: {
      text: '',
    },
    plotLines: [{
      value: 2,
      width: 1,
      color: '#fdd835',
    }],
  },

  yAxis: {
    lineWidth: 1,
    allowDecimals: false,
    title: {
      text: '',
    },
    labels: {
      formatter: function formatter() {
        return `${this.value} ${__i18n('LANG.SERVICE.COUNT')}`
      },
      style: {
        width: '50px',
        'text-align': 'right',
        'min-width': '50px',
      },
      useHTML: true,
    },
    min: 0,
  },

  tooltip: {
    formatter: function formatter() {
      return `${__i18n('LANG.SERVICE.QUANT')}:${this.y} ${__i18n('COM.COUNT')}`
    },
    backgroundColor: '#607d8b',
    borderColor: '#607d8b',
    borderRadius: 5,
    borderWidth: 3,
    color: 'white',
    style: {
      color: '#FFFFFF',
    },
  },

  plotOptions: {
    series: {
      marker: {
        symbol: 'circle',
      },
    },
  },

  legend: {
    enabled: false,
  },

  credits: {
    enabled: false,
  },
}

function getPeriodConfig(period) {
  if (period === 'week') {
    return {
      xAxis: {
        tickPixelInterval: null,
        tickInterval: 24 * 3600 * 1000,
      },
    }
  } else if (period === 'month') {
    return {
      xAxis: {
        tickPixelInterval: 120,
        tickInterval: null,
      },
    }
  }

  return {
    xAxis: {
      tickPixelInterval: 150,
      tickInterval: null,
    },
  }
}

const dynamicConfig = {
  chart: {
    renderTo: 'dyn-charts',
  },

  yAxis: {
    lineWidth: 1,
    title: {
      text: '',
    },
    labels: {
      formatter: function formatter() {
        return `${String(this.value)}%`
      },
      style: {
        width: '50px',
        'text-align': 'right',
        'min-width': '50px',
      },
      useHTML: true,
    },
    min: 0,
    max: 100,
  },

  tooltip: {
    formatter: function formatter() {
      const valid = this.point.valid ? `${parseFloat(this.point.valid).toFixed(2)}%` : __i18n('COM._EMPTY_')

      return [
        `<b>${this.point.series.name}</b>`,
        `${this.point.day}<br/>`,
        `${__i18n('LANG.SERVICE.VALID_COUNT')}: ${valid}`,
        `${__i18n('LANG.SERVICE.BUYED')}: ${this.point.total}`,
        `${__i18n('LANG.SERVICE.CLOSED')}: ${this.point.closed}`,
        `${__i18n('LANG.SERVICE.CHECKED')}: ${this.point.checked}`,
      ].join('<br>')
    },
    backgroundColor: '#607d8b',
    borderColor: '#607d8b',
    borderRadius: 5,
    borderWidth: 3,
    color: 'white',
    style: {
      color: '#FFFFFF',
    },
  },
}

export default function getConfig(period, data, dynamic = false) {
  return merge(
    { series: data },
    config,
    getPeriodConfig(period),
    dynamic ? dynamicConfig : {}
  )
}
