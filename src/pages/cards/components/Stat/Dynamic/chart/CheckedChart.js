import React from 'react'
import Highcharts from 'react-highcharts'
import { StyleSheet } from 'quantum'
import getConfig from './config'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    background: '#f5f5f5',
  },
})

const CheckedChart = ({ data }) => (
  <div className={styles()}>
    <Highcharts
      id='checked-chart'
      config={getConfig('week', data)}
      className='checked-chart small-charts'
      data-title={__i18n('LANG.SERVICE.CHECKED')}
    />
  </div>
)

export default CheckedChart
