import React from 'react'
import { StyleSheet } from 'quantum'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import Toggle from './Toggle'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    background: '#f5f5f5',
  },
})

const Controls = ({ show, children, onToggle }) => (
  <div className={styles()}>
    <RowLayout>
      <Layout>
        <Condition match={show}>
          {children}
        </Condition>
      </Layout>
      <Layout>
        <Toggle
          toggled={show}
          onClick={onToggle}
        />
      </Layout>
    </RowLayout>
  </div>
)

export default Controls
