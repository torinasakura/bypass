import React from 'react'
import { StyleSheet } from 'quantum'
import { ArrowIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    background: '#d3e0e5',
    cursor: 'pointer',
    padding: '10px 0',
    textAlign: 'center',
    boxSizing: 'border-box',
    borderBottom: '2px solid transparent',
    '&:hover': {
      borderBottom: '2px solid #546e7a',
    },
    '& span': {
      display: 'inline-block',
      textAlign: 'center',
      border: '1px solid #546e7a',
      borderRadius: '50%',
      transition: 'all 0.3s',
      width: '32px',
      height: '32px',
      '& svg': {
        position: 'relative',
        top: '9px',
      },
    },
  },
  toggled: {
    '& span': {
      transform: 'rotate(-180deg)',
    },
  },
})

const Toggle = ({ toggled, onClick }) => (
  <div
    className={styles({ toggled })}
    onClick={onClick}
  >
    <span>
      <ArrowIcon down width={18} fill='#546e7a' />
    </span>
  </div>
)

export default Toggle
