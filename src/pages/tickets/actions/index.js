import { open, close } from '../../../app/actions/modal'
import * as actions from '../constants'

export function load(offset = 0) {
  return async (dispatch, getState, { get }) => {
    const perpage = getState().tickets.get('perpage')

    const { result } = await get('ticket', { body: { offset, perpage }, loader: offset === 0 })

    return dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}

export function loadDetail(id) {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('ticket/view', { body: { ticket_id: id } })

    return dispatch({
      type: actions.loadDetail,
      ...result,
    })
  }
}

export function openCreateModal() {
  return open('tickets/CreateTicket')
}

export function closeCreateModal() {
  return async dispatch => {
    dispatch(close())
    dispatch({ type: actions.changeTicketSubject, value: '' })
    dispatch({ type: actions.changeTicketMessage, value: '' })
  }
}

export function changeTicketSubject(value) {
  return {
    type: actions.changeTicketSubject,
    value,
  }
}

export function changeTicketMessage(value) {
  return {
    type: actions.changeTicketMessage,
    value,
  }
}

export function createTicket() {
  return async (dispatch, getState, { post }) => {
    const ticket = getState().tickets.get('ticket')

    const body = {
      status: 'open',
      ticket_id: '0',
      subject: ticket.get('subject'),
      text: ticket.get('message'),
    }

    const { response } = await post('ticket/new', { body })

    if (response.ok) {
      dispatch(close())
      dispatch(load())
    }

    dispatch({ type: actions.changeTicketSubject, value: '' })
    dispatch({ type: actions.changeTicketMessage, value: '' })
  }
}

export function changeMessage(value) {
  return {
    type: actions.changeMessage,
    value,
  }
}

export function sendMessage() {
  return async (dispatch, getState, { post }) => {
    const ticket = getState().tickets.getIn(['chat', 'ticket'])
    const text = getState().tickets.getIn(['chat', 'text'])
    const id = getState().router.params.id

    const body = {
      ticket_id: id,
      subject: ticket.get('ticket_subject'),
      status: ticket.get('ticket_status'),
      text,
    }

    const { response } = await post('ticket/new', { body })

    if (response.ok) {
      dispatch(loadDetail(id))
      dispatch(changeMessage(''))
    }
  }
}

export function changeStatus(status) {
  return async (dispatch, getState, { post }) => {
    const ticketId = getState().router.params.id

    const body = {
      ticket_id: ticketId,
      status,
    }

    const { result } = await post('ticket/change', { body })

    dispatch({
      type: actions.changeStatus,
      status: result,
    })
  }
}
