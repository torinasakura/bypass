import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { timeConverter } from '../../../app/utils/utils'
import * as actions from '../constants'

const initialState = fromJS({
  tickets: [],
  offset: 0,
  perpage: 40,
  total: 0,
  chat: {
    ticket: {},
    message: [],
    text: '',
  },
  ticket: {
    subject: '',
    message: '',
  },
})

export default createReducer(initialState, {
  [actions.load]: (state, { data, offset, perpage, count }) => {
    const tickets = data.map((ticket, index) => ({
      ...ticket,
      index: index + 1,
      lup_time: timeConverter(ticket.lup_time),
    }))

    return state.merge({
      tickets: fromJS(tickets),
      total: parseInt(count, 10),
      offset,
      perpage,
    })
  },

  [actions.clear]: (state) => state.merge(initialState),

  [actions.loadDetail]: (state, { ticket, message }) => state.set('chat', fromJS({ ticket, message })),

  [actions.changeTicketSubject]: (state, { value }) => state.setIn(['ticket', 'subject'], value),

  [actions.changeTicketMessage]: (state, { value }) => state.setIn(['ticket', 'message'], value),

  [actions.changeMessage]: (state, { value }) => state.setIn(['chat', 'text'], value),

  [actions.changeStatus]: (state, { status }) => state.setIn(['chat', 'ticket', 'ticket_status'], status),
})
