import { connect } from 'react-redux'
import { load, openCreateModal } from '../actions'
import * as Tickets from '../components/Tickets'

const connector = connect(
  state => ({
    total: state.tickets.get('total'),
    tickets: state.tickets.get('tickets'),
  }),
  dispatch => ({
    onLoad: (offset) => {
      dispatch(load(offset))
    },
    onOpenCreateModal: () => {
      dispatch(openCreateModal())
    },
  }),
)

export const Desktop = connector(Tickets.Desktop)

export const Tablet = connector(Tickets.Tablet)

export const Mobile = connector(Tickets.Mobile)
