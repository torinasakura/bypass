import { connect } from 'react-redux'
import { changeMessage, sendMessage, changeStatus } from '../actions'
import * as Chat from '../components/Chat'

const connector = connect(
  state => ({
    message: state.tickets.getIn(['chat', 'message']).toJS(),
    ticket: state.tickets.getIn(['chat', 'ticket']).toJS(),
    text: state.tickets.getIn(['chat', 'text']),
  }),
  dispatch => ({
    onChangeMessage: ({ target }) => {
      dispatch(changeMessage(target.value))
    },
    onSendMessage: () => {
      dispatch(sendMessage())
    },
    onChangeStatus: status => {
      dispatch(changeStatus(status))
    },
  }),
)

export const Desktop = connector(Chat.Desktop)

export const Tablet = connector(Chat.Tablet)

export const Mobile = connector(Chat.Mobile)
