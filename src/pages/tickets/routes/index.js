import { createViewport } from 'bypass/app/utils'
import { load, loadDetail } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'tickets',
    onEnter() {
      dispatch(load())
    },
    getComponent(location, callback) {
      System.import('../containers/Tickets')
            .then(Tickets => callback(null, createViewport(Tickets)))
    },
  },
  {
    path: 'tickets/:id',
    onEnter({ params }) {
      dispatch(loadDetail(params.id))
    },
    getComponent(location, callback) {
      System.import('../containers/Chat')
            .then(Chat => callback(null, createViewport(Chat)))
    },
  }]
}
