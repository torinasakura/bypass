import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import { openCreateModal } from '../actions'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Condition } from 'bypass/ui/condition'
import { Checkbox } from 'bypass/ui/checkbox'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    margin: '0 0 10px 30px',
  },
})

class DummyProtection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      check: false,
      wait: false,
      write: false,
    }
  }

  onChangeCheck = ({ target }) => {
    this.setState({ check: target.checked, wait: false, write: false })
  }

  onChangeWait = ({ target }) => {
    this.setState({ wait: target.checked, write: false })
  }

  onChangeWrite = ({ target }) => {
    this.setState({ write: target.checked })
  }

  canCreate = () => {
    const { check, wait, write } = this.state

    return check && wait && write
  }

  render() {
    const { onClose, onCreate } = this.props
    const { check, wait, write } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('LANG.SERVICE.MESSAGE')}
          </Text>
        </Header>
        <Body alignCenter>
          <div className={styles()}>
            <div>
              <Checkbox checked={check} onChange={this.onChangeCheck} /> {__i18n('MISC.STEP2STEP.ICHECK')}
            </div>
            <Condition match={check}>
              <div>
                <Checkbox checked={wait} onChange={this.onChangeWait} /> {__i18n('MISC.STEP2STEP.IWAIT')}
              </div>
            </Condition>
            <Condition match={check && wait}>
              <div>
                <Checkbox checked={write} onChange={this.onChangeWrite} /> {__i18n('MISC.STEP2STEP.IWRITE')}
              </div>
            </Condition>
          </div>
        </Body>
        <Footer alignCenter>
          <Button
            disabled={!this.canCreate()}
            onClick={onCreate}
          >
              {__i18n('LANG.BUTTONS.CREATE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  null,
  dispatch => ({
    onCreate: () => {
      dispatch(openCreateModal())
    },
  }),
)(DummyProtection)
