import React from 'react'
import { connect } from 'react-redux'
import { changeTicketSubject, changeTicketMessage, closeCreateModal, createTicket } from '../actions'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'
import { Input } from 'bypass/ui/input'
import { Textarea } from 'bypass/ui/textarea'

const canCreate = (subject = '', message = '') => subject.length > 1 && message.length > 1

const CreateTicket = ({ subject, message, onChangeSubject, onChangeMessage, onCreate, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.NEW_TICKET')}
      </Text>
    </Header>
    <Body alignCenter>
      <RowLayout>
        <Layout>
          <Input
            value={subject}
            placeholder={__i18n('TICKETS.SUBJECT')}
            onChange={onChangeSubject}
          />
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <Textarea
            rounded
            rows={4}
            value={message}
            placeholder={__i18n('TICKETS.NEW_MSG_PLCHLDR')}
            onChange={onChangeMessage}
          />
        </Layout>
      </RowLayout>
    </Body>
    <Footer alignCenter>
      <Button
        disabled={!canCreate(subject, message)}
        onClick={onCreate}
      >
          {__i18n('LANG.BUTTONS.CREATE')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    subject: state.tickets.getIn(['ticket', 'subject']),
    message: state.tickets.getIn(['ticket', 'message']),
  }),
  dispatch => ({
    onChangeSubject: ({ target }) => {
      dispatch(changeTicketSubject(target.value))
    },
    onChangeMessage: ({ target }) => {
      dispatch(changeTicketMessage(target.value))
    },
    onCreate: () => {
      dispatch(createTicket())
    },
    onClose: () => {
      dispatch(closeCreateModal())
    },
  }),
)(CreateTicket)
