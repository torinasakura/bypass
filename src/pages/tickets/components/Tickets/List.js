import React from 'react'
import { Table, Column, BodyCell } from 'bypass/ui/table'
import Link from 'bypass/ui/link/Link'

const statusMessages = {
  open: __i18n('TICKETS.OPEN'),
  closed: __i18n('TICKETS.CLOSED'),
  in_progress: __i18n('TICKETS.IN_PROGRESS'),
}

const assignmentMessages = {
  owner: __i18n('COM.MANAGERS.OWNER'),
  admin: __i18n('COM.MANAGERS.ADMIN'),
  support: __i18n('COM.MANAGERS.SUPPORT'),
  you: `<${__i18n('TICKETS.YOU')}>`,
}

const directionMessages = {
  to: __i18n('COM.MANAGERS.ADMIN'),
  you: `<${__i18n('TICKETS.YOU')}>`,
}

const dataGetter = (dataKey, rowData) => {
  if (rowData.get instanceof Function) {
    return rowData.get(dataKey)
  }

  return rowData[dataKey]
}

const statusDataGetter = (dataKey, rowData) => {
  const status = dataGetter(dataKey, rowData)

  return statusMessages[status] ? statusMessages[status] : statusMessages.in_progress
}

const assignmentDataGetter = (dataKey, rowData) => {
  const assignment = dataGetter(dataKey, rowData)

  return assignmentMessages[assignment] ? assignmentMessages[assignment] : assignmentMessages.you
}

const directionDataGetter = (dataKey, rowData) => {
  const direction = dataGetter(dataKey, rowData)

  return directionMessages[direction] ? directionMessages[direction] : directionMessages.you
}

const subjectRenderer = (cellData, dataKey, rowData, rowIndex) => {
  const id = dataGetter('ticket_id', rowData)
  const unread = dataGetter('ticket_state', rowData) === 'unread'

  return (
    <BodyCell oddRow={(rowIndex % 2) !== 0}>
      <Link
        small
        decorated
        bold={unread}
        to={`/tickets/${id}`}
      >
        {cellData}
      </Link>
    </BodyCell>
  )
}

const List = ({ minWidth, rowHeight, total, tickets, onLoad }) => (
  <Table
    list={tickets}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onLoad={onLoad}
  >
    <Column
      label={__i18n('LANG.TABLE.COLS.INDEX')}
      width='80px'
      dataKey='index'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.ID')}
      width='90px'
      dataKey='ticket_id'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.THEME')}
      flexGrow={1}
      width='140px'
      dataKey='ticket_subject'
      cellRenderer={subjectRenderer}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.STATUS')}
      width='140px'
      dataKey='ticket_status'
      cellDataGetter={statusDataGetter}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.OPENED')}
      width='140px'
      dataKey='lup_time'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.TO')}
      flexGrow={1}
      width='140px'
      dataKey='ticket_assignment'
      cellDataGetter={assignmentDataGetter}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.FROM')}
      flexGrow={1}
      width='140px'
      dataKey='ticket_direction'
      cellDataGetter={directionDataGetter}
    />
  </Table>
)

export default List
