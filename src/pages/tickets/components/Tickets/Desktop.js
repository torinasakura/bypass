import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import Notice from './Notice'
import List from './List'

const Desktop = ({ tickets, total, onLoad, onOpenCreateModal }) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1}>
                <Title offset='right'>
                  {__i18n('LANG.SERVICE.TICKETS')}
                </Title>
              </Layout>
              <Layout>
                <Notice />
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
          <Layout grow={1} shrink={1}>
            <List
              tickets={tickets}
              total={total}
              rowHeight={21}
              onLoad={onLoad}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} />
              <Layout>
                <Button onClick={onOpenCreateModal}>
                  {__i18n('LANG.BUTTONS.CREATE')}
                </Button>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
