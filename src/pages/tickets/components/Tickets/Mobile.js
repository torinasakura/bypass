import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import List from './List'

const Mobile = ({ tickets, total, onLoad, onOpenCreateModal }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title white size='small'>
            {__i18n('LANG.SERVICE.TICKETS')}
          </Title>
        </Layout>
        <Layout touch scrollX grow={1} shrink={1}>
          <List
            tickets={tickets}
            total={total}
            minWidth={560}
            rowHeight={21}
            onLoad={onLoad}
          />
        </Layout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1}>
              <Button fill size='small' onClick={onOpenCreateModal}>
                {__i18n('LANG.BUTTONS.CREATE')}
              </Button>
            </Layout>
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
