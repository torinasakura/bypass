import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import List from './List'
import Notice from './Notice'

const Tablet = ({ tickets, total, onLoad, onOpenCreateModal }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('LANG.SERVICE.TICKETS')}
          </Title>
        </Layout>
        <Layout grow={1} shrink={1}>
          <List
            tickets={tickets}
            total={total}
            rowHeight={36}
            onLoad={onLoad}
          />
        </Layout>
        <Layout basis='15px' />
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Button onClick={onOpenCreateModal}>
                {__i18n('LANG.BUTTONS.CREATE')}
              </Button>
            </Layout>
            <Layout grow={1} />
          </ColumnLayout>
        </Layout>
        <Layout basis='15px' />
        <Layout>
          <Notice align='center' />
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
