import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    height: 'auto',
    borderLeft: '1px solid #9e9e9e',
    padding: '0 20px 0 15px',
    margin: 'auto 0',
    fontSize: '12px',
    '& div:last-child': {
      marginTop: '4px',
    },
  },
  fill: {
    borderLeft: 0,
    padding: '0 5px',
  },
})

const Field = ({ children, label, fill }) => (
  <div className={styles({ fill })}>
    <div>
      {label}
    </div>
    <div>
      <strong>
        {children}
      </strong>
    </div>
  </div>
)

export default Field
