/* eslint-disable camelcase */
import React from 'react'
import { StyleSheet } from 'quantum'
import { timeConverter } from 'bypass/app/utils/utils'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { Status } from './status'
import { Field } from './field'

const assignmentMessages = {
  owner: __i18n('COM.MANAGERS.OWNER'),
  admin: __i18n('COM.MANAGERS.ADMIN'),
  support: __i18n('COM.MANAGERS.SUPPORT'),
}

const styles = StyleSheet.create({
  self: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  fill: {
    paddingBottom: '8px',
  },
})

const Fields = ({ ticket_status, lup_time, ticket_direction, ticket_assignment, fill, onChangeStatus }) => (
  <div className={styles({ fill })}>
    <ColumnLayout justify='space-around'>
      <Layout>
        <Field fill={fill} label={`${__i18n('LANG.TABLE.COLS.STATUS')}:`}>
          <Status onChange={onChangeStatus}>
            {ticket_status}
          </Status>
        </Field>
      </Layout>
      <Layout>
        <Field fill={fill} label={`${__i18n('TICKETS.OPEN')}:`}>
          {timeConverter(lup_time)}
        </Field>
      </Layout>
      <Layout>
        <Field fill={fill} label={`${__i18n('LANG.TABLE.COLS.FROM')}:`}>
          {ticket_direction === 'to' ? __i18n('COM.MANAGERS.ADMIN') : `<${__i18n('TICKETS.YOU')}>`}
        </Field>
      </Layout>
      <Layout>
        <Field fill={fill} label={`${__i18n('LANG.TABLE.COLS.TO')}:`}>
          {assignmentMessages[ticket_assignment]}
        </Field>
      </Layout>
    </ColumnLayout>
  </div>
)

export default Fields
