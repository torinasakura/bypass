import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    paddingLeft: '19px',
    fontSize: '16px',
    fontWeight: 'bold',
    color: '#546e7a',
    display: 'inline-flex',
    alignItems: 'center',
  },
  center: {
    width: '100%',
    justifyContent: 'center',
    padding: '5px 0',
  },
})

const Header = ({ children, center }) => (
  <div className={styles({ center })}>
    {children}
  </div>
)

export default Header
