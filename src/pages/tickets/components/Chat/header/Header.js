/* eslint-disable camelcase */
import React from 'react'
import { StyleSheet } from 'quantum'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import Fields from './Fields'
import Title from './Title'

const styles = StyleSheet.create({
  self: {
    height: '60px',
    background: '#d3e0e5',
    width: '100%',
    display: 'flex',
  },
})

const Header = ({ ticket_subject, ...props }) => (
  <div className={styles()}>
    <ColumnLayout>
      <Layout grow={1} shrink={1}>
        <Title>
          {ticket_subject}
        </Title>
      </Layout>
      <Layout>
        <Fields {...props} />
      </Layout>
    </ColumnLayout>
  </div>
)

export default Header
