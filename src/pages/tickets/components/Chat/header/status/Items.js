import React from 'react'
import { StyleSheet } from 'quantum'
import { Layer } from 'bypass/ui/layer'
import { Link } from 'bypass/ui/link'

const messages = {
  open: __i18n('TICKETS.OPEN'),
  closed: __i18n('TICKETS.CLOSED'),
  in_progress: __i18n('TICKETS.IN_PROGRESS'),
}

const getStatuses = current => ['open', 'closed', 'in_progress'].filter(status => status !== current)

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    cursor: 'pointer',
    fontSize: '12px',
    fontWeight: 'bold',
    padding: '6px 10px',
    borderRadius: '4px',
    '& div': {
      marginTop: '5px',
    },
  },
})

const Items = ({ children, onChange, onClose }) => (
  <Layer align='tl-tl' offset='6px 10px' onOutsideClick={onClose}>
    <div className={styles()} onMouseLeave={onClose}>
      {messages[children]}
      {getStatuses(children).map(status => (
        <div key={status}>
          <Link
            small
            dashed
            color='blue'
            onClick={() => onChange && onChange(status)}
          >
            {messages[status]}
          </Link>
        </div>
      ))}
    </div>
  </Layer>
)

export default Items
