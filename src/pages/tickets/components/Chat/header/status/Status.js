/* eslint-disable no-shadow */
import React from 'react'
import { StyleSheet } from 'quantum'
import { connect, hover } from 'bypass/ui/state'
import { Condition } from 'bypass/ui/condition'
import Items from './Items'

const messages = {
  open: __i18n('TICKETS.OPEN'),
  closed: __i18n('TICKETS.CLOSED'),
  in_progress: __i18n('TICKETS.IN_PROGRESS'),
}

const styles = StyleSheet.create({
  self: {
    minWidth: '55px',
  },
})

const Status = ({ children, hover, onMouseEnter, onMouseLeave, onChange }) => (
  <div
    className={styles()}
    onMouseEnter={onMouseEnter}
  >
    <span>
      {messages[children]}
    </span>
    <Condition match={children !== 'closed' && hover}>
      <Items onChange={onChange} onClose={onMouseLeave}>
        {children}
      </Items>
    </Condition>
  </div>
)

export default connect([hover], Status)
