/* eslint-disable camelcase */
import React from 'react'
import { StyleSheet } from 'quantum'
import { RowLayout, Layout } from 'bypass/ui/layout'
import Fields from './Fields'
import Title from './Title'

const styles = StyleSheet.create({
  self: {
    height: 'auto',
    background: '#d3e0e5',
    width: '100%',
    display: 'flex',
  },
})

const MultilineHeader = ({ ticket_subject, ...props }) => (
  <div className={styles()}>
    <RowLayout>
      <Layout grow={1} shrink={1}>
        <Title center>
          {ticket_subject}
        </Title>
      </Layout>
      <Layout>
        <Fields {...props} fill />
      </Layout>
    </RowLayout>
  </div>
)

export default MultilineHeader
