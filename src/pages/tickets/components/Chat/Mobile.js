import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container, BackToPage } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Textarea } from 'bypass/ui/textarea'
import { Button } from 'bypass/ui/button'
import { MultilineHeader } from './header'
import { Messages } from './messages'

const Mobile = ({ text, ticket, message, onChangeMessage, onSendMessage, onChangeStatus }) => (
  <Container>
    <AutoSizer>
      <RowLayout scrollY>
        <Layout>
          <BackToPage to='/tickets' center>
            {__i18n('LANG.SERVICE.BACK_TO_TICKETS')}
          </BackToPage>
        </Layout>
        <Layout>
          <Title size='small'>
            {__i18n('LANG.SERVICE.TICKETS')}
          </Title>
        </Layout>
        <Layout basis='10px' />
        <Layout grow={1}>
          <ColumnLayout>
            <Layout basis='10px' />
            <Layout grow={1}>
              <RowLayout>
                <Layout>
                  <MultilineHeader {...ticket} onChangeStatus={onChangeStatus} />
                </Layout>
                <Layout grow={1} shrink={1}>
                  <Messages limit message={message} />
                </Layout>
                <Layout basis='10px' />
                <Layout>
                  <Condition match={ticket.ticket_status !== 'closed'}>
                    <Textarea
                      rows={4}
                      minLength={10}
                      required
                      gray
                      value={text}
                      onChange={onChangeMessage}
                    />
                  </Condition>
                </Layout>
                <Layout basis='10px' />
                <Layout>
                  <Condition match={ticket.ticket_status !== 'closed'}>
                    <Button fill onClick={onSendMessage}>
                      {__i18n('LANG.BUTTONS.SEND')}
                    </Button>
                  </Condition>
                </Layout>
              </RowLayout>
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='10px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
