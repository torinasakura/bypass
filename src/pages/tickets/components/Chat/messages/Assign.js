import React from 'react'
import { StyleSheet } from 'quantum'

const messages = {
  admin: `<${__i18n('COM.MANAGERS.ADMIN')}`,
  owner: `<${__i18n('COM.MANAGERS.OWNER')}>`,
  support: `<${__i18n('COM.MANAGERS.SUPPORT')}>`,
  you: `<${__i18n('TICKETS.YOU')}>`,
}

const styles = StyleSheet.create({
  self: {
    fontSize: '12px',
    fontWeight: 'bold',
    color: '#546e7a',
    float: 'left',
    margin: '0 10px 0 0',
  },
  'assignment=owner': {
    color: '#000000',
  },
  'assignment=support': {
    color: '#784d4d',
  },
})

const Assign = ({ assignment }) => (
  <span className={styles({ assignment })}>
    {messages[assignment] ? messages[assignment] : messages.you}
  </span>
)

export default Assign
