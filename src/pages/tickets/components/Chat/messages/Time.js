import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontSize: '12px',
    color: '#9e9e9e',
  },
})

const Time = ({ children }) => (
  <span className={styles()}>
    {children}
  </span>
)

export default Time
