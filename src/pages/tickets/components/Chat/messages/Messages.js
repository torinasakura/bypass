import React from 'react'
import { StyleSheet } from 'quantum'
import Message from './Message'

const styles = StyleSheet.create({
  self: {
    overflow: 'auto',
    background: '#e7ecf0',
    padding: '11px 12px 11px 12px',
    width: '100%',
    height: 'auto',
  },
  limit: {
    maxHeight: '200px',
  },
})

const Messages = ({ message, limit }) => (
  <div className={styles({ limit })}>
    {message.map((reply, index) => (<Message key={index} {...reply} />))}
  </div>
)

export default Messages
