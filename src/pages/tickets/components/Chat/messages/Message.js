/* eslint-disable camelcase */
import React from 'react'
import { StyleSheet } from 'quantum'
import { timeConverter } from 'bypass/app/utils/utils'
import Assign from './Assign'
import Time from './Time'

const styles = StyleSheet.create({
  self: {
    background: '#ffffff',
    clear: 'both',
    margin: '0 7px 8px',
    width: '60%',
    padding: '8px 10px',
    borderRadius: '5px',
    fontSize: '13px',
    lineHeight: '20px',
    position: 'relative',
  },
  right: {
    float: 'right',
    background: '#fffde7',
  },
})

const Message = ({ send_id, assignment, add_time, text }) => (
  <div className={styles({ right: send_id === 1 })}>
    <div>
      <Assign assignment={assignment} />
      <Time>
        {timeConverter(add_time)}
      </Time>
    </div>
    <div>{text}</div>
  </div>
)

export default Message
