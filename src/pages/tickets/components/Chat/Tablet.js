import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container, BackToPage } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Textarea } from 'bypass/ui/textarea'
import { Button } from 'bypass/ui/button'
import { Header } from './header'
import { Messages } from './messages'

const Tablet = ({ text, ticket, message, onChangeMessage, onSendMessage, onChangeStatus }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <BackToPage to='/tickets' center>
            {__i18n('LANG.SERVICE.BACK_TO_TICKETS')}
          </BackToPage>
        </Layout>
        <Layout>
          <Title size='medium'>
            {__i18n('LANG.SERVICE.TICKETS')}
          </Title>
        </Layout>
        <Layout grow={1}>
          <ColumnLayout>
            <Layout basis='14px' />
            <Layout grow={1}>
              <RowLayout>
                <Layout>
                  <Header {...ticket} onChangeStatus={onChangeStatus} />
                </Layout>
                <Layout grow={1} shrink={1}>
                  <Messages message={message} />
                </Layout>
                <Layout basis='10px' />
                <Layout>
                  <Condition match={ticket.ticket_status !== 'closed'}>
                    <Textarea
                      cols={30}
                      rows={4}
                      minLength={10}
                      required
                      gray
                      value={text}
                      onChange={onChangeMessage}
                    />
                  </Condition>
                </Layout>
                <Layout basis='10px' />
                <Layout>
                  <ColumnLayout>
                    <Layout grow={1} />
                    <Layout>
                      <Condition match={ticket.ticket_status !== 'closed'}>
                        <Button onClick={onSendMessage}>
                          {__i18n('LANG.BUTTONS.SEND')}
                        </Button>
                      </Condition>
                    </Layout>
                  </ColumnLayout>
                </Layout>
              </RowLayout>
            </Layout>
            <Layout basis='14px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='10px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
