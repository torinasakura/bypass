import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container, BackToPage } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Textarea } from 'bypass/ui/textarea'
import { Button } from 'bypass/ui/button'
import { Header } from './header'
import { Messages } from './messages'

const Desktop = ({ text, ticket, message, onChangeMessage, onSendMessage, onChangeStatus }) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout>
            <ColumnLayout align='center'>
              <Layout>
                <BackToPage large to='/tickets'>
                  {__i18n('LANG.SERVICE.BACK_TO_TICKETS')}
                </BackToPage>
              </Layout>
              <Layout grow={1}>
                <Title offset='left'>
                  {__i18n('LANG.SERVICE.TICKETS')}
                </Title>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout>
            <Header {...ticket} onChangeStatus={onChangeStatus} />
          </Layout>
          <Layout grow={1} shrink={1}>
            <Messages message={message} />
          </Layout>
          <Layout basis='10px' />
          <Layout>
            <Condition match={ticket.ticket_status !== 'closed'}>
              <Textarea
                cols={30}
                rows={4}
                minLength={10}
                required
                gray
                value={text}
                onChange={onChangeMessage}
              />
            </Condition>
          </Layout>
          <Layout basis='10px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} />
              <Layout>
                <Condition match={ticket.ticket_status !== 'closed'}>
                  <Button onClick={onSendMessage}>
                    {__i18n('LANG.BUTTONS.SEND')}
                  </Button>
                </Condition>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='10px' />
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
