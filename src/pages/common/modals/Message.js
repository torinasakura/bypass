import React from 'react'
import { Modal, Header, Body } from 'bypass/ui/modal'

const Message = ({ header, body, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      {header}
    </Header>
    <Body alignCenter>
      {body}
    </Body>
  </Modal>
)

export default Message
