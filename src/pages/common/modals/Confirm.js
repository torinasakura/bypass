import React from 'react'
import { Button } from 'bypass/ui/button'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, Layout } from 'bypass/ui/layout'

const Message = ({ header, body, confirm, cancel, onConfirm, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      {header}
    </Header>
    <Body alignCenter>
      {body}
    </Body>
    <Footer>
      <ColumnLayout justify='center'>
        <Layout>
          <Button onClick={onConfirm}>
            {confirm}
          </Button>
        </Layout>
        <Layout basis='10px' />
        <Layout>
          <Button onClick={onClose}>
            {cancel}
          </Button>
        </Layout>
      </ColumnLayout>
    </Footer>
  </Modal>
)

export default Message
