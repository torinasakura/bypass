import React, { Component } from 'react'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Textarea } from 'bypass/ui/textarea'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

class EditComment extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      value: props.comment,
    }
  }

  onChange = ({ target }) => {
    this.setState({ value: target.value })
  }

  onSave = () => {
    const { onSave } = this.props
    const { value } = this.state

    if (onSave) {
      onSave(value)
    }
  }

  render() {
    const { onClose } = this.props
    const { value } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('LANG.SERVICE.NOTE')}
          </Text>
        </Header>
        <Body alignCenter>
          <Textarea
            value={value}
            onChange={this.onChange}
          />
        </Body>
        <Footer alignCenter>
          <Button onClick={this.onSave}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default EditComment
