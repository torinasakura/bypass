import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { Faq } from './faq'

const Desktop = ({ categories }) => (
  <Wrapper>
    <Container>
      <Title>
        {__i18n('LANG.SERVICE.FAQ')}
      </Title>
      <Faq
        center
        categories={categories}
      />
    </Container>
  </Wrapper>
)

export default Desktop
