import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { Faq } from './faq'

const Mobile = ({ categories }) => (
  <Container>
    <Title white size='small'>
      {__i18n('LANG.SERVICE.FAQ')}
    </Title>
    <Faq
      type='mobile'
      categories={categories}
    />
  </Container>
)

export default Mobile
