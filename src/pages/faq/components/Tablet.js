import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { Faq } from './faq'

const Tablet = ({ categories }) => (
  <Container>
    <Title size='medium'>
      {__i18n('LANG.SERVICE.FAQ')}
    </Title>
    <Faq
      type='tablet'
      categories={categories}
    />
  </Container>
)

export default Tablet
