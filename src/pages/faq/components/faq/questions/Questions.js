import React from 'react'
import { Accordion, Panel } from 'bypass/ui/accordion'
import Title from './Title'
import Answer from './Answer'

const Questions = ({ active, questions = [] }) => (
  <div>
    <Accordion>
      {questions.map((question, index) => (
        <Panel
          key={index}
          title={(
            <Title
              active={active}
              index={index}
            >
              {question.question}
            </Title>
          )}
        >
          <Answer>
            {question.answer}
          </Answer>
        </Panel>
      ))}
    </Accordion>
  </div>
)

export default Questions
