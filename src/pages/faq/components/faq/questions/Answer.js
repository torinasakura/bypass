import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    padding: '10px 12px 10px 15px',
    fontSize: '14px',
    color: '#585858',
    background: '#f1f1f1',
    '& p': {
      margin: '0 0 10px',
    },
    '& p:last-child': {
      marginBottom: '0px',
    },
    '& ul': {
      padding: 0,
      margin: 0,
      marginLeft: '15px',
    },
    '& code': {
      padding: '2px 4px',
      fontSize: '90%',
      color: '#c7254e',
      background: '#f9f2f4',
      borderRadius: '4px',
    },
  },
})

const Answer = ({ children }) => (
  <div
    className={styles()}
    dangerouslySetInnerHTML={{ __html: children }}
  />
)

export default Answer
