import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    '& strong': {
      color: '#000000',
      flexBasis: '40px',
      display: 'inline-flex',
      flexShrink: 0,
    },
    '& span': {
      display: 'inline-flex',
      flexShrink: 1,
    },
  },
})

const Title = ({ active, index, children }) => (
  <span className={styles()}>
    <strong>
      {active}.{index + 1}
    </strong>
    <span>
      {children}
    </span>
  </span>
)

export default Title
