import React, { Component } from 'react'
import { StyleSheet } from 'quantum'
import { Tabs } from './tabs'
import { Questions } from './questions'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
  center: {
    maxWidth: '940px',
    margin: '0 auto',
  },
})

class Faq extends Component {
  static defaultProps = {
    categories: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      active: null,
      questions: [],
    }

    if (props.categories.length > 0) {
      this.state.active = props.categories[0].category_id
      this.state.questions = props.categories[0].question
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.categories !== this.props.categories && nextProps.categories.length > 0) {
      const active = nextProps.categories[0].category_id
      const questions = nextProps.categories[0].question

      this.setState({ active, questions })
    }
  }

  onChange = (active) => {
    if (active !== this.state.active) {
      this.setState({ active, questions: this.getQuestions(active) })
    }
  }

  getQuestions(active) {
    const { categories } = this.props
    const { question } = categories.find(category => category.category_id === active) || {}

    return question
  }

  render() {
    const { categories, type, center } = this.props
    const { active, questions } = this.state

    return (
      <div className={styles({ center })}>
        <Tabs
          type={type}
          active={active}
          categories={categories}
          onChange={this.onChange}
        />
        <Questions
          type={type}
          active={active}
          questions={questions}
        />
      </div>
    )
  }
}

export default Faq
