import React from 'react'
import DesktopTabStyles from './DesktopTabStyles'
import TabletTabStyles from './TabletTabStyles'
import MobileTabStyles from './MobileTabStyles'

const styles = {
  desktop: DesktopTabStyles,
  tablet: TabletTabStyles,
  mobile: MobileTabStyles,
}

const getStyles = ({ type, ...modifiers }) => {
  const target = styles[type] || styles.desktop

  return target({ type, ...modifiers })
}

const Tab = ({ type, active, children, onClick }) => (
  <div
    className={getStyles({ type, active })}
    onClick={onClick}
  >
    {children}
  </div>
)

export default Tab
