import { StyleSheet } from 'quantum'

export default StyleSheet.create({
  self: {
    cursor: 'pointer',
    height: '28px',
    fontSize: '16px',
    lineHeight: '25px',
    padding: '0 11px',
    position: 'relative',
    border: '1px solid transparent',
    margin: '0 2px',
    color: '#546e7a',
    '&:hover': {
      background: '#e7ecf0',
      borderColor: '#d3e0e5',
    },
  },
  active: {
    background: '#e7ecf0',
    borderColor: '#d3e0e5',
    cursor: 'initial',
  },
})
