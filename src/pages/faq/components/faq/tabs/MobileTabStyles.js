import { StyleSheet } from 'quantum'

export default StyleSheet.create({
  self: {
    background: '#eceff1',
    flex: '0 0 48%',
    height: '28px',
    fontSize: '13px',
    lineHeight: '25px',
    padding: '0 11px',
    margin: '0 2px 3px 2px',
    boxSizing: 'border-box',
    color: '#546e7a',
    textAlign: 'center',
  },
  active: {
    background: '#37474f',
    color: '#ffffff',
  },
})
