import { StyleSheet } from 'quantum'

export default StyleSheet.create({
  self: {
    height: '42px',
    display: 'inline-flex',
    flexDirection: 'row',
    flexGrow: 1,
    fontSize: '13px',
    lineHeight: '25px',
    color: '#546e7a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    boxShadow: 'inset 0 -3px 0 0 #03a9f4',
  },
})
