import React from 'react'
import { StyleSheet } from 'quantum'
import Tab from './Tab'

const styles = StyleSheet.create({
  self: {},
  'type=desktop': {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '20px',
  },
  'type=tablet': {
    background: '#eceff1',
    display: 'flex',
  },
  'type=mobile': {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginTop: '3px',
  },
})

const Tabs = ({ type = 'desktop', active, categories, onChange }) => (
  <div className={styles({ type })}>
    {categories.map((category, index) => (
      <Tab
        key={index}
        type={type}
        active={active === category.category_id}
        onClick={() => onChange(category.category_id)}
      >
        {category.description}
      </Tab>
    ))}
  </div>
)

export default Tabs
