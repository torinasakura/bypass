import { connect } from 'react-redux'
import * as Faq from '../components'

const connector = connect(
  state => ({
    categories: state.faq.categories,
  }),
)

export const Desktop = connector(Faq.Desktop)

export const Tablet = connector(Faq.Tablet)

export const Mobile = connector(Faq.Mobile)
