import * as actions from '../constants'

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('faq', { body: { lang: window.LOCALE } })

    return dispatch({
      type: actions.load,
      categories: result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}
