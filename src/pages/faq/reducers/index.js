import * as actions from '../constants'

const initialState = {
  categories: [],
}

export default function faq(state = initialState, { type, categories = {} }) {
  switch (type) {
    case actions.load:
      return {
        categories: Object.keys(categories).map(id => categories[id]),
      }
    case actions.clear:
      return initialState
    default:
      return state
  }
}
