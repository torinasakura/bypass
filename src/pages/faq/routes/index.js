import { createViewport } from 'bypass/app/utils'
import { load, clear } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'faq',
    onEnter() {
      dispatch(load())
    },
    onLeave() {
      dispatch(clear())
    },
    getComponent(location, callback) {
      System.import('../containers/Faq')
            .then(Faq => callback(null, createViewport(Faq)))
    },
  }]
}
