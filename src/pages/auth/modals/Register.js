import React from 'react'
import { Text } from 'bypass/ui/text'
import { Modal, Header, Body } from 'bypass/ui/modal'

const Register = ({ token, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LOGIN.YOUR_PASW')}: <strong>{token}</strong>
      </Text>
    </Header>
    <Body white alignCenter>
      <Text size={18}>
        {__i18n('LOGIN.ATTENTION')}
      </Text>
    </Body>
  </Modal>
)

export default Register
