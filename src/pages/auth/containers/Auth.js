import { connect } from 'react-redux'
import { changePassword, login, register } from '../actions'
import Auth from '../components/Auth'

const connector = connect(
  state => ({
    password: state.auth.password,
  }),
  dispatch => ({
    onChangePassword: ({ target }) => {
      dispatch(changePassword(target.value))
    },
    onLogin: () => {
      dispatch(login())
    },
    onRegister: () => {
      dispatch(register())
    },
  }),
)

const Desktop = connector(Auth)

export default Desktop
