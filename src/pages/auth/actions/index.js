import cookie from 'js-cookie'
import { load } from '../../../app/actions/config'
import { open } from '../../../app/actions/modal'

export const actions = {
  login: '@@bypass/auth/LOGIN',
  changePassword: '@@bypass/auth/CHANGE_PASSWORD',
  logout: '@@bypass/auth/LOGOUT',
}

export function login() {
  return async (dispatch, getState, { post }) => {
    const password = getState().auth.password

    if (!password) {
      return
    }

    const { result, error } = await post('login/authorize', { body: { password }, skipError: true })

    if (result.token) {
      cookie.set('token', result.token)

      dispatch({
        type: actions.login,
        token: result.token,
      })

      dispatch(load())

      window.location.hash = '/news'
    }

    if (error == 1) {
      dispatch(open('common/Errors', {
        header: __i18n('LANG.ERRORS.ERROR'),
        body: __i18n('LANG.ERRORS.INCORRECT_PASSWORD'),
      }))
    }
  }
}

export function changePassword(password) {
  return {
    type: actions.changePassword,
    password,
  }
}

export function register() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('register', { body: { reseller_id: window.reseller_id } })

    dispatch(open('auth/Register', { token: result }))
  }
}

export function logout() {
  return async dispatch => {
    cookie.remove('token')

    dispatch({ type: actions.logout })

    window.location.hash = '/'
  }
}
