import React from 'react'
import { RowLayout, Layout } from 'bypass/ui/layout'
import Nav from './Nav'
import Content from './Content'
import Form from './Form'

const Auth = ({ password, onChangePassword, onLogin, onRegister }) => (
  <RowLayout>
    <Layout>
      <Nav onRegister={onRegister} />
    </Layout>
    <Layout grow={1}>
      <Content>
        <Form
          password={password}
          onChangePassword={onChangePassword}
          onLogin={onLogin}
        />
      </Content>
    </Layout>
  </RowLayout>
)

export default Auth
