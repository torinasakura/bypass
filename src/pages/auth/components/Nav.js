import React from 'react'
import { Navigation, Item, ItemGroup, ChangeLangItem } from 'bypass/ui/navigation'

const Nav = ({ onRegister }) => (
  <Navigation>
    <ItemGroup fill justify='center'>
      <ChangeLangItem />
      <Item withouthBorder onClick={onRegister}>
        {__i18n('NAV.REGISTER')}
      </Item>
    </ItemGroup>
  </Navigation>
)

export default Nav
