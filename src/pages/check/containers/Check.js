import { connect } from 'react-redux'
import * as Check from '../components/Check'
import {
  changeFormatString, checkFormatString, changeRawData,
  preview, check, saveFormat, loadFormat,
} from '../actions/check'

const connector = connect(
  state => ({
    limit: state.check.check.get('limit'),
    formatString: state.check.check.get('formatString'),
    rawData: state.check.check.get('rawData'),
    errors: state.check.check.get('errors').toJS(),
    toCheck: state.check.check.get('toCheck').toJS(),
  }),
  dispatch => ({
    onChangeFormatString: ({ target }) => {
      dispatch(changeFormatString(target.value))
    },
    onCheckFormatString: () => {
      dispatch(checkFormatString())
    },
    onChangeRawData: ({ target }) => {
      dispatch(changeRawData(target.value))
    },
    onPreview: () => {
      dispatch(preview())
    },
    onCheck: () => {
      dispatch(check())
    },
    onSaveFormat: () => {
      dispatch(saveFormat())
    },
    onLoadFormat: () => {
      dispatch(loadFormat())
    },
  }),
)

export const Desktop = connector(Check.Desktop)

export const Tablet = connector(Check.Tablet)

export const Mobile = connector(Check.Mobile)
