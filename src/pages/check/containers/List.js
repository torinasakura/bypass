import { connect } from 'react-redux'
import { select, selectAll, remove, editComment } from '../actions/list'
import * as List from '../components/List'

const connector = connect(
  state => ({
    list: state.check.list.get('data'),
    hasSelected: state.check.list.get('hasSelected'),
    allSelected: state.check.list.get('allSelected'),
  }),
  dispatch => ({
    onSelect: row => dispatch(select(row)),
    onSelectAll: () => dispatch(selectAll()),
    onRemove: () => dispatch(remove()),
    onEditComment: () => dispatch(editComment()),
  }),
)

export const Desktop = connector(List.Desktop)

export const Tablet = connector(List.Tablet)

export const Mobile = connector(List.Mobile)
