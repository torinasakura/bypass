import { createViewport } from 'bypass/app/utils'
import { init } from '../actions/check'
import { load } from '../actions/list'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'check',
    childRoutes: [{
      path: 'add',
      onEnter() {
        dispatch(init())
      },
      getComponent(location, callback) {
        System.import('../containers/Check')
              .then(Check => callback(null, createViewport(Check)))
      },
    },
    {
      path: 'list',
      onEnter() {
        dispatch(load())
      },
      getComponent(location, callback) {
        System.import('../containers/List')
              .then(List => callback(null, createViewport(List)))
      },
    }],
  }]
}
