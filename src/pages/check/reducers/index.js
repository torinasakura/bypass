import { combineReducers } from 'redux'
import check from './check'
import list from './list'

export default combineReducers({
  check,
  list,
})
