import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import * as actions from '../constants/list'

const initialState = fromJS({
  data: [],
  mapResponse: [],
})

export default createReducer(initialState, {
  [actions.load]: (state, { data, map_response, provider = [] }) => {
    const providerMap = provider.reduce((map, item) => ({
      ...map,
      [item.provider_id]: item.provider,
    }), {})

    return state.merge(fromJS({
      total: data.length,
      mapResponse: map_response,
      data: data.map(item => ({
        ...item,
        provider: providerMap[item.provider_id],
      })),
    }))
  },

  [actions.select]: (state, { row }) => {
    const index = state.get('data')
                       .findIndex(item => item === row)

    if (index !== -1) {
      const updated = state.setIn(['data', index, 'selected'], !row.get('selected'))

      return updated.set(
        'hasSelected',
        updated.get('data')
               .some(item => item.get('selected'))
      )
    }

    return state
  },

  [actions.selectAll]: state => {
    const selected = !state.get('allSelected')
    const checks = state.get('data').map(check => check.set('selected', selected))

    return state.set('data', checks)
                .set('allSelected', selected)
                .set('hasSelected', selected)
  },
})
