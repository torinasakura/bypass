/* eslint-disable camelcase */
import { fromJS } from 'immutable'
import { uniq, zipObject, map } from 'lodash'
import { createReducer } from 'bypass/utils/index'
import { splitFieldsBySeps, checkField, sanitizeHtml } from 'bypass/app/utils/utils'
import * as actions from '../constants/check'

const initialState = fromJS({
  provider: [],
  checkProvider: null,
  formatString: '',
  rawData: '',
  errors: {
    format: false,
  },
  toCheck: {
    cards: [],
    columns: [],
  },
  limit: 0,
  formats: [],
})

const minFieldsList = ['num', 'm', 'y', 'cvv2']

export default createReducer(initialState, {
  [actions.init]: (state, { max_test_count, test_count, provider = [] }) => {
    const available = provider.find(item => item.available == 1) // eslint-disable-line eqeqeq

    return state.merge(fromJS({
      checkProvider: available ? available.provider_id : null,
      limit: max_test_count - test_count,
      max_test_count,
      test_count,
      provider,
    }))
  },

  [actions.changeProvider]: (state, { provider }) => state.set('checkProvider', provider),

  [actions.changeFormatString]: (state, { value = '' }) =>
    state.set('formatString', value)
         .setIn(['errors', 'format'], false),

  [actions.checkFormatString]: state => {
    const formatString = state.get('formatString')

    if (!formatString.length) {
      return state
    }

    const fields = formatString.split('%').filter((field, index) => index % 2)
    const isCorrectFormat = minFieldsList.every(field => fields.includes(field))

    return state.setIn(['errors', 'format'], !isCorrectFormat)
  },

  [actions.changeRawData]: (state, { value }) => state.set('rawData', value),

  [actions.precheck]: state => {
    const formatString = state.get('formatString')
    const format = formatString.split('%').filter((v, i) => i % 2)
    const separators = formatString.trim().split('%').filter((v, i) => (!(i % 2)))
    const sanitized = sanitizeHtml(state.get('rawData').replace(/<(br|br\/)>/ig, '\n'))
    const items = uniq(sanitized.split('\n').filter(v => !!v.length))

    if (!items.length) {
      return state
    }

    const preparedData = items.map(item => {
      const checkArray = splitFieldsBySeps(item, separators)
      const card = zipObject(format, checkArray)
      const tmpIdx = formatString.indexOf('tmp')

      if (checkArray.length) {
        if (!checkArray[0].length) {
          checkArray.splice(0, 1)
        }

        if (!checkArray[checkArray.length - 1].length) {
          checkArray.splice(checkArray.length - 1, 1)
        }
      }

      if (checkArray.length === format.length) {
        if (tmpIdx !== -1) {
          delete card.tmp
        }

        return map(card, (val, key) => checkField(key, val, card))
      }

      return {
        value: item,
        error: true,
      }
    })

    const cards = preparedData.filter(dataItem => !dataItem.error && dataItem.every(child => !child.error))

    return state.merge(fromJS({
      toCheck: {
        cards,
        columns: cards.length ? cards[0].map(v => v.field) : [],
      },
    }))
  },

  [actions.loadFormat]: (state, { result }) => state.set('formats', result),
})
