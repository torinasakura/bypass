import React from 'react'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import { close } from 'bypass/app/actions/modal'
import { changeFormatString, removeFormat } from '../actions/check'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { Condition } from 'bypass/ui/condition'
import { CloseIcon } from 'bypass/ui/icons'
import { Text } from 'bypass/ui/text'

const styles = StyleSheet.create({
  self: {
    border: '2px solid #a1a1a1',
    borderRadius: '5px',
    padding: '3px',
    margin: '15px 15px 20px 15px',
    overflowY: 'auto',
    background: '#ffffff',
    display: 'block',
    '& div': {
      paddingLeft: '10px',
      lineHeight: '38px',
      fontSize: '16px',
      textAlign: 'left',
      cursor: 'pointer',
      display: 'flex',
      flexDirection: 'row',
      '& div:first-child': {
        flexGrow: 1,
      },
      '& div:last-child svg': {
        position: 'relative',
        top: '10px',
        right: '5px',
        display: 'none',
      },
      '&:hover': {
        background: '#ececec',
        '& svg': {
          display: 'inline-block !important',
        },
      },
    },
  },
})

const Formats = ({ formats = [], onSelect, onRemove, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('CHECK.LOAD_TPL_LABEL')}
      </Text>
    </Header>
    <Body alignCenter>
      <Condition match={formats.length === 0}>
        <div>
          {__i18n('LANG.SERVICE.EMPTY_TPL')}
        </div>
      </Condition>
      <Condition match={formats.length}>
        <div className={styles()}>
          {formats.map(format => (
            <div key={format.format_id}>
              <div onClick={() => onSelect(format.format_string)}>
                {format.format_string}
              </div>
              <div onClick={() => onRemove(format.format_id)}>
                <CloseIcon width={16} fill='#b71c1c' />
              </div>
            </div>
          ))}
        </div>
      </Condition>
    </Body>
  </Modal>
)

export default connect(
  state => ({
    formats: state.check.check.get('formats'),
  }),
  dispatch => ({
    onSelect: format => {
      dispatch(changeFormatString(format))
      dispatch(close())
    },
    onRemove: format => {
      dispatch(removeFormat(format))
    },
  }),
)(Formats)
