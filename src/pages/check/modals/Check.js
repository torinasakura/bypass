import React from 'react'
import { connect } from 'react-redux'
import { changeProvider, runCheck } from '../actions/check'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Select } from 'bypass/ui/select'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const Check = ({ cards, providers = [], provider, onChange, onRunCheck, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.CARDS_CHECK')}
      </Text>
    </Header>
    <Body alignCenter>
      <RowLayout>
        <Layout>
          <ColumnLayout>
            <Layout basis='20%' />
            <Layout basis='30%'>
              {__i18n('SEARCH.FAST_BUY.CARD_SELECTED')}:
            </Layout>
            <Layout basis='10px' />
            <Layout basis='30%'>
              {cards.length}
            </Layout>
            <Layout basis='10%' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout>
            <Layout basis='20%' />
            <Layout basis='30%'>
              {__i18n('LANG.TABLE.COLS.PROVIDER')}:
            </Layout>
            <Layout basis='30%'>
              <Select
                options={providers}
                value={provider}
                valueKey='provider_id'
                textKey='provider'
                onChange={onChange}
              />
            </Layout>
            <Layout basis='10%' />
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </Body>
    <Footer alignCenter>
      <Button onClick={onRunCheck}>
        {__i18n('LANG.BUTTONS.CHECK')}
      </Button>
    </Footer>
  </Modal>
)

export default connect(
  state => ({
    cards: state.check.check.getIn(['toCheck', 'cards']).toJS(),
    providers: state.check.check.get('provider').toJS(),
    provider: state.check.check.get('checkProvider'),
  }),
  dispatch => ({
    onChange: ({ target }) => {
      dispatch(changeProvider(target.value))
    },
    onRunCheck: () => {
      dispatch(runCheck())
    },
  }),
)(Check)
