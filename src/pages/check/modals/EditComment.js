import React, { Component } from 'react'
import { connect } from 'react-redux'
import { saveComment } from '../actions/list'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Textarea } from 'bypass/ui/textarea'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

class EditComment extends Component {
  static defaultProps = {
    selected: [],
  }

  constructor(props, context) {
    super(props, context)

    const [check] = props.selected

    this.state = {
      value: check ? check.comment : '',
    }
  }

  onChange = ({ target }) => {
    this.setState({ value: target.value })
  }

  onSave = () => {
    const { onSaveComment } = this.props
    const { value } = this.state

    onSaveComment(value)
  }

  render() {
    const { onClose } = this.props
    const { value } = this.state

    return (
      <Modal onClose={onClose}>
        <Header>
          <Text size={19}>
            {__i18n('LANG.SERVICE.NOTE')}
          </Text>
        </Header>
        <Body alignCenter>
          <Textarea
            value={value}
            onChange={this.onChange}
          />
        </Body>
        <Footer alignCenter>
          <Button onClick={this.onSave}>
            {__i18n('LANG.BUTTONS.SAVE')}
          </Button>
        </Footer>
      </Modal>
    )
  }
}

export default connect(
  state => ({
    selected: state.check.list.get('data').filter(check => check.get('selected')).toJS(),
  }),
  dispatch => ({
    onSaveComment: comment => dispatch(saveComment(comment)),
  }),
)(EditComment)
