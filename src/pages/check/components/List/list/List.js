import React from 'react'
import { getExp, timeConverter } from 'bypass/app/utils/utils'
import { Table, Column, BodyCell, HeaderCell } from 'bypass/ui/table'
import { Checkbox } from 'bypass/ui/checkbox'
import { Spinner, Block } from 'bypass/ui/loader'

const emptyOrValueDataGetter = (dataKey, rowData) => (
  rowData.get(dataKey) ? rowData.get(dataKey) : `<${__i18n('LANG.SERVICE.EMPTY')}>`
)

const List = ({ minWidth, rowHeight, total, list, allSelected, onSelect, onSelectAll }) => (
  <Table
    list={list}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onRowClick={onSelect}
  >
    <Column
      label={__i18n('LANG.TABLE.COLS.INDEX')}
      width='50px'
      dataKey='index'
      cellDataGetter={(dataKey, rowData, columnData, columnIndex, rowIndex) => rowIndex + 1}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.NUMBER')}
      width='100px'
      flexGrow={1}
      dataKey='number'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.EXP')}
      width='45px'
      dataKey='exp'
      cellDataGetter={(dataKey, rowData) => getExp(rowData.get(dataKey))}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.CVV2')}
      width='45px'
      dataKey='cvv2'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.ZIP')}
      width='60px'
      flexGrow={1}
      dataKey='zip'
      tooltip
      cellDataGetter={emptyOrValueDataGetter}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.ADDRESS')}
      width='90px'
      flexGrow={1}
      dataKey='address'
      tooltip
      cellDataGetter={emptyOrValueDataGetter}
    />
    <Column
      label={__i18n('ORDERS.TABLE.ADD_TIME')}
      width='120px'
      flexGrow={1}
      dataKey='add_time'
      cellDataGetter={(dataKey, rowData) => timeConverter(rowData.get(dataKey), true, true)}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.PRICE_ITEM')}
      width='45px'
      dataKey='price'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.PROVIDER')}
      width='70px'
      dataKey='provider'
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.STATUS')}
      width='60px'
      dataKey='status'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <div style={{ position: 'relative', padding: 8 }}>
            <Spinner local>
              <Block small />
              <Block small />
              <Block small />
            </Spinner>
          </div>
        </BodyCell>
      )}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.RESPONSE')}
      width='90px'
      flexGrow={1}
      dataKey='response'
      cellDataGetter={emptyOrValueDataGetter}
    />
    <Column
      label={__i18n('LANG.TABLE.COLS.COMMENT')}
      width='120px'
      flexGrow={1}
      dataKey='comment'
      cellDataGetter={(dataKey, rowData) => (
        rowData.get(dataKey) ? rowData.get(dataKey) : `<${__i18n('LANG.SERVICE.EMPTY_NOTE')}>`
      )}
    />
    <Column
      label={'_'}
      width='40px'
      dataKey='selected'
      headerRenderer={() => (
        <HeaderCell justify='center'>
          <Checkbox checked={allSelected} onChange={onSelectAll} />
        </HeaderCell>
      )}
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0} selected={rowData.get('selected')}>
          <Checkbox checked={cellData} />
        </BodyCell>
      )}
    />
  </Table>
)

export default List
