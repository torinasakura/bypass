import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import { List } from './list'

const Tablet = ({ list, total, hasSelected, allSelected, onSelect, onSelectAll, onEditComment, onRemove }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('LANG.SERVICE.CHECK_RESULT')}
          </Title>
        </Layout>
        <Layout grow={1} shrink={1}>
          <List
            list={list}
            total={total}
            rowHeight={26}
            allSelected={allSelected}
            onSelect={onSelect}
            onSelectAll={onSelectAll}
          />
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Button disabled={!hasSelected} onClick={onEditComment}>
                {__i18n('LANG.BUTTONS.EDIT_COMMENT')}
              </Button>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Button type='red' disabled={!hasSelected} onClick={onRemove}>
                {__i18n('LANG.BUTTONS.DELETE')}
              </Button>
            </Layout>
            <Layout basis='10px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
