import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Button } from 'bypass/ui/button'
import { Preview } from './preview'
import { Format } from './format'
import CardFormat from './CardFormat'
import Hint from './Hint'
import Stat from './Stat'

const Tablet = ({
  formatString, rawData, errors, limit, toCheck, onCheck,
  onSaveFormat, onLoadFormat, onChangeFormatString,
  onCheckFormatString, onChangeRawData, onPreview,
}) => (
  <Container>
    <ColumnLayout>
      <Layout basis='25px' />
      <Layout grow={1} shrink={1}>
        <RowLayout>
          <Layout>
            <Title size='medium'>
              {__i18n('LANG.SERVICE.ADD_TO_CHECK')}
            </Title>
          </Layout>
          <Layout>
            <ColumnLayout>
              <Layout grow={1} shrink={1}>
                <Format
                  errors={errors}
                  value={formatString}
                  onChange={onChangeFormatString}
                  onBlur={onCheckFormatString}
                />
              </Layout>
              <Layout basis='20px' />
              <Layout basis='350px'>
                <ColumnLayout align='center'>
                  <Layout >
                    <Button type='light' size='small' onClick={onSaveFormat}>
                      {__i18n('CHECK.SAVE_TPL_LABEL')}
                    </Button>
                  </Layout>
                  <Layout basis='10px' />
                  <Layout align='center'>
                    <Button type='light' size='small' onClick={onLoadFormat}>
                      {__i18n('CHECK.LOAD_TPL_LABEL')}
                    </Button>
                  </Layout>
                </ColumnLayout>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} shrink={1}>
                <RowLayout>
                  <Layout>
                    <CardFormat
                      value={rawData}
                      onChange={onChangeRawData}
                    />
                  </Layout>
                  <Layout basis='10px' />
                  <Layout>
                    <ColumnLayout align='center'>
                      <Layout grow={1}>
                        <Stat limit={limit} />
                      </Layout>
                      <Layout>
                        <Button disabled={!formatString.length} onClick={onPreview}>
                          {__i18n('CHECK.PREVIEW')}
                        </Button>
                      </Layout>
                    </ColumnLayout>
                  </Layout>
                </RowLayout>
              </Layout>
              <Layout basis='20px' />
              <Layout basis='350px'>
                <Hint color />
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Condition match={toCheck.cards.length}>
              <Preview {...toCheck} />
            </Condition>
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} />
              <Layout>
                <Button disabled={!toCheck.cards.length} onClick={onCheck}>
                  {__i18n('LANG.BUTTONS.CHECK_CARDS')}
                </Button>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
        </RowLayout>
      </Layout>
      <Layout basis='25px' />
    </ColumnLayout>
  </Container>
)

export default Tablet
