import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontSize: '18px',
    lineHeight: '20px',
    color: '#333',
    marginBottom: '10px',
    fontWeight: 'bold',
  },
})

const Title = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Title
