import React from 'react'
import { StyleSheet } from 'quantum'
import { Input } from 'bypass/ui/input'
import { Tooltip } from 'bypass/ui/tooltip'
import { Condition } from 'bypass/ui/condition'
import Legend from './Legend'
import Title from '../Title'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
  indent: {
    padding: '0 10px',
  },
})

const Format = ({ indent, value, errors = {}, onChange, onBlur }) => (
  <div className={styles({ indent })}>
    <div>
      <Title>
        {__i18n('CHECK.FORMAT_LABEL')}
      </Title>
    </div>
    <div>
      <Input
        value={value}
        onBlur={onBlur}
        onChange={onChange}
        placeholder={__i18n('CHECK.FORMAT_PLCHLDR')}
      />
      <Condition match={errors.format}>
        <Tooltip
          message
          align='top'
          offset='10px 0'
        >
          {__i18n('CHECK.FORMAT_TOOLTIP')}
        </Tooltip>
      </Condition>
    </div>
    <Legend />
  </div>
)

export default Format
