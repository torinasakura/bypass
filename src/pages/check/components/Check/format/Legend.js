import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontSize: '13px',
    marginTop: '5px',
  },
})

const Legend = () => (
  <div
    className={styles()}
    dangerouslySetInnerHTML={{ __html: __i18n('CHECK.LEGEND_LABEL') }}
  />
)

export default Legend
