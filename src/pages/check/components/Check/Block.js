import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    background: '#f1f1f1',
    border: '1px solid #cccccc',
    padding: '15px 20px 20px',
    marginBottom: '0px',
    width: '100%',
  },
})

const Block = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Block
