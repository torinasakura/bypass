import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Button } from 'bypass/ui/button'
import { Link } from 'bypass/ui/link'
import { Preview } from './preview'
import { Format } from './format'
import CardFormat from './CardFormat'
import Hint from './Hint'
import Stat from './Stat'
import Block from './Block'

const Desktop = ({
  formatString, rawData, errors, limit, toCheck,
  onSaveFormat, onLoadFormat, onCheck, onChangeFormatString,
  onCheckFormatString, onChangeRawData, onPreview,
}) => (
  <Wrapper>
    <Container indent>
      <RowLayout>
        <Layout>
          <Title>
            {__i18n('LANG.SERVICE.ADD_TO_CHECK')}
          </Title>
        </Layout>
        <Layout>
          <Block>
            <ColumnLayout>
              <Layout grow={1} shrink={1}>
                <Format
                  errors={errors}
                  value={formatString}
                  onChange={onChangeFormatString}
                  onBlur={onCheckFormatString}
                />
              </Layout>
              <Layout basis='20px' />
              <Layout basis='350px'>
                <ColumnLayout align='center'>
                  <Layout >
                    <Link decorated color='blue' onClick={onSaveFormat}>
                      {__i18n('CHECK.SAVE_TPL_LABEL')}
                    </Link>
                  </Layout>
                  <Layout basis='20px' />
                  <Layout align='center'>
                    <Link decorated color='blue' onClick={onLoadFormat}>
                      {__i18n('CHECK.LOAD_TPL_LABEL')}
                    </Link>
                  </Layout>
                </ColumnLayout>
              </Layout>
            </ColumnLayout>
          </Block>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <Block>
            <ColumnLayout>
              <Layout grow={1} shrink={1}>
                <CardFormat
                  large
                  value={rawData}
                  onChange={onChangeRawData}
                />
              </Layout>
              <Layout basis='20px' />
              <Layout basis='350px'>
                <RowLayout>
                  <Layout grow={1}>
                    <Hint />
                  </Layout>
                  <Layout>
                    <ColumnLayout align='center'>
                      <Layout grow={1}>
                        <Stat limit={limit} />
                      </Layout>
                      <Layout>
                        <Button disabled={!formatString.length} onClick={onPreview}>
                          {__i18n('CHECK.PREVIEW')}
                        </Button>
                      </Layout>
                    </ColumnLayout>
                  </Layout>
                </RowLayout>
              </Layout>
            </ColumnLayout>
          </Block>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <Condition match={toCheck.cards.length}>
            <Block>
              <Preview {...toCheck} />
            </Block>
          </Condition>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Button disabled={!toCheck.cards.length} onClick={onCheck}>
                {__i18n('LANG.BUTTONS.CHECK_CARDS')}
              </Button>
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </Container>
  </Wrapper>
)

export default Desktop
