import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    fontSize: '14px',
    color: '#6e6e6e',
  },
  center: {
    textAlign: 'center',
  },
})

const Stat = ({ toCheck = [], limit, center }) => (
  <div className={styles({ center })}>
    {__i18n('CHECK.CARD_ADDED')}: {toCheck.length} / {limit}
  </div>
)

export default Stat
