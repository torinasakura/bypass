import React from 'react'
import { StyleSheet } from 'quantum'
import { ContentEditable } from 'bypass/ui/ContentEditable'
import Title from './Title'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
  indent: {
    padding: '0 10px',
  },
})

const CardFormat = ({ indent, large, value, onChange }) => (
  <div className={styles({ indent })}>
    <div>
      <Title>
        {__i18n('CHECK.FORM_LABEL')}
      </Title>
    </div>
    <div>
      <ContentEditable
        cols='30'
        rows='10'
        html={value}
        large={large}
        onChange={onChange}
        placeholder={__i18n('CHECK.AREA_PLCHLDR')}
      />
    </div>
  </div>
)

export default CardFormat
