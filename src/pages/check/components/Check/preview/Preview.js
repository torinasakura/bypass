import React from 'react'
import { StyleSheet } from 'quantum'
import Title from '../Title'
import Header from './Header'
import Body from './Body'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    '& table': {
      borderCollapse: 'collapse',
      borderSpacing: '0',
      width: '100%',
      border: '1px solid #cccccc',
    },
  },
})

const Preview = ({ cards, columns }) => (
  <div className={styles()}>
    <Title>
      {__i18n('CHECK.PREVIEW')}
    </Title>
    <table>
      <Header columns={columns} />
      <Body cards={cards} />
    </table>
  </div>
)

export default Preview
