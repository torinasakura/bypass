import React from 'react'
import { StyleSheet } from 'quantum'

const names = {
  num: __i18n('LANG.TABLE.COLS.NUMBER'),
  cvv2: __i18n('LANG.TABLE.COLS.CVV2'),
  m: __i18n('LANG.TABLE.COLS.M'),
  y: __i18n('LANG.TABLE.COLS.Y'),
  zip: __i18n('LANG.TABLE.COLS.ZIP'),
  addr: __i18n('LANG.TABLE.COLS.ADDRESS'),
}

const styles = StyleSheet.create({
  self: {
    '& th': {
      background: '#e7ecf0',
      color: '#000000',
      borderRight: '1px solid #ccc',
      fontSize: '12px',
      lineHeight: '20px',
      padding: '3px 10px',
      fontWeight: 'normal',
      textAlign: 'left',
      borderBottom: '1px solid #ccc',
    },
  },
})

const Header = ({ columns }) => (
  <thead className={styles()}>
    <tr>
      <th>{__i18n('LANG.TABLE.COLS.INDEX')}</th>
      {columns.map((column, index) => (
        <th key={index}>
          {names[column]}
        </th>
      ))}
    </tr>
  </thead>
)

export default Header
