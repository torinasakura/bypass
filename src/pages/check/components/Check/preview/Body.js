import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    '& td': {
      background: '#ffffff',
      color: '#000000',
      borderRight: '1px solid #ccc',
      fontSize: '12px',
      lineHeight: '20px',
      padding: '3px 10px',
      fontWeight: 'normal',
      textAlign: 'left',
      borderBottom: '1px solid #ccc',
    },
  },
})

const Body = ({ cards }) => (
  <tbody className={styles()}>
    {cards.map((card, index) => (
      <tr key={index}>
        <td>{index + 1}</td>
        {card.map((field, fieldIndex) => (
          <td key={fieldIndex}>
            {field.value}
          </td>
        ))}
      </tr>
    ))}
  </tbody>
)

export default Body
