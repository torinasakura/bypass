import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import { Button } from 'bypass/ui/button'
import { Preview } from './preview'
import { Format } from './format'
import CardFormat from './CardFormat'
import Hint from './Hint'
import Stat from './Stat'

const Mobile = ({
  formatString, rawData, errors, limit, toCheck,
  onSaveFormat, onLoadFormat, onCheck, onChangeFormatString,
  onCheckFormatString, onChangeRawData, onPreview,
}) => (
  <Container>
    <RowLayout>
      <Layout>
        <Title size='small'>
          {__i18n('LANG.SERVICE.ADD_TO_CHECK')}
        </Title>
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Format
          indent
          errors={errors}
          value={formatString}
          onChange={onChangeFormatString}
          onBlur={onCheckFormatString}
        />
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <ColumnLayout>
          <Layout grow={1} >
            <Button fill type='light' size='small' onClick={onSaveFormat}>
              {__i18n('CHECK.SAVE_TPL_LABEL')}
            </Button>
          </Layout>
          <Layout grow={1}>
            <Button fill type='light' size='small' onClick={onLoadFormat}>
              {__i18n('CHECK.LOAD_TPL_LABEL')}
            </Button>
          </Layout>
        </ColumnLayout>
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <CardFormat
          indent
          value={rawData}
          onChange={onChangeRawData}
        />
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Hint color />
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Stat center limit={limit} />
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Button fill disabled={!formatString.length} onClick={onPreview}>
          {__i18n('CHECK.PREVIEW')}
        </Button>
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Condition match={toCheck.cards.length}>
          <Preview {...toCheck} />
        </Condition>
      </Layout>
      <Layout basis='10px' />
      <Layout>
        <Button fill disabled={!toCheck.cards.length} onClick={onCheck}>
          {__i18n('LANG.BUTTONS.CHECK_CARDS')}
        </Button>
      </Layout>
    </RowLayout>
  </Container>
)

export default Mobile
