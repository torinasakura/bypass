import React from 'react'
import { StyleSheet } from 'quantum'
import Title from './Title'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    fontSize: '14px',
    color: '#6e6e6e',
  },
  color: {
    '&:last-child': {
      background: '#eceff1',
      padding: '10px',
    },
  },
})

const Hint = ({ color }) => (
  <div className={styles({ color })}>
    <div>
      <Title>
        {__i18n('CHECK.HINT_LABEL')}
      </Title>
    </div>
    <div>
      {__i18n('CHECK.INSTRUCT')}
    </div>
  </div>
)

export default Hint
