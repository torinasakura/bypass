import { hide } from 'bypass/app/actions/loader'
import { open, close } from 'bypass/app/actions/modal'
import * as actions from '../constants/check'

export function init() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('test/init')

    dispatch({
      type: actions.init,
      ...result,
    })
  }
}

export function changeFormatString(value) {
  return {
    type: actions.changeFormatString,
    value,
  }
}

export function checkFormatString() {
  return {
    type: actions.checkFormatString,
  }
}

export function changeRawData(value) {
  return {
    type: actions.changeRawData,
    value,
  }
}

export function preview() {
  return {
    type: actions.precheck,
  }
}

export function check() {
  return open('check/Check')
}

export function changeProvider(provider) {
  return {
    type: actions.changeProvider,
    provider,
  }
}

export function checkAdd(buyId) {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('test/add', { body: { buy_id: buyId }, hideLoader: false })
    const code = parseInt(result, 10)

    if (code === 0 || code === -1) {
      setTimeout(() => {
        dispatch(checkAdd(buyId))
      }, 15000)
      return
    }

    if ([-2, -3, -4].includes(code)) {
      const messages = {
        '-2': __i18n('LANG.ERRORS.CARDS.OVER_LIMITED'),
        '-3': __i18n('LANG.ERRORS.CARDS.SMALL_BALANCE'),
        '-4': __i18n('LANG.ERRORS.CARDS.INCORRECT_DATA'),
      }

      dispatch(open('common/Errors', {
        header: __i18n('LANG.ERRORS.ERROR'),
        body: messages[result],
      }))

      return
    }

    dispatch(hide())
    dispatch(close())

    window.location.hash = '/check/list'

    return
  }
}

export function runCheck() {
  return async (dispatch, getState, { post }) => {
    const providerId = getState().check.check.get('checkProvider')
    const cards = getState().check.check.getIn(['toCheck', 'cards']).toJS()

    const data = cards.map(item => {
      const card = {}

      item.forEach(child => { card[child.field] = child.value })

      const obj = {
        exp: card.y + card.m,
        number: card.num,
        address: card.addr,
      }

      if (card.zip) obj.zip = card.zip
      if (card.cvv2) obj.cvv2 = card.cvv2

      return obj
    })

    const body = {
      data: JSON.stringify(data),
      provider_id: providerId,
    }

    const { result } = await post('test/add', { body, hideLoader: false })

    dispatch(checkAdd(result))
  }
}

export function saveFormat() {
  return async (dispatch, getState, { post }) => {
    const format = getState().check.check.get('formatString')

    const { result } = await post('test/format/save', { body: { format } })

    if (result) {
      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.MESSAGE'),
        body: __i18n('LANG.SERVICE.TPL_SUCCESS'),
      }))
    }
  }
}

export function loadFormat() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('test/format/load')

    dispatch({
      type: actions.loadFormat,
      result,
    })

    dispatch(open('check/Formats'))
  }
}

export function removeFormat(format) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('test/format/delete', { body: { format_id: format } })

    dispatch({
      type: actions.loadFormat,
      result,
    })
  }
}
