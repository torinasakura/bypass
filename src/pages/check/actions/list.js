import { open, close } from 'bypass/app/actions/modal'
import * as actions from '../constants/list'

export function load() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('test')

    dispatch({
      type: actions.load,
      provider: getState().config.provider,
      ...result,
    })
  }
}

export function select(row) {
  return {
    type: actions.select,
    row,
  }
}

export function selectAll() {
  return {
    type: actions.selectAll,
  }
}

export function editComment() {
  return open('check/EditComment')
}

export function saveComment(comment) {
  return async (dispatch, getState, { post }) => {
    const tests = getState().check.list.get('data').filter(check => check.get('selected'))
                                                   .map(check => check.get('test_id')).toJS()

    const body = {
      test_id: tests,
      comment,
    }

    const { result } = await post('test/comment/edit', { body })

    if (result) {
      dispatch(close())
      dispatch(load())
    }
  }
}

export function remove() {
  return async (dispatch, getState, { post }) => {
    const tests = getState().check.list.get('data').filter(check => check.get('selected'))
                                                   .map(check => check.get('test_id')).toJS()

    const { result } = await post('test/delete', { body: { test_id: tests } })

    if (result) {
      dispatch(load())

      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.MESSAGE'),
        body: `${__i18n('LANG.SERVICE.DELETED_CARDS')}${tests.length}`,
      }))
    }
  }
}
