import { connect } from 'react-redux'
import { topup, checkPayment, tryCreateTicket } from '../actions'
import * as Balance from '../components/Balance'

const connector = connect(
  state => ({
    btc: state.billing.get('btc').toJS(),
  }),
  dispatch => ({
    onBtcTopup: () => {
      dispatch(topup())
    },
    onCheckPayment: () => {
      dispatch(checkPayment())
    },
    onTryCreateTicket: () => {
      dispatch(tryCreateTicket())
    },
  }),
)

export const Desktop = connector(Balance.Desktop)

export const Tablet = connector(Balance.Tablet)

export const Mobile = connector(Balance.Mobile)
