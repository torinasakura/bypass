import { connect } from 'react-redux'
import { load, openTransaction } from '../actions'
import * as List from '../components/List'

const connector = connect(
  state => ({
    list: state.billing.get('list'),
  }),
  dispatch => ({
    onLoad: () => {
      dispatch(load())
    },
    onOpenTransaction: id => {
      dispatch(openTransaction(id))
    },
  }),
)

export const Desktop = connector(List.Desktop)

export const Tablet = connector(List.Tablet)

export const Mobile = connector(List.Mobile)
