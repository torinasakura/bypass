import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Bitcoin, Trouble } from './blocks'

const Tablet = ({ btc, onBtcTopup, onCheckPayment, onTryCreateTicket }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('LANG.SERVICE.LOAD_BALANCE')}
          </Title>
        </Layout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Bitcoin
                {...btc}
                onBtcTopup={onBtcTopup}
                onCheckPayment={onCheckPayment}
              />
            </Layout>
            <Layout grow={1} />
          </ColumnLayout>
        </Layout>
        <Layout grow={1}>
          <ColumnLayout justify='center' align='center'>
            <Layout>
              <Trouble onTryCreateTicket={onTryCreateTicket} />
            </Layout>
          </ColumnLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
