import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { Bitcoin, Trouble } from './blocks'

const Mobile = ({ btc, onBtcTopup, onCheckPayment, onTryCreateTicket }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small'>
            {__i18n('LANG.SERVICE.LOAD_BALANCE')}
          </Title>
        </Layout>
        <Layout>
          <RowLayout align='center'>
            <Layout basis='10px' />
            <Layout>
              <Bitcoin
                small
                {...btc}
                onBtcTopup={onBtcTopup}
                onCheckPayment={onCheckPayment}
              />
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Trouble small onTryCreateTicket={onTryCreateTicket} />
            </Layout>
            <Layout basis='10px' />
          </RowLayout>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
