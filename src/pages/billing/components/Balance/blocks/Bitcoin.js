import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'
import { RowLayout, Layout } from 'bypass/ui/layout'
import { Condition } from 'bypass/ui/condition'
import Buttons from './Buttons'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #e0e0e0',
    width: '350px',
    height: '300px',
    fontSize: '14px',
    textAlign: 'center',
  },
  small: {
    width: '300px',
  },
})

const Bitcoin = ({ enable, small, onBtcTopup, onCheckPayment }) => (
  <div className={styles({ small })}>
    <RowLayout align='center'>
      <Layout grow={1}>
        <div style={{ paddingTop: 8 }}>
          <img
            alt='Bitcoin Logo'
            src='/images/payment-systems/bc.png'
          />
        </div>
      </Layout>
      <Layout>
        <Condition match={enable}>
          <div dangerouslySetInnerHTML={{ __html: __i18n('BILLING.BTC.LOAD_TEXT') }} />
        </Condition>
        <Condition match={!enable}>
          <div>
            {__i18n('BILLING.LOAD_UNAV')}
          </div>
        </Condition>
      </Layout>
      <Layout grow={1}>
        <Buttons>
          <Button disabled={!enable} onClick={onBtcTopup}>
            {__i18n('LANG.BUTTONS.LOAD')}
          </Button>
          <Button disabled={!enable} onClick={onCheckPayment}>
            {__i18n('BILLING.CHECK')}
          </Button>
        </Buttons>
      </Layout>
    </RowLayout>
  </div>
)

export default Bitcoin
