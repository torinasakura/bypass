import React from 'react'
import { StyleSheet } from 'quantum'
import { Button } from 'bypass/ui/button'

const styles = StyleSheet.create({
  self: {
    border: '1px solid #e0e0e0',
    width: '350px',
    height: '100px',
    fontSize: '16px',
    color: '#b71c1c',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  small: {
    width: '300px',
  },
})

const Trouble = ({ small, onTryCreateTicket }) => (
  <div className={styles({ small })}>
    <div>
      {__i18n('BILLING.IF_TROUBLE')}
    </div>
    <div>
      <Button type='light' onClick={onTryCreateTicket}>
        {__i18n('BILLING.NEW_TICKET')}
      </Button>
    </div>
  </div>
)

export default Trouble
