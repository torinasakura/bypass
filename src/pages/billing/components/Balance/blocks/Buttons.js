import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    '& button': {
      marginBottom: '12px',
      paddingLeft: '50px',
      paddingRight: '50px',
    },
  },
})

const Buttons = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Buttons
