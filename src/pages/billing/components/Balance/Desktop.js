import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Bitcoin, Trouble } from './blocks'

const Desktop = ({ btc, onBtcTopup, onCheckPayment, onTryCreateTicket }) => (
  <Wrapper>
    <Container>
      <RowLayout>
        <Layout>
          <Title>
            {__i18n('LANG.SERVICE.LOAD_BALANCE')}
          </Title>
        </Layout>
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Bitcoin
                {...btc}
                onBtcTopup={onBtcTopup}
                onCheckPayment={onCheckPayment}
              />
            </Layout>
            <Layout grow={1} />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout justify='center' align='center'>
            <Layout>
              <Trouble onTryCreateTicket={onTryCreateTicket} />
            </Layout>
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </Container>
  </Wrapper>
)

export default Desktop
