import React from 'react'
import { Table, Column, BodyCell } from 'bypass/ui/table'
import { Condition } from 'bypass/ui/condition'
import { PlusIcon, MinusIcon } from 'bypass/ui/icons'
import { Price } from 'bypass/ui/price'
import Link from './TransactionLink'

const directionRenderer = (cellData, dataKey, rowData, rowIndex) => (
  <BodyCell justify='center' oddRow={(rowIndex % 2) !== 0}>
    <Condition match={cellData === 'out'}>
      <MinusIcon fill='#b71c1c' width={14} />
    </Condition>
    <Condition match={cellData === 'in'}>
      <PlusIcon fill='#4caf50' width={15} />
    </Condition>
  </BodyCell>
)

const amountRenderer = (cellData, dataKey, rowData, rowIndex) => (
  <BodyCell justify='end' oddRow={(rowIndex % 2) !== 0}>
    <Price green>
      {cellData}
    </Price>
  </BodyCell>
)

const balanceRenderer = (cellData, dataKey, rowData, rowIndex) => (
  <BodyCell justify='end' oddRow={(rowIndex % 2) !== 0}>
    <Price>
      {cellData}
    </Price>
  </BodyCell>
)

const List = ({ minWidth, rowHeight, total, list, onLoad, onOpenTransaction }) => (
  <Table
    list={list}
    total={total}
    minWidth={minWidth}
    rowHeight={rowHeight}
    onLoad={onLoad}
  >
    <Column
      label={__i18n('BILLING.TABLE.COLS.INDEX')}
      width='60px'
      dataKey='index'
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.DIR')}
      width='90px'
      dataKey='direction'
      cellRenderer={directionRenderer}
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.TIME')}
      width='140px'
      dataKey='add_time'
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.CMNT')}
      width='140px'
      flexGrow={1}
      dataKey='comment'
      cellRenderer={(cellData, dataKey, rowData, rowIndex) => (
        <BodyCell oddRow={(rowIndex % 2) !== 0}>
          <Link rowData={rowData} onClick={onOpenTransaction}>
            {cellData}
          </Link>
        </BodyCell>
      )}
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.AMOUNT')}
      width='140px'
      dataKey='amount'
      cellRenderer={amountRenderer}
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.BALANCE')}
      width='140px'
      dataKey='balance'
      cellRenderer={balanceRenderer}
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.COUNT')}
      width='120px'
      dataKey='count'
      justify='center'
    />
    <Column
      label={__i18n('BILLING.TABLE.COLS.ORDER_ID')}
      width='120px'
      dataKey='order_id'
      justify='center'
    />
  </Table>
)

export default List
