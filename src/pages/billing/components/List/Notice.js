import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'block',
    fontSize: '14px',
    background: 'rgba(236, 239, 241, 0.5)',
    padding: '12px',
    width: '100%',
  },
  'align=center': {
    textAlign: 'center',
  },
  white: {
    background: '#ffffff',
  },
  small: {
    fontSize: '10px',
    padding: '6px',
  },
})

const Notice = ({ align, white, small }) => (
  <div
    className={styles({ align, white, small })}
    dangerouslySetInnerHTML={{ __html: __i18n('TICKETS.SCHEDULE') }}
  />
)

export default Notice
