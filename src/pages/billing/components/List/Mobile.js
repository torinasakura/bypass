import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import List from './List'

const Mobile = ({ list, total, onLoad, onOpenTransaction }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='small' white>
            {__i18n('LANG.SERVICE.BILLING')}
          </Title>
        </Layout>
        <Layout touch scrollX grow={1} shrink={1}>
          <List
            list={list}
            total={total}
            rowHeight={29}
            minWidth={900}
            onLoad={onLoad}
            onOpenTransaction={onOpenTransaction}
          />
        </Layout>
        <Layout>
          <Button type='red' fill>
            {__i18n('BILLING.TABLE.CLEAR')}
          </Button>
        </Layout>
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Mobile
