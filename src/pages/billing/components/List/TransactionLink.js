/* eslint-disable eqeqeq */
import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    fontSize: '12px',
    textDecoration: 'none',
    color: '#9e9e9e',
  },
  decorated: {
    textDecoration: 'underline',
  },
})

const TransactionLink = ({ children, rowData, onClick }) => {
  const commentId = rowData.get('comment_id')
  const transitionId = rowData.get('transaction_id')

  return (
    <span
      className={styles({ decorated: commentId == 1 })}
      onClick={(commentId == 1 && onClick) && onClick.bind(null, transitionId)}
    >
      {children}
    </span>
  )
}

export default TransactionLink
