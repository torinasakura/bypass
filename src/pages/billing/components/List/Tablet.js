import React from 'react'
import { Title } from 'bypass/ui/title'
import { Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import List from './List'

const Tablet = ({ list, total, onLoad, onOpenTransaction }) => (
  <Container>
    <AutoSizer>
      <RowLayout>
        <Layout>
          <Title size='medium'>
            {__i18n('LANG.SERVICE.BILLING')}
          </Title>
        </Layout>
        <Layout touch scrollX grow={1} shrink={1}>
          <List
            list={list}
            total={total}
            rowHeight={29}
            minWidth={900}
            onLoad={onLoad}
            onOpenTransaction={onOpenTransaction}
          />
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <ColumnLayout>
            <Layout grow={1} />
            <Layout>
              <Button type='red'>
                {__i18n('BILLING.TABLE.CLEAR')}
              </Button>
            </Layout>
            <Layout basis='20px' />
          </ColumnLayout>
        </Layout>
        <Layout basis='20px' />
      </RowLayout>
    </AutoSizer>
  </Container>
)

export default Tablet
