import React from 'react'
import { Title } from 'bypass/ui/title'
import { Wrapper, Container } from 'bypass/ui/page'
import { AutoSizer, ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Button } from 'bypass/ui/button'
import List from './List'

const Desktop = ({ list, total, onLoad, onOpenTransaction }) => (
  <Wrapper>
    <Container indent>
      <AutoSizer>
        <RowLayout>
          <Layout>
            <Title>
              {__i18n('LANG.SERVICE.BILLING')}
            </Title>
          </Layout>
          <Layout grow={1} shrink={1}>
            <List
              list={list}
              total={total}
              rowHeight={29}
              onLoad={onLoad}
              onOpenTransaction={onOpenTransaction}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <ColumnLayout>
              <Layout grow={1} />
              <Layout>
                <Button type='red'>
                  {__i18n('BILLING.TABLE.CLEAR')}
                </Button>
              </Layout>
            </ColumnLayout>
          </Layout>
          <Layout basis='20px' />
        </RowLayout>
      </AutoSizer>
    </Container>
  </Wrapper>
)

export default Desktop
