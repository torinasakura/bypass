/* eslint-disable no-use-before-define */
/* eslint-disable camelcase */
import cookie from 'js-cookie'
import { open } from 'bypass/app/actions/modal'
import { hide } from 'bypass/app/actions/loader'
import { load as loadConfig } from 'bypass/app/actions/config'
import config from 'bypass/app/config'
import * as actions from '../constants'

const paymentMessages = {
  'duplicate record': __i18n('LANG.ERRORS.PAYMENT.DUPLICATE RECORD'),
  'bad address': __i18n('LANG.ERRORS.PAYMENT.BAD ADDRESS'),
  'cant connect (new_address)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (NEW_ADDRESS)'),
  'cant connect (rawaddr)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (RAWADDR)'),
  'bad response (block)': __i18n('LANG.ERRORS.PAYMENT.BAD RESPONSE (BLOCK)'),
  'cant connect (block)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (BLOCK)'),
  'empty address': __i18n('LANG.ERRORS.PAYMENT.EMPTY ADDRESS'),
  'empty amount after fee': __i18n('LANG.ERRORS.PAYMENT.EMPTY AMOUNT AFTER FEE'),
  'empty amount': __i18n('LANG.ERRORS.PAYMENT.EMPTY AMOUNT'),
  'cant connect (course)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (COURSE)'),
  'no payment': __i18n('LANG.ERRORS.PAYMENT.NO PAYMENT'),
  'cant connect (wm)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (WM)'),
  'cant connect (pm)': __i18n('LANG.ERRORS.PAYMENT.CANT CONNECT (PM)'),
  not_enroll: __i18n('LANG.ERRORS.PAYMENT.NOT_ENROLL'),
  pending: __i18n('LANG.ERRORS.PAYMENT.PENDING'),
}

export function load(offset = 0) {
  return async (dispatch, getState, { get }) => {
    const perpage = getState().billing.get('perpage')

    const { result } = await get('billing', { body: { offset, perpage }, loader: offset === 0 })

    return dispatch({
      type: actions.load,
      ...result,
    })
  }
}

export function loadTopup() {
  return async (dispatch, getState, { get }) => {
    const { result } = await get('billing/topup')

    return dispatch({
      type: actions.loadTopup,
      ...result,
    })
  }
}

export function clear() {
  return {
    type: actions.clear,
  }
}

export function topup() {
  return async (dispatch, getState) => {
    const address = getState().billing.getIn(['btc', 'address'])

    if (address) {
      dispatch(open('billing/Topup', { address }))
    } else {
      dispatch(open('billing/CreateWallet', {
        onCreate: () => dispatch(createWallet()),
      }))
    }
  }
}

async function check(id, resolve, reject, get) {
  const { result } = await get('billing/check', { body: { process_id: id }, hideLoader: false })

  if (result.status == 1) { // eslint-disable-line eqeqeq
    resolve(result)
  } else {
    setTimeout(() => check(id, resolve, reject, get), config.cart.buy_interval)
  }
}

function checkProcess(id, get) {
  return new Promise((resolve, reject) => check(id, resolve, reject, get))
}

export function createWallet() {
  return async (dispatch, getState, { get }) => {
    const { result: createResult } = await get('billing/create/address', { hideLoader: false })

    if (createResult === 'already exists') {
      dispatch(topup())
      dispatch(hide())
      return
    }

    const { error_code } = await checkProcess(createResult, get)

    if (error_code === 'bad address') {
      dispatch(open('common/Errors', {
        header: __i18n('LANG.SERVICE.OPERATION_STATUS'),
        body: __i18n('LANG.ERRORS.BTC_INCORRECT_RESPONSE'),
      }))
      dispatch(hide())
      return
    }

    await dispatch(load())
    await dispatch(loadTopup())

    setTimeout(() => dispatch(topup()), 250)
  }
}

export function checkPayment() {
  return async (dispatch, getState, { get }) => {
    const body = { system: 'bitcoin' }

    const { result: checkResult } = await get('billing/create/payment', { body, hideLoader: false })

    const { error_code } = await checkProcess(checkResult, get)

    dispatch(hide())

    if (error_code === 'ok') {
      cookie.remove('balance_req_interval')

      dispatch(loadConfig())

      dispatch(open('common/Message', {
        header: __i18n('LANG.SERVICE.OPERATION_STATUS'),
        body: __i18n('LANG.SERVICE.PAYMENT_SUCCESS'),
      }))
    } else if (error_code === 'pending') {
      if (cookie.get('balance_req_interval')) {
        cookie.set('balance_req_interval', parseInt(cookie.get('balance_req_interval'), 10) * 2)
      } else {
        cookie.set('balance_req_interval', config.billing.balance_req_interval * 2)
      }
    } else {
      dispatch(open('common/Errors', {
        header: __i18n('LANG.SERVICE.OPERATION_STATUS'),
        body: paymentMessages[error_code]
          ? paymentMessages[error_code]
          : __i18n('LANG.ERRORS.COMMON'),
      }))
    }
  }
}

export function tryCreateTicket() {
  return async dispatch => {
    dispatch(open('tickets/DummyProtection'))
  }
}

export function openTransaction(id) {
  return async (dispatch, getState, { post }) => {
    const { result } = await post('billing/payment', { body: { transaction_id: id } })

    dispatch(open('billing/Transaction', result.data))
  }
}
