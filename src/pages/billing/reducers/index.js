import { fromJS } from 'immutable'
import { createReducer } from 'bypass/utils/index'
import { timeConverter } from '../../../app/utils/utils'
import * as actions from '../constants'

const initialState = fromJS({
  list: [],
  offset: 0,
  perpage: 40,
  total: 0,
  btc: {},
})

export default createReducer(initialState, {
  [actions.loadTopup]: (state, { btc }) => state.merge({ btc }),

  [actions.load]: (state, { data, offset, perpage, count }) => {
    const items = data.map((item, index) => ({
      ...item,
      index: index + 1,
      add_time: timeConverter(item.add_time, true, true),
    }))

    return state.merge({
      list: fromJS(items),
      total: parseInt(count, 10),
      offset,
      perpage,
    })
  },

  [actions.clear]: (state) => state.merge(initialState),
})
