import { createViewport } from 'bypass/app/utils'
import { load, loadTopup } from '../actions'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'billing',
    childRoutes: [{
      path: 'balance',
      onEnter() {
        dispatch(loadTopup())
      },
      getComponent(location, callback) {
        System.import('../containers/Balance')
              .then(Balance => callback(null, createViewport(Balance)))
      },
    },
    {
      path: 'list',
      onEnter() {
        dispatch(load())
      },
      getComponent(location, callback) {
        System.import('../containers/List')
              .then(List => callback(null, createViewport(List)))
      },
    }],
  }]
}
