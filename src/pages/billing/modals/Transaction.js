/* eslint-disable camelcase */
import React from 'react'
import { StyleSheet } from 'quantum'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { ColumnLayout, RowLayout, Layout } from 'bypass/ui/layout'
import { Text } from 'bypass/ui/text'

const styles = StyleSheet.create({
  self: {
    margin: '0 40px 20px 40px',
    overflow: 'hidden',
  },
})

const Field = ({ children, label }) => (
  <Layout>
    <ColumnLayout>
      <Layout basis='80px'>
        {label}:
      </Layout>
      <Layout>
        {children}
      </Layout>
    </ColumnLayout>
  </Layout>
)

const Transaction = ({ transaction_id, type, amount, batch, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.TRANSACT')} {transaction_id}
      </Text>
    </Header>
    <Body alignCenter>
      <div className={styles()}>
        <RowLayout>
          <Field label={__i18n('BILLING.TYPE')}>
            {type}
          </Field>
          <Field label={__i18n('BILLING.AMOUNT')}>
            {amount}
          </Field>
          <Field label={__i18n('BILLING.COURSE')}>
            1 : {type}
          </Field>
          <Field label={__i18n('BILLING.BATCH')}>
            <div>
              {batch}
            </div>
          </Field>
        </RowLayout>
      </div>
    </Body>
  </Modal>
)

export default Transaction
