import React from 'react'
import { Modal, Header, Body, Footer } from 'bypass/ui/modal'
import { Button } from 'bypass/ui/button'
import { Text } from 'bypass/ui/text'

const CreateWallet = ({ onCreate, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.LOAD_OVER')} Bitcoin
      </Text>
    </Header>
    <Body alignCenter>
      <div style={{ padding: '0 20px' }}>
        {__i18n('BILLING.BTC.CREATE_WALLET')}
      </div>
    </Body>
    <Footer alignCenter>
      <Button onClick={onCreate}>
        {__i18n('LANG.SERVICE.CREATE_PURSE')}
      </Button>
    </Footer>
  </Modal>
)

export default CreateWallet
