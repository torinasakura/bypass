import React from 'react'
import QRCode from 'react-qr'
import { Modal, Header, Body } from 'bypass/ui/modal'
import { Text } from 'bypass/ui/text'

const Topup = ({ address, onClose }) => (
  <Modal onClose={onClose}>
    <Header>
      <Text size={19}>
        {__i18n('LANG.SERVICE.LOAD_OVER')} Bitcoin
      </Text>
    </Header>
    <Body alignCenter>
      <div>
        {__i18n('BILLING.POINT_ADDRESS')} <b>{address}</b>
      </div>
      <br />
      <QRCode text={address} />
      <br />
      <br />
      <i>
        {__i18n('BILLING.PRESS_CHECK')}
      </i>
    </Body>
  </Modal>
)

export default Topup
