import { actions } from './../actions'

const initialState = {
  mobileMenu: false,
}

export default function workspace(state = initialState, { type }) {
  switch (type) {
    case actions.openMobileMenu:
      return {
        ...state,
        mobileMenu: true,
      }
    case actions.closeMobileMenu:
      return {
        ...state,
        mobileMenu: false,
      }
    default:
      return state
  }
}
