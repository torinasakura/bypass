import React from 'react'
import { connect } from 'react-redux'
import { Desktop, Tablet, Mobile } from '../components/footer'

const Container = ({ viewport: { type } = {}, ...props }) => {
  if (type === 'Mobile') {
    return (
      <Mobile {...props} />
    )
  } else if (type === 'Tablet') {
    return (
      <Tablet {...props} />
    )
  }

  return (
    <Desktop {...props} />
  )
}

export default connect(
  state => ({
    viewport: state.viewport,
    update: state.config.update,
    checkers: state.config.checkers,
    support: state.config.support,
    providers: state.config.provider,
    supportSystem: state.config.settings.support_system,
  }),
)(Container)
