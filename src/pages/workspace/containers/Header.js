import React from 'react'
import { connect } from 'react-redux'
import { logout } from '../../auth/actions'
import { toggleMobileMenu } from '../actions'
import { Desktop, Mobile } from '../components/header'

const Container = ({ viewport: { type, orientation } = {}, ...props }) => {
  if (type === 'Desktop' || (type === 'Tablet' && orientation === 'Landscape')) {
    return (
      <Desktop {...props} />
    )
  }

  return (
    <Mobile {...props} />
  )
}

export default connect(
  state => ({
    viewport: state.viewport,
    balance: state.config.balance,
  }),
  dispatch => ({
    onLogout: () => {
      dispatch(logout())
    },
    onToggleMenu: () => {
      dispatch(toggleMobileMenu())
    },
  })
)(Container)
