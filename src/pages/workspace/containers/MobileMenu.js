import React from 'react'
import { connect } from 'react-redux'
import MobileMenu from '../components/MobileMenu'

const Container = ({ viewport: { type, orientation } = {}, ...props }) => {
  if (type === 'Desktop' || (type === 'Tablet' && orientation === 'Landscape')) {
    return null
  }

  return (
    <MobileMenu {...props} />
  )
}

export default connect(
  state => ({
    viewport: state.viewport,
  }),
)(Container)
