import React from 'react'
import { connect } from 'react-redux'
import { StyleSheet } from 'quantum'
import Swipeable from 'react-swipeable'
import { openMobileMenu, closeMobileMenu } from '../actions'
import { RowLayout, Layout, AutoSizer } from 'bypass/ui/layout'
import Header from './Header'
import Footer from './Footer'
import MobileMenu from './MobileMenu'

const styles = StyleSheet.create({
  self: {
    background: '#f5f5f5',
    width: '100%',
    height: '100%',
    position: 'relative',
    zIndex: 1,
  },
  push: {
    left: '245px',
    transition: 'left 0.2s',
  },
})

const Workspace = ({ children }) => (
  <RowLayout>
    <Layout>
      <Header />
    </Layout>
    <Layout grow={1} shrink={1} scrollY>
      <AutoSizer>
        {children}
      </AutoSizer>
    </Layout>
    <Layout>
      <Footer />
    </Layout>
  </RowLayout>
)

const Container = ({ viewport: { type, orientation } = {}, push, onOpenMenu, onCloseMenu, ...props }) => {
  if (type === 'Desktop' || (type === 'Tablet' && orientation === 'Landscape')) {
    return (
      <div className={styles()}>
        <Workspace {...props} />
      </div>
    )
  }

  return (
    <Swipeable
      onSwipingRight={onOpenMenu}
      onSwipingLeft={onCloseMenu}
    >
      <div className={styles({ push })}>
        <MobileMenu />
        <Workspace {...props} />
      </div>
    </Swipeable>
  )
}

export default connect(
  state => ({
    viewport: state.viewport,
    push: state.workspace.mobileMenu,
  }),
  dispatch => ({
    onOpenMenu: () => {
      dispatch(openMobileMenu())
    },
    onCloseMenu: () => {
      dispatch(closeMobileMenu())
    },
  })
)(Container)
