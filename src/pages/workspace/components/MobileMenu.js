import React from 'react'
import { StyleSheet } from 'quantum'
import { Layer } from 'bypass/ui/layer'
import { Menu, Item, RootItem } from 'bypass/ui/menu/mobile'

const styles = StyleSheet.create({
  self: {
    width: '245px',
    bottom: 0,
  },
})

const MobileMenu = (props, context) => (
  <Layer align='tl-tl' target={document.body} classes={{ element: styles() }}>
    <Menu context={context}>
      <Item to='/news'>{__i18n('NAV.MAIN')}</Item>
      <RootItem text={__i18n('NAV.CARDS.TEXT')}>
        <Item to='/cards/search'>{__i18n('NAV.CARDS.SEARCH')}</Item>
        <Item to='/cards/cart'>{__i18n('NAV.CARDS.CART')}</Item>
        <Item to='/cards/orders'>{__i18n('NAV.CARDS.ORDERS')}</Item>
        <Item to='/cards/stat/stat'>{__i18n('NAV.CARDS.STAT')}</Item>
      </RootItem>
      <RootItem text={__i18n('NAV.CHECK.TEXT')}>
        <Item to='/check/add'>{__i18n('NAV.CHECK.CHECK')}</Item>
        <Item to='/check/list'>{__i18n('NAV.CHECK.HISTORY')}</Item>
      </RootItem>
      <RootItem text={__i18n('NAV.BILLING.TEXT')}>
        <Item to='/billing/balance'>{__i18n('NAV.BILLING.BALANCE')}</Item>
        <Item to='/billing/list'>{__i18n('NAV.BILLING.MAIN')}</Item>
      </RootItem>
      <Item to='/tickets'>{__i18n('NAV.TICKETS')}</Item>
      <Item to='/faq'>{__i18n('NAV.FAQ')}</Item>
      <Item to='/profile'>{__i18n('NAV.PROFILE')}</Item>
    </Menu>
  </Layer>
)

MobileMenu.contextTypes = {
  router: React.PropTypes.object,
}

export default MobileMenu
