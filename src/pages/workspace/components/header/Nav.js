import React from 'react'
import { StyleSheet } from 'quantum'
import { ItemGroup, Item, Menu, MenuItem } from 'bypass/ui/navigation'

const cardsMenu = (
  <Menu>
    <MenuItem to='/cards/search'>
      {__i18n('NAV.CARDS.SEARCH')}
    </MenuItem>
    <MenuItem to='/cards/cart'>
      {__i18n('NAV.CARDS.CART')}
    </MenuItem>
    <MenuItem to='/cards/orders'>
      {__i18n('NAV.CARDS.ORDERS')}
    </MenuItem>
    <MenuItem to='/cards/stat/stat'>
      {__i18n('NAV.CARDS.STAT')}
    </MenuItem>
  </Menu>
)

const checkMenu = (
  <Menu>
    <MenuItem to='check/add'>
      {__i18n('NAV.CHECK.CHECK')}
    </MenuItem>
    <MenuItem to='/check/list'>
      {__i18n('NAV.CHECK.HISTORY')}
    </MenuItem>
  </Menu>
)

const billingMenu = (
  <Menu>
    <MenuItem to='/billing/balance'>
      {__i18n('NAV.BILLING.BALANCE')}
    </MenuItem>
    <MenuItem to='/billing/list'>
      {__i18n('NAV.BILLING.MAIN')}
    </MenuItem>
  </Menu>
)

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
    height: '100%',
    flexGrow: 1,
  },
})

const Nav = () => (
  <div className={styles()}>
    <ItemGroup fill limit justify='space-around'>
      <Item to='/news'>
        {__i18n('NAV.MAIN')}
      </Item>
      <Item menu={cardsMenu}>
        {__i18n('NAV.CARDS.TEXT')}
      </Item>
      <Item menu={checkMenu}>
        {__i18n('NAV.CHECK.TEXT')}
      </Item>
      <Item menu={billingMenu}>
        {__i18n('NAV.BILLING.TEXT')}
      </Item>
      <Item to='/tickets'>
        {__i18n('NAV.TICKETS')}
      </Item>
      <Item to='/faq'>
        {__i18n('NAV.FAQ')}
      </Item>
      <Item to='/profile'>
        {__i18n('NAV.PROFILE')}
      </Item>
    </ItemGroup>
  </div>
)

export default Nav
