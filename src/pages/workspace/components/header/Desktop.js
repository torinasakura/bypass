import React from 'react'
import { Navigation } from 'bypass/ui/navigation'
import Nav from './Nav'
import Menu from './Menu'
import Divider from './Divider'

const Header = ({ balance, onLogout }) => (
  <Navigation>
    <Nav />
    <Divider />
    <Menu balance={balance} onLogout={onLogout} />
  </Navigation>
)

export default Header
