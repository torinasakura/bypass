import React from 'react'
import { ItemGroup, Item, ChangeLangItem } from 'bypass/ui/navigation'
import { ExitIcon } from 'bypass/ui/icons'

const Menu = ({ balance, fill = false, onLogout }) => (
  <ItemGroup fill={fill} justify={fill ? 'space-around' : null}>
    <Item to='/billing/balance' withouthBorder>
      $ {balance}
    </Item>
    <ChangeLangItem />
    <Item withouthBorder onClick={onLogout}>
      <ExitIcon width={24} />
    </Item>
  </ItemGroup>
)

export default Menu
