import React from 'react'
import { Navigation, Item } from 'bypass/ui/navigation'
import { MenuIcon } from 'bypass/ui/icons'
import Menu from './Menu'

const Header = ({ balance, onLogout, onToggleMenu }) => (
  <Navigation full small>
    <Item withouthBorder onClick={onToggleMenu}>
      <MenuIcon width={36} />
    </Item>
    <Menu fill balance={balance} onLogout={onLogout} />
  </Navigation>
)

export default Header
