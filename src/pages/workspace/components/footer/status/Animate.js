import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    display: 'inline-block',
    borderRadius: '50%',
    margin: '0 5px',
    verticalAlign: 'middle',
    width: '14px',
    height: '14px',
    border: '1px solid #e0e0e0',
    animation: 'pulsate 2s infinite',
  },
  'status=active': {
    borderColor: '#8bc34a',
  },
})

const Animate = ({ status }) => (
  <span className={styles({ status })} />
)

export default Animate
