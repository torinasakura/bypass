import React from 'react'
import { StyleSheet } from 'quantum'
import { Tooltip } from 'bypass/ui/tooltip'
import { Condition } from 'bypass/ui/condition'
import { connect, toggle } from 'bypass/ui/state'
import Animate from './Animate'
import Message from './Message'

const styles = StyleSheet.create({
  self: {
  },
})

const Status = ({ children, toggled, status = 'active', animate = true, decorated, onClick }) => {
  const onClose = event => {
    event.preventDefault()

    if (onClick) {
      onClick()
    }
  }

  return (
    <span className={styles({ status })} onClick={onClick}>
      <Condition match={animate}>
        <Animate status={status} />
      </Condition>
      <Message status={status} decorated={decorated} />
      <Condition match={toggled}>
        <Tooltip align='top' onClose={onClose} offset='12px -10px'>
          {children}
        </Tooltip>
      </Condition>
    </span>
  )
}

export default connect([toggle], Status)
