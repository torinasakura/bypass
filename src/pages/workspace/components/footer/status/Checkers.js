/* eslint-disable eqeqeq */
import React from 'react'
import Status from './Status'
import Item from './Item'

const Checkers = ({ status, providers = [], animate, decorated }) => (
  <Status status={status} animate={animate} decorated={decorated}>
    {providers.map(provider => (
      <Item key={provider.provider_id} active={provider.available == 1}>
        {provider.provider}
      </Item>
    ))}
  </Status>
)

export default Checkers
