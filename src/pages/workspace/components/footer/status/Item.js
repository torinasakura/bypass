import React from 'react'
import { StyleSheet } from 'quantum'
import { CircleIcon } from 'bypass/ui/icons'

const styles = StyleSheet.create({
  self: {
    margin: '3px 0px',
    cursor: 'pointer',
    transition: 'all 0.3s',
    fontSize: '14px',
    color: '#ffffff',
    display: 'flex',
    alignItems: 'center',
    '&:hover': {
      color: '#e6e6e6',
    },
  },
})

const Item = ({ children, active }) => (
  <div className={styles()}>
    <CircleIcon width={16} green={active} />
    {'\u00A0'}
    {children}
  </div>
)

export default Item
