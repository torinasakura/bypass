import React from 'react'
import { StyleSheet } from 'quantum'

const messages = {
  active: __i18n('FOOTER.STATUS.WORK'),
  partial: __i18n('FOOTER.STATUS.PART'),
}

const styles = StyleSheet.create({
  self: {
    color: '#ffffff',
    fontSize: '12px',
    transition: 'all 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: '#d9d9d9',
    },
  },
  decorated: {
    borderBottom: '1px dotted',
  },
  'status=active': {
    color: '#8bc34a',
    '&:hover': {
      color: '#649130',
    },
  },
})

const Message = ({ status = 'active', decorated = true }) => (
  <span className={styles({ status, decorated })}>
    {messages[status]}
  </span>
)

export default Message
