import React from 'react'
import Status from './Status'
import Item from './Item'

const Support = ({ status, supportSystem = {}, animate, decorated }) => (
  <Status status={status} animate={animate} decorated={decorated}>
    <Item active={supportSystem.o === 1}>
      {__i18n('COM.MANAGERS.OWNER')}
    </Item>
    <Item active={supportSystem.a === 1}>
      {__i18n('COM.MANAGERS.ADMIN')}
    </Item>
    <Item active={supportSystem.s === 1}>
      {__i18n('COM.MANAGERS.SUPPORT')}
    </Item>
  </Status>
)

export default Support
