import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    padding: '12px 0',
    margin: '4px',
    fontSize: '12px',
  },
  fill: {
    background: 'rgba(96, 125, 139, 0.2)',
    width: '100%',
    textAlign: 'center',
  },
  middle: {
    padding: '12px 0px',
  },
  small: {
    padding: '4px 0',
    margin: '2px',
  },
})

const Block = ({ children, fill, middle, small }) => (
  <div className={styles({ fill, middle, small })}>
    {children}
  </div>
)

export default Block
