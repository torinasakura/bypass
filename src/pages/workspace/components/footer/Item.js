import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    transition: 'color 0.3s',
    color: '#ffffff',
    fontSize: '12px',
  },
  yellow: {
    color: '#fdd835',
  },
  decorated: {
    borderBottom: '1px dotted',
  },
  middle: {
    display: 'inline-flex',
    alignItems: 'center',
  },
})

const Item = ({ children, yellow, decorated, middle }) => (
  <span className={styles({ yellow, decorated, middle })}>
    {children}
  </span>
)

export default Item
