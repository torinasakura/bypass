import React from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'react-router'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import Block from './Block'
import Item from './Item'
import Support from './status/Support'
import Checkers from './status/Checkers'

const styles = StyleSheet.create({
  self: {
    background: '#37474f',
    boxShadow: '0 -2px 4px rgba(0, 0, 0, 0.24)',
    width: '100%',
    padding: '4px 2px',
    margin: '0px',
  },
})

const Tablet = ({ update, checkers, support, supportSystem, providers }) => (
  <div className={styles()}>
    <div>
      <ColumnLayout>
        <Layout grow={1}>
          <Block fill middle>
            <Link to='/faq' style={{ textDecoration: 'none' }}>
              <Item decorated>
                {__i18n('FOOTER.NEED_HELP')}
              </Item>
            </Link>
          </Block>
        </Layout>
        <Layout grow={1}>
          <Block fill middle>
            <Item>
              {__i18n('FOOTER.CHECK_SYS.TEXT')}: <Checkers status={checkers} providers={providers} />
            </Item>
          </Block>
        </Layout>
        <Layout grow={1}>
          <Block fill middle>
            <Item>
              {__i18n('FOOTER.SUPPORT.TEXT')}: <Support status={support} supportSystem={supportSystem} />
            </Item>
          </Block>
        </Layout>
      </ColumnLayout>
    </div>
    <div>
      <ColumnLayout>
        <Layout grow={1}>
          <Block fill middle>
            <Item>
              {__i18n('FOOTER.UPDATE')}:
            </Item>
            {'\u00A0'}
            <Item yellow>
              {update}
            </Item>
          </Block>
        </Layout>
      </ColumnLayout>
    </div>
  </div>
)

export default Tablet
