import React from 'react'
import { StyleSheet } from 'quantum'
import { Link } from 'react-router'
import { ColumnLayout, Layout } from 'bypass/ui/layout'
import { HelpIcon, CheckmarkIcon, CommentIcon } from 'bypass/ui/icons'
import Block from './Block'
import Item from './Item'
import Support from './status/Support'
import Checkers from './status/Checkers'

const styles = StyleSheet.create({
  self: {
    background: '#37474f',
    boxShadow: '0 -2px 4px rgba(0, 0, 0, 0.24)',
    width: '100%',
    padding: '4px 2px',
    margin: '0px',
  },
})

const Mobile = ({ update, checkers, support, supportSystem, providers }) => (
  <div className={styles()}>
    <div>
      <ColumnLayout>
        <Layout grow={1}>
          <Block fill small>
            <Link to='/faq' style={{ textDecoration: 'none' }}>
              <Item middle>
                <HelpIcon fill='#ffffff' />
                {'\u00A0'}
                {__i18n('FOOTER.HELP')}
              </Item>
            </Link>
          </Block>
        </Layout>
        <Layout grow={1}>
          <Block fill small>
            <Item middle>
              <CheckmarkIcon fill='#ffffff' />
              {'\u00A0'}
              <Checkers status={checkers} providers={providers} animate={false} decorated={false} />
            </Item>
          </Block>
        </Layout>
        <Layout grow={1}>
          <Block fill small>
            <Item middle>
              <CommentIcon fill='#ffffff' />
              {'\u00A0'}
              <Support status={support} supportSystem={supportSystem} animate={false} decorated={false} />
            </Item>
          </Block>
        </Layout>
      </ColumnLayout>
    </div>
    <div>
      <ColumnLayout>
        <Layout grow={1}>
          <Block fill small>
            <Item>
              {__i18n('FOOTER.UPDATE')}:
            </Item>
            {'\u00A0'}
            <Item yellow>
              {update}
            </Item>
          </Block>
        </Layout>
      </ColumnLayout>
    </div>
  </div>
)

export default Mobile
