export const actions = {
  openMobileMenu: '@@bypass/workspace/OPEN_MOBILE_MENU',
  closeMobileMenu: '@@bypass/workspace/CLOSE_MOBILE_MENU',
}

export function openMobileMenu() {
  return {
    type: actions.openMobileMenu,
  }
}

export function closeMobileMenu() {
  return {
    type: actions.closeMobileMenu,
  }
}

export function toggleMobileMenu() {
  return async (dispatch, getState) => {
    if (getState().workspace.mobileMenu) {
      return dispatch(closeMobileMenu())
    }

    return dispatch(openMobileMenu())
  }
}
