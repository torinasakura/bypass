import React from 'react'
import Viewport from '../containers/Viewport'

export function createViewport(components) {
  return () => React.createElement(Viewport, { components })
}
