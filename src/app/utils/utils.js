/* eslint-disable */
import _ from 'lodash'

import BrowserDetect from 'detect-browser'
//import SearchState from '../stores/search-form-model'
//import AuthStore from '../stores/auth'

const global = Function('return this')()

const EXEP_CODES = ['UK', 'GB']
export function convertDateToUTC(date) {
  return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds())
}
export function formatDate(d, withtime, getMonth) {
  var out = (d.getUTCDate() < 10) ? '0' + d.getUTCDate() : d.getUTCDate()
  var cnv = function (val) {
    return parseInt(val, 10) < 10 ? '0' + val : val
  }
  var months = __i18n('LANG.SERVICE.MONTH')
  out += getMonth ? ' ' + months[d.getMonth()] + ' ' : '.' + cnv(d.getMonth() + 1) + '.' //никакой магии - кол-во месяцев считается с нуля!
  out += d.getFullYear()
  if (withtime) {
    out += (/*getMonth ? ' в ' :    */', ') + cnv(d.getHours()) + ':' + cnv(d.getMinutes()) + ':' + cnv(d.getSeconds())
  }
  return out
}

export function timeConverter(unixtime, withtime, getMonth) {
  var res = ''
  var nowUt = Math.floor(convertDateToUTC(new Date()).getTime() / 1000)
  var Ut = unixtime ? +convertDateToUTC(new Date(unixtime * 1000)) / 1000 : nowUt
  var diff = nowUt - Ut
  if (diff < -1) {
    res += formatDate(convertDateToUTC(new Date(Ut * 1000)), withtime, getMonth)
  } else if (diff < 60) {
    res += Math.abs(diff === 0 ? 1 : diff) + ' ' + __i18n('LANG.SERVICE.S_AGO') //было бы глупо писать 0 секунд назад
  } else if (diff < 3600) {
    res += Math.round(diff / 60) + ' ' + __i18n('LANG.SERVICE.M_AGO')
  } else if (diff < 86400) {
    var h = Math.round(diff / 3600)
    if (h > 23) {
      res += formatDate(convertDateToUTC(new Date(Ut * 1000)), withtime, getMonth)
    } else {
      res += h + ' ' + __i18n('LANG.SERVICE.H_AGO')
    }
  } else {
    res += formatDate(convertDateToUTC(new Date(Ut * 1000)), withtime, getMonth)
  }
  return res
}

export function formatMonthNumber(month) {
  if (parseInt(month - 0) < 10) {
    return '0' + parseInt(month)
  }
  return month
}

export function toObject(map, data, type) {
  return data.map(item=> {
    let obj = {}
    map.forEach((f, i)=> {
      obj[f] = item[i]
      if (type == 'base') {
        if (f == 'base_id') obj.value = item[i]
        if (f == 'name') obj.option = item[i]
      }
      if (type == 'brand') {
        if (f == 'brand_id') obj.value = item[i]
        if (f == 'brand') obj.option = item[i]
      }
      if (type == 'country') {
        if (f == 'country_code') obj.value = item[i]
        if (f == 'country') obj.option = item[i]
      }
      if (type == 'level') {
        if (f == 'level_id') obj.value = item[i]
        if (f == 'level') obj.option = item[i]
      }
      if (type == 'load') {
        if (f == 'load_id') obj.value = item[i]
        if (f == 'level') obj.option = item[i]
      }
    })
    return obj
  })
}

export function prepareCardData(cards, fn, offset) {
  return _.map(cards, function (card, i) {
    var result = _.assign(card, {
      index: (offset ? i + offset + 1 : i + 1),
      exp: getExp(card.exp),
      countryByBinCode: countryByBinCode(card),
      card_id: +card.card_id
    })
    if (_.isFunction(fn)) {
      return _.assign(result, fn(result))
    }
    return result
  })
}

export function extendCardData(cards, offset) {
  return prepareCardData(cards, function (card) {

    var fullPrice = parseFloat(card.add_price) + parseFloat(card.seller_price), load_valid = 0
    if (card.load_valid != null) {
      load_valid = (+card.load_valid).toFixed(2)
    }
    _.assign(card, {
      seller_price_parts: fullPrice.toFixed(2).split('.'),
      load_valid: load_valid,
      add_time: card.add_time ? formatDate(new Date(card.add_time * 1000)) : 0,
      proc_country_code: countryByBinCode(card)
    })

    return card
  }, offset)
}

export function getmmss(s) {
  var diff = s % 60
  var time = {}
  if (diff == 0) {
    time.m = s / 60
    time.s = '00'
  } else {
    time.m = (s - diff) / 60
    time.s = parseInt(diff, 10) < 10 ? '0' + diff : diff
  }
  return time
}


export function getExp(exp) {
  return exp.slice(4, 6) + '/' + exp.slice(2, 4)
}


export function countryByBinCode(card) {
  if (card.country_code) {
    if (card.country_code == card.bin_country_code || !card.bin_country_code) return card.country_code
    if (EXEP_CODES.indexOf(card.bin_country_code) !== -1 && EXEP_CODES.indexOf(card.country_code) !== -1) {
      return card.country_code
    }
    return card.country_code + ' | ' + card.bin_country_code
  } else {
    return ''
  }

}

export function downloadFile(sUrl) {
  //iOS devices do not support downloading. We have to inform user about this.
  if (/(iP)/g.test(navigator.userAgent)) {
    alert('Your device does not support files downloading. Please try again in desktop browser.')
    return false
  }

  //If in Chrome or Safari - download via virtual link click
  if (BrowserDetect.name == 'chrome' || BrowserDetect.name == 'safari') {
    //Creating new link node.
    var link = document.createElement('a')
    link.href = sUrl

    if (link.download !== undefined) {
      //Set HTML5 download attribute. This will prevent file from opening if supported.
      link.download = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length)
    }
    //Dispatching click event.
    if (document.createEvent) {
      var e = document.createEvent('MouseEvents')
      e.initEvent('click', true, true)
      link.dispatchEvent(e)
      return true
    }
  }

  // Force file download (whether supported by server).
  var query = '?download'
  global.open(sUrl + query, '_self')
  return true
}

export function getCvsData(cards, csv_format) {
  return cards.map(card=> {
    var item = {}
    for (var i = 0; i < csv_format.length; i++) {
      var field = csv_format[i]
      if (field == 'returned') {
        switch (card.returned) {
        case 1:
          item[i] = __i18n('LANG.SERVICE.CHECKING')
          break
        case 2:
          item[i] = __i18n('LANG.SERVICE.VALID')
          break
        case 3:
          item[i] = __i18n('LANG.SERVICE.INVALID')
          break
        case 4:
          item[i] = __i18n('LANG.SERVICE.NOCASHBACK')
          break
        case 0:
        default :
          item[i] = __i18n('LANG.SERVICE.UNCHECKED')
          break
        }
      } else {
        switch (field) {
        case 'mm':
          var m = card.exp.slice(4)
          item[i] = m
          break
        case 'yyyy':
          var y = card.exp.slice(0, 4)
          item[i] = y
          break
        case 'exp':
          var exp = card.exp.slice(4) + '/' + card.exp.slice(2, 4)
          item[i] = exp
          break
          //TODO: нужно произвести каст
        case 'first_name':
          if (card['holder']) {
            var fullName = card['holder'].split(' ')
            item[i] = fullName[0]
          } else {
            item[i] = ''
          }
          break
        case 'last_name':
          if (card['holder']) {
            var fullName = card['holder'].split(' ')
            fullName.splice(0, 1)
            item[i] = fullName.join(' ')
          } else {
            item[i] = ''
          }
          break
        default:
          item[i] = card[field]
          break
        }
      }
    }
    return item
  })
}

export function selectText(el, win) {
  win = win || global
  var doc = win.document, sel, range
  if (win.getSelection && doc.createRange) {
    sel = win.getSelection()
    range = doc.createRange()
    range.selectNodeContents(el)
    sel.removeAllRanges()
    sel.addRange(range)
  } else if (doc.body.createTextRange) {
    range = doc.body.createTextRange()
    range.moveToElementText(el)
    range.select()
  }
}

function getSelectedText(win) {
  win = win || global
  var doc = win.document
  var text = ''
  if (win.getSelection) {
    text = win.getSelection()
  } else if (document.getSelection) {
    text = doc.getSelection()
  } else if (doc.selection) {
    text = doc.selection.createRange().text
  }
  return text
}

export function checkGiftSumm(amount) {
  var isCorrect = /^[0-9]+\.\d{2}$/.test(amount)
  if (!amount.length) return false
  if (!isCorrect) {
    var parts = amount.split('.'), dec = +parts[0], float = +parts[1]
    if (_.isNaN(parseInt(dec))) {
      return false
    }
    if (_.isNaN(float)) {
      amount = dec + '.00'
    } else {
      float += ''
      switch (float.length) {
      case 1:
        parts[1] = float + '0'
        break
      default:
        parts[1] = parts[1].slice(0, 2)
        break
      }
      amount = parts.join('.')
    }
  }
  return amount
}

export function lastTime(unixtime_ms, interval_ms) {
  var sec = Math.round(interval_ms / 1000) - Math.round(unixtime_ms / 1000)
  if (sec < 0) {
    throw Error('Отрицательный интервал')
  }
  var res = ''
  if (sec < 60) {
    res += sec + ' ' + __i18n('LANG.SERVICE.S')
  } else if (sec < 3600) {
    res += Math.round(sec / 60) + ' ' + __i18n('LANG.SERVICE.M')
  } else if (sec < 86400) {
    res += Math.round(sec / 3600) + ' ' + __i18n('LANG.SERVICE.H')
  }
  return res
}

export function splitFieldsBySeps(srow, seps, format) {
  var _try = srow.split(seps[0])
  var _srow = srow
  if (_try.length == seps.length + 1) {
    return _try
  }
  const result = []

  for (let i = 0, len = seps.length; i <= len; i++) {
    var sep = i < seps.length ? seps[i] : seps[i - 1],
      id = _srow.indexOf(sep),
      ret = _srow.substr(0, id)
    if (i == seps.length) {
      result.push(_srow)
    }
    _srow = _srow.substr(id + sep.length)
    result.push(ret)
  }

  return result.filter(v=>v.length)
}

export function checkField(field, data, card) {
  let res = {field, value: data}
  switch (field) {
  case 'num':
    if (/[0-9]+/.test(data) && data == data.match('[0-9]+')[0] && data.length >= 15 && data.length <= 16) {
      return res
    } else {
      res.error = true
      return res
    }
    break
  case 'cvv2':
    if (/[0-9]+/.test(data) && data == data.match('[0-9]+')[0] && data.length >= 3 && data.length <= 4) {
      return res
    } else {
      res.error = true
      return res
    }
    break
  case 'm':
    var expMonth = data, curDate = new Date(),
      expYear = card.y.length == 2 ? '20' + card.y : card.y
    if (expMonth.length == 1 && expMonth < 10) {
      expMonth = '0' + expMonth
    }
    res.value = expMonth
    if (expMonth && (expMonth.length == 2) && parseInt(expMonth) < 13) {
      if (expYear && (curDate.getFullYear() == parseInt(expYear))) {
        if (expMonth && curDate.getMonth() + 1 <= parseInt(expMonth)) {
          //return res
        } else {
          res.error = true
        }
      } else {
          //me.validError.push(0)
          //return expMonth
      }
    } else {
      res.error = true
    }
    return res
    break
  case 'y':
    if (data) {
      var expYear = data.length == 2 ?
        '20' + data :
          data,
        curDate = new Date(),
        isValid

      isValid = parseInt(expYear) >= curDate.getFullYear()
      if (!isValid) res.error = true
      res.value = expYear
    } else {
      res.error = true
    }
    return res
    break
  case 'zip':
    if (!/[0-9a-z]{1,16}/i.test(data)) {
      res.error = true
    }
    return res
    break
  case 'addr':
    if (data.length >= 1 && data.length <= 64) {

    } else {
      res.error = true
    }
    return res
    break
  }
}

export function sanitizeHtml(html) {
  let regExp = /<\/?[^>]+>/gi
  html = html.replace(regExp, '')
  return html
}

export function getFormatTpl(csv_format, delim) {
  return _.isArray(delim) ? csv_format.map((c, i, arr)=> {
    return '%' + c + '%' + (delim[i] ? delim[i] : '')
  }).join('') : csv_format.map((c, i, arr)=> {
    return '%' + c + '%'
  }).join(delim)
}

export function prepareStat(statData, load_ids, baseById, loadsColor, loadById) {
  var data4chart = [],
    totalData = [],
    closedData = [],
    checkedData = []

  load_ids.forEach((v, i)=> {
    var loadArr = _.filter(statData, {load_id: '' + v})
    if (!loadArr.length) return
    var base_name = baseById[loadById[v].base_id].name,
      baseData = {
        name: base_name,
        color: loadsColor[i],
        pointInterval: 24 * 3600 * 1000,
        pointStart: Date.parse(loadArr[0].day),
        load_id: v
      }
    data4chart.push(_.assign({}, baseData, {
      data: loadArr.map(function (v) {
        return extData(v, 'valid')
      })
    }))

    totalData.push(_.assign({}, baseData, {
      data: loadArr.map(function (v) {
        return extData(v, 'total')
      })
    }))
    closedData.push(_.assign({}, baseData, {
      data: loadArr.map(function (v) {
        return extData(v, 'closed')
      })
    }))
    checkedData.push(_.assign({}, baseData, {
      data: loadArr.map(function (v) {
        return extData(v, 'checked')
      })
    }))
  })

  function extData(v, dataType) {
    return _.assign({
      name: v.day,
      y: +v[dataType]
    }, v)
  }

  return {
    data4chart,
    totalData,
    closedData,
    checkedData
  }
}

export function inTimeRange(time) {
  return {
    then: (resolve, reject)=> {
      var d = new Date(),
        h = d.getUTCHours() + 3 ///MSK
      if (time[0] < h && h < time[1]) {
        resolve()
      } else {
        reject()
      }
    }
  }
}

export function formatSearch(state, toSave) {
  const search_string = {}
  _.forOwn(state, (val, key)=> {
    switch (key) {
    case 'bin':
    case 'base':
    case 'brand':
    case 'type':
    case 'level':
    case 'country':
    case 'state':
    case 'seller':
    case 'load':
      if (val.value.length === 0) return
      search_string[key] = val.value
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break
    case 'bank':
      if (toSave) {
        search_string[key] = val.value.map(v=> {
          return {
            bank_id: v.bank_id || v.value,
            bank: v.label
          }
        })
      } else {
        search_string[key] = val.value.map(v=>v.value)
      }
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break
    case 'city':
    case 'address':
      if (toSave) {
        search_string[key] = val.value
      } else {
        search_string[key] = val.value.map(v=>v.value)
      }
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break
    case 'valid':
      search_string[key] = {from: val.from, to: val.to}
      break
    case 'dates':
      search_string.from = val.from.y + val.from.m
      search_string.to = val.to.y + val.to.m
      break
    case 'sort':
      search_string.direction = val.direction
      search_string.order = val.order
      break
    case 'zip':
      if (val.mask && val.mask !== '') {
        search_string.zip = [val.mask]
        search_string.zip_mask = 1
      } else {
        search_string.zip = toSave ? val.value : val.value.map(v => v.value)
        search_string.zip_mask = 0
      }
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break
    }
  })

  return search_string
}

export function formatBinSearch(state, toSave) {
  const search_string = {}
  _.forOwn(state, (val, key)=> {
    switch (key) {
    case 'bin':
    case 'brand':
    case 'type':
    case 'level':
    case 'country':
      if (val.value.length === 0) return
      search_string[key] = val.value
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break

    case 'bank':
      if (toSave) {
        search_string[key] = val.search.map(v=> {
          return {
            bank_id: v.value,
            bank: v.label
          }
        })
      } else {
        search_string[key] = val.search.map(v=>v.label)
      }
      if (val.inversed) {
        search_string[`${key}_not_flag`] = 1
      }
      break

    case 'sort':
      search_string.direction = val.direction
      search_string.order = val.order
      break

    }
  })

  return search_string
}

export function unformatSearch(state) {
  const search_string = {dates: {}, sort: {}}
  _.forOwn(state, (val, key)=> {
    switch (key) {
    case 'bin':
    case 'brand':
    case 'type':
    case 'level':
    case 'country':
    case 'state':
    case 'seller':
    case 'load':
      search_string[key] = {
        value: val,
        inversed: state[`${key}_not_flag`]
      }
      break
    case 'bank':
      search_string[key] = {
        value: val.map(v=> {
          return {label: v.bank, value: v.bank_id}
        }),
        search: val.map(v=> {
          return {label: v.bank, value: v.bank_id}
        }),
        inversed: !!state[`${key}_not_flag`]
      }
      break
    case 'base':
    case 'city':
    case 'address':
      search_string[key] = {search: val, value: val, inversed: !!state[`${key}_not_flag`]}
      break
    case 'valid':
      search_string[key] = val
      search_string[key].touched = false
      search_string[key].touched = !_.isEqual(SearchState.valid, search_string.valid)
      break
    case 'from':
    case 'to':
      search_string.dates[key] = {
        m: val.slice(4),
        y: val.slice(0, 4)
      }
      search_string.dates.touched = false
      break
    case 'direction':
    case 'order':
      search_string.sort[key] = val
      search_string.sort.touched = false
      break
    case 'zip':
      if (state.zip_mask && parseInt(state.zip_mask) === 1) {
        if (!search_string[key]) {
          search_string[key] = {}
        }
        search_string[key].mask = val[0]
      } else {
        search_string[key] = {search: val, value: val}
      }
      search_string[key].inversed = !!state[`${key}_not_flag`]
      break
    }
  })

  if (state.from && state.to) search_string.dates.touched = !_.isEqual(SearchState.dates, search_string.dates)
  if (state.direction && state.order) search_string.sort.touched = !_.isEqual(SearchState.sort, search_string.sort)

  return search_string
}

export function reduceSearch(search) {
  const searchClone = _.cloneDeep(search)
  _.forOwn(searchClone, (val, key)=> {
    const check = v => {
      return val[v] && val[v].length !== 0
    }
    if (key == 'zip' && ['search', 'value', 'mask'].some(check)) {
      return
    }
    if (key !== 'sort' && key !== 'valid' && key !== 'dates') {
      if (val.value && val.value.length === 0) { // || val.search && val.search.length === 0) {
        return delete searchClone[key]
      }
      //if (_.has(val, 'search')) {
      //  return delete val.value
      //}
    }
  })
  return searchClone
}

export function getDocumentHeight() {
  const name = 'Height'
  const elem = document
  const doc = elem.documentElement

  return Math.max(
    elem.body['scroll' + name],
    elem.body['offset' + name],
    doc['scroll' + name],
    doc['offset' + name],
    doc['client' + name])
}

export function requireAuth(nextState, replaceState) {
  if (nextState.location.pathname === '/cards/bin/search') {
    return
  }
  if (nextState.location.pathname !== '/' && !AuthStore.loggedIn()) {
    replaceState(null, '/')
  }
  if (AuthStore.loggedIn() && nextState.location.pathname === '/') {
    replaceState(null, '/main')
  }
}

export function parseBins(text) {
  text = text.split('\n').join(' ')
  var re = new RegExp('\(\?\:\\b\)\(\\d\{6,\}\)\(\?\=\\b\)', 'g')
  var v = null
  var bins = []
  //var re = /(?:\b)(\d{6,})(?=\b)/g
  while (v = re.exec(text)) {
    v = v[1].toString()
    if (v.length > 6) {
      v = v.substr(0, 6)
    }
    if (bins.indexOf(v) == -1) {
      bins.push(v)
    }
  }
  return bins
}

export function serialize(obj) {
  var str = []
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
    }
  return str.join('&')
}

let d = convertDateToUTC(new Date())
let month = d.getMonth() + 1

export const SearchState = {
  bin: {
    inversed: false,
    value: []
  },
  brand: {
    inversed: false,
    value: []
  },
  type: {
    inversed: false,
    value: []
  },
  level: {
    inversed: false,
    value: []
  },
  bank: {
    inversed: false,
    value: [],
    search: []
  },
  country: {
    inversed: false,
    value: []
  },
  state: {
    inversed: false,
    value: []
  },
  seller: {
    inversed: false,
    value: []
  },

  base: {
    inversed: false,
    value: []
  },
  city: {
    inversed: false,
    value: [],
    search: []
  },
  address: {
    inversed: false,
    value: [],
    search: []
  },
  zip: {
    inversed: false,
    value: [],
    search: [],
    mask: ''
  },
  load: {
    inversed: false,
    value: []
  },
  valid: {
    from: 0,
    to: 100,
    touched: false
  },
  dates: {
    from: {
      m: formatMonthNumber(month),
      y: d.getFullYear() + ''
    },
    to: {
      m: formatMonthNumber(month),
      y: (d.getFullYear() + 50) + ''
    },
    touched: false
  },
  sort: {
    order: 'card_id', direction: 'desc',
    touched: false
  }
}
