const config = window.appConfig || {
  api_url: 'http://backend.rabbittor.ml:3000/',
  sessionId: 0,
  search: {
    request_interval: 2000,
  },
  cart: {
    error_codes: [-3, -2, 0],
    buy_interval: 3000,
  },
  cash: {
    bt: {
      address: '',
    },
  },
  billing: {
    balance_req_interval: 300000,
  },
  notify_check_limit: 5,
  notify_interval: 60000,
  itemsPerPage: 10,
  pagination: {},
  schedule: {
    support: [0, 24],
    admin: [9, 21],
  },
}

export default config
