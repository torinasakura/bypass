import { show, hide } from './loader'
import { open } from './modal'

const errorMessages = {
  limit: {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.SAVED_SEARCH_LIMIT'),
  },
  duplicate: {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.SEARCH_EXISTING'),
  },
  'auth error': {
    header: __i18n('LANG.ERRORS.AUTH_ERROR'),
    body: __i18n('LANG.ERRORS.AUTH_ERROR'),
  },
  'no param': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.INCORRECT_REQUEST'),
  },
  'bad validation': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.INCORRECT_REQUEST'),
  },
  'already exists': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.ALREADY EXISTS'),
  },
  'no access': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.NO ACCESS'),
  },
  'unknown action': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.UNKNOWN ACTION'),
  },
  'empty param': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.EMPTY PARAM'),
  },
  'notify check timeout': {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.NOTIFY_CHECK_TIMEOUT'),
  },
  timeout: {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.GENERAL.TIMEOUT'),
  },
  common: {
    header: __i18n('LANG.ERRORS.ERROR'),
    body: __i18n('LANG.ERRORS.COMMON'),
  },
}

export function start() {
  return async dispatch => {
    dispatch(show())
  }
}

export function end() {
  return async dispatch => {
    setTimeout(() => {
      dispatch(hide())
    }, 100)
  }
}

export function error(message) {
  return async dispatch => {
    if (message === 'general error') {
      window.location.hash = '#/500'
      return
    } else if (message === 'not found') {
      window.location.hash = '#/404'
      return
    }

    const props = errorMessages[message] ? errorMessages[message] : errorMessages.common

    dispatch(hide())
    dispatch(open('common/Errors', props))

    return
  }
}
