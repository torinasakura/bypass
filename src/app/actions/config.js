import cookie from 'js-cookie'
import config from '../config'
import { error } from './remote'

let notifyCheckCounter = 0

export const actions = {
  load: '@@bypass/config/LOAD',
}

export function load() {
  return async (dispatch, getState, { get }) => {
    if (!getState().auth.token) {
      return
    }

    notifyCheckCounter++

    const { result, response } = await get('notify', { loader: false })

    if (!(response && response.ok)) {
      if (notifyCheckCounter > config.notify_check_limit) {
        notifyCheckCounter = 0
        dispatch(error('notify check timeout'))
      }

      return
    }

    if (result.settings.version) {
      const version = cookie('version')

      if (version) {
        if (version !== result.settings.version) {
          cookie('version', result.settings.version)
          location.reload(true)
        }
      } else {
        cookie('version', result.settings.version)
      }
    }

    dispatch({
      type: actions.load,
      data: {
        ...result,
        settings: {
          ...result.settings,
          support_system: JSON.parse(result.settings.support_system),
        },
        options: {
          ...result.options,
          show_confirm: JSON.parse(result.options.show_confirm),
        },
      },
    })

    notifyCheckCounter = 0
  }
}
