import { is } from 'ramda'
import isJSON from 'is-json'
import { start, end, error } from '../actions/remote'
import { param } from './utils'

export default function api(dispatch, getState, config) {
  const defaultOptions = {
    headers: {},
    credentials: 'same-origin',
    loader: true,
    hideLoader: true,
  }

  const token = getState().auth.token

  const request = async (method, href, options = {}) => {
    const params = ['json', `token=${token}`]

    const mergeOptions = {
      ...defaultOptions,
      ...options,
      method,
      headers: {
        ...defaultOptions.headers,
        ...options.headers,
      },
    }

    if (mergeOptions.method === 'POST') {
      mergeOptions.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'

      const body = mergeOptions.body

      if (body && is(Object, body) && String(body) !== '[object File]') {
        mergeOptions.body = param(body)
      }
    }

    if (mergeOptions.method === 'GET') {
      const body = mergeOptions.body

      if (body && is(Object, body) && String(body) !== '[object File]') {
        params.push(param(body))
        delete mergeOptions.body
      }
    }

    try {
      const url = `${config.api_url}${href}?${params.join('&')}`

      if (mergeOptions.loader) {
        dispatch(start())
      }

      const response = await fetch(url, mergeOptions)

      const body = await response.text()

      const result = (body && isJSON(body)) ? JSON.parse(body) : body

      if (mergeOptions.loader && mergeOptions.hideLoader) {
        dispatch(end())
      }

      if (result.error && !mergeOptions.skipError) {
        dispatch(error(result.result))

        return Promise.reject(result.result)
      }

      return { ...result, response }
    } catch (err) {
      console.log(err) // eslint-disable-line no-console

      dispatch(end())

      Promise.reject(err)

      return { response: {} }
    }
  }

  return {
    get: async function get(href, options) {
      return await request('GET', href, options)
    },
    post: async function post(href, options) {
      return await request('POST', href, options)
    },
    patch: async function patch(href, options) {
      return await request('PATCH', href, options)
    },
    destroy: async function destroy(href, options) {
      return await request('DELETE', href, options)
    },
  }
}
