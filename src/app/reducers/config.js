/* eslint-disable camelcase */
import { actions } from '../actions/config'
import { formatDate, convertDateToUTC } from '../utils/utils'

const initialState = {
  balance: 0.01,
  settings: {},
  support_system: {},
  provider: [],
}

export default function loader(state = initialState, { type, data }) {
  if (type === actions.load) {
    const { version, support_system } = data.settings

    const update = formatDate(convertDateToUTC(new Date(version * 1000)), 1, 1)

    const support = Object.keys(support_system || {}).every(key => support_system[key] == 1) ? 'active' : 'partial'

    const checkers = data.provider.every(provider => provider.available == 1) ? 'active' : 'partial'

    return {
      ...data,
      update,
      support,
      checkers,
    }
  }

  return state
}
