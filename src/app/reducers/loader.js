import { actions } from '../actions/loader'

const initialState = {
  show: false,
}

export default function loader(state = initialState, action) {
  switch (action.type) {
    case actions.show:
      return { show: true }
    case actions.hide:
      return { show: false }
    default:
      return state
  }
}
