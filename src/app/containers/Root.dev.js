import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { ReduxRouter } from 'redux-router'
import { AppContainer } from 'react-hot-loader'
import Container from './Container'
import DevTools from './DevTools'
import Loader from './Loader'
import Modal from './Modal'
import getRoutes from '../routes'

class Root extends Component {
  render() {
    const { store } = this.props

    return (
      <AppContainer>
        <Provider store={store}>
          <Container>
            <ReduxRouter routes={getRoutes(store)} />
            <DevTools />
            <Loader />
            <Modal />
          </Container>
        </Provider>
      </AppContainer>
    )
  }
}

export default Root
