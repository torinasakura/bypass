import React from 'react'
import { StyleSheet } from 'quantum'

const styles = StyleSheet.create({
  self: {
    height: '100%',
    width: '100%',
    overflow: 'hidden',
  },
})

const Container = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Container
