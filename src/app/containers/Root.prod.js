import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { ReduxRouter } from 'redux-router'
import Container from './Container'
import Loader from './Loader'
import Modal from './Modal'
import getRoutes from '../routes'

class Root extends Component {
  render() {
    const { store } = this.props

    return (
      <Provider store={store}>
        <Container>
          <ReduxRouter routes={getRoutes(store)} />
          <Modal />
          <Loader />
        </Container>
      </Provider>
    )
  }
}

export default Root
