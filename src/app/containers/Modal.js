import { createElement } from 'react'
import { connect } from 'react-redux'
import { close } from '../actions/modal'

const modals = require.context(
  '../../pages',
  true,
  /^\.\/[a-z]+\/modals\/([a-z]+)\.js$/i
)

const map = modals.keys().reduce((result, key) => {
  const [, page, , file] = key.split('/')

  return {
    ...result,
    [`${page}/${file.replace('.js', '')}`]: key,
  }
}, {})

const Modals = ({ name, props, onClose }) => {
  if (name && map[name]) {
    const target = modals(map[name])

    return createElement(target.default, {
      onClose,
      ...props,
    })
  }

  return null
}

export default connect(
  state => ({
    name: state.modal.name,
    props: state.modal.props,
    viewport: state.viewport,
  }),
  dispatch => ({
    onClose: () => {
      dispatch(close())
    },
  }),
)(Modals)
