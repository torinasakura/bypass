import createHashHistory from 'history/lib/createHashHistory'

export const history = createHashHistory({
  queryKey: false,
})
