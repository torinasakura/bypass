import Visiblity from 'visibilityjs'
import { load } from '../../actions/config'
import config from '../../config'

export default function notify({ dispatch }) {
  dispatch(load())
  Visiblity.every(config.notify_interval, () => dispatch(load()))
}
