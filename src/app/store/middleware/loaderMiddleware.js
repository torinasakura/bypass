import { ROUTER_DID_CHANGE } from 'redux-router/lib/constants'

export default function loaderMiddleware({ dispatch, getState }) {
  return next => action => {
    if (action.type === ROUTER_DID_CHANGE) {
      setTimeout(() => {
        const { workspace = {} } = getState()
        // Actions.hideLoader()

        if (workspace.mobileMenu) {
          dispatch({ type: '@@bypass/workspace/CLOSE_MOBILE_MENU' })
        }
      }, 200)
    }

    return next(action)
  }
}
