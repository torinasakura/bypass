import viewportListener from 'react-media-queries/lib/viewportListener'
import { createStore, compose, applyMiddleware } from 'redux'
import { reduxReactRouter } from 'redux-router'
import { createViewportListener } from '../../utils'
import rootReducer from '../reducers'
import { history } from '../history'
import { loaderMiddleware, apiMiddleware } from './middleware'
import { notify } from './listeners'

const createHistory = () => history

const enhancer = compose(
  reduxReactRouter({ createHistory }),
  applyMiddleware(apiMiddleware, loaderMiddleware),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  viewportListener(createViewportListener(store))
  notify(store)

  return store
}
