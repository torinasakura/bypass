/* eslint-disable no-console */
import path from 'path'
import Express from 'express'
import serveStatic from 'serve-static'
const app = new Express()

app.use(serveStatic(path.resolve(__dirname, '../dist')))

app.get(/(ru|en|es|cn)/, (req, res) => {
  res.sendFile(path.resolve(__dirname, `../dist/index.${req.params[0]}.html`))
})

app.listen(3030, error => {
  if (error) {
    throw error
  }

  console.info('Bypass demo server listening on port %s', 3030)
})
