/* eslint-disable */
import fs from 'fs'
import path from 'path'
import map from 'map-stream'
import vfs from 'vinyl-fs'
import JSFtp from 'jsftp'
import JSFtpMkdir from 'jsftp-mkdirp'
import chalk from 'chalk'

JSFtpMkdir(JSFtp)

function upload(options, file, callback) {
  const finalRemotePath = path.join('/dev', file.relative).replace(/\\/g, '/')
  const remoteDirname = path.dirname(finalRemotePath).replace(/\\/g, '/')
  const ftp = new JSFtp(options)

  // https://github.com/sindresorhus/jsftp-mkdirp/issues/6
  // ftp.mkdirp(remoteDirname)
  //  .then(() => {
  //  })
  //  .catch(err => console.log(err))

  ftp.put(file.contents, finalRemotePath, error => {
    if (error) return callback(error)

    console.log('uploaded:', chalk.green('✔ ') + file.relative)

    ftp.raw.quit()
    callback(null, file)
  })
}

function run() {
  const options = JSON.parse(fs.readFileSync('.ftpconfig'))

  vfs.src('./dist/**/*.*')
    .pipe(map(upload.bind(null, options)))
}

run()
