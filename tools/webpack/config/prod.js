import path from 'path'
import webpack from 'webpack'
import I18nPlugin from 'i18n-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import CssResolvePlugin from 'quantum/lib/webpack/css-resolve-plugin'
import locales from './../../../src/locales'
import * as common from './common'

export const context = common.context

export const postcss = common.postcss

export const entry = common.entry

export const resolve = common.resolve

export const module = {
  loaders: [
    {
      test: /\.js?$/,
      loader: 'quantum/lib/webpack/loader',
      exclude: /node_modules/,
    },
    {
      test: /\.js?$/,
      loader: 'babel',
      exclude: /node_modules/,
      query: {
        babelrc: false,
        presets: [
          'es2015',
          'stage-0',
          'react',
        ],
        plugins: [
          'transform-runtime',
          'quantum/lib/babel/plugin',
        ],
      },
    },
    {
      test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
      loader: 'file?name=[path][name].[ext]',
    },
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader'),
    },
  ],
}

const config = Object.keys(locales).map(locale => ({
  name: locale,
  entry,
  output: {
    filename: `/scripts/dist/[name].${locale}.js`,
    path: `${common.context}/dist`,
  },
  context,
  postcss,
  resolve,
  module,
  plugins: [
    new ExtractTextPlugin('/style/index.css'),
    new CssResolvePlugin(),
    new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
    new HtmlWebpackPlugin({
      filename: `index.${locale}.html`,
      template: path.resolve(__dirname, 'index.ejs'),
      LOCALE: locale,
      LOCALES_LIST: Object.keys(locales),
    }),
    new I18nPlugin(
      locales[locale],
      {
        nested: true,
        failOnMissing: true,
        functionName: '__i18n',
      }
    ),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new webpack.optimize.UglifyJsPlugin(),
  ],
}))

Object.keys(locales).forEach(locale => {
  config.push({
    name: locale,
    entry: [
      'babel-polyfill',
      './src/seller/app/app.js',
    ],
    output: {
      filename: `[name].${locale}.js`,
      path: `${common.context}/dist/seller/`,
    },
    context,
    postcss,
    resolve,
    module,
    plugins: [
      new ExtractTextPlugin('/style/index.css'),
      new CssResolvePlugin(),
      new webpack.ProvidePlugin({
        fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
      }),
      new I18nPlugin(
        locales[locale],
        {
          nested: true,
          failOnMissing: true,
          functionName: '__i18n',
        }
      ),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
      }),
      new webpack.optimize.UglifyJsPlugin(),
    ],
  })
})

config.push({
  name: 'seller.init',
  entry: './src/seller/run.js',
  output: {
    filename: 'seller.run.js',
    path: `${common.context}/dist/seller/`,
  },
  context,
  postcss,
  resolve,
  module,
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, 'index.seller.ejs'),
    }),
  ],
})

export default config
