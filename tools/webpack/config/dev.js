import path from 'path'
import webpack from 'webpack'
import I18nPlugin from 'i18n-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CssResolvePlugin from 'quantum/lib/webpack/css-resolve-plugin'
import { ru } from './../../../src/locales'
import * as common from './common'

const locales = { ru }

export const context = common.context

export const postcss = common.postcss

export const resolve = common.resolve

export const entry = [
  'webpack-hot-middleware/client',
  'react-hot-loader/patch',
].concat(common.entry)

export const output = {
  filename: '[name].js',
}

const module = {
  loaders: [
    {
      test: /\.js?$/,
      loader: 'quantum/lib/webpack/loader',
      exclude: /node_modules/,
    },
    {
      test: /\.js?$/,
      loader: 'babel',
      exclude: /node_modules/,
      query: {
        babelrc: false,
        presets: [
          'es2015',
          'stage-0',
          'react',
        ],
        plugins: [
          'quantum/lib/babel/plugin',
          'react-hot-loader/babel',
          'transform-runtime',
        ],
      },
    },
    {
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader', 'postcss-loader'],
    },
    {
      test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
      loader: 'file?name=[path][name].[ext]',
    },
  ],
}

const config = Object.keys(locales).map(locale => ({
  name: locale,
  entry,
  output: {
    filename: `[name].${locale}.js`,
    path: `/${locale}`,
  },
  context,
  postcss,
  resolve,
  module,
  plugins: [
    new CssResolvePlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, 'index.ejs'),
      LOCALE: locale,
      LOCALES_LIST: Object.keys(locales),
    }),
    new I18nPlugin(
      locales[locale],
      {
        nested: true,
        failOnMissing: true,
        functionName: '__i18n',
      }
    ),
  ],
}))

Object.keys(locales).forEach(locale => {
  config.push({
    name: locale,
    entry: [
      'webpack-hot-middleware/client',
      'react-hot-loader/patch',
      'babel-polyfill',
      './src/seller/app/app.js',
    ],
    output: {
      filename: `[name].${locale}.js`,
      path: '/seller/',
    },
    context,
    postcss,
    resolve,
    module,
    plugins: [
      new CssResolvePlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.ProvidePlugin({
        fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
      }),
      new I18nPlugin(
        locales[locale],
        {
          nested: true,
          failOnMissing: true,
          functionName: '__i18n',
        }
      ),
    ],
  })
})

config.push({
  name: 'seller.init',
  entry: './src/seller/run.js',
  output: {
    filename: 'seller.run.js',
    path: '/seller/',
  },
  context,
  postcss,
  resolve,
  module,
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, 'index.seller.ejs'),
    }),
  ],
})

export default config
