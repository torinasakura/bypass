import path from 'path'
import autoprefixer from 'autoprefixer'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import locales from './../../../src/locales'

const htmlPlugins = Object.keys(locales).reduce((result, locale) => {
  result.push(new HtmlWebpackPlugin({
    filename: `index.${locale}.html`,
    template: path.resolve(__dirname, 'index.ejs'),
    LOCALE: locale,
    LANG: locales[locale],
    LOCALES_LIST: Object.keys(locales),
  }))

  return result
}, [])

export const context = path.resolve(__dirname, './../../../')

export const entry = [
  'babel-polyfill',
  './src/app/app.js',
]

export const resolve = {
  alias: {
    bypass: path.resolve(__dirname, './../../../src'),
  },
}

export const postcss = [
  autoprefixer({
    browsers: [
      '>2%',
      'last 2 versions',
    ],
  }),
]

export const plugins = htmlPlugins
