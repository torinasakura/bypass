/* eslint-disable no-console */
import webpack from 'webpack'
import config from './config/prod'

webpack(config).run(error => {
  if (error) {
    console.log(error)
  }
})
