/* eslint-disable no-console */
import path from 'path'
import Express from 'express'
import webpack from 'webpack'
import serveStatic from 'serve-static'
import devMiddleware from 'webpack-dev-middleware'
import hotMiddleware from 'webpack-hot-middleware'
import config from './config/dev'

const compiler = webpack(config)
const app = new Express()

app.use(devMiddleware(compiler, { noInfo: true, hot: true, historyApiFallback: true }))
app.use(hotMiddleware(compiler))

app.use(serveStatic(path.resolve(__dirname, './../../resources')))

app.listen(3030, error => {
  if (error) {
    throw error
  }

  console.info('Webpack development server listening on port %s', 3030)
})

