#!/bin/bash

if [ -d build ]; then
  rm -r build
fi

mkdir build

zip -r build/dist.zip dist
zip -r build/source.zip resources src test tools .babelrc .eslintignore .eslintrc .gitignore docker-compose.selenium.yml docker-compose.yml make package.json README.md

zip -r build.zip build
