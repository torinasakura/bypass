## Dependencies

```
docker-compose run npm install
```

## Dev server

```
docker-compose up dev
```

## Production demo server

```
docker-compose up demo
```

## Deploy

### Config

Deploy required ftp configuration file .ftpconfig in root folder

```
{
  "host": "deploy.host",
  "user": "user",
  "pass": "password"
}

```

### Command

```
docker-compose run npm run deploy
```

## Test

Run Selenium Hub

```
docker-compose -f docker-compose.selenium.yml up hub
```

Run app

```
docker-compose -f docker-compose.selenium.yml up app
```

### Dev

Run local node

```
npm run node:dev
```

Run tests

```
docker-compose -f docker-compose.selenium.yml up test-dev
```

### Linux

Run chrome and firefox

```
docker-compose -f docker-compose.selenium.yml up chrome firefox
```

Run tests

```
docker-compose -f docker-compose.selenium.yml up test-linux
```

### Mobile

Run appium

```
appium --nodeconfig test/config/appium.json
```

Run tests

```
docker-compose -f docker-compose.selenium.yml up test-ios
```

## Build

```
docker-compose run npm run build
```

## Production configuration

For app configuration customize `window.appConfig` object in template files `index.cn.html`, `index.en.html`, `index.es.html`, `index.ru.html` and `seller/index.html` in dist folder.

### Configuration description

```
{
  api_url: 'http://backend.rabbittor.ml:3000/', // backend url address for app
  // search cards configuration block
  search: {
    request_interval: 2000,
  },
  // cart configuration block
  cart: {
    error_codes: [-3, -2, 0], // backend result error codes for failed buy
    buy_interval: 3000, // check buy availability
  },
  // billing configuration block
  billing: {
    balance_req_interval: 300000, // check payment interval
  },
  notify_check_limit: 5, // max notify error requests
  notify_interval: 60000, // interval for reload config
}
```